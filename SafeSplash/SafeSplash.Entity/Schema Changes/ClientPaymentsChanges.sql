﻿ALTER TABLE [SafeSplashPortal].[dbo].[tb_ClientPayment]
ADD [clientPayment_ACHRecurring] bit

ALTER TABLE [SafeSplashPortal].[dbo].[tb_ClientPayment]
ADD [clientPayment_ACHRecurringAmount] money

ALTER TABLE [SafeSplashPortal].[dbo].[tb_ClientPayment]
ADD [clientPayment_ACHRecurringStartDate] datetime

ALTER TABLE [SafeSplashPortal].[dbo].[tb_ClientPayment]
ADD [clientPayment_ACHLast4] nvarchar(4)

ALTER TABLE [SafeSplashPortal].[dbo].[tb_ClientPayment]
ADD [clientPayment_ACHAccountType] nvarchar(4)

ALTER TABLE [SafeSplashPortal].[dbo].[tb_ClientPayment]
ADD [clientPayment_ACHSuspended] bit
