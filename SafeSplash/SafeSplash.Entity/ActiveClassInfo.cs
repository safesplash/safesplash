﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AKMII.SafeSplash.Entity
{
    [Serializable]
    public class ActiveClassInfo
    {
        public string ClassName { get; set; }

        public string Description { get; set; }

        public string LevelName { get; set; }

        public int MaximumStud { get; set; }

        public bool IsPerpetual { get; set; }

        public DateTime StartDay { get; set; }
        
        public DateTime EndDay { get; set; }
        
        public string InstructorName { get; set; }

        public string LocationName { get; set; }
    }
}
