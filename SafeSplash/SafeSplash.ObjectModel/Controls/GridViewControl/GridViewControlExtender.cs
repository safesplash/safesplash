﻿using System;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.ComponentModel.Design;
using AjaxControlToolkit;

namespace SafeSplash.ObjectModel.Controls.GridViewControl
{
    /// <summary>
    /// 
    /// </summary>
    [Designer(typeof(GridViewControlDesigner))]
    [ClientScriptResource("SafeSplash.ObjectModel.Controls.GridViewControl.GridViewControlBehavior", "SafeSplash.ObjectModel.Controls.GridViewControl.GridViewControlBehavior")]
    [TargetControlType(typeof(GridView))]
    public class GridViewControlExtender : ExtenderControlBase
    {
        /// <summary>
        /// 
        /// </summary>
        [ExtenderControlProperty]
        [DefaultValue("")]
        public string HeaderCellHoverCssClass
        {
            get
            {
                return this.GetPropertyValue("HeaderCellHoverCssClass", "");
            }
            set
            {
                this.SetPropertyValue("HeaderCellHoverCssClass", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [ExtenderControlProperty]
        [DefaultValue("")]
        public string HeaderCellSelectCssClass
        {
            get
            {
                return this.GetPropertyValue("HeaderCellSelectCssClass", "");
            }
            set
            {
                this.SetPropertyValue("HeaderCellSelectCssClass", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [ExtenderControlProperty]
        [DefaultValue("")]
        public string CellHoverCssClass
        {
            get
            {
                return this.GetPropertyValue("CellHoverCssClass", "");
            }
            set
            {
                this.SetPropertyValue("CellHoverCssClass", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [ExtenderControlProperty]
        [DefaultValue("")]
        public string CellSelectCssClass
        {
            get
            {
                return this.GetPropertyValue("CellSelectCssClass", "");
            }
            set
            {
                this.SetPropertyValue("CellSelectCssClass", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [ExtenderControlProperty]
        [DefaultValue("")]
        public string ColumnHoverCssClass
        {
            get
            {
                return this.GetPropertyValue("ColumnHoverCssClass", "");
            }
            set
            {
                this.SetPropertyValue("ColumnHoverCssClass", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [ExtenderControlProperty]
        [DefaultValue("")]
        public string ColumnSelectCssClass
        {
            get
            {
                return this.GetPropertyValue("ColumnSelectCssClass", "");
            }
            set
            {
                this.SetPropertyValue("ColumnSelectCssClass", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [ExtenderControlProperty]
        [DefaultValue("")]
        public string RowHoverCssClass
        {
            get
            {
                return this.GetPropertyValue("RowHoverCssClass", "");
            }
            set
            {
                this.SetPropertyValue("RowHoverCssClass", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [ExtenderControlProperty]
        [DefaultValue("")]
        public string RowSelectCssClass
        {
            get
            {
                return this.GetPropertyValue("RowSelectCssClass", "");
            }
            set
            {
                this.SetPropertyValue("RowSelectCssClass", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [ExtenderControlProperty]
        [DefaultValue("")]
        [Browsable(false)]
        public string DataRowCssClass
        {
            get
            {
                return this.GetPropertyValue("DataRowCssClass", "");
            }
            set
            {
                this.SetPropertyValue("DataRowCssClass", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [ExtenderControlProperty]
        [DefaultValue("")]
        [Browsable(false)]
        public string AlternateDataRowCssClass
        {
            get
            {
                return this.GetPropertyValue("AlternateDataRowCssClass", "");
            }
            set
            {
                this.SetPropertyValue("AlternateDataRowCssClass", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [ExtenderControlProperty]
        [DefaultValue("")]
        [Browsable(false)]
        public string HeaderRowCssClass
        {
            get
            {
                return this.GetPropertyValue("HeaderRowCssClass", "");
            }
            set
            {
                this.SetPropertyValue("HeaderRowCssClass", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            GridView target = this.TargetControl as GridView;
            this.DataRowCssClass = target.RowStyle.CssClass;
            this.AlternateDataRowCssClass = target.AlternatingRowStyle.CssClass;
            this.HeaderRowCssClass = target.HeaderStyle.CssClass;
        }
    }
}
