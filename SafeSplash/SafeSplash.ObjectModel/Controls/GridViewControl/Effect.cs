﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.ObjectModel.Controls.GridViewControl
{
    /// <summary>
    /// 
    /// </summary>
    public enum Effect
    {
        /// <summary>
        /// 
        /// </summary>
        CellsOnly = 0,
        /// <summary>
        /// 
        /// </summary>
        RowsOnly = 1,
        /// <summary>
        /// 
        /// </summary>
        ColumnsOnly = 2,
        /// <summary>
        /// 
        /// </summary>
        RowsAndColumns = 3
    }
}
