﻿using System;
using System.Data;
//using Microsoft.SharePoint;
//using Microsoft.SharePoint.WebControls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using AKMII.SafeSplash.BLL;
using AKMII.SafeSplash.Entity;

namespace SafeSplash.ObjectModel.Controls.PeoplePicker
{
    public class PeopleDataManager
    {

        public static DataTable ValidateResourceByKey(String key)
        {
            DataTable dt = null;
            EmployeeBll employeeBll = new EmployeeBll();
            tb_Employee employee = employeeBll.GetByID(new Guid(key));
            if (employee != null)
            {
                List<tb_Employee> employeeList = new List<tb_Employee>();
                employeeList.Add(employee);
                dt = Class.GenericHelper.ToDataTable(employeeList);
            }
            return dt;
        }

        public static DataTable ValidateResource(String name)
        {
            DataTable dt = null;
            EmployeeBll employeeBll = new EmployeeBll();
            List<tb_Employee> employeeList = employeeBll.QueryEmployeeByName(name);
            if (employeeList != null)
            {
                dt = Class.GenericHelper.ToDataTable(employeeList);
            }
            return dt;
        }

        public static DataTable SearchForResources(String groupName, String keyword)
        {
            if (!String.IsNullOrEmpty(groupName))
            {
                EmployeeBll employeeBll = new EmployeeBll();
                List<tb_Employee> employeeList = employeeBll.QueryEmployeeByNameALocation(new Guid(groupName), keyword);
                if (employeeList != null)
                    return Class.GenericHelper.ToDataTable(employeeList);
            }
            return null;
        }

        ///RAB
        //public static PickerEntity ConvertFromDataRow(DataRow dataRow, PickerEntity entity)
        //{
        //    if (entity == null)
        //    {
        //        entity = new PickerEntity();
        //    }
        //    entity.Key = Convert.ToString(dataRow["empl_ID"]);
        //    entity.DisplayText = dataRow["empl_FirstName"].ToString() + " " + dataRow["empl_LastName"].ToString();
        //    entity.Description = dataRow["empl_FirstName"].ToString() + " " + dataRow["empl_LastName"].ToString();
        //    // Fill hashtable with item values
        //    entity.EntityData = new Hashtable();
        //    foreach (DataColumn dc in dataRow.Table.Columns)
        //    {
        //        entity.EntityData[dc.ColumnName] = dataRow[dc.ColumnName];
        //    }
        //    return entity;
        //}
    }
}