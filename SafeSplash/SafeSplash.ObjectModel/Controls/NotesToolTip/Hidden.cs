﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: TagPrefix("SafeSplash.ObjectModel.Controls.NotesToolTip", "NotesToolTip")]
namespace SafeSplash.ObjectModel.Controls.NotesToolTip
{
    public class Hidden : TextBox
    {
        public Hidden()
            : base()
        {
            this.Style.Add("display", "none");
        }
    }
}
