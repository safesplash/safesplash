﻿using System;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using AKMII.SafeSplash.BLL;
using AKMII.SafeSplash.Entity;

namespace SafeSplash.ObjectModel
{
    class SafeSplashMemberShipProvider : MembershipProvider
    {
        private string _AppName = "SafeSplashMemberShipProvider";

        // Dummy ValidateUserStart ******************************
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }
        
        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new Exception("The method or operation not implemented.");
        }

        public override bool UnlockUser(string userName)
        {
            throw new Exception("The method or operation not implemented.");
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new Exception("The method or operation not implemented.");
        }
        
        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new Exception("The method or operation not implemented.");
        }
        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new Exception("The method or operation not implemented.");
        }
        public override string GetPassword(string username, string answer)
        {
            throw new Exception("The method or operation not implemented.");
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new Exception("The method or operation not implemented.");
        }

        public override int MinRequiredPasswordLength
        {
            get { return 1; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return 1; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { return ""; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return MembershipPasswordFormat.Clear; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return false; }
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new Exception("The method or operation not implemented.");
        }

        public override int PasswordAttemptWindow { get { return 1; } }
        public override int MaxInvalidPasswordAttempts { get { return 5; } }

        public override bool RequiresQuestionAndAnswer { get { return false; } }
        public override bool EnablePasswordReset { get { return true; } }
        public override bool EnablePasswordRetrieval { get { return true; } }

        public override string ApplicationName
        {
            get { return _AppName; }
            set { if (_AppName != value) { _AppName = value; } }
        }

        // Dummy End ******************************
        

        public override bool ValidateUser(string username, string password)
        {
            return true; // Membership.ValidateUser(username, password);
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection muc = new MembershipUserCollection();

            try
            {
                UserBll userBll = new UserBll();
                List<aspnet_Users> allUsers = userBll.GetAllUsers ();
                foreach (aspnet_Users user in allUsers)
                {
                    aspnet_Membership membership = userBll.GetMemberShipByUserID(user.UserId);
                    muc.Add (new MembershipUser( "SafeSplashMemberShipProvider", user.UserName, user.UserId, membership.Email, membership.PasswordQuestion, "", true, false,
                        DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.MinValue));
                }
                totalRecords = allUsers.Count;
            }
            catch (Exception e)
            {
                throw new Exception("Exception in GetAllUsers():" + e.Message);
            }
            return muc;
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection muc = new MembershipUserCollection();
            try
            {
                UserBll userBll = new UserBll();
                List<aspnet_Users> allUsers = userBll.FindUsersByName(usernameToMatch);
                foreach (aspnet_Users user in allUsers)
                {
                    aspnet_Membership membership = userBll.GetMemberShipByUserID(user.UserId);
                    muc.Add(new MembershipUser("SafeSplashMemberShipProvider", user.UserName, user.UserId, membership.Email, membership.PasswordQuestion, "", true, false,
                        DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.MinValue));
                }
                totalRecords = allUsers.Count;
            }
            catch (Exception e)
            {
                throw new Exception("Exception in GetAllUsers():" + e.Message);
            }
            return muc;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            MembershipUser mu = null;
            try
            {
                UserBll userBll = new UserBll();
                aspnet_Users user = userBll.GetUserByID((Guid)providerUserKey);
                if ( user != null)
                {
                    aspnet_Membership membership = userBll.GetMemberShipByUserID(user.UserId);
                    mu = new MembershipUser("SafeSplashMemberShipProvider", user.UserName, user.UserId, membership.Email, membership.PasswordQuestion, "", true, false,
                        DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.MinValue);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception in GetUser()1:" + e.Message);
            }
            return mu;
        }
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            MembershipUser mu = null;
            try
            {
                UserBll userBll = new UserBll();
                aspnet_Users user = userBll.GetUserByName((string)username);
                if (user != null)
                {
                    aspnet_Membership membership = userBll.GetMemberShipByUserID(user.UserId);
                    mu = new MembershipUser("SafeSplashMemberShipProvider", user.UserName, user.UserId, membership.Email, membership.PasswordQuestion, "", true, false,
                        DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.MinValue);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Exception in GetUser()2:" + e.Message);
            }
            return mu;
        }
    }
}
