﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using AKMII.SafeSplash.BLL;

namespace SafeSplash.ObjectModel
{
    public class MembershipManage
    {
        MembershipProvider membershipProvider;

        public MembershipManage()
        {
            membershipProvider = Membership.Providers[Class.GenericHelper.GetAppSettings(Class.Constant.AppSetting_MembershipProvider)];
        }

        public MembershipManage(string providerName)
        {
            membershipProvider = Membership.Providers[providerName];
        }

        public MembershipCreateStatus CreateUser(string userName, string password, string email, UserType userType, out Guid userID)
        {
            userID = new Guid();
            MembershipCreateStatus status;
            //membershipProvider.CreateUser(userName, password, email, "Default", "Default", true, userID, out status);
            //MembershipUser newUser = Membership.CreateUser(userName, pw, email);
            MembershipUser newUser = Membership.CreateUser(userName.Trim(), password, email, "Default", "Default", true, out status);
            if (!Roles.IsUserInRole(userName.Trim(), userType.ToString())) Roles.AddUserToRole(userName.Trim(), userType.ToString());
            UserBll userBll = new UserBll();
            userID = userBll.GetUserIDByUserName(userName.Trim());
            //string customizedAnswer = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile("Default", "SHA1");
            //userBll.UpdateQuestionAnswer(userID, customizedAnswer);
            //userBll.SaveCopyPassword(userID, password);
            return status;
        }

        public bool DeleteUser(string userName, bool deleteAllRelatedData)
        {
            return Membership.DeleteUser(userName, deleteAllRelatedData);
        }

        public void ChangeRole(string userName, string oldRoleName, string newRoleName)
        {
            Roles.RemoveUserFromRole(userName, oldRoleName);
            Roles.AddUserToRole(userName, newRoleName);
        }
        //public MembershipCreateStatus CreateUser(string userName, string email, UserType userType, out Guid userID)
        //{
        //    string pw = Membership.GeneratePassword(8, 1);
        //    return CreateUser(userName, pw, email, userType, out userID);
        //}

        public string GeneratePassword()
        {
            return Membership.GeneratePassword(8, 1);
        }

        public string GetUserPasswordQuestion(string userName)
        {
            MembershipUser user = membershipProvider.GetUser(userName, false);
            return user.PasswordQuestion;
        }

        public MembershipUser GetUserByName(string userName)
        {
            MembershipUser user = membershipProvider.GetUser(userName, false);
            return user;
        }

        public bool ResetPassword(string userName, string answer, out string newPassword)
        {
            try
            {
                newPassword = membershipProvider.ResetPassword(userName, answer);
            }
            catch (MembershipPasswordException exception)
            {
                newPassword = String.Empty;
                return false;
            }
            return true;
        }
        public bool ChangeUserPassword(string userName, string oldPW, string newPW)
        {
            bool result = true;
            try
            {
                result = membershipProvider.ChangePassword(userName, oldPW, newPW);
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public bool ChangeUserStatus(string userName, bool userStatus)
        {
            bool result = true;
            try
            {
                MembershipUser user = membershipProvider.GetUser(userName, false);
                user.IsApproved = userStatus;
                membershipProvider.UpdateUser(user);
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public bool JudgeWetherUserNameOrEmailExist(string userName, string email)
        {
            MembershipUser user = Membership.GetUser(userName);
            string name = Membership.GetUserNameByEmail(email);
            if (user == null && string.IsNullOrEmpty(name))
                return true;
            else
                return false;
        }

        public bool IsUserExist(string userName)
        {
            MembershipUser user = membershipProvider.GetUser(userName, false);
            return (user != null);
        }

        public bool ValidateUser(string userName, string userPwd)
        {
            return Membership.ValidateUser(userName, userPwd);
        }
                
        public bool ActivateUser(string userName, string oldPW, string newPW, string question1, string answer1)
        {
            bool result = true;
            MembershipUser user = Membership.Providers["AspNetSqlMembershipProvider"].GetUser(userName, true);
            result = Membership.Providers["AspNetSqlMembershipProvider"].ChangePassword(userName, oldPW, newPW);
            if (result)
            {
                result = Membership.Providers["AspNetSqlMembershipProvider"].ChangePasswordQuestionAndAnswer(userName, newPW, question1, answer1);
            }
            return result;
            //string value = "Count:"+ Membership.Providers.Count.ToString() ;
            //foreach (MembershipProvider provider in Membership.Providers)
            //{
            //    value += "ApplicationName:" + provider.ApplicationName + "Name:" + provider.Name + "Description: " + provider.Description;
            //}
            //value += "Current:" + Membership.Provider.Name + "Description:" + Membership.Provider.Description;
            //return value;
            //{
                //MembershipUser user = Membership.GetUser(userName, true);
                //user.ChangePassword(user.GetPassword(), newPW);
                //user.ChangePasswordQuestionAndAnswer(newPW, question1, answer1);
                //string customizedAnswer = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(answer1, "SHA1");
                //userBll.UpdateQuestionAnswer(userID, customizedAnswer);
            //}
            //catch (Exception ex)
            //{
            //    result = false;
            //}
            //return result;
        }

        public bool CreateRole(string roleName)
        {
            bool result = true;
            try
            {
                Roles.CreateRole(roleName);
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
    }
}
