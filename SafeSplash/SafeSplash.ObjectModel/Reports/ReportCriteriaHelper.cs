﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.ObjectModel.Reports
{
    public class ReportCriteriaHelper
    {
        public static bool ValidateReportYear(string year)
        {
            int resultYear;
            if (!int.TryParse(year, out resultYear)) { return false; }
            return (resultYear >= 2010 && resultYear <= DateTime.MaxValue.Year);
        }

        public static bool ValidateReportYears(int fromYear, int toYear)
        {
            return (toYear >= fromYear);
        }

        public static bool ValidateReportYearDates(DateTime fromDate, DateTime toDate)
        {
            return (toDate.CompareTo(fromDate) >= 0);
        }
    }
}
