﻿using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using Microsoft.SharePoint;
//using Microsoft.SharePoint.WebControls;

namespace SafeSplash.ObjectModel.Reports
{
    public class ExportToExcel
    {
        public static void ExportTable(string fileName, Table dataTable)
        {
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    dataTable.RenderControl(htw);

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName));
                    HttpContext.Current.Response.ContentType = "application/ms-excel";
                    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //render the htmlwriter into the response.
                    
                    HttpContext.Current.Response.Write(sw.ToString());
                    HttpContext.Current.Response.End();
                    HttpContext.Current.Response.Flush();
                }
            }
        }

        public static void ExportGridView(string fileName, GridView dataGrid)
        {
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    dataGrid.RenderControl(htw);

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", fileName));
                    HttpContext.Current.Response.ContentType = "application/ms-excel";
                    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //render the htmlwriter into the response.

                    HttpContext.Current.Response.Write(sw.ToString());
                    HttpContext.Current.Response.End();
                    HttpContext.Current.Response.Flush();
                }
            }
        }

        //public static void ExportSPGrid(string fileName, SPGridView dataSPGrid)
        //{
        //}
    }
}
