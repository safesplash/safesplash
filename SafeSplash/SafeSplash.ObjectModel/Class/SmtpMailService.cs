﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.IO;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace SafeSplash.ObjectModel.Class
{
    public class SmtpMailService
    {
        private string smtpServer;

        private string mailAccount;

        private string password;

        private int mailPort;


        public SmtpMailService(string smtpServer, string mailAccount, string password, int mailPort)
        {
            this.smtpServer = smtpServer;
            this.mailAccount = mailAccount;
            this.password = password;
            this.mailPort = mailPort;
        }

        public void Send(Mail body)
        {
            SmtpClient client = new SmtpClient(smtpServer, mailPort);
            //client.EnableSsl = false;
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(mailAccount, password);

            //ServicePointManager.ServerCertificateValidationCallback = 
            //    delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) 
            //    { 
            //        return true; 
           //     };

            bool isExecute = true;
            int retryCount = 0;
            MailMessage message = null;

            while (isExecute)
            {
                try
                {
                    message = ConstructMailMessage(body);
                    client.Send(message);
                    isExecute = false;

                    if (body.Recipients != null)
                    {
                        foreach (var recipient in body.Recipients)
                        {
                            //Logger.Info("Send mail to " + recipient);
                        }
                    }
                }
                catch (Exception ex)
                {
                    string exception = ex.Message;
                    //Logger.Error("Exception ocurrs in sending mail", ex);
                    retryCount++;

                    if (retryCount > 3)
                    {
                        isExecute = false;
                    }
                }
                finally
                {
                    if (message != null)
                    {
                        message.Dispose();
                        message = null;
                    }
                }
            }
        }

        #region Prvivate Methods

        private MailMessage ConstructMailMessage(Mail body)
        {
            MailMessage message = new MailMessage();

            message.From = new MailAddress(mailAccount);
            if (body.Recipients != null)
            {
                foreach (var recipient in body.Recipients)
                {
                    message.To.Add(new MailAddress(recipient));
                }
            }

            if (body.CcRecipients != null)
            {
                foreach (var ccRecipient in body.CcRecipients)
                {
                    if (!string.IsNullOrEmpty(ccRecipient))
                    {
                        try
                        {
                            message.CC.Add(new MailAddress(ccRecipient));
                        }
                        catch
                        {
                            // shouldn't be fatal exception if CC recipient is bad
                        }
                    }
                }
            }

            if (body.BccRecipients != null)
            {
                foreach (var bccRecipient in body.BccRecipients)
                {
                    if (!string.IsNullOrEmpty(bccRecipient))
                    {
                        try
                        {
                            message.Bcc.Add(new MailAddress(bccRecipient));
                        }
                        catch
                        {
                            // shouldn't be fatal exception if BCC recipient is bad
                        }
                    }
                }
            }

            message.Subject = body.Subject;
            message.Body = body.Content;

            if (!string.IsNullOrEmpty(body.AttachmentFile))
            {
                if (File.Exists(body.AttachmentFile))
                {
                    message.Attachments.Add(new Attachment(body.AttachmentFile));
                }
            }

            message.IsBodyHtml = true;

            return message;
        }

        #endregion
    }
}
