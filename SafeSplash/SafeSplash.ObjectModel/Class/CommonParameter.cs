﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.ObjectModel
{
    class CommonParameter
    {
    }

    public enum ClientStatus
    {
        //client had registered ,but not completed
        prospect,
        //registration has completed
        inactive,
        //currently has one or more students in the class
        current,
        //did not participate in any course within two years
        past
    }

    public static class Constants
    {
        public const string RoleClient = "Client";
        public const string RoleInstructor = "Instructor";
        public const string RoleFacilityUser = "FacilityUser";
        public const string RoleFacilityManager = "FacilityManager";
        public const string RoleCorpAdmin = "CorpAdmin";
    }

    public enum UserType
    {
        Client,
        Instructor,
        FacilityUser,
        FacilityManager,
        CorpAdmin,
        Default
    }

    public enum TaskStatus
    {
        NotStarted,
        InProgress,
        Completed,
        Closed
    }

    public enum TaskViewType
    {
        Opening,
        Closed,
        Created
    }

    public enum BillingType
    {
        Recurring,
        SplashPack,
        Camp
    }

    public enum BillingCycle
    {
        Monthly,
        OneTime
    }
}
