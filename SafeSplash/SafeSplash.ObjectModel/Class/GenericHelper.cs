﻿using System;
using System.Data;
using System.Reflection;
using System.Globalization;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Linq;
using System.Text;
//using Microsoft.SharePoint;
//using Microsoft.SharePoint.Utilities;
using System.Net;
using System.Web;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;


namespace SafeSplash.ObjectModel.Class
{
    public static class GenericHelper
    {
        public static string GenerateRandomPassword()
        {
            int length = 0, N = 0;
            string rndomString = "";
            Random _random = new Random(System.DateTime.Now.Millisecond);
            while (length < 7)
            {
                N = _random.Next(0, 125);
                if ((N >= 0) && (N <= 9))
                {
                    rndomString = rndomString + N.ToString();
                    length = length + 1;
                }
                else if ((N >= 65) && (N <= 86))
                {
                    rndomString = rndomString + (char)N;
                    length = length + 1;
                }
            }
            return rndomString;
        }

        public static void CloseDialog(System.Web.UI.Page page, int flag, string args)
        {
            page.Response.Clear();
            page.Response.Write(string.Format(CultureInfo.InvariantCulture,
                 "<script type=\"text/javascript\">window.frameElement.commonModalDialogClose({0}, '{1}');</script>", flag, args));
            page.Response.End();
        }

        public static void pageRedirect(System.Web.UI.Page page, string link)
        {
            page.Response.Clear();
            page.Response.Write(string.Format(CultureInfo.InvariantCulture,
                 "<script type=\"text/javascript\">window.location = \"" +link+ "\";</script>"));
            page.Response.End();
        }
        /// <summary>
        /// Convert a List{T} to a DataTable.
        /// </summary>
        public static DataTable ToDataTable<T>(List<T> items)
        {
            var tb = new DataTable(typeof(T).Name);
            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in props)
            {
                Type t = GetCoreType(prop.PropertyType);
                tb.Columns.Add(prop.Name, t);
            }
            foreach (T item in items)
            {
                var values = new object[props.Length];

                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }
                tb.Rows.Add(values);
            }
            return tb;
        }



        /// <summary>
        /// Determine of specified type is nullable
        /// </summary>
        public static bool IsNullable(Type t)
        {
            return !t.IsValueType || (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>));
        }
        /// <summary>
        /// Return underlying type if type is Nullable otherwise return the type
        /// </summary>
        public static Type GetCoreType(Type t)
        {
            if (t != null && IsNullable(t))
            {
                if (!t.IsValueType)
                {
                    return t;
                }
                else
                {
                    return Nullable.GetUnderlyingType(t);
                }
            }
            else
            {
                return t;
            }
        }

        public static string GetAppSettings(string key)
        {
            return "RAB"; //  SPUtility.GetLocalizedString("$Resources:" + Constant.ResxFile_App_Setting + "," + key, Constant.ResxFile_App_Setting, SPContext.Current.Web.Language);
        }

        public static string GetAppSettings(string key, uint language)
        {
            return "RAB"; // return SPUtility.GetLocalizedString("$Resources:" + Constant.ResxFile_App_Setting + "," + key, Constant.ResxFile_App_Setting, language);
        }

        public static void SendMail(Mail body)
        {
            MailMessage mailMsg = new MailMessage();

            foreach (var to in body.Recipients)
            {
                if (to.Length > 0)
                {
                    mailMsg.To.Add(new MailAddress(to));
                }
            }
            foreach (var cc in body.CcRecipients)
            {
                if (cc.Length > 0)
                {
                    mailMsg.CC.Add(new MailAddress(cc));
                }
            }
            foreach (var bcc in body.BccRecipients)
            {
                if (bcc.Length > 0)
                {
                    mailMsg.Bcc.Add(new MailAddress(bcc));
                }
            }
            mailMsg.From = new MailAddress(body.FromAddress);
            mailMsg.Subject = body.Subject;
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(body.Content, null, MediaTypeNames.Text.Html));
            SmtpClient smtpClient = new SmtpClient("smtp.office365.com", Convert.ToInt32(587));
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("registration@safesplash.com", "splash$123");
            smtpClient.Credentials = credentials;
            smtpClient.EnableSsl = true;
            //client.DeliveryMethod = SmtpDeliveryMethod.Network;
            //client.UseDefaultCredentials = false;
            //want to comment this, is a hack, will have to test in off hours.
            ServicePointManager.ServerCertificateValidationCallback =
                     delegate(object s, X509Certificate certificate,X509Chain chain, SslPolicyErrors sslPolicyErrors)
                    { return true; };
            try
            {
                smtpClient.Send(mailMsg);
            }
            catch (Exception ex)
            {
                var hi = "hi";
            }
        }

        //public static void SendMail(Mail body, uint language)
        //{
        //    //body.Recipients = new List<string>() { "mikerohrbach@yahoo.com" };
        //    //body.CcRecipients = new List<string>();
        //    //body.BccRecipients = new List<string>();
        //    int mailPort = Convert.ToInt32(GetAppSettings(Constant.AppSetting_MailPort, language));
        //    SmtpMailService smtpService = new SmtpMailService(GetAppSettings(Constant.AppSetting_SmtpServer, language), GetAppSettings(Constant.AppSetting_MailAccount, language), GetAppSettings(Constant.AppSetting_MailPassword, language), mailPort);
        //    smtpService.Send(body);
        //}

        public static string GetMailContent(string key)
        {
            return "RAB"; // return SPUtility.GetLocalizedString("$Resources:" + Constant.ResxFile_Mail_Content + "," + key, Constant.ResxFile_Mail_Content, SPContext.Current.Web.Language);
        }

        public static string GetMailContent(string key, uint Language)
        {
            return "RAB"; // return SPUtility.GetLocalizedString("$Resources:" + Constant.ResxFile_Mail_Content + "," + key, Constant.ResxFile_Mail_Content, Language);
        }

        public static void DDLSelectedValue(DropDownList ddl, string value)
        {
            if (ddl != null)
            {
                foreach (ListItem item in ddl.Items)
                {
                    if (item.Value == value)
                    {
                        item.Selected = true;
                    }
                    else
                    {
                        item.Selected = false;
                    }
                }
            }
        }

        public static UserType ConvertToMemberType(string memberType)
        {
            switch (memberType)
            {
                case "Client":
                    return UserType.Client;
                case "FacilityUser":
                    return UserType.FacilityUser;
                case "FacilityManager":
                    return UserType.FacilityManager;
                case "CorpAdmin":
                    return UserType.CorpAdmin;
                case "Instructor":
                    return UserType.Instructor;
                default:
                    return UserType.Default;
            }
        }

        public static string WrappableText(string source)
        {
            if (source != null)
            {
                string nwln = Environment.NewLine;
                return "<p>" +
                source.Replace(nwln + nwln, "</p><p>")
                .Replace(nwln, "<br />") + "</p>";
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Add a user to a Sharepoint group
        /// </summary>
        /// <param name="userLoginName">Login name of the user to add</param>
        /// <param name="userGroupName">Group name to add</param>
        public static void AddUserToAGroup(System.Web.UI.Page page, string userLoginName, string userGroupName)
        {
            ///RAB
            ////Executes this method with Full Control rights even if the user does not otherwise have Full Control
            //SPSecurity.RunWithElevatedPrivileges(delegate
            //{
            //    //Don't use context to create the spSite object since it won't create the object with elevated privileges but with the privileges of the user who execute the this code, which may casues an exception
            //    using (SPSite spSite = new SPSite(page.Request.Url.ToString()))
            //    {
            //        using (SPWeb spWeb = spSite.OpenWeb())
            //        {
            //            try
            //            {
            //                //Allow updating of some sharepoint lists, (here spUsers, spGroups etc...)
            //                spWeb.AllowUnsafeUpdates = true;

            //                SPUser spUser = spWeb.EnsureUser(userLoginName);

            //                if (spUser != null)
            //                {
            //                    SPGroup spGroup = spWeb.Groups[userGroupName];

            //                    if (spGroup != null)
            //                        spGroup.AddUser(spUser);
            //                }
            //            }
            //            catch (Exception ex)
            //            {
            //                //Error handling logic should go here
            //            }
            //            finally
            //            {
            //                spWeb.AllowUnsafeUpdates = false;
            //            }
            //        }
            //    }

            //});
        }

        public static void RemoveUserFromAGroup(System.Web.UI.Page page, string userLoginName, string userGroupName)
        {
            ///RAB
            ////Executes this method with Full Control rights even if the user does not otherwise have Full Control
            //SPSecurity.RunWithElevatedPrivileges(delegate
            //{
            //    //Don't use context to create the spSite object since it won't create the object with elevated privileges but with the privileges of the user who execute the this code, which may casues an exception
            //    using (SPSite spSite = new SPSite(page.Request.Url.ToString()))
            //    {
            //        using (SPWeb spWeb = spSite.OpenWeb())
            //        {
            //            try
            //            {
            //                //Allow updating of some sharepoint lists, (here spUsers, spGroups etc...)
            //                spWeb.AllowUnsafeUpdates = true;

            //                SPUser spUser = spWeb.EnsureUser(userLoginName);

            //                if (spUser != null)
            //                {
            //                    SPGroup spGroup = spWeb.Groups[userGroupName];

            //                    if (spGroup != null)
            //                        spGroup.RemoveUser(spUser);
            //                }
            //            }
            //            catch (Exception ex)
            //            {
            //                //Error handling logic should go here
            //            }
            //            finally
            //            {
            //                spWeb.AllowUnsafeUpdates = false;
            //            }
            //        }
            //    }

            //});
        }

        public static DataTable Join(DataTable First, DataTable Second, DataColumn[] FJC, DataColumn[] SJC)
        {
            //Create Empty Table
            DataTable table = new DataTable("Join");
            // Use a DataSet to leverage DataRelation
            using (DataSet ds = new DataSet())
            {
                //Add Copy of Tables
                ds.Tables.AddRange(new DataTable[] { First.Copy(), Second.Copy() });
                //Identify Joining Columns from First
                DataColumn[] parentcolumns = new DataColumn[FJC.Length];
                for (int i = 0; i < parentcolumns.Length; i++)
                {
                    parentcolumns[i] = ds.Tables[0].Columns[FJC[i].ColumnName];
                }
                //Identify Joining Columns from Second
                DataColumn[] childcolumns = new DataColumn[SJC.Length];
                for (int i = 0; i < childcolumns.Length; i++)
                {
                    childcolumns[i] = ds.Tables[1].Columns[SJC[i].ColumnName];
                }
                //Create DataRelation
                DataRelation r = new DataRelation(string.Empty, parentcolumns, childcolumns, false);
                ds.Relations.Add(r);
                //Create Columns for JOIN table
                for (int i = 0; i < First.Columns.Count; i++)
                {
                    table.Columns.Add(First.Columns[i].ColumnName, First.Columns[i].DataType);
                }
                for (int i = 0; i < Second.Columns.Count; i++)
                {
                    //Beware Duplicates
                    if (!table.Columns.Contains(Second.Columns[i].ColumnName))
                        table.Columns.Add(Second.Columns[i].ColumnName, Second.Columns[i].DataType);
                    else
                        table.Columns.Add(Second.Columns[i].ColumnName + "_Second", Second.Columns[i].DataType);
                }
                //Loop through First table
                table.BeginLoadData();
                foreach (DataRow firstrow in ds.Tables[0].Rows)
                {
                    //Get "joined" rows
                    DataRow[] childrows = firstrow.GetChildRows(r);
                    object[] parentarray = firstrow.ItemArray;
                    if (childrows != null && childrows.Length > 0)
                    {
                        foreach (DataRow secondrow in childrows)
                        {
                            object[] secondarray = secondrow.ItemArray;
                            object[] joinarray = new object[parentarray.Length + secondarray.Length];
                            Array.Copy(parentarray, 0, joinarray, 0, parentarray.Length);
                            Array.Copy(secondarray, 0, joinarray, parentarray.Length, secondarray.Length);
                            table.LoadDataRow(joinarray, true);
                        }
                    }
                    else
                    {
                        table.LoadDataRow(parentarray, true);
                    }
                }
                table.EndLoadData();
            }
            return table;
        }

        public static string GetTaskStatusTextByValue (TaskStatus status)
        {
            switch (status)
            {
                case TaskStatus.InProgress:
                    return Constant.TaskStatus_Text_InProgress;
                case TaskStatus.NotStarted:
                    return Constant.TaskStatus_Text_NotStarted;
                case TaskStatus.Completed:
                    return Constant.TaskStatus_Text_Completed;
                case TaskStatus.Closed:
                    return Constant.TaskStatus_Text_Closed;
                default:
                    return string.Empty;
            }
        }

        public static string GetTaskStatusTextByValue(string status)
        {
            switch (status)
            {
                case "InProgress":
                    return Constant.TaskStatus_Text_InProgress;
                case "NotStarted":
                    return Constant.TaskStatus_Text_NotStarted;
                case "Completed":
                    return Constant.TaskStatus_Text_Completed;
                case "Closed":
                    return Constant.TaskStatus_Text_Closed;
                default:
                    return string.Empty;
            }
        }

        public static System.Drawing.Color GetLevelColor(string levelName)
        {
            switch (levelName.ToUpper())
            {
                case "PARENT-N-ME I":
                    return System.Drawing.Color.LightGray;
                case "PARENT-N-ME II":
                    return System.Drawing.Color.DarkGray;
                case "LEVEL 1":
                    return System.Drawing.Color.Pink;
                case "LEVEL 2A":
                    return System.Drawing.Color.Turquoise;
                case "LEVEL 2B":
                    return System.Drawing.Color.Orange;
                case "LEVEL 3":
                    return System.Drawing.Color.Green;
                case "LEVEL 4":
                    return System.Drawing.Color.Yellow;
                case "LEVEL 5":
                    return System.Drawing.Color.Purple;
                case "LEVEL 6":
                    return System.Drawing.Color.White;
                case "LEVEL 7":
                    return System.Drawing.Color.Red;
                case "LEVEL 8":
                    return System.Drawing.Color.Blue;
                case "PRIVATE":
                    return System.Drawing.Color.Salmon;
                case "SEMI PRIVATE":
                    return System.Drawing.Color.Salmon;
                case "SPECIAL NEEDS":
                    return System.Drawing.Color.Salmon;
                case "ADULT 1":
                    return System.Drawing.Color.FromArgb(131, 245, 44);
                case "ADULT 2":
                    return System.Drawing.Color.FromArgb (255,110,199);
                case "BEGINNER":
                    return System.Drawing.Color.FromArgb(255, 131, 0);
                case "INTERMEDIATE":
                    return System.Drawing.Color.FromArgb(255, 131, 0);
                case "ADVANCED":
                    return System.Drawing.Color.FromArgb(255, 131, 0);
                case "COMPETITIVE":
                    return System.Drawing.Color.FromArgb(255, 131, 0);
                default:
                    return System.Drawing.Color.White;
            }
        }

        public static int DetermineStudentAge(DateTime studentBirthday, DateTime asOfDate)
        {
            int age = asOfDate.Year - studentBirthday.Year;
            bool isLeapYear = DateTime.IsLeapYear(asOfDate.Year);
            int year = asOfDate.Year;
            int month = studentBirthday.Month;
            int day = studentBirthday.Day;
            if (month == 2 && day == 29 && !isLeapYear) { day = 28; }
            if (new DateTime(year, month, day).CompareTo(asOfDate) >= 0)
            {
                age = age - 1;
            }
            return age;
        }
    }
}
