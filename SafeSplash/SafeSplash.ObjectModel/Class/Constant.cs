﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.ObjectModel.Class
{
    public class Constant
    {
        public static string AppSetting_SmtpServer = "AppSetting_SmtpServer";
        public static string AppSetting_MailAccount = "AppSetting_MailAccount";
        public static string AppSetting_MailPassword = "AppSetting_MailPassword";
        public static string AppSetting_MailPort = "AppSetting_MailPort";
        public static string AppSetting_MembershipProvider = "AppSetting_MembershipProvider";

        public static string AppSetting_AuthorizeNet_ApiLogin = "ApiLogin";
        public static string AppSetting_AuthorizeNet_TransactionKey = "TransactionKey";

        public static string AppSetting_ACH_MerchantId = "MerchantId";
        public static string AppSetting_ACH_ApiId = "ApiId";
        public static string AppSetting_ACH_TransactionKey = "TransactionKey";
        public static string AppSetting_ACH_ProcessingPassword = "ProcessingPassword";
        public static string AppSetting_ACH_TestCardNumber = "TestCardNumber";

        public static string List_ClientClassLevels = "List_ClientClassLevels";

        public static string DisclosuresForm_Waiver = "DisclosuresForm_Waiver";
        public static string DisclosuresForm_Policies = "DisclosuresForm_Policies";
        public static string DisclosuresForm_Billing = "DisclosuresForm_Billing";
        public static string ResxFile_Mail_Content = "Mail_Content";
        //public static string Employee_New_Notify_Title = "Employee_New_Notify_Title";
        //public static string Employee_New_Notify_Body = "Employee_New_Notify_Body";
        public static string Employee_Certification_Expire_Body = "Employee_Certification_Expire_Body";
        public static string Employee_Certification_Expire_Title = "Employee_Certification_Expire_Title";
        public static string Task_New_Notify_Title = "Task_New_Notify_Title";
        public static string Task_New_Notify_Body = "Task_New_Notify_Body";
        public static string Task_Edit_Notify_Title = "Task_Edit_Notify_Title";
        public static string Task_Edit_Notify_Body = "Task_Edit_Notify_Body";
        //public static string Payment_Notify_Title = "Payment_Notify_Title";
        //public static string Payment_Notify_Body = "Payment_Notify_Body";
        //public static string Payment_Schedule_Stop_Title = "Payment_Schedule_Stop_Title";
        //public static string Payment_Schedule_Stop_Body = "Payment_Schedule_Stop_Body";

        public static string Task_Employee_Certification_Expire_Title = "Task_Employee_Certification_Expire_Title";
        public static string Task_Employee_Certification_Expire_Body = "Task_Employee_Certification_Expire_Body";

        public static string Task_New_ClientRegister_Title = "Task_New_ClientRegister_Title";
        public static string Task_New_ClientRegister_Body = "Task_New_ClientRegister_Body";

        public static string Task_Client_UpdateBillingInfo_Title = "Task_Client_UpdateBillingInfo_Title";
        public static string Task_Client_UpdateBillingInfo_Body = "Task_Client_UpdateBillingInfo_Body";

        //public static string Client_New_Notify_Title = "Client_New_Notify_Title";
        //public static string Client_New_Notify_Body = "Client_New_Notify_Body";

        //public static string Client_Withdrawal_Request_Title = "Client_Withdrawal_Request_Title";
        //public static string Client_Withdrawal_Request_Body = "Client_Withdrawal_Request_Body";

        //public static string Client_Class_Book_Recurring_Title = "Client_Class_Book_Recurring_Title";
        //public static string Client_Class_Book_Recurring_Body = "Client_Class_Book_Recurring_Body";

        //public static string Client_Class_Book_Camp_Title = "Client_Class_Book_Camp_Title";
        //public static string Client_Class_Book_Camp_Body = "Client_Class_Book_Camp_Body";

        //public static string New_ClientRegister_Title = "New_ClientRegister_Title";
        //public static string New_ClientRegister_Body = "New_ClientRegister_Body";

        public static string User_Login_Page = "/_Layouts/Login.aspx";

        public static string ResxFile_App_Setting = "SafeSplash_Config";

        public static string ImageLibrary_EmployeeProfile = "ImageLibrary_EmployeeProfile";
        public static string SP_EmployeeProfile_DefaultPic = "/_Layouts/ss/images/profile-default.png";

        // Tasks Status
        public static string TaskStatus_Text_NotStarted = "Not Started";
        public static string TaskStatus_Text_InProgress = "In Progress";
        public static string TaskStatus_Text_Completed = "Completed";
        public static string TaskStatus_Text_Closed = "Closed";

        public static string WithdrawStatus_Text_InProgress = "In Progress";
        public static string WithdrawStatus_Text_Accepted = "Accepted";
        public static string WithdrawStatus_Text_Denied = "Denied";
        public static string WithdrawStatus_Text_Updated = "Updated";
    }
}
