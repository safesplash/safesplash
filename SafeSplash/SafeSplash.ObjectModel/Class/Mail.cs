﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace SafeSplash.ObjectModel.Class
{
    public class Mail
    {
        private List<string> recipients = new List<string>();
        private List<string> ccRecipients = new List<string>();
        private List<string> bccRecipients = new List<string>();

        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the recipient.
        /// </summary>
        public List<string> Recipients
        {
            get
            {
                return recipients;
            }
            set
            {
                if (value != null)
                {
                    recipients.Clear();
                    foreach (var mail in value)
                    {
                        recipients.AddRange(mail.Split(';'));
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets from address.
        /// </summary>
        public string FromAddress { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        public MailPriority Priority { get; set; }

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        public string AttachmentFile { get; set; }

        /// <summary>
        /// Gets or sets the mail cc
        /// </summary>
        //public List<string> CarbonCopy { get; set; }

        public List<string> CcRecipients
        {
            get
            {
                return ccRecipients;
            }
            set
            {
                if (value != null)
                {
                    ccRecipients.Clear();
                    foreach (var cc in value)
                    {
                        ccRecipients.AddRange(cc.Split(';'));
                    }
                }
            }
        }

        public List<string> BccRecipients
        {
            get
            {
                return bccRecipients;
            }
            set
            {
                if (value != null)
                {
                    bccRecipients.Clear();
                    foreach (var bcc in value)
                    {
                        bccRecipients.AddRange(bcc.Split(';'));
                    }
                }
            }
        }



        /// <summary>
        /// Gets or sets the create date.
        /// </summary>
        public DateTime? CreateDate { get; set; }
    }
}
