﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SafeSplash.ObjectModel.Class
{
    public class Validator
    {
        public static bool IsValidMoneyAmount(string stringToValidate)
        {
            if (string.IsNullOrEmpty(stringToValidate)) return false;
            string moneyPattern = @"^\-?\(?\$?\s*\-?\s*\(?(((\d{1,3}((\,\d{3})*|\d*))?(\.\d{1,4})?)|((\d{1,3}((\,\d{3})*|\d*))(\.\d{0,4})?))\)?$";

            try
            {
                Regex.Match(stringToValidate, moneyPattern);
            }
            catch (ArgumentException)
            {
                return false;
            }

            return true;
        }

    }
}
