﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using AKMII.SafeSplash.BLL;
using AKMII.SafeSplash.Entity;

namespace SafeSplash.ObjectModel.Class
{
    public static class ControlInitialization
    {
        public static void DDL_Gender(DropDownList gender, bool blankItem)
        {
            gender.Items.Clear();
            if (blankItem)
            {
                gender.Items.Add(new ListItem("", ""));
            }
            gender.Items.Add(new ListItem("Male", "Male"));
            gender.Items.Add(new ListItem("Female", "Female"));
        }

        public static void DDL_HowHear(DropDownList howHear, bool blankItem)
        {
            howHear.Items.Clear();
            if (blankItem)
            {
                howHear.Items.Add(new ListItem("", ""));
            }
            HowHearUsBll howhearUsBll = new HowHearUsBll();
            List<tb_HowHearUs> howhearUsList = howhearUsBll.GetAll();
            if (howhearUsList != null)
            {
                foreach (tb_HowHearUs howHearUs in howhearUsList)
                {
                    howHear.Items.Add(new ListItem(howHearUs.howhear_Title, howHearUs.howhear_Title));
                }
            }
        }

        public static void DDL_Month(DropDownList month, bool blankItem)
        {
            month.Items.Clear();
            if (blankItem)
            {
                month.Items.Add(new ListItem("", ""));
            }
            month.Items.Add(new ListItem("January", "1"));
            month.Items.Add(new ListItem("February", "2"));
            month.Items.Add(new ListItem("March", "3"));
            month.Items.Add(new ListItem("April", "4"));
            month.Items.Add(new ListItem("May", "5"));
            month.Items.Add(new ListItem("June", "6"));
            month.Items.Add(new ListItem("July", "7"));
            month.Items.Add(new ListItem("August", "8"));
            month.Items.Add(new ListItem("September", "9"));
            month.Items.Add(new ListItem("October", "10"));
            month.Items.Add(new ListItem("November", "11"));
            month.Items.Add(new ListItem("December", "12"));
        }

        public static void DDL_DayOfWeek(DropDownList dayOfWeek, bool blankItem)
        {
            dayOfWeek.Items.Clear();
            if (blankItem)
            {
                dayOfWeek.Items.Add(new ListItem("", ""));
            }
            dayOfWeek.Items.Add(new ListItem(DayOfWeek.Sunday.ToString(), DayOfWeek.Sunday.ToString()));
            dayOfWeek.Items.Add(new ListItem(DayOfWeek.Monday.ToString(), DayOfWeek.Monday.ToString()));
            dayOfWeek.Items.Add(new ListItem(DayOfWeek.Tuesday.ToString(), DayOfWeek.Tuesday.ToString()));
            dayOfWeek.Items.Add(new ListItem(DayOfWeek.Wednesday.ToString(), DayOfWeek.Wednesday.ToString()));
            dayOfWeek.Items.Add(new ListItem(DayOfWeek.Thursday.ToString(), DayOfWeek.Thursday.ToString()));
            dayOfWeek.Items.Add(new ListItem(DayOfWeek.Friday.ToString(), DayOfWeek.Friday.ToString()));
            dayOfWeek.Items.Add(new ListItem(DayOfWeek.Saturday.ToString(), DayOfWeek.Saturday.ToString()));
        }

        public static void DDL_Day(DropDownList day, bool blankItem)
        {
            day.Items.Clear();
            if (blankItem)
            {
                day.Items.Add(new ListItem("", ""));
            }
            day.Items.Add(new ListItem("01", "1"));
            day.Items.Add(new ListItem("02", "2"));
            day.Items.Add(new ListItem("03", "3"));
            day.Items.Add(new ListItem("04", "4"));
            day.Items.Add(new ListItem("05", "5"));
            day.Items.Add(new ListItem("06", "6"));
            day.Items.Add(new ListItem("07", "7"));
            day.Items.Add(new ListItem("08", "8"));
            day.Items.Add(new ListItem("09", "9"));
            day.Items.Add(new ListItem("10", "10"));
            day.Items.Add(new ListItem("11", "11"));
            day.Items.Add(new ListItem("12", "12"));
            day.Items.Add(new ListItem("13", "13"));
            day.Items.Add(new ListItem("14", "14"));
            day.Items.Add(new ListItem("15", "15"));
            day.Items.Add(new ListItem("16", "16"));
            day.Items.Add(new ListItem("17", "17"));
            day.Items.Add(new ListItem("18", "18"));
            day.Items.Add(new ListItem("19", "19"));
            day.Items.Add(new ListItem("20", "20"));
            day.Items.Add(new ListItem("21", "21"));
            day.Items.Add(new ListItem("22", "22"));
            day.Items.Add(new ListItem("23", "23"));
            day.Items.Add(new ListItem("24", "24"));
            day.Items.Add(new ListItem("25", "25"));
            day.Items.Add(new ListItem("26", "26"));
            day.Items.Add(new ListItem("27", "27"));
            day.Items.Add(new ListItem("28", "28"));
            day.Items.Add(new ListItem("29", "29"));
            day.Items.Add(new ListItem("30", "30"));
            day.Items.Add(new ListItem("31", "31"));
        }

        public static void DDL_StudentYear(DropDownList year, bool blankItem)
        {
            year.Items.Clear();
            if (blankItem)
            {
                year.Items.Add(new ListItem("", ""));
            }

            int iYear = DateTime.Today.Year;
            for (int i = 0; i < 140; i++)
            {
                year.Items.Add(new ListItem(iYear.ToString(), iYear.ToString()));
                iYear --;
            }
        }

        public static void DDL_ParentRelationship(DropDownList relationship, bool blankItem)
        {
            relationship.Items.Clear();
            if (blankItem)
            {
                relationship.Items.Add(new ListItem("", ""));
            }
            relationship.Items.Add(new ListItem("Mother", "Mother"));
            relationship.Items.Add(new ListItem("Father", "Father"));
            relationship.Items.Add(new ListItem("Other", "Other"));
        }

        public static void DDL_Relationship(DropDownList relationship, bool blankItem)
        {
            relationship.Items.Clear();
            if (blankItem)
            {
                relationship.Items.Add(new ListItem("", ""));
            }
            relationship.Items.Add(new ListItem("Mother", "Mother"));
            relationship.Items.Add(new ListItem("Father", "Father"));
            relationship.Items.Add(new ListItem("Grandparent", "Grandparent"));
            relationship.Items.Add(new ListItem("Sibling", "Sibling"));
            relationship.Items.Add(new ListItem("Aunt", "Aunt"));
            relationship.Items.Add(new ListItem("Uncle", "Uncle"));
            relationship.Items.Add(new ListItem("Cousin", "Cousin"));
            relationship.Items.Add(new ListItem("Nanny", "Nanny"));
            relationship.Items.Add(new ListItem("Friend", "Friend"));
            relationship.Items.Add(new ListItem("Other", "Other"));
        }

        public static void DDL_ClassDay(DropDownList classDay, bool blankItem)
        {
            classDay.Items.Clear();
            if (blankItem)
            {
                classDay.Items.Add(new ListItem("", ""));
            }
            classDay.Items.Add(new ListItem("Sunday", "Sunday"));
            classDay.Items.Add(new ListItem("Monday", "Monday"));
            classDay.Items.Add(new ListItem("Tuesday", "Tuesday"));
            classDay.Items.Add(new ListItem("Wednesday", "Wednesday"));
            classDay.Items.Add(new ListItem("Thursday", "Thursday"));
            classDay.Items.Add(new ListItem("Friday", "Friday"));
            classDay.Items.Add(new ListItem("Saturday", "Saturday"));
        }

        public static void DDL_SecurityQuestion(DropDownList securityQuestion, bool blankItem)
        {
            securityQuestion.Items.Clear();
            if (blankItem)
            {
                securityQuestion.Items.Add(new ListItem("Please Select", ""));
            }
            securityQuestion.Items.Add(new ListItem("What was your childhood nickname?", "What was your childhood nickname?"));
            securityQuestion.Items.Add(new ListItem("Who was your third grade teacher?", "Who was your third grade teacher?"));
            securityQuestion.Items.Add(new ListItem("What was the first concert you attended?", "What was the first concert you attended?"));
            securityQuestion.Items.Add(new ListItem("What is your mother's maiden name?", "What is your mother's maiden name?"));
            securityQuestion.Items.Add(new ListItem("What was the name of your first pet?", "What was the name of your first pet?"));
            securityQuestion.Items.Add(new ListItem("What street did you grow up on?", "What street did you grow up on?"));
            securityQuestion.Items.Add(new ListItem("What is your father's middle name?", "What is your father's middle name?"));
            securityQuestion.Items.Add(new ListItem("When is your mother's birthday?", "When is your mother's birthday?"));
        }

        public static void DDL_ActiveClassTime(DropDownList activeClassTime, bool blankItem)
        {
            activeClassTime.Items.Clear();
            if (blankItem)
            {
                activeClassTime.Items.Add(new ListItem("", ""));
            }
            activeClassTime.Items.Add(new ListItem("7:00 AM", "07:00:00"));
            activeClassTime.Items.Add(new ListItem("7:15 AM", "07:15:00"));
            activeClassTime.Items.Add(new ListItem("7:30 AM", "07:30:00"));
            activeClassTime.Items.Add(new ListItem("7:45 AM", "07:45:00"));
            activeClassTime.Items.Add(new ListItem("8:00 AM", "08:00:00"));
            activeClassTime.Items.Add(new ListItem("8:15 AM", "08:15:00"));
            activeClassTime.Items.Add(new ListItem("8:30 AM", "08:30:00"));
            activeClassTime.Items.Add(new ListItem("8:45 AM", "08:45:00"));
            activeClassTime.Items.Add(new ListItem("9:00 AM", "09:00:00"));
            activeClassTime.Items.Add(new ListItem("9:15 AM", "09:15:00"));
            activeClassTime.Items.Add(new ListItem("9:30 AM", "09:30:00"));
            activeClassTime.Items.Add(new ListItem("9:45 AM", "09:45:00"));
            activeClassTime.Items.Add(new ListItem("10:00 AM", "10:00:00"));
            activeClassTime.Items.Add(new ListItem("10:15 AM", "10:15:00"));
            activeClassTime.Items.Add(new ListItem("10:30 AM", "10:30:00"));
            activeClassTime.Items.Add(new ListItem("10:45 AM", "10:45:00"));
            activeClassTime.Items.Add(new ListItem("11:00 AM", "11:00:00"));
            activeClassTime.Items.Add(new ListItem("11:15 AM", "11:15:00"));
            activeClassTime.Items.Add(new ListItem("11:30 AM", "11:30:00"));
            activeClassTime.Items.Add(new ListItem("11:45 AM", "11:45:00"));
            activeClassTime.Items.Add(new ListItem("12:00 PM", "12:00:00"));
            activeClassTime.Items.Add(new ListItem("12:15 PM", "12:15:00"));
            activeClassTime.Items.Add(new ListItem("12:30 PM", "12:30:00"));
            activeClassTime.Items.Add(new ListItem("12:45 PM", "12:45:00"));
            activeClassTime.Items.Add(new ListItem("1:00 PM", "13:00:00"));
            activeClassTime.Items.Add(new ListItem("1:15 PM", "13:15:00"));
            activeClassTime.Items.Add(new ListItem("1:30 PM", "13:30:00"));
            activeClassTime.Items.Add(new ListItem("1:45 PM", "13:45:00"));
            activeClassTime.Items.Add(new ListItem("2:00 PM", "14:00:00"));
            activeClassTime.Items.Add(new ListItem("2:15 PM", "14:15:00"));
            activeClassTime.Items.Add(new ListItem("2:30 PM", "14:30:00"));
            activeClassTime.Items.Add(new ListItem("2:45 PM", "14:45:00"));
            activeClassTime.Items.Add(new ListItem("3:00 PM", "15:00:00"));
            activeClassTime.Items.Add(new ListItem("3:15 PM", "15:15:00"));
            activeClassTime.Items.Add(new ListItem("3:30 PM", "15:30:00"));
            activeClassTime.Items.Add(new ListItem("3:45 PM", "15:45:00"));
            activeClassTime.Items.Add(new ListItem("4:00 PM", "16:00:00"));
            activeClassTime.Items.Add(new ListItem("4:15 PM", "16:15:00"));
            activeClassTime.Items.Add(new ListItem("4:30 PM", "16:30:00"));
            activeClassTime.Items.Add(new ListItem("4:45 PM", "16:45:00"));
            activeClassTime.Items.Add(new ListItem("5:00 PM", "17:00:00"));
            activeClassTime.Items.Add(new ListItem("5:15 PM", "17:15:00"));
            activeClassTime.Items.Add(new ListItem("5:30 PM", "17:30:00"));
            activeClassTime.Items.Add(new ListItem("5:45 PM", "17:45:00"));
            activeClassTime.Items.Add(new ListItem("6:00 PM", "18:00:00"));
            activeClassTime.Items.Add(new ListItem("6:15 PM", "18:15:00"));
            activeClassTime.Items.Add(new ListItem("6:30 PM", "18:30:00"));
            activeClassTime.Items.Add(new ListItem("6:45 PM", "18:45:00"));
            activeClassTime.Items.Add(new ListItem("7:00 PM", "19:00:00"));
            activeClassTime.Items.Add(new ListItem("7:15 PM", "19:15:00"));
            activeClassTime.Items.Add(new ListItem("7:30 PM", "19:30:00"));
            activeClassTime.Items.Add(new ListItem("7:45 PM", "19:45:00"));
            activeClassTime.Items.Add(new ListItem("8:00 PM", "20:00:00"));
            activeClassTime.Items.Add(new ListItem("8:15 PM", "20:15:00"));
            activeClassTime.Items.Add(new ListItem("8:30 PM", "20:30:00"));
            activeClassTime.Items.Add(new ListItem("8:45 PM", "20:45:00"));
            activeClassTime.Items.Add(new ListItem("9:00 PM", "21:00:00"));
        }

        public static void DDL_ClassTime(DropDownList classTime, string classDay, bool blankItem)
        {
            classTime.Items.Clear();
            if (blankItem)
            {
                classTime.Items.Add(new ListItem("", ""));
            }
            if (!String.IsNullOrEmpty(classDay))
            {
                if (classDay == "Saturday" || classDay == "Sunday")
                {
                    classTime.Items.Add(new ListItem("8AM - 10AM", "8AM - 10AM"));
                    classTime.Items.Add(new ListItem("10AM - 1PM", "10AM - 1PM"));
                }
                else
                {
                    classTime.Items.Add(new ListItem("8AM - 12PM", "8AM - 12PM"));
                    classTime.Items.Add(new ListItem("12PM - 4PM", "12PM - 4PM"));
                    classTime.Items.Add(new ListItem("4PM - 8PM", "4PM - 8PM"));
                }
            }
        }

        public static void DDL_Country(DropDownList country, bool blankItem)
        {
            country.Items.Clear();
            if (blankItem)
            {
                country.Items.Add(new ListItem("", ""));
            }
            country.Items.Add(new ListItem("United States", "United States"));
        }

        //public static void DDL_TaskStatus(DropDownList taskStatus, bool blankItem)
        //{
        //    taskStatus.Items.Clear();
        //    if (blankItem)
        //    {
        //        taskStatus.Items.Add(new ListItem("", ""));
        //    }
        //    taskStatus.Items.Add(new ListItem(GenericHelper.GetTaskStatusTextByValue(TaskStatus.NotStarted), TaskStatus.NotStarted.ToString()));
        //    taskStatus.Items.Add(new ListItem(GenericHelper.GetTaskStatusTextByValue(TaskStatus.InProgress), TaskStatus.InProgress.ToString()));
        //    taskStatus.Items.Add(new ListItem(GenericHelper.GetTaskStatusTextByValue(TaskStatus.Completed), TaskStatus.Completed.ToString()));
        //    taskStatus.Items.Add(new ListItem(GenericHelper.GetTaskStatusTextByValue(TaskStatus.Closed), TaskStatus.Closed.ToString()));
        //}

        public static void DDL_EmployeeYear(DropDownList year, bool blankItem)
        {
            year.Items.Clear();
            if (blankItem)
            {
                year.Items.Add(new ListItem("", ""));
            }

            int iYear = DateTime.Today.Year;
            for (int i = 0; i < 80; i++)
            {
                year.Items.Add(new ListItem(iYear.ToString(), iYear.ToString()));
                iYear--;
            }
        }

        public static void DDL_CreditCardYear(DropDownList year, bool blankItem)
        {
            year.Items.Clear();
            if (blankItem)
            {
                year.Items.Add(new ListItem("", ""));
            }

            int iYear = DateTime.Today.Year;
            for (int i = 0; i < 16; i++)
            {
                year.Items.Add(new ListItem(iYear.ToString(), iYear.ToString()));
                iYear++;
            }
        }

        public static void DDL_BillingType(DropDownList ddlBillingType, bool blankItem)
        {
            ddlBillingType.Items.Clear();
            if (blankItem)
            {
                ddlBillingType.Items.Add(new ListItem("", ""));
            }
            ddlBillingType.Items.Add(new ListItem(BillingType.Recurring.ToString(), BillingType.Recurring.ToString()));
            ddlBillingType.Items.Add(new ListItem(BillingType.SplashPack.ToString(), BillingType.SplashPack.ToString()));
            ddlBillingType.Items.Add(new ListItem(BillingType.Camp.ToString(), BillingType.Camp.ToString()));
        }

        public static void DDL_BillingCycle(DropDownList ddlBillingCycle, bool blankItem)
        {
            ddlBillingCycle.Items.Clear();
            if (blankItem)
            {
                ddlBillingCycle.Items.Add(new ListItem("", ""));
            }
            ddlBillingCycle.Items.Add(new ListItem(BillingCycle.Monthly.ToString(), BillingCycle.Monthly.ToString()));
            ddlBillingCycle.Items.Add(new ListItem(BillingCycle.OneTime.ToString(), BillingCycle.OneTime.ToString()));
        }

        public static void DDL_State(DropDownList ddlState, bool blankItem)
        {
            StateBll stateBll = new StateBll();
            List<tb_State> listState = stateBll.GetAll();

            ddlState.DataSource = listState;
            ddlState.DataTextField = "state_Name";
            ddlState.DataValueField = "state_Code";
            ddlState.DataBind();
            if (blankItem)
            {
                ddlState.Items.Insert(0, new ListItem("", ""));
            }
        }
    }
}
