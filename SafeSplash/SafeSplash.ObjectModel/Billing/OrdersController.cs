﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AuthorizeNet;
using AuthorizeNet.Helpers;
using SafeSplash.ObjectModel.Class;
using AKMII.SafeSplash.BLL;
using AKMII.SafeSplash.Entity;

namespace SafeSplash.ObjectModel.Billing
{
    public class OrdersController
    {
        string AuthorizeNet_ApiLogin = String.Empty;
        string AuthorizeNet_TransactionKey = String.Empty;
        public OrdersController(string apiLogin, string transactionKey)
        {
            AuthorizeNet_ApiLogin = apiLogin;
            AuthorizeNet_TransactionKey = transactionKey;
        }
        //pretend this is injected with IoC
        IGateway OpenGateway()
        {
            //we used the form builder so we can now just load it up
            //using the form reader
            var login = AuthorizeNet_ApiLogin;
            var transactionKey = AuthorizeNet_TransactionKey;

            //this is set to test mode - change as needed.
            var gate = new Gateway(login, transactionKey, true);
            return gate;
        }

        public void Destroy(tb_Order order)
        {
            //this is a return, or a Void
            //just need the transaction ID
            var gate = OpenGateway();

            //void it
            var request = new VoidRequest(order.order_TransactionID.ToString());
            var response = gate.Send(request);

            if (response.Approved)
            {
                order.order_AuthCode = response.AuthorizationCode;
                order.order_Message = "Your order was refunded - we've put a fresh pot on";
            }
            else
            {
                //error... oops. Reload the page
                order.order_Message = response.Message;
            }
        }

        public void Create(tb_Order order)
        {
            var gate = OpenGateway();

            //build the request from the Form post
            var apiRequest = CheckoutFormReaders.BuildAuthAndCaptureFromPost();

            //send to Auth.NET
            var response = gate.Send(apiRequest);

            //be sure the amount paid is the amount required
            if (response.Amount < order.order_Amount)
            {
                order.order_Message = "The amount paid for is less than the amount of the order. Something's fishy...";
                order.order_Status = "Failed";
            }

            if (response.Approved)
            {
                order.order_AuthCode = response.AuthorizationCode;
                order.order_TransactionID = response.TransactionID;                
                order.order_Message = string.Format("Thank you! Order approved: {0}", response.AuthorizationCode);
                order.order_Status = "Succeed";

            }
            else
            {
                order.order_AuthCode = response.AuthorizationCode;
                order.order_TransactionID = response.TransactionID;
                order.order_Message = response.Message;
                order.order_Status = "Failed";
            }
        }

        public bool SimResponse(tb_Order order, tb_ClientBilling billingInfo)
        {
            DateTime dtExpires = (DateTime)billingInfo.clientBill_Expires;
            //step 1 - create the request
            var request = new AuthorizationRequest(billingInfo.clientBill_CardNumber, dtExpires.Month.ToString("D2") + dtExpires.Year.ToString().Substring(2), (decimal)order.order_Amount, order.client_ID.ToString());

            //step 2 - create the gateway, sending in your credentials and setting the Mode to Test (boolean flag)
            //which is true by default
            //this login and key are the shared dev account - you should get your own if you 
            //want to do more testing
            var gate = OpenGateway();

            //step 3 - make some money
            var response = gate.Send(request);
            order.order_Created = DateTime.Now;
            if (response.Approved)
            {
                order.order_AuthCode = response.AuthorizationCode;
                order.order_TransactionID = response.TransactionID;
                order.order_Status = "Succeed";

                order.order_Message = string.Format("Thank you! Order approved: {0}", response.AuthorizationCode);
                return true;
            }
            else
            {
                order.order_AuthCode = response.AuthorizationCode;
                order.order_TransactionID = response.TransactionID;
                order.order_Status = "Failed";
                //pin the message to the order so we can show it to the user
                order.order_Message = response.Message;
                return false;
            }
        }
    }
}
