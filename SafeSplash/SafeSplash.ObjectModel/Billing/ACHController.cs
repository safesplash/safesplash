﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.BLL;
using AKMII.SafeSplash.Entity;
using System.Security.Cryptography;

using System.Web.Configuration;
using SafeSplash.ObjectModel.Class;
using SafeSplash.ObjectModel.Billing;

namespace SafeSplash.ObjectModel.Billing
{
    public class ACHController
    {
        private const string COUNTRYCODE_US = "01";
        private const string PG_CC_SALE = "10";
        private const string PG_NA = "";
        private const string PG_INITIALS = "SSPG";
        private const string PG_MONTHLY = "20";
        private const string PG_SCHEDULE = "12";

        private string _listid;
        private string _itemid;
        private string _clientid;
        private string _paymentid;

        private string _merchantid;
        private string _apiid;
        private string _transactionid;
        private string _password;
        private string _testcard;

        public ACHController()
        {
            getConfiguration();
        }

        private void getConfiguration()
        {
            _merchantid = WebConfigurationManager.AppSettings[Constant.AppSetting_ACH_MerchantId];
            _apiid = WebConfigurationManager.AppSettings[Constant.AppSetting_ACH_ApiId];
            _transactionid = WebConfigurationManager.AppSettings[Constant.AppSetting_ACH_TransactionKey];
            _password = WebConfigurationManager.AppSettings[Constant.AppSetting_ACH_ProcessingPassword];
            _testcard = WebConfigurationManager.AppSettings[Constant.AppSetting_ACH_TestCardNumber];

            if (string.IsNullOrEmpty(_merchantid) ||
                string.IsNullOrEmpty(_apiid) ||
                string.IsNullOrEmpty(_transactionid) ||
                string.IsNullOrEmpty(_password))
                throw new Exception("Appilication setting not configured (" + Constant.AppSetting_ACH_MerchantId + "='" + _merchantid +
                                    "', " + Constant.AppSetting_ACH_ApiId + "='" + _apiid + "', " + Constant.AppSetting_ACH_TransactionKey + "='" + _transactionid +
                                    "', " + Constant.AppSetting_ACH_ProcessingPassword + "='" + _password + ").");

            _merchantid = _merchantid.Trim();
            _apiid = _apiid.Trim();
            _transactionid = _transactionid.Trim();
            _password = _password.Trim();

            if (!string.IsNullOrEmpty(_testcard)) _testcard = _testcard.Trim();
        }

//        public void GetClientInfo(string apiLoginId, string secTranKey, tb_Order order, tb_ClientBilling billingInfo, bool testModel)
//        {
//            if (testModel)
//            {
//                myremotepost.Url = "https://sandbox.paymentsgateway.net/swp/default.aspx";
//            }
//            else
//            {

//                myremotepost.Url = "https://swp.paymentsgateway.net/default.aspx";
//            }
//            myremotepost.Add("pg_api_login_id", apiLoginId);

//            int RandomNumber;
//            Random RandomClass = new Random();
//            RandomNumber = RandomClass.Next(1000, 1000000);
//            myremotepost.Add("pg_transaction_order_number", RandomNumber.ToString());
//            order.order_TransactionID = RandomNumber.ToString();
//            string hash;
//            hash = generateHash(secTranKey, apiLoginId, transaction_type, version_Number, order.order_Amount.ToString(), utc_time.ToString(), order.order_TransactionID);
//            myremotepost.Add("pg_ts_hash", hash);

//            myremotepost.Add("pg_transaction_type", transaction_type);
//            myremotepost.Add("pg_version_number", version_Number);
//            myremotepost.Add("pg_total_amount", order.order_Amount.ToString());

//            myremotepost.Add("pg_payment_card_number", "4111111111111111");
//            myremotepost.Add("pg_payment_card_expdate_year", "2015");
//            myremotepost.Add("pg_payment_card_expdate_month", "02");

//            myremotepost.Add("pg_payment_card_type", "visa");
//            myremotepost.Add("pg_payment_card_name", "testAccount");

//            order.order_Message = myremotepost.Post();
/////            return myremotepost.Post();
//        }

//        private string generateHash(string secTranKey, string apiLoginID, string transactionType, string versionNumber, string totalAmount, string utcTime, string transactionOrderNumber)
//        {
//            //Build Send String
//            string send;
//            send = apiLoginID + "|" + transactionType + "|" + versionNumber + "|" + totalAmount + "|" + utcTime + "|" + transactionOrderNumber;

//            return CalculateHMACMD5(send, secTranKey);

//        }

//        public string CalculateHMACMD5(string input, string strSecureTransKey)
//        {
//            StringBuilder sb = new StringBuilder();
//            byte[] data = Encoding.Default.GetBytes(input);
//            byte[] secretkey = new byte[64];
//            secretkey = Encoding.Default.GetBytes(strSecureTransKey);
//            HMACMD5 hmacMD5 = new HMACMD5(secretkey);
//            byte[] macSender = hmacMD5.ComputeHash(data);

//            for (int i = 0; i < macSender.Length; i++)
//            {
//                sb.Append(macSender[i].ToString("x2"));
//            }

//            hmacMD5.Clear();
//            return sb.ToString();
//        }
//    }

//    public class RemotePost
//    {
//        private System.Collections.Specialized.NameValueCollection Inputs = new System.Collections.Specialized.NameValueCollection();

//        public string Url = "";
//        public string Method = "post";
//        public string FormName = "form1";

//        public void Add(string name, string value)
//        {
//            Inputs.Add(name, value);
//        }

//        public string Post()
//        {
//            System.Web.HttpContext.Current.Response.Clear();

//            System.Web.HttpContext.Current.Response.Write("<html><head>");

//            System.Web.HttpContext.Current.Response.Write(string.Format("</head><body onload=\"document.{0}.submit()\">", FormName));
//            System.Web.HttpContext.Current.Response.Write(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", FormName, Method, Url));
//            for (int i = 0; i < Inputs.Keys.Count; i++)
//            {
//                System.Web.HttpContext.Current.Response.Write(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", Inputs.Keys[i], Inputs[Inputs.Keys[i]]));
//            }
//            System.Web.HttpContext.Current.Response.Write("</form>");
//            System.Web.HttpContext.Current.Response.Write("</body></html>");

//            System.Web.HttpContext.Current.Response.End();

//            //var result = "";
//            //StringBuilder postData = new StringBuilder();
//            //postData.Append("<html><head>");
//            //postData.Append(string.Format("</head><body onload=\"document.{0}.submit()\">", FormName));
//            //postData.Append(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", FormName, Method, Url));
//            //for (int i = 0; i < Inputs.Keys.Count; i++)
//            //{
//            //    postData.Append(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", Inputs.Keys[i], Inputs[Inputs.Keys[i]]));
//            //}
//            //postData.Append("</form>");
//            //postData.Append("</body></html>");

//            ////override the local cert policy
//            //ServicePointManager.CertificatePolicy = new PolicyOverride();

//            //var webRequest = (HttpWebRequest)WebRequest.Create(this.Url);
//            //webRequest.Method = "POST";
//            //webRequest.ContentLength = postData.Length;
//            //webRequest.ContentType = "application/x-www-form-urlencoded";

//            //// post data is sent as a stream
//            //StreamWriter myWriter = null;
//            //myWriter = new StreamWriter(webRequest.GetRequestStream());
//            //myWriter.Write(postData.ToString());
//            //myWriter.Close();

//            //// returned values are returned as a stream, then read into a string
//            //var response = (HttpWebResponse)webRequest.GetResponse();
//            //using (StreamReader responseStream = new StreamReader(response.GetResponseStream()))
//            //{
//            //    result = responseStream.ReadToEnd();
//            //    responseStream.Close();
//            //}

//            // the response string is broken into an array
//            // The split character specified here must match the delimiting character specified above
//            //var response_array = result.Split('|');
//            //return DecideResponse(response_array);
//            return "";
//        }
    }

    class PolicyOverride : ICertificatePolicy
    {

        bool ICertificatePolicy.CheckValidationResult(ServicePoint srvPoint, System.Security.Cryptography.X509Certificates.X509Certificate cert, WebRequest request, int certificateProblem)
        {
            return true;
        }
    }
}
