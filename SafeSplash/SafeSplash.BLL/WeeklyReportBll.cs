﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class WeeklyReportBll
    {
        protected SafeSplashDBDataContext ctx = null;

        public WeeklyReportBll()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public WeeklyReportBll(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }

        public string GetEnrollmentsList()
        {
            string res = "";
            DateTime dt = new DateTime(2011, 10, 20);

            LocationBll lbll = new LocationBll();
            Guid id = lbll.GetByName("Lancaster, CA").loca_ID;
           List<tb_StudentBooks> studentBooks = (from client in ctx.tb_Clients
                            join cs in ctx.tb_ClientStudent
                            on client.client_ID equals cs.client_ID
                            join c in ctx.tb_StudentBooks
                            on cs.stud_ID equals c.stud_ID
                            join cl in ctx.tb_ActiveClass
                            on c.activecs_ID equals cl.activecs_ID
                            where cl.loca_ID.Value == id && ((DateTime)c.studch_JoinData).CompareTo(dt) <= 0 && (c.studch_IsWithdrawal == null || c.studch_IsWithdrawal == false || (c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(dt) > 0))
                            select c).Distinct().ToList<tb_StudentBooks>();
           if (studentBooks != null)
           {
               foreach (tb_StudentBooks bok in studentBooks)
               {
                   ClassBll clbll = new ClassBll();
                   tb_Class cls = clbll.GetByActiveClassID(bok.activecs_ID.Value);
                   if (cls.class_IsPrivate.Value)
                   {
                       res += "Class Type:Private" + "     |     ";
                   }
                   else if (cls.class_IsSemiPrivate.Value)
                   {
                       res += "Class Type:SemiPrivate" + "     |     ";
                   }
                   else
                   {
                       res += "Class Type:" + cls.class_Name + "     |     ";
                   }
                   List<tb_ClientStudent> stus = (from cs in ctx.tb_ClientStudent
                                                  where cs.stud_ID == bok.stud_ID.Value
                                                  select cs).Distinct().ToList<tb_ClientStudent>();
                   res += "Student Name:" + stus[0].stud_FirstName + "  " + stus[0].stud_LastName + "     |     ";
                   List<tb_Employee> ems = (from em in ctx.tb_Employees
                                            join ac in ctx.tb_ActiveClass
                                            on em.empl_ID equals ac.activecs_Teacher.Value
                                            where ac.activecs_ID == bok.activecs_ID.Value
                                            select em).Distinct().ToList<tb_Employee>();
                   res += "Teacher:" + ems[0].empl_FirstName + "  " + ems[0].empl_LastName + "     |     ";

                   ActiveClassDaysBll daybll = new ActiveClassDaysBll();
                   List<tb_ActiveClassDays> days = daybll.GetByActiveClassID(bok.activecs_ID.Value);
                   res += "Day of week:" + days[0].activeDay_Day + " Start time:" + days[0].activeDay_StartTime + "     |     ";
                   for (int i = 0; i < 7; i++)
                   {
                       if (dt.AddDays(0 - i).DayOfWeek.ToString().ToUpper() == days[0].activeDay_Day.ToUpper())
                       {
                           res += "Date:" + dt.AddDays(0 - i).ToShortDateString() + "</br>";
                       }
                   }

               }
           }
            return res;
        }

        public List<tb_WeeklyEnrollReport> GetWeeklyEnrollmentReport(Guid loca_ID, DateTime dtFrom, DateTime dtTo, string strDayOfWeek)
        {
            List<tb_WeeklyEnrollReport> weeklyReportList = new List<tb_WeeklyEnrollReport>();
            List<tb_Location> locationsList = new List<tb_Location>();
            LocationBll locationBll = new LocationBll();
            tb_Location location = locationBll.GetByID(loca_ID);
            locationsList.Add(location);
            if (dtTo.CompareTo(dtFrom) >= 0)
            {
                
                DateTime dtCurrentDate = dtFrom;
                while (dtCurrentDate.DayOfWeek.ToString() != strDayOfWeek)
                {
                    dtCurrentDate = dtCurrentDate.AddDays(1);
                }
                while (dtCurrentDate.CompareTo(dtTo) <= 0)
                {
                    tb_WeeklyEnrollReport weeklyReport = GetWeeklyEnrollReportByLoc(locationsList, dtCurrentDate);
                    if (weeklyReport != null)
                    {
                        weeklyReportList.Add(weeklyReport);
                    }
                    dtCurrentDate = dtCurrentDate.AddDays(7);
                }
            }
            return weeklyReportList;
            //List<tb_WeeklyEnrollReport> weeklyReportList = new List<tb_WeeklyEnrollReport>();

            //weeklyReportList = (from c in ctx.tb_WeeklyEnrollReports
            //                    join cl in ctx.tb_Locations
            //                    on c.loca_ID equals cl.loca_ID
            //                    where c.wer_Date.CompareTo(dtFrom) >= 0 && c.wer_Date.CompareTo(dtTo) <= 0 && c.loca_ID == loca_ID
            //                    orderby c.wer_Date
            //                    select c).Distinct().ToList<tb_WeeklyEnrollReport>();
            //return weeklyReportList;
        }
        public int totalSpots(IEnumerable<tb_StudentBooks> books)
        {
            int iClassSportsFilled = 0;
            if (books != null)
            {
                foreach (tb_StudentBooks stbk in books)
                {
                    List<tb_Class> clas = (from acs in ctx.tb_ActiveClass
                                           join css in ctx.tb_Class
                                           on acs.class_ID equals css.class_ID
                                           where acs.activecs_ID == stbk.activecs_ID.Value
                                           select css).Distinct().ToList<tb_Class>();
                    if (clas != null && clas.Count > 0)
                    {
                        tb_Class cls = clas[0];
                        if (cls.class_IsPrivate.Value)
                        {
                            iClassSportsFilled += 4;
                        }
                        else if (cls.class_IsSemiPrivate.Value)
                        {
                            iClassSportsFilled += 2;
                        }
                        else
                        {
                            iClassSportsFilled++;
                        }
                    }
                }
            }
            return iClassSportsFilled;
        }
        public tb_WeeklyEnrollReport GetWeeklyEnrollReportByLoc(List<tb_Location> locationsList, DateTime currentDate)
        {
            DateTime dtFirstDayofWeek = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day).AddDays(-6);
            currentDate = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day).AddDays(1).AddSeconds(-1);
            

            foreach (tb_Location location in locationsList)
            {

                // Class Spots Filled
                List<tb_StudentBooks> studentBooks = new List<tb_StudentBooks>();

                //studentBooks = (from c in ctx.tb_StudentBooks
                //                join cl in ctx.tb_ActiveClass
                //                on c.activecs_ID equals cl.activecs_ID
                //                where cl.loca_ID == location.loca_ID && ((DateTime)c.studch_JoinData).CompareTo(currentDate) <= 0 && (c.studch_IsWithdrawal == null || c.studch_IsWithdrawal == false || (c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) > 0))
                //                select c).Distinct().ToList<tb_StudentBooks>();
                studentBooks = (from client in ctx.tb_Clients 
                                join cs in ctx.tb_ClientStudent
                                on client.client_ID equals cs.client_ID
                                join c in ctx.tb_StudentBooks
                                on cs.stud_ID equals c.stud_ID
                                join cl in ctx.tb_ActiveClass
                                on c.activecs_ID equals cl.activecs_ID
                                where cl.loca_ID.Value == location.loca_ID
                                && (!cl.activecs_EndDay.HasValue || cl.activecs_EndDay.Value.Date >= dtFirstDayofWeek.Date)
                                && (c.studch_JoinData.Value.Date).CompareTo(currentDate.Date) <= 0
                                && (!c.studch_IsWithdrawal.HasValue || !c.studch_IsWithdrawal.Value || c.studch_WidthdrawalData.Value.Date.CompareTo(dtFirstDayofWeek.Date) >= 0)
                                select c).Distinct().ToList<tb_StudentBooks>();

                int iClassSportsFilled = totalSpots(studentBooks);
                
               

                // % of Capacity
                List<tb_ActiveClass> activeClassList = new List<tb_ActiveClass>();

                activeClassList = (from c in ctx.tb_ActiveClass
                                   where c.loca_ID.Value == location.loca_ID 
                                   && c.activecs_StartDay.HasValue 
                                   && c.activecs_StartDay.Value.CompareTo(currentDate) <= 0 
                                   && (!c.activecs_EndDay.HasValue ||  c.activecs_EndDay.Value.CompareTo(dtFirstDayofWeek) >= 0)
                                   select c).Distinct().ToList<tb_ActiveClass>();
                int iCapacity = 0;
                if (activeClassList != null)
                {
                    foreach (tb_ActiveClass activeClass in activeClassList)
                    {

                        tb_Class classType = ctx.tb_Class.SingleOrDefault<tb_Class>(d => d.class_ID == (Guid)activeClass.class_ID);

                        if (classType != null && classType.class_StudentCapacity != null)
                        {
                            if (classType.class_IsPrivate.Value)
                            {
                                iCapacity += (classType.class_StudentCapacity.Value * 4);
                            }
                            else if (classType.class_IsSemiPrivate.Value)
                            {
                                iCapacity += (classType.class_StudentCapacity.Value * 2);
                            }
                            else
                            {
                                iCapacity += classType.class_StudentCapacity.Value;
                            }
                        }
                    }
                }
                decimal dPrecentCapacity = 0;
                if (iCapacity != 0) dPrecentCapacity = ((decimal)iCapacity - (decimal)iClassSportsFilled) / (decimal)iCapacity;

                // # of RIP
                List<tb_ClientStudent> RIPClientsList = new List<tb_ClientStudent>();

                RIPClientsList = (from c in ctx.tb_Clients
                                  join student in ctx.tb_ClientStudent  on c.client_ID equals student.client_ID
                                  join loca in ctx.tb_ClientLocation on c.client_ID equals loca.client_ID
                                  where 
                                  loca.loca_ID.Value == location.loca_ID 
                                  && c.client_CreatedDate.HasValue
                                  && c.client_CreatedDate.Value.Date <= currentDate.Date 
                                  && (!c.client_IsCompleteRegister.HasValue || !c.client_IsCompleteRegister.Value)
                                  select student).Distinct().ToList<tb_ClientStudent>();

                int iRIP = 0;
                if (RIPClientsList != null)
                {
                    iRIP = RIPClientsList.Count;
                }

                // Spots Withdraw
                List<tb_StudentBooks> SpotsWithdrawList = new List<tb_StudentBooks>();

                SpotsWithdrawList = (from c in ctx.tb_StudentBooks
                                     join cl in ctx.tb_ActiveClass
                                     on c.activecs_ID equals cl.activecs_ID
                                     where cl.loca_ID == location.loca_ID && c.studch_IsWithdrawal != null && c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) <= 0 && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofWeek) >= 0
                                     select c).Distinct().ToList<tb_StudentBooks>();
                int iSpotsWithdraw = SpotsWithdrawList.Count;

                // % of Spots Withdrawn
                decimal dPrecentSportsWithdrawn = iClassSportsFilled == 0 ? 0 : (decimal)iSpotsWithdraw / ((decimal)iClassSportsFilled);

                tb_WeeklyEnrollReport weeklyEnrollReport = new tb_WeeklyEnrollReport();
                weeklyEnrollReport.wer_ID = Guid.NewGuid();
                weeklyEnrollReport.wer_Date = currentDate;
                weeklyEnrollReport.loca_ID = location.loca_ID;
                weeklyEnrollReport.wer_PercentCapacity = dPrecentCapacity;
                weeklyEnrollReport.wer_PercentSpotsWithdrawn = dPrecentSportsWithdrawn;
                weeklyEnrollReport.wer_SpotsFilled = iClassSportsFilled;
                weeklyEnrollReport.wer_SpotsWithdrawn = iSpotsWithdraw;
                weeklyEnrollReport.wer_StudentsWithRIP = iRIP;
                return weeklyEnrollReport;
            }
            return null;
        }

        public tb_WeeklyEnrollReport GetWeeklyEnrollReport(DateTime currentDate)
        {
            List<tb_Location> locationsList = new List<tb_Location>();
            locationsList = ctx.GetTable<tb_Location>().ToList<tb_Location>();
            return GetWeeklyEnrollReportByLoc(locationsList, currentDate);
        }

        public void CreateWeeklyEnrollReport(DateTime currentDate)
        {
            tb_WeeklyEnrollReport weeklyEnrollReport = GetWeeklyEnrollReport(currentDate);
            ctx.tb_WeeklyEnrollReports.InsertOnSubmit(weeklyEnrollReport);
            ctx.SubmitChanges();
        }
    }
}
