﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class ClientClassLevelBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_ClientClassLevel info)
        {
            ctx.tb_ClientClassLevels.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_ClientClassLevel info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_ClientClassLevel configInfo = ctx.tb_ClientClassLevels.SingleOrDefault<tb_ClientClassLevel>(d => d.level_ID == id);
            if (configInfo != null)
            {
                ctx.tb_ClientClassLevels.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_ClientClassLevel> GetAll()
        {
            List<tb_ClientClassLevel> list = new List<tb_ClientClassLevel>();

            list = (from c in ctx.tb_ClientClassLevels
                    orderby c.level_Name
                    select c).ToList<tb_ClientClassLevel>();
            return list;
        }

        public tb_ClientClassLevel GetByID(Guid id)
        {
            tb_ClientClassLevel dataInfo = ctx.tb_ClientClassLevels.SingleOrDefault<tb_ClientClassLevel>(d => d.level_ID == id);
            return dataInfo;
        }

        public tb_ClientClassLevel GetByName(string levelName)
        {
            tb_ClientClassLevel dataInfo = ctx.tb_ClientClassLevels.SingleOrDefault<tb_ClientClassLevel>(d => d.level_Name == levelName);
            return dataInfo;
        }
    }
}
