﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class ClientRelationshipBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_ClientRelationship info)
        {
            ctx.tb_ClientRelationship.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_ClientRelationship info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_ClientRelationship configInfo = ctx.tb_ClientRelationship.SingleOrDefault<tb_ClientRelationship>(d => d.clientrs_ID == id);
            if (configInfo != null)
            {
                ctx.tb_ClientRelationship.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_ClientRelationship> GetAll()
        {
            List<tb_ClientRelationship> list = new List<tb_ClientRelationship>();

            list = ctx.GetTable<tb_ClientRelationship>().ToList<tb_ClientRelationship>();
            return list;
        }

        public tb_ClientRelationship GetByID(Guid id)
        {
            tb_ClientRelationship dataInfo = ctx.tb_ClientRelationship.SingleOrDefault<tb_ClientRelationship>(d => d.clientrs_ID == id);
            return dataInfo;
        }
    }
}
