﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class ClassLevelBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_ClassLevel info)
        {
            ctx.tb_ClassLevel.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_ClassLevel info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_ClassLevel configInfo = ctx.tb_ClassLevel.SingleOrDefault<tb_ClassLevel>(d => d.level_ID == id);
            if (configInfo != null)
            {
                ctx.tb_ClassLevel.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_ClassLevel> GetAll()
        {
            List<tb_ClassLevel> list = new List<tb_ClassLevel>();

            list = (from c in ctx.tb_ClassLevel
                    orderby c.level_Name
                    select c).ToList<tb_ClassLevel>();
            return list;
        }

        public tb_ClassLevel GetByID(Guid id)
        {
            tb_ClassLevel dataInfo = ctx.tb_ClassLevel.SingleOrDefault<tb_ClassLevel>(d => d.level_ID == id);
            return dataInfo;
        }

        public tb_ClassLevel GetByName(string levelName)
        {
            tb_ClassLevel dataInfo = ctx.tb_ClassLevel.SingleOrDefault<tb_ClassLevel>(d => d.level_Name == levelName);
            return dataInfo;
        }
    }
}
