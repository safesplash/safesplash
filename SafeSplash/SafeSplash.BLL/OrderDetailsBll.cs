﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Linq;
using AKMII.SafeSplash.Entity;

namespace AKMII.SafeSplash.BLL
{
    public class OrderDetailsBll
    {
        protected SafeSplashDBDataContext ctx = null;

        public OrderDetailsBll()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public OrderDetailsBll(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }

        public void Add(tb_OrderDetails info)
        {
            ctx.tb_OrderDetails.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Add(List<tb_OrderDetails> infos)
        {
            ctx.tb_OrderDetails.InsertAllOnSubmit<tb_OrderDetails>(infos);
            ctx.SubmitChanges();
        }

        public List<tb_OrderDetails> GetAll()
        {
            List<tb_OrderDetails> list = new List<tb_OrderDetails>();

            list = ctx.GetTable<tb_OrderDetails>().ToList<tb_OrderDetails>();
            return list;
        }

        public List<tb_OrderDetails> GetByStudent(Guid studID)
        {
            List<tb_OrderDetails> list = new List<tb_OrderDetails>();
            list = (from c in ctx.tb_OrderDetails
                    where c.stud_ID == studID
                    select c).ToList<tb_OrderDetails>();
            return list;
        }

        public void Delete(List<tb_OrderDetails> infos)
        {
            if (infos != null)
            {
                ctx.tb_OrderDetails.DeleteAllOnSubmit(infos);
                ctx.SubmitChanges();
            }
        }

        public List<tb_OrderDetails> GetCurrentMonthOrderDetail(Guid studID, Guid activecs_ID, int year, int month)
        {
            List<tb_OrderDetails> list = new List<tb_OrderDetails>();
            list = (from c in ctx.tb_OrderDetails
                    where c.stud_ID == studID && c.activecs_ID == activecs_ID && c.orderdetails_Year == year && c.orderdetails_Month == month
                    select c).ToList<tb_OrderDetails>();
            return list;
        }

        public List<tb_OrderDetails> GetOneTimehOrderDetail(Guid studID, Guid activecs_ID)
        {
            List<tb_OrderDetails> list = new List<tb_OrderDetails>();
            list = (from c in ctx.tb_OrderDetails
                    where c.stud_ID == studID && c.activecs_ID == activecs_ID
                    select c).ToList<tb_OrderDetails>();
            return list;
        }
    }
}
