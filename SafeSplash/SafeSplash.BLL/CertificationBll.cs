﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class CertificationBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_Certification info)
        {
            ctx.tb_Certification.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_Certification info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_Certification configInfo = ctx.tb_Certification.SingleOrDefault<tb_Certification>(d => d.cert_ID == id);
            if (configInfo != null)
            {
                ctx.tb_Certification.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_Certification> GetAll()
        {
            List<tb_Certification> list = new List<tb_Certification>();

            list = ctx.GetTable<tb_Certification>().ToList<tb_Certification>();
            return list;
        }

        public tb_Certification GetByID(Guid id)
        {
            tb_Certification dataInfo = ctx.tb_Certification.SingleOrDefault<tb_Certification>(d => d.cert_ID == id);
            return dataInfo;
        }

        public bool Existing(string cert_Name)
        {
            tb_Certification dataInfo = ctx.tb_Certification.SingleOrDefault<tb_Certification>(d => d.cert_Name == cert_Name);
            return (!(dataInfo==null));
        }
    }
}
