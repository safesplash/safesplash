﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class StudentBookBll
    {
        protected SafeSplashDBDataContext ctx = null;

        public StudentBookBll()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public StudentBookBll(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }

        public void Add(tb_StudentBooks info)
        {
            ctx.tb_StudentBooks.InsertOnSubmit(info);
            ctx.SubmitChanges();            
        }

        //public void Add(tb_StudentBooks info, Guid employeeId, string employeeFirstName, string employeeLastName, string action, DateTime actionDate, string comment)
        //{
        //    Add(info);
        //    try
        //    {
        //        // write history record
        //        IStudentBookHistoryDataAccess da = new StudentBookHistoryDataAccess();
        //        IStudentBookHistoryDataObject dataObject = new StudentBookHistoryDataObject()
        //        {
        //            StudentChId = info.studch_ID,
        //            ActiveClassId = info.activecs_ID.Value,
        //            StudentId = info.stud_ID.Value,
        //            StudentJoinDate = info.studch_JoinData.Value,
        //            IsWithdrawal = info.studch_IsWithdrawal,
        //            WithdrawalDate = info.studch_WidthdrawalData,
        //            ReasonId = info.reason_ID,
        //            WithdrawalNote = info.studch_WithdrawalNote,
        //            EmployeeId = employeeId,
        //            EmployeeFirstName = employeeFirstName,
        //            EmployeeLastName = employeeLastName,
        //            ActionDescription = action,
        //            ActionDate = actionDate,
        //            Comment = comment
        //        };
        //        da.Save(dataObject);
        //    }
        //    catch (Exception)
        //    {
        //        // will log this when we add logging
        //    }
        //}

        public List<tb_ActiveClass> GetStudentBooks(Guid studentID)
        {
            List<tb_ActiveClass> clientBooks = new List<tb_ActiveClass>();

            clientBooks = (from c in ctx.tb_ActiveClass
                       join cl in ctx.tb_StudentBooks
                           on c.activecs_ID equals cl.activecs_ID
                           where cl.stud_ID == studentID
                           select c).ToList<tb_ActiveClass>();
            return clientBooks;
        }

        public List<tb_ClientStudent> GetStudentsByBooks(Guid activeClassID)
        {
            return GetStudentsByBooks(DateTime.Today, activeClassID);
        }

        public List<tb_ClientStudent> GetStudentsByBooksAndWithdrawalDate(Guid activeClassID, DateTime withdrawalDate)
        {
            return GetStudentsByBooks(withdrawalDate, activeClassID);
        }

        public List<tb_ClientStudent> GetStudentsByBooks(DateTime dtCurrentDate, Guid activeClassID)
        {
            List<tb_ClientStudent> clientBooks = new List<tb_ClientStudent>();

            clientBooks = (from c in ctx.tb_ClientStudent
                           join cl in ctx.tb_StudentBooks
                           on c.stud_ID equals cl.stud_ID
                           where cl.activecs_ID == activeClassID && ((DateTime)cl.studch_JoinData).CompareTo(dtCurrentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(dtCurrentDate) >= 0))
                           select c).ToList<tb_ClientStudent>();
            return clientBooks;
        }

        public List<tb_StudentBooks> GetStudentsByBooks(Guid activeClassID, DateTime dtCurrentDate)
        {
            List<tb_StudentBooks> clientBooks = new List<tb_StudentBooks>();

            clientBooks = (from c in ctx.tb_ClientStudent
                           join cl in ctx.tb_StudentBooks
                           on c.stud_ID equals cl.stud_ID
                           where cl.activecs_ID == activeClassID && ((DateTime)cl.studch_JoinData).CompareTo(dtCurrentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(dtCurrentDate) >= 0))
                           select cl).ToList<tb_StudentBooks>();
            return clientBooks;
        }

        public List<tb_StudentBooks> GetClassBooks(Guid activeClassID)
        {
            List<tb_StudentBooks> clientBooks = new List<tb_StudentBooks>();

            clientBooks = (from c in ctx.tb_StudentBooks
                           where c.activecs_ID == activeClassID && (c.studch_IsWithdrawal == null || c.studch_IsWithdrawal == false || (c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(DateTime.Today) >= 0))
                           select c).ToList<tb_StudentBooks>();
            return clientBooks;
        }

        public List<tb_StudentBooks> GetClassBooksForOverbook(Guid activeClassID)
        {
            List<tb_StudentBooks> clientBooks = new List<tb_StudentBooks>();

            clientBooks = (from c in ctx.tb_StudentBooks
                           where c.activecs_ID == activeClassID && c.studch_JoinData.Value.CompareTo(DateTime.Today.AddDays(28))<=0 && (!c.studch_IsWithdrawal.HasValue || !c.studch_IsWithdrawal.Value || ((DateTime)c.studch_WidthdrawalData).CompareTo(DateTime.Today) >= 0)
                           select c).ToList<tb_StudentBooks>();
            return clientBooks;
        }

        public string GetAvAge(Guid aciveClassID)
        {
            List<tb_ClientStudent> clientBooks = GetStudentsByBooks(aciveClassID);
            int iNumber = clientBooks.Count;
            double totalAge = 0;
            foreach (tb_ClientStudent student in clientBooks)
            {
                if (student.stud_DOB != null)
                {
                    double age = DateTime.Now.Subtract((DateTime)student.stud_DOB).TotalDays / 365;
                    totalAge += age;
                }
            }
            return iNumber == 0 ? "0" : String.Format("{0:0.#}", totalAge / iNumber);
        }

        public void Update(tb_StudentBooks info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_StudentBooks configInfo = ctx.tb_StudentBooks.SingleOrDefault<tb_StudentBooks>(d => d.studch_ID == id);
            if (configInfo != null)
            {
                ctx.tb_StudentBooks.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public void Delete(List<tb_StudentBooks> infos)
        {
            if (infos != null)
            {
                ctx.tb_StudentBooks.DeleteAllOnSubmit(infos);
                ctx.SubmitChanges();
            }
        }

        public List<tb_StudentBooks> GetAll()
        {
            List<tb_StudentBooks> list = new List<tb_StudentBooks>();

            list = ctx.GetTable<tb_StudentBooks>().ToList<tb_StudentBooks>();
            return list;
        }

        public tb_StudentBooks GetByID(Guid id)
        {
            tb_StudentBooks dataInfo = ctx.tb_StudentBooks.SingleOrDefault<tb_StudentBooks>(d => d.studch_ID == id);
            return dataInfo;
        }

        public bool IsBooked(Guid stud_ID, Guid activeClass_ID)
        {
            tb_StudentBooks dataInfo = ctx.tb_StudentBooks.SingleOrDefault<tb_StudentBooks>(d => d.stud_ID == stud_ID && d.activecs_ID == activeClass_ID);
            return dataInfo != null;
        }

        public List<tb_StudentBooks> GetByStudentAClass(Guid studentID, Guid activeClassID)
        {
            List<tb_StudentBooks> list = new List<tb_StudentBooks>();
            list = (from c in ctx.tb_StudentBooks
                    where c.stud_ID == studentID && c.activecs_ID == activeClassID
                    select c).ToList<tb_StudentBooks>();
            return list;
        }

        public bool IsClassBooked(Guid studentID, Guid activeClassID, DateTime dtSelectedDate)
        {
            List<tb_StudentBooks> list = new List<tb_StudentBooks>();
            list = (from c in ctx.tb_StudentBooks
                    where c.stud_ID == studentID && c.activecs_ID == activeClassID && (((DateTime)c.studch_JoinData).CompareTo(dtSelectedDate) <= 0) && (c.studch_IsWithdrawal == null || (!(bool)c.studch_IsWithdrawal) || ((bool)c.studch_IsWithdrawal && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtSelectedDate) >= 0))
                    select c).ToList<tb_StudentBooks>();
            if (list == null || list.Count == 0)
                return false;
            else
                return true;
        }

        public List<tb_StudentBooks> GetByStudentID(Guid studID)
        {
            List<tb_StudentBooks> list = new List<tb_StudentBooks>();
            list = (from c in ctx.tb_StudentBooks
                    where c.stud_ID == studID
                    select c).ToList<tb_StudentBooks>();
            return list;
        }

        public List<tb_StudentBooks> GetAllWithdrawaledRequest()
        {
            List<tb_StudentBooks> list = new List<tb_StudentBooks>();
            list = (from c in ctx.tb_StudentBooks
                    where c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(DateTime.Today) < 0
                    orderby c.studch_WidthdrawalData descending
                    select c).ToList<tb_StudentBooks>();
            return list;
        }

        public List<tb_StudentBooks> GetAllWithdrawaledRequest(Guid[] locationIDs)
        {
            List<tb_StudentBooks> list = new List<tb_StudentBooks>();
            list = (from c in ctx.tb_StudentBooks
                    join cl in ctx.tb_ActiveClass
                    on c.activecs_ID equals cl.activecs_ID
                    where c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(DateTime.Today) < 0 && locationIDs.Contains((Guid)cl.loca_ID)
                    orderby c.studch_WidthdrawalData descending
                    select c).ToList<tb_StudentBooks>();
            return list;
        }

        public List<tb_ActiveClass> GetCurrentBookedClass(Guid StudID)
        {
            List<tb_ActiveClass> list = new List<tb_ActiveClass>();
            list = (from cl in ctx.tb_ActiveClass
                    join ct in ctx.tb_StudentBooks
                    on cl.activecs_ID equals ct.activecs_ID
                    where ct.stud_ID==StudID && (ct.studch_IsWithdrawal == null || !(bool)ct.studch_IsWithdrawal || ((bool)ct.studch_IsWithdrawal && ((DateTime)ct.studch_WidthdrawalData).CompareTo(DateTime.Today) >= 0))
                       && (cl.activecs_EndDay == null || (DateTime)cl.activecs_EndDay >= DateTime.Today)
                    select cl).Distinct().ToList<tb_ActiveClass>();
            return list;
        }

        public List<tb_StudentBooks> GetStudentCurrentPast(Guid studentID)
        {
            List<tb_StudentBooks> list = new List<tb_StudentBooks>();
            list = (from c in ctx.tb_ActiveClass
                    join cl in ctx.tb_StudentBooks
                    on c.activecs_ID equals cl.activecs_ID
                    where cl.stud_ID == studentID && (!cl.studch_IsWithdrawal.HasValue || !cl.studch_IsWithdrawal.Value || ((cl.studch_WidthdrawalData.Value).CompareTo(DateTime.Today) >= 0))
                    select cl).Distinct().ToList<tb_StudentBooks>();
            return list;
        }

        public List<tb_StudentBooks> GetStudentWithdrawaled(Guid studentID)
        {
            List<tb_StudentBooks> list = new List<tb_StudentBooks>();
            list = (from c in ctx.tb_ActiveClass
                    join cl in ctx.tb_StudentBooks
                    on c.activecs_ID equals cl.activecs_ID
                    where cl.stud_ID == studentID && (cl.studch_IsWithdrawal.HasValue && cl.studch_IsWithdrawal.Value && cl.studch_WidthdrawalData.Value.CompareTo(DateTime.Today) < 0)
                    select cl).Distinct().ToList<tb_StudentBooks>();
            return list;
        }

        public List<tb_ActiveClass> GetAllBooks(Guid studentID)
        {
            List<tb_ActiveClass> list = new List<tb_ActiveClass>();
            list = (from c in ctx.tb_ActiveClass
                    join cl in ctx.tb_StudentBooks
                    on c.activecs_ID equals cl.activecs_ID
                    where cl.stud_ID == studentID
                    select c).Distinct().ToList<tb_ActiveClass>();
            return list;
        }
    }
}
