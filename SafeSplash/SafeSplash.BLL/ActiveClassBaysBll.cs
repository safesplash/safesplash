﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class ActiveClassBaysBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_ActiveClassBays info)
        {
            ctx.tb_ActiveClassBays.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Add(List<tb_ActiveClassBays> infos)
        {
            ctx.tb_ActiveClassBays.InsertAllOnSubmit<tb_ActiveClassBays>(infos);
            ctx.SubmitChanges();
        }

        public void Update(tb_ActiveClassBays info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(List<tb_ActiveClassBays> infos)
        {
            ctx.tb_ActiveClassBays.DeleteAllOnSubmit<tb_ActiveClassBays>(infos);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_ActiveClassBays configInfo = ctx.tb_ActiveClassBays.Single<tb_ActiveClassBays>(d => d.activeBay_ID == id);
            ctx.tb_ActiveClassBays.DeleteOnSubmit(configInfo);
            ctx.SubmitChanges();
        }

        public List<tb_ActiveClassBays> GetAll()
        {
            List<tb_ActiveClassBays> list = new List<tb_ActiveClassBays>();

            list = ctx.GetTable<tb_ActiveClassBays>().ToList<tb_ActiveClassBays>();
            return list;
        }

        public tb_ActiveClassBays GetByID(Guid id)
        {
            tb_ActiveClassBays dataInfo = ctx.tb_ActiveClassBays.SingleOrDefault<tb_ActiveClassBays>(d => d.activeBay_ID == id);
            return dataInfo;
        }

        public List<tb_ActiveClassBays> GetByBayID(Guid bayID)
        {
            List<tb_ActiveClassBays> listBay = new List<tb_ActiveClassBays>();

            listBay = (from c in ctx.tb_ActiveClassBays
                       where c.Bay_ID == bayID
                       select c).ToList<tb_ActiveClassBays>();
            return listBay;
        }

        public bool ExistBay(Guid bayID)
        {
            List<tb_ActiveClassBays> listBay = GetByBayID(bayID);
            if (listBay == null || listBay.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<tb_ActiveClassBays> GetByActiveDaysID(Guid activeDaysID)
        {
            List<tb_ActiveClassBays> listActiveClassBays = new List<tb_ActiveClassBays>();

            listActiveClassBays = (from c in ctx.tb_ActiveClassBays
                                   where c.activeDay_ID == activeDaysID
                                   select c).ToList<tb_ActiveClassBays>();
            return listActiveClassBays;
        }
    }
}
