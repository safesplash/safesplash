﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class ClientBll
    {
        protected SafeSplashDBDataContext ctx = null;
        protected ClientStudentBll clientStudBll = null;
        protected ClientLocationBll clientLocaBll = null;

        public ClientBll()
        {
            ctx = new SafeSplashDBDataContext();
            clientLocaBll = new ClientLocationBll();
            clientStudBll = new ClientStudentBll();
        }

        public ClientBll(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }

        public void Add(tb_Client clientInfo, string locationID)
        {
            ctx.tb_Clients.InsertOnSubmit(clientInfo);
            ctx.SubmitChanges();
            tb_ClientLocation locationInfo = new tb_ClientLocation();
            locationInfo.clientloca_ID = Guid.NewGuid();
            locationInfo.client_ID = clientInfo.client_ID;
            locationInfo.loca_ID = new Guid(locationID);
            locationInfo.clientloca_IsCurrent = true;
            clientLocaBll.Add(locationInfo);
        }

        public void Add(tb_Client clientInfo, List<tb_ClientStudent> studInfos, Guid locationID)
        {
            tb_ClientLocation locationInfo = new tb_ClientLocation();
            locationInfo.clientloca_ID = Guid.NewGuid();
            locationInfo.client_ID = clientInfo.client_ID;
            locationInfo.loca_ID = locationID;
            locationInfo.clientloca_IsCurrent = true;

            ctx.tb_Clients.InsertOnSubmit(clientInfo);
            ctx.SubmitChanges();

            clientLocaBll.Add(locationInfo);

            foreach (tb_ClientStudent tempInfo in studInfos)
            {
                tempInfo.client_ID = clientInfo.client_ID;
            }
            clientStudBll.Add(studInfos);
        }

        public void Update(tb_Client info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_Client configInfo = ctx.tb_Clients.SingleOrDefault<tb_Client>(d => d.client_ID == id);
            if (configInfo != null)
            {
                ctx.tb_Clients.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public bool DeleteClientInfo(Guid id)
        {
            try
            {
                List<tb_ClientComments> clientComments = new List<tb_ClientComments>();
                clientComments = (from c in ctx.tb_ClientComments
                                  where c.client_ID == id
                                  select c).ToList<tb_ClientComments>();
                if (clientComments != null)
                {
                    ctx.tb_ClientComments.DeleteAllOnSubmit(clientComments);
                    ctx.SubmitChanges();
                }

                List<tb_ClientLocation> clientLocation = new List<tb_ClientLocation>();
                clientLocation = (from c in ctx.tb_ClientLocation
                                  where c.client_ID == id
                                  select c).ToList<tb_ClientLocation>();
                if (clientLocation != null)
                {
                    ctx.tb_ClientLocation.DeleteAllOnSubmit(clientLocation);
                    ctx.SubmitChanges();
                }

                List<tb_ClientRelationship> clientRelationship = new List<tb_ClientRelationship>();
                clientRelationship = (from c in ctx.tb_ClientRelationship
                                      where c.client_ID == id
                                      select c).ToList<tb_ClientRelationship>();
                if (clientRelationship != null)
                {
                    ctx.tb_ClientRelationship.DeleteAllOnSubmit(clientRelationship);
                    ctx.SubmitChanges();
                }

                List<tb_ClientPayment> clientPayment = new List<tb_ClientPayment>();
                clientPayment = (from c in ctx.tb_ClientPayments
                                 join cl in ctx.tb_ClientBillings
                                    on c.clientBill_ID equals cl.clientBill_ID
                                 where cl.client_ID == id
                                 select c).ToList<tb_ClientPayment>();
                if (clientPayment != null)
                {
                    ctx.tb_ClientPayments.DeleteAllOnSubmit(clientPayment);
                    ctx.SubmitChanges();
                }

                List<tb_ClientBilling> clientBilling = new List<tb_ClientBilling>();
                clientBilling = (from c in ctx.tb_ClientBillings
                                 where c.client_ID == id
                                 select c).ToList<tb_ClientBilling>();
                if (clientBilling != null)
                {
                    ctx.tb_ClientBillings.DeleteAllOnSubmit(clientBilling);
                    ctx.SubmitChanges();
                }

                List<tb_ClientStudent> clientStudent = new List<tb_ClientStudent>();
                clientStudent = (from c in ctx.tb_ClientStudent
                                 where c.client_ID == id
                                 select c).ToList<tb_ClientStudent>();
                if (clientStudent != null)
                {
                    OrderDetailsBll orderDetailsBll = new OrderDetailsBll();
                    StudentAttendanceBll studAttendanceBll = new StudentAttendanceBll();
                    StudentBookBll studentBookBll = new StudentBookBll();
                    foreach (tb_ClientStudent student in clientStudent)
                    {
                        List<tb_StudentBooks> studBooksList = studentBookBll.GetByStudentID(student.stud_ID);
                        studentBookBll.Delete(studBooksList);

                        List<tb_StudentAttendance> studAttendance = studAttendanceBll.GetByStudent(student.stud_ID);
                        studAttendanceBll.Delete(studAttendance);

                        List<tb_OrderDetails> orderDetailsList = orderDetailsBll.GetByStudent(student.stud_ID);
                        orderDetailsBll.Delete(orderDetailsList);
                    }

                    ctx.tb_ClientStudent.DeleteAllOnSubmit(clientStudent);
                    ctx.SubmitChanges();
                }

                OrderBll orderBll = new OrderBll();
                List<tb_Order> orderList = orderBll.GetByClient(id);
                orderBll.Delete(orderList);

                tb_Client configInfo = ctx.tb_Clients.SingleOrDefault<tb_Client>(d => d.client_ID == id);
                if (configInfo != null)
                {
                    ctx.tb_Clients.DeleteOnSubmit(configInfo);
                    ctx.SubmitChanges();
                    aspnet_Users aspUser = ctx.aspnet_Users.SingleOrDefault<aspnet_Users>(user => configInfo.user_ID == user.UserId);
                    aspnet_UsersInRoles aspUsersInRoles = ctx.aspnet_UsersInRoles.SingleOrDefault<aspnet_UsersInRoles>(userInRole => userInRole.UserId == aspUser.UserId);
                    aspnet_Membership aspMembership = ctx.aspnet_Membership.SingleOrDefault<aspnet_Membership>(membership => membership.UserId == aspUser.UserId);
                    ctx.aspnet_UsersInRoles.DeleteOnSubmit(aspUsersInRoles);
                    ctx.SubmitChanges();
                    ctx.aspnet_Membership.DeleteOnSubmit(aspMembership);
                    ctx.SubmitChanges();
                    ctx.aspnet_Users.DeleteOnSubmit(aspUser);
                    ctx.SubmitChanges();
                }
                
            }
            catch
            {
                return false;
            }
            return true;
        }

        public List<tb_Client> GetAll()
        {
            List<tb_Client> list = new List<tb_Client>();

            list = ctx.GetTable<tb_Client>().OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
            return list;
        }

        public tb_Client GetByID(Guid id)
        {
            tb_Client dataInfo = ctx.tb_Clients.SingleOrDefault<tb_Client>(d => d.client_ID == id);
            return dataInfo;
        }

        public tb_Client GetByUserID(Guid userID)
        {
            tb_Client dataInfo = ctx.tb_Clients.SingleOrDefault<tb_Client>(d => d.user_ID == userID);
            return dataInfo;
        }

        public List<tb_Client> GetByEmail(string email)
        {
            List<tb_Client> clients = new List<tb_Client>();

            clients = (from c in ctx.tb_Clients
                       where c.client_Email == email
                       select c).ToList<tb_Client>();
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }

        public List<tb_Client> GetByLocationID(Guid locaID)
        {
            List<tb_Client> clients = new List<tb_Client>();

            clients = (from c in ctx.tb_Clients
                       join cl in ctx.tb_ClientLocation
                           on c.client_ID equals cl.client_ID
                       where cl.loca_ID == locaID
                       select c).ToList<tb_Client>();
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }

        public List<tb_Client> GetByStatus(string status)
        {
            List<tb_Client> list = new List<tb_Client>();

            list = (from c in ctx.tb_Clients
                    where c.client_Status == status
                    select c).ToList<tb_Client>();

            return list.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }

        public List<tb_Client> GetByStatus(string status, Guid[] locationIDs)
        {
            List<tb_Client> list = new List<tb_Client>();

            list = (from c in ctx.tb_Clients
                    join cl in ctx.tb_ClientLocation
                    on c.client_ID equals cl.client_ID
                    where c.client_Status == status && locationIDs.Contains((Guid)cl.loca_ID)
                    select c).ToList<tb_Client>();

            return list.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }

        public string GetNewClientCode()
        {
            string maxCode = (from c in ctx.tb_Clients select c.client_Code).Max<string>();
            string newCode = string.Empty;

            if (string.IsNullOrEmpty(maxCode))
            {
                newCode = "000001";
            }
            else
            {
                newCode = (int.Parse(maxCode) + 1).ToString();

                for (int i = 6 - newCode.Length; i > 0; i--)
                {
                    newCode = string.Concat("0", newCode);
                }
            }
            return newCode;
        }

        public void AddClientComment(tb_ClientComments comment)
        {
            ctx.tb_ClientComments.InsertOnSubmit(comment);
            ctx.SubmitChanges();
        }

        public void AddClientBillingInfo(tb_ClientBilling billing)
        {
            ctx.tb_ClientBillings.InsertOnSubmit(billing);
            ctx.SubmitChanges();
        }

        public List<tb_ClientComments> GetClientComments(Guid clientID)
        {
            List<tb_ClientComments> list = new List<tb_ClientComments>();
            list = (from c in ctx.tb_ClientComments
                    where c.client_ID == clientID
                    select c).OrderBy(comment => comment.clientComm_CreateDate).ToList<tb_ClientComments>();
            return list;
        }

        public tb_ClientComments GetClientComment(Guid id)
        {
            tb_ClientComments comment = new tb_ClientComments();

            comment = (from c in ctx.tb_ClientComments
                           where c.clientComm_ID == id
                       select c).SingleOrDefault<tb_ClientComments>();

            return comment;
        }

        public void UpdateClientComment(tb_ClientComments comment)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, comment);
            ctx.SubmitChanges();
        }

        public void DeleteClientComment(Guid id)
        {
            tb_ClientComments configInfo = ctx.tb_ClientComments.SingleOrDefault<tb_ClientComments>(d => d.clientComm_ID == id);
            if (configInfo != null)
            {
                ctx.tb_ClientComments.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }



        public tb_ClientBilling GetClientBilling(Guid clientID)
        {
            tb_ClientBilling billingInfo = new tb_ClientBilling();

            billingInfo = (from c in ctx.tb_ClientBillings
                    where c.client_ID == clientID
                    select c).SingleOrDefault<tb_ClientBilling>();

            return billingInfo;
        }

        public List<tb_Client> GetAllCurrentClients()
        {
            List<tb_Client> clients = new List<tb_Client>();

            clients = (from c in ctx.tb_Clients
                       join cl in ctx.tb_ClientStudent
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                        join cc in ctx.tb_ActiveClass
                            on ct.activecs_ID equals cc.activecs_ID
                       where (!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value || ((ct.studch_WidthdrawalData.Value).CompareTo (DateTime.Today) >= 0)) 
                       && (!cc.activecs_EndDay.HasValue || cc.activecs_EndDay.Value >= DateTime.Today)
                       && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                       select c).Distinct().ToList<tb_Client>();
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }
        public const string SearchKey_Student_FirstName = "fristName";
        public const string SearchKey_Student_LastName = "lastName";
        public List<tb_Client> GetCurrentClientsByStudent(Dictionary<string, string> pars)
        {
            if (pars == null || pars.Count == 0)
            {
                return new List<tb_Client>();
            }
            string firstName = "";
            string lastName = "";
            bool hasFirstName = pars.ContainsKey(SearchKey_Student_FirstName);
            bool hasLastName = pars.ContainsKey(SearchKey_Student_LastName);
            if (hasFirstName)
            {
                firstName = pars[SearchKey_Student_FirstName];
            }
            if (hasLastName)
            {
                lastName = pars[SearchKey_Student_LastName];
            }
            List<tb_Client> clients = null;

            clients = (from c in ctx.tb_Clients
                       join cl in ctx.tb_ClientStudent
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                       join cc in ctx.tb_ActiveClass
                           on ct.activecs_ID equals cc.activecs_ID
                       where (!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value || ((ct.studch_WidthdrawalData.Value).CompareTo(DateTime.Today) >= 0))
                       && (!cc.activecs_EndDay.HasValue || cc.activecs_EndDay.Value >= DateTime.Today)
                       && (!hasFirstName || cl.stud_FirstName.ToLower().Contains(firstName.ToLower()))
                       && (!hasLastName || cl.stud_LastName.ToLower().Contains(lastName.ToLower()))
                       && c.client_IsCompleteRegister.HasValue&&c.client_IsCompleteRegister.Value
                       select c).Distinct().ToList<tb_Client>();
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }
        public List<tb_Client> GetCurrentClientsByStudent(Guid[] locationIDs, Dictionary<string, string> pars)
        {
            if (pars == null || pars.Count == 0)
            {
                return new List<tb_Client>();
            }
            string firstName = "";
            string lastName = "";
            bool hasFirstName = pars.ContainsKey(SearchKey_Student_FirstName);
            bool hasLastName = pars.ContainsKey(SearchKey_Student_LastName);
            if (hasFirstName)
            {
                firstName = pars[SearchKey_Student_FirstName];
            }
            if (hasLastName)
            {
                lastName = pars[SearchKey_Student_LastName];
            }
            List<tb_Client> clients = null;

            clients = (from c in ctx.tb_Clients
                       join location in ctx.tb_ClientLocation
                       on c.client_ID equals location.client_ID
                       join cl in ctx.tb_ClientStudent
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                       join cc in ctx.tb_ActiveClass
                           on ct.activecs_ID equals cc.activecs_ID
                       where (!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value || ((ct.studch_WidthdrawalData.Value).CompareTo(DateTime.Today) >= 0))
                       && (!cc.activecs_EndDay.HasValue || cc.activecs_EndDay.Value >= DateTime.Today)
                       && (!hasFirstName || cl.stud_FirstName.ToLower().Contains(firstName.ToLower()))
                       && (!hasLastName || cl.stud_LastName.ToLower().Contains(lastName.ToLower()))
                       && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                       && locationIDs.Contains((Guid)location.loca_ID)
                       select c).Distinct().ToList<tb_Client>();
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }

        public List<tb_Client> GetAllCurrentClients(Guid[] locationIDs)
        {
            List<tb_Client> clients = new List<tb_Client>();

            clients = (from c in ctx.tb_Clients
                       join location in ctx.tb_ClientLocation
                       on c.client_ID equals location.client_ID
                       join cl in ctx.tb_ClientStudent
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                       join cc in ctx.tb_ActiveClass
                           on ct.activecs_ID equals cc.activecs_ID
                       where (!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value || ct.studch_WidthdrawalData.Value>=DateTime.Today) && (!cc.activecs_EndDay.HasValue || cc.activecs_EndDay.Value >= DateTime.Today) && locationIDs.Contains((Guid)location.loca_ID)
                       && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                       select c).Distinct().ToList<tb_Client>();
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }
        public List<tb_Client> GetProspectClientsByStudent(Dictionary<string,string> stuPars)
        {
            if (stuPars == null || stuPars.Count == 0)
            {
                return new List<tb_Client>();
            }
            string firstName = "";
            string lastName = "";
            bool hasFirstName = stuPars.ContainsKey(SearchKey_Student_FirstName);
            bool hasLastName = stuPars.ContainsKey(SearchKey_Student_LastName);
            if (hasFirstName)
            {
                firstName = stuPars[SearchKey_Student_FirstName];
            }
            if (hasLastName)
            {
                lastName = stuPars[SearchKey_Student_LastName];
            }
            IEnumerable<tb_Client> clients = null;
            IEnumerable<tb_Client> clientEnrolled = null;
            clients = (from client in ctx.tb_Clients
                       join location in ctx.tb_ClientLocation
                       on client.client_ID equals location.client_ID
                       join student in ctx.tb_ClientStudent
                           on client.client_ID equals student.client_ID
                       join book in ctx.tb_StudentBooks
                            on student.stud_ID equals book.stud_ID into student_book
                       from stu_bk in student_book.DefaultIfEmpty()
                       where stu_bk == null && client.client_IsCompleteRegister.HasValue && client.client_IsCompleteRegister.Value 
                       && (!hasFirstName || student.stud_FirstName.ToLower().Contains(firstName.ToLower())) 
                       && (!hasLastName || student.stud_LastName.ToLower().Contains(lastName.ToLower()))
                       select client).Distinct();
            clientEnrolled = (from client in ctx.tb_Clients
                              join location in ctx.tb_ClientLocation
                                  on client.client_ID equals location.client_ID
                              join student in ctx.tb_ClientStudent
                                  on client.client_ID equals student.client_ID
                              join book in ctx.tb_StudentBooks
                                   on student.stud_ID equals book.stud_ID
                              where client.client_IsCompleteRegister.HasValue && client.client_IsCompleteRegister.Value
                             && (!hasFirstName || student.stud_FirstName.ToLower().Contains(firstName.ToLower()))
                             && (!hasLastName || student.stud_LastName.ToLower().Contains(lastName.ToLower()))
                              select client).Distinct();
            clients = clients.Except(clientEnrolled);
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }
        public List<tb_Client> GetProspectClientsByStudent(Guid[] locationIDs,Dictionary<string,string> stuPars)
        {
            if (stuPars == null || stuPars.Count == 0)
            {
                return new List<tb_Client>();
            }
            string firstName = "";
            string lastName = "";
            bool hasFirstName = stuPars.ContainsKey(SearchKey_Student_FirstName);
            bool hasLastName = stuPars.ContainsKey(SearchKey_Student_LastName);
            if (hasFirstName)
            {
                firstName = stuPars[SearchKey_Student_FirstName];
            }
            if (hasLastName)
            {
                lastName = stuPars[SearchKey_Student_LastName];
            }
            IEnumerable<tb_Client> clients = null;
            IEnumerable<tb_Client> clientEnrolled = null;
            clients = (from client in ctx.tb_Clients
                       join location in ctx.tb_ClientLocation
                       on client.client_ID equals location.client_ID
                       join student in ctx.tb_ClientStudent
                           on client.client_ID equals student.client_ID 
                       join book in ctx.tb_StudentBooks
                            on student.stud_ID equals book.stud_ID into student_book
                       from stu_bk in student_book.DefaultIfEmpty()
                       where stu_bk == null && client.client_IsCompleteRegister.HasValue && client.client_IsCompleteRegister.Value &&  locationIDs.Contains((Guid)location.loca_ID)
                       && (!hasFirstName || student.stud_FirstName.ToLower().Contains(firstName.ToLower()))
                       && (!hasLastName || student.stud_LastName.ToLower().Contains(lastName.ToLower()))
                       select client).Distinct();
            clientEnrolled = (from client in ctx.tb_Clients
                              join location in ctx.tb_ClientLocation
                                  on client.client_ID equals location.client_ID
                              join student in ctx.tb_ClientStudent
                                  on client.client_ID equals student.client_ID
                              join book in ctx.tb_StudentBooks
                                   on student.stud_ID equals book.stud_ID 
                              where client.client_IsCompleteRegister.HasValue && client.client_IsCompleteRegister.Value && locationIDs.Contains((Guid)location.client_ID)
                             && (!hasFirstName || student.stud_FirstName.ToLower().Contains(firstName.ToLower()))
                       && (!hasLastName || student.stud_LastName.ToLower().Contains(lastName.ToLower()))
                              select client).Distinct();
            clients = clients.Except(clientEnrolled);
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }
        /// <summary>
        /// Searh all prospect clients
        /// </summary>
        /// <returns></returns>
        public List<tb_Client> GetProspectClients()
        {
            IEnumerable<tb_Client> clients = null;
            IEnumerable<tb_Client> clientEnrolled = null;
            clients = (from client in ctx.tb_Clients
                       join student in ctx.tb_ClientStudent
                           on client.client_ID equals student.client_ID into client_student
                       from cli_st in client_student.DefaultIfEmpty()
                       join book in ctx.tb_StudentBooks
                            on cli_st.stud_ID equals book.stud_ID into student_book
                       from stu_bk in student_book.DefaultIfEmpty()
                       where (cli_st == null || stu_bk == null) && client.client_IsCompleteRegister.HasValue && client.client_IsCompleteRegister.Value
                       select client).Distinct();
            clientEnrolled = (from client in ctx.tb_Clients
                              join student in ctx.tb_ClientStudent
                                  on client.client_ID equals student.client_ID
                              join book in ctx.tb_StudentBooks
                                   on student.stud_ID equals book.stud_ID
                              where client.client_IsCompleteRegister.HasValue && client.client_IsCompleteRegister.Value
                              select client).Distinct();
            clients = clients.Except(clientEnrolled);
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }

        /// <summary>
        /// Search prospoct clients by locations
        /// </summary>
        /// <param name="locationIDs"></param>
        /// <returns></returns>
        public List<tb_Client> GetProspectClients(Guid[] locationIDs)
        {
            IEnumerable<tb_Client> clients = null;
            IEnumerable<tb_Client> clientEnrolled = null;
            clients = (from client in ctx.tb_Clients
                       join location in ctx.tb_ClientLocation
                       on client.client_ID equals location.client_ID
                       where locationIDs.Contains((Guid)location.loca_ID)
                       join student in ctx.tb_ClientStudent
                           on client.client_ID equals student.client_ID into client_student
                       from cli_st in client_student.DefaultIfEmpty()
                       join book in ctx.tb_StudentBooks
                            on cli_st.stud_ID equals book.stud_ID into student_book
                       from stu_bk in student_book.DefaultIfEmpty()
                       where (cli_st == null || stu_bk == null) && client.client_IsCompleteRegister.HasValue && client.client_IsCompleteRegister.Value
                       select client).Distinct();
            clientEnrolled = (from client in ctx.tb_Clients
                              join location in ctx.tb_ClientLocation
                                  on client.client_ID equals location.client_ID
                              join student in ctx.tb_ClientStudent
                                  on client.client_ID equals student.client_ID
                              join book in ctx.tb_StudentBooks
                                   on student.stud_ID equals book.stud_ID into student_book
                              where client.client_IsCompleteRegister.HasValue && client.client_IsCompleteRegister.Value && locationIDs.Contains((Guid)location.client_ID)
                              select client).Distinct();
            clients = clients.Except(clientEnrolled);
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }

        public List<tb_Client> GetPastClients()
        {
            IEnumerable <tb_Client> clients = null;
            clients = (from c in ctx.tb_Clients
                       join cl in ctx.tb_ClientStudent
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                       join cc in ctx.tb_ActiveClass
                           on ct.activecs_ID equals cc.activecs_ID
                       where 
                       (
                       (ct.studch_IsWithdrawal.HasValue && ct.studch_IsWithdrawal.Value && (ct.studch_WidthdrawalData.Value.CompareTo(DateTime.Today.AddYears(-2)) >= 0) && (ct.studch_WidthdrawalData.Value.CompareTo(DateTime.Today) < 0)) 
                       || (  (!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value) && cc.activecs_EndDay.HasValue &&  (cc.activecs_EndDay.Value.CompareTo(DateTime.Today.AddYears(-2)) >= 0) && (cc.activecs_EndDay.Value.CompareTo(DateTime.Today)<0))
                       )
                       && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                       select c).Distinct();
            //FIXED - REENROLLED CLIENT WILL SHOW IN PAST CLIENT PAGE, AND IT'S WRONG - 9/6 2011
            //BEGIN
            IEnumerable<tb_Client> clientsCurrent = null;
            clientsCurrent = (from c in ctx.tb_Clients
                       join cl in ctx.tb_ClientStudent
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                        join cc in ctx.tb_ActiveClass
                            on ct.activecs_ID equals cc.activecs_ID
                       where (!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value || ((ct.studch_WidthdrawalData.Value).CompareTo (DateTime.Today) >= 0)) 
                       && (!cc.activecs_EndDay.HasValue || cc.activecs_EndDay.Value >= DateTime.Today)
                       && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                       select c).Distinct();
            clients = clients.Except(clientsCurrent);
            //END
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }
        public List<tb_Client> GetPastClientsByStudent(Dictionary<string, string> stuPars)
        {
            if (stuPars == null || stuPars.Count == 0)
            {
                return new List<tb_Client>();
            }
            IEnumerable<tb_Client> clients = null;
            string firstName = "";
            string lastName = "";
            if (stuPars.ContainsKey(SearchKey_Student_FirstName)) { firstName = stuPars[SearchKey_Student_FirstName]; }
            if (stuPars.ContainsKey(SearchKey_Student_LastName)) { lastName = stuPars[SearchKey_Student_LastName]; }
            clients = (from c in ctx.tb_Clients
                       join cl in ctx.tb_ClientStudent
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                       join cc in ctx.tb_ActiveClass
                           on ct.activecs_ID equals cc.activecs_ID
                       where
                       (
                       (ct.studch_IsWithdrawal.HasValue && ct.studch_IsWithdrawal.Value && ct.studch_WidthdrawalData.Value.CompareTo(DateTime.Today.AddYears(-2)) >= 0)
                       ||
                       ((!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value)   &&     cc.activecs_EndDay.HasValue   &&   cc.activecs_EndDay.Value.CompareTo(DateTime.Today.AddYears(-2)) >= 0)
                        )
                        &&(!stuPars.ContainsKey(SearchKey_Student_FirstName) || cl.stud_FirstName.ToLower().Contains(firstName.ToLower()))
                        &&(!stuPars.ContainsKey(SearchKey_Student_LastName) || cl.stud_LastName.ToLower().Contains(lastName.ToLower()))
                       && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                       
                       select c).Distinct();
            //FIXED - REENROLLED CLIENT WILL SHOW IN PAST CLIENT PAGE, AND IT'S WRONG - 9/6 2011
            //BEGIN
            IEnumerable<tb_Client> clientsCurrent = new List<tb_Client>();
            clientsCurrent = (from c in ctx.tb_Clients
                              join cl in ctx.tb_ClientStudent
                                  on c.client_ID equals cl.client_ID
                              join ct in ctx.tb_StudentBooks
                                   on cl.stud_ID equals ct.stud_ID
                              join cc in ctx.tb_ActiveClass
                                  on ct.activecs_ID equals cc.activecs_ID
                              where
                              (!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value || (ct.studch_WidthdrawalData.Value.CompareTo(DateTime.Today) >= 0))
                              && (!cc.activecs_EndDay.HasValue || cc.activecs_EndDay.Value >= DateTime.Today)
                              && (!stuPars.ContainsKey(SearchKey_Student_FirstName) || cl.stud_FirstName.ToLower().Contains(firstName.ToLower()))
                              && (!stuPars.ContainsKey(SearchKey_Student_LastName) || cl.stud_LastName.ToLower().Contains(lastName.ToLower()))
                              && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                              select c).Distinct();

            clients = clients.Except(clientsCurrent);
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }
        public List<tb_Client> GetPastClientsByStudent(Guid[] locationIDs, Dictionary<string, string> stuPars)
        {
            if (stuPars == null || stuPars.Count == 0)
            {
                return new List<tb_Client>();
            }
            IEnumerable<tb_Client> clients = null;
            string firstName = "";
            string lastName = "";
            if (stuPars.ContainsKey(SearchKey_Student_FirstName)) { firstName = stuPars[SearchKey_Student_FirstName]; }
            if (stuPars.ContainsKey(SearchKey_Student_LastName)) { lastName = stuPars[SearchKey_Student_LastName]; }
            clients = (from c in ctx.tb_Clients
                       join cl in ctx.tb_ClientStudent
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                       join cc in ctx.tb_ActiveClass
                           on ct.activecs_ID equals cc.activecs_ID
                       where
                       (
                       (ct.studch_IsWithdrawal.HasValue && ct.studch_IsWithdrawal.Value && ct.studch_WidthdrawalData.Value.CompareTo(DateTime.Today.AddYears(-2)) >= 0)
                       ||
                       (
                           (!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value)
                           &&
                           cc.activecs_EndDay.HasValue
                           &&
                           cc.activecs_EndDay.Value.CompareTo(DateTime.Today.AddYears(-2)) >= 0
                        )
                        )
                       && locationIDs.Contains((Guid)cc.loca_ID) 
                       && (!stuPars.ContainsKey(SearchKey_Student_FirstName) || cl.stud_FirstName.ToLower().Contains(firstName.ToLower()))
                       && (!stuPars.ContainsKey(SearchKey_Student_LastName)||cl.stud_LastName.ToLower().Contains(lastName.ToLower()))
                       && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                       select c).Distinct();
            //FIXED - REENROLLED CLIENT WILL SHOW IN PAST CLIENT PAGE, AND IT'S WRONG - 9/6 2011
            //BEGIN
            IEnumerable<tb_Client> clientsCurrent = new List<tb_Client>();
            clientsCurrent = (from c in ctx.tb_Clients
                              join cl in ctx.tb_ClientStudent
                                  on c.client_ID equals cl.client_ID
                              join ct in ctx.tb_StudentBooks
                                   on cl.stud_ID equals ct.stud_ID
                              join cc in ctx.tb_ActiveClass
                                  on ct.activecs_ID equals cc.activecs_ID
                              where
                              (!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value || (ct.studch_WidthdrawalData.Value.CompareTo(DateTime.Today) >= 0))
                              && (!cc.activecs_EndDay.HasValue || cc.activecs_EndDay.Value >= DateTime.Today) && locationIDs.Contains((Guid)cc.loca_ID)
                              && (!stuPars.ContainsKey(SearchKey_Student_FirstName) || cl.stud_FirstName.ToLower().Contains(firstName.ToLower()))
                              && (!stuPars.ContainsKey(SearchKey_Student_LastName) || cl.stud_LastName.ToLower().Contains( lastName.ToLower()))
                              && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                              select c).Distinct();
            clients = clients.Except(clientsCurrent);

            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }
        public List<tb_Client> GetPastClients(Guid[] locationIDs)
        {
            IEnumerable<tb_Client> clients = null;
            clients = (from c in ctx.tb_Clients
                       join cl in ctx.tb_ClientStudent
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                       join cc in ctx.tb_ActiveClass
                           on ct.activecs_ID equals cc.activecs_ID
                       where 
                       (
                       ((bool)ct.studch_IsWithdrawal && ((DateTime)ct.studch_WidthdrawalData).CompareTo(DateTime.Today.AddYears(-2)) >= 0) 
                       || ((ct.studch_IsWithdrawal == null || !(bool)ct.studch_IsWithdrawal) && cc.activecs_EndDay != null && ((DateTime)cc.activecs_EndDay).CompareTo(DateTime.Today.AddYears(-2)) >= 0)
                       )
                       && locationIDs.Contains((Guid)cc.loca_ID)
                       && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                       select c).Distinct();
            //FIXED - REENROLLED CLIENT WILL SHOW IN PAST CLIENT PAGE, AND IT'S WRONG - 9/6 2011
            //BEGIN
            IEnumerable<tb_Client> clientsCurrent = null;
            clientsCurrent = (from c in ctx.tb_Clients
                              join cl in ctx.tb_ClientStudent
                                  on c.client_ID equals cl.client_ID
                              join ct in ctx.tb_StudentBooks
                                   on cl.stud_ID equals ct.stud_ID
                              join cc in ctx.tb_ActiveClass
                                  on ct.activecs_ID equals cc.activecs_ID
                              where (!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value || ((ct.studch_WidthdrawalData.Value).CompareTo(DateTime.Today) >= 0))
                              && (!cc.activecs_EndDay.HasValue || cc.activecs_EndDay.Value >= DateTime.Today) && locationIDs.Contains((Guid)cc.loca_ID)
                              && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                              select c).Distinct();
            clients = clients.Except(clientsCurrent).OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName);
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }

        public List<tb_Client> GetInactiveClients()
        {
            IEnumerable<tb_Client> clients = null;
            clients = (from c in ctx.tb_Clients
                       join cl in ctx.tb_ClientStudent
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                       join cc in ctx.tb_ActiveClass
                           on ct.activecs_ID equals cc.activecs_ID
                       where (((bool)ct.studch_IsWithdrawal && ((DateTime)ct.studch_WidthdrawalData).CompareTo(DateTime.Today.AddYears(-2)) < 0) 
                       || 
                       ((ct.studch_IsWithdrawal == null || !(bool)ct.studch_IsWithdrawal) && cc.activecs_EndDay != null && ((DateTime)cc.activecs_EndDay).CompareTo(DateTime.Today.AddYears(-2)) < 0)
                       )
                       &&c.client_IsCompleteRegister.HasValue&&c.client_IsCompleteRegister.Value
                       select c).Distinct().ToList<tb_Client>();
            //FIXED - REENROLLED CLIENT WILL SHOW IN INACTIVE CLIENTS PAGE, AND IT'S WRONG - 9/6 2011
            //BEGIN
            IEnumerable<tb_Client> clientsCurrent = new List<tb_Client>();
            clientsCurrent = (from c in ctx.tb_Clients
                       join cl in ctx.tb_ClientStudent
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                        join cc in ctx.tb_ActiveClass
                            on ct.activecs_ID equals cc.activecs_ID
                       where (!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value || ((ct.studch_WidthdrawalData.Value).CompareTo (DateTime.Today) >= 0)) 
                       && (!cc.activecs_EndDay.HasValue || cc.activecs_EndDay.Value >= DateTime.Today)
                       && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                       select c).Distinct().ToList<tb_Client>();
            clients = clients.Except(clientsCurrent);
            //END
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }
        public List<tb_Client> GetInactiveClientsByStudent(Dictionary<string, string> stuPars)
        {
            if (stuPars == null || stuPars.Count == 0)
            {
                return new List<tb_Client>();
            }
            IEnumerable<tb_Client> clients = null;
            IEnumerable<tb_Client> clientsCurrent = null;
            string firstName = "";
            string lastName = "";
            if (stuPars.ContainsKey(SearchKey_Student_FirstName)) { firstName = stuPars[SearchKey_Student_FirstName]; }
            if (stuPars.ContainsKey(SearchKey_Student_LastName)) { lastName = stuPars[SearchKey_Student_LastName]; }

            clients = (from c in ctx.tb_Clients
                       join cl in ctx.tb_ClientStudent
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                       join cc in ctx.tb_ActiveClass
                           on ct.activecs_ID equals cc.activecs_ID
                       where (((bool)ct.studch_IsWithdrawal && ((DateTime)ct.studch_WidthdrawalData).CompareTo(DateTime.Today.AddYears(-2)) < 0) 
                       || ((ct.studch_IsWithdrawal == null || !(bool)ct.studch_IsWithdrawal) && cc.activecs_EndDay != null && ((DateTime)cc.activecs_EndDay).CompareTo(DateTime.Today.AddYears(-2)) < 0) )
                       && (stuPars.ContainsKey(SearchKey_Student_FirstName) || cl.stud_FirstName.ToLower().Contains(firstName.ToLower()))
                       && (stuPars.ContainsKey(SearchKey_Student_LastName) || cl.stud_LastName.ToLower().Contains( lastName.ToLower()))
                       && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                       select c).Distinct();
            //FIXED - REENROLLED CLIENT WILL SHOW IN INACTIVE CLIENTS PAGE, AND IT'S WRONG - 9/6 2011
            //BEGIN

            clientsCurrent = (from c in ctx.tb_Clients
                              join cl in ctx.tb_ClientStudent
                                  on c.client_ID equals cl.client_ID
                              join ct in ctx.tb_StudentBooks
                                   on cl.stud_ID equals ct.stud_ID
                              join cc in ctx.tb_ActiveClass
                                  on ct.activecs_ID equals cc.activecs_ID
                              where (!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value || ((ct.studch_WidthdrawalData.Value).CompareTo(DateTime.Today) >= 0))
                              && (!cc.activecs_EndDay.HasValue || cc.activecs_EndDay.Value >= DateTime.Today)
                              && (stuPars.ContainsKey(SearchKey_Student_FirstName) || cl.stud_FirstName.ToLower().Contains(firstName.ToLower()))
                              && (stuPars.ContainsKey(SearchKey_Student_LastName) || cl.stud_LastName.ToLower().Contains( lastName.ToLower()))
                              && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                              select c).Distinct();
            clients = clients.Except(clientsCurrent);
            //END
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }

        public List<tb_Client> GetInactiveClientsByStudent(Guid[] locationIDs, Dictionary<string, string> stuPars)
        {
            if (stuPars == null || stuPars.Count == 0)
            {
                return new List<tb_Client>();
            }
            IEnumerable<tb_Client> clients = null;
            IEnumerable<tb_Client> clientsCurrent = null;
            string firstName = "";
            string lastName = "";
            if (stuPars.ContainsKey(SearchKey_Student_FirstName)) { firstName = stuPars[SearchKey_Student_FirstName]; }
            if (stuPars.ContainsKey(SearchKey_Student_LastName)) { lastName = stuPars[SearchKey_Student_LastName]; }


            clients = (from c in ctx.tb_Clients
                       join cl in ctx.tb_ClientStudent
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                       join cc in ctx.tb_ActiveClass
                           on ct.activecs_ID equals cc.activecs_ID
                       where (((bool)ct.studch_IsWithdrawal && ((DateTime)ct.studch_WidthdrawalData).CompareTo(DateTime.Today.AddYears(-2)) < 0)
                       || ((ct.studch_IsWithdrawal == null || !(bool)ct.studch_IsWithdrawal) && cc.activecs_EndDay != null && ((DateTime)cc.activecs_EndDay).CompareTo(DateTime.Today.AddYears(-2)) < 0))
                       && (!stuPars.ContainsKey(SearchKey_Student_FirstName) || cl.stud_FirstName.ToLower().Contains( firstName.ToLower()))
                       &&(!stuPars.ContainsKey(SearchKey_Student_LastName) || cl.stud_LastName.ToLower().Contains( lastName.ToLower()))
                       && c.client_IsCompleteRegister.HasValue&&c.client_IsCompleteRegister.Value
                       && locationIDs.Contains((Guid)cc.loca_ID)
                       select c).Distinct();
            //FIXED - REENROLLED CLIENT WILL SHOW IN INACTIVE CLIENTS PAGE, AND IT'S WRONG - 9/6 2011
            //BEGIN

            clientsCurrent = (from c in ctx.tb_Clients
                              join cl in ctx.tb_ClientStudent
                                  on c.client_ID equals cl.client_ID
                              join ct in ctx.tb_StudentBooks
                                   on cl.stud_ID equals ct.stud_ID
                              join cc in ctx.tb_ActiveClass
                                  on ct.activecs_ID equals cc.activecs_ID
                              where (!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value || ((ct.studch_WidthdrawalData.Value).CompareTo(DateTime.Today) >= 0))
                              && (!cc.activecs_EndDay.HasValue || cc.activecs_EndDay.Value >= DateTime.Today)
                              && (!stuPars.ContainsKey(SearchKey_Student_FirstName) || cl.stud_FirstName.ToLower().Contains( firstName.ToLower()))
                              && (!stuPars.ContainsKey(SearchKey_Student_LastName) || cl.stud_LastName.ToLower().Contains( lastName.ToLower()))
                              &&c.client_IsCompleteRegister.HasValue&&c.client_IsCompleteRegister.Value
                              && locationIDs.Contains((Guid)cc.loca_ID)
                              select c);
            clients = clients.Except(clientsCurrent);
            //END
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }
        public List<tb_Client> GetInactiveClients(Guid[] locationIDs)
        {
            IEnumerable<tb_Client> clients = new List<tb_Client>();

            clients = (from c in ctx.tb_Clients
                       join cl in ctx.tb_ClientStudent
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                       join cc in ctx.tb_ActiveClass
                           on ct.activecs_ID equals cc.activecs_ID
                       where (((bool)ct.studch_IsWithdrawal && ((DateTime)ct.studch_WidthdrawalData).CompareTo(DateTime.Today.AddYears(-2)) < 0)
                       || ((ct.studch_IsWithdrawal == null || !(bool)ct.studch_IsWithdrawal) && cc.activecs_EndDay != null && ((DateTime)cc.activecs_EndDay).CompareTo(DateTime.Today.AddYears(-2)) < 0))
                       && locationIDs.Contains((Guid)cc.loca_ID)
                       && c.client_IsCompleteRegister.HasValue&&c.client_IsCompleteRegister.Value
                       select c).Distinct();
            //FIXED - REENROLLED CLIENT WILL SHOW IN INACTIVE CLIENTS PAGE, AND IT'S WRONG - 9/6 2011
            //BEGIN
            IEnumerable<tb_Client> clientsCurrent = new List<tb_Client>();
            clientsCurrent = (from c in ctx.tb_Clients
                              join cl in ctx.tb_ClientStudent
                                  on c.client_ID equals cl.client_ID
                              join ct in ctx.tb_StudentBooks
                                   on cl.stud_ID equals ct.stud_ID
                              join cc in ctx.tb_ActiveClass
                                  on ct.activecs_ID equals cc.activecs_ID
                              where (!ct.studch_IsWithdrawal.HasValue || !ct.studch_IsWithdrawal.Value || ((ct.studch_WidthdrawalData.Value).CompareTo(DateTime.Today) >= 0))
                              && (!cc.activecs_EndDay.HasValue || cc.activecs_EndDay.Value >= DateTime.Today) 
                              && locationIDs.Contains((Guid)cc.loca_ID)
                              && c.client_IsCompleteRegister.HasValue && c.client_IsCompleteRegister.Value
                              select c).Distinct();
            clients = clients.Except(clientsCurrent).OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName);
            //END
            return clients.OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
        }

        public tb_Location GetClientLocation(Guid clientID)
        {
            List<tb_Location> locations = new List<tb_Location>();

            locations = (from c in ctx.tb_Clients
                       join cl in ctx.tb_ClientLocation
                           on c.client_ID equals cl.client_ID
                       join ct in ctx.tb_Locations
                            on cl.loca_ID equals ct.loca_ID
                       where c.client_ID == clientID
                       select ct).ToList<tb_Location>();
            if (locations != null && locations.Count > 0)
            {
                return locations[0];
            }
            else
            {
                return null;
            }
        }
    }
}
