﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class ClientStudentBll
    {
        protected SafeSplashDBDataContext ctx = null;

        public ClientStudentBll()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public ClientStudentBll(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }

        public void Add(tb_ClientStudent info)
        {
            ctx.tb_ClientStudent.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Add(List<tb_ClientStudent> infos)
        {
            ctx.tb_ClientStudent.InsertAllOnSubmit(infos);
            ctx.SubmitChanges();
        }

        public void Update(tb_ClientStudent info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_ClientStudent configInfo = ctx.tb_ClientStudent.SingleOrDefault<tb_ClientStudent>(d => d.stud_ID == id);
            if (configInfo != null)
            {
                ctx.tb_ClientStudent.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_ClientStudent> GetAll()
        {
            List<tb_ClientStudent> list = new List<tb_ClientStudent>();

            list = ctx.GetTable<tb_ClientStudent>().ToList<tb_ClientStudent>();
            return list;
        }

        public tb_ClientStudent GetByID(Guid id)
        {
            tb_ClientStudent dataInfo = ctx.tb_ClientStudent.SingleOrDefault<tb_ClientStudent>(d => d.stud_ID == id);
            return dataInfo;
        }

        public List<tb_ClientStudent> GetByClientID(Guid clientID)
        {
            List<tb_ClientStudent> listStud = new List<tb_ClientStudent>();

            listStud = (from cs in ctx.tb_ClientStudent
                        join c in ctx.tb_Clients on cs.client_ID equals c.client_ID
                        where c.client_ID == clientID
                        select cs).ToList<tb_ClientStudent>();

            return listStud;
        }

        public List<tb_ClientStudent> GetAllCurrentStudent(Guid client_ID)
        {
            List<tb_ClientStudent> students = new List<tb_ClientStudent>();

            students = (from cl in ctx.tb_ClientStudent
                       join ct in ctx.tb_StudentBooks
                            on cl.stud_ID equals ct.stud_ID
                       join cc in ctx.tb_ActiveClass
                           on ct.activecs_ID equals cc.activecs_ID
                       where (ct.studch_IsWithdrawal == null || !(bool)ct.studch_IsWithdrawal || ((bool)ct.studch_IsWithdrawal && ((DateTime)ct.studch_WidthdrawalData).CompareTo(DateTime.Today) >= 0))
                       && (cc.activecs_EndDay == null || (DateTime)cc.activecs_EndDay >= DateTime.Today)
                        select cl).Distinct().ToList<tb_ClientStudent>();
            return students;
        }

        public List<tb_ClientStudent> FilterStudentByName(string studName)
        {
            List<tb_ClientStudent> list = new List<tb_ClientStudent>();
            string[] splitName = studName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (splitName.Length == 2)
            {
                list = (from c in ctx.tb_ClientStudent
                        where c.stud_FirstName.Contains(splitName[0]) || c.stud_LastName.Contains(splitName[1])
                        select c).ToList<tb_ClientStudent>();
            }
            else
            {

                list = (from c in ctx.tb_ClientStudent
                        where c.stud_FirstName.Contains(studName) || c.stud_LastName.Contains(studName)
                        select c).ToList<tb_ClientStudent>();
            }
            return list;
        }

        public List<tb_ClientStudent> GetStudentsByBooks(Guid activeClassID, DateTime dtCurrentDate)
        {
            List<tb_ClientStudent> clientBooks = new List<tb_ClientStudent>();

            clientBooks = (from c in ctx.tb_ClientStudent
                           join cl in ctx.tb_StudentBooks
                           on c.stud_ID equals cl.stud_ID
                           //where cl.activecs_ID == activeClassID && ((DateTime)cl.studch_JoinData).CompareTo(dtCurrentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(dtCurrentDate) > 0))
                           where cl.activecs_ID == activeClassID && ((DateTime)cl.studch_JoinData).CompareTo(dtCurrentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(dtCurrentDate) >= 0))
                           select c).ToList<tb_ClientStudent>();
            return clientBooks;
        }
    }
}
