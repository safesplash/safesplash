﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class ActiveClassBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();
        protected ActiveClassDaysBll dayBll = new ActiveClassDaysBll();
        protected ActiveClassBaysBll bayBll = new ActiveClassBaysBll();

        public void Add(tb_ActiveClass info)
        {
            ctx.tb_ActiveClass.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Add(tb_ActiveClass info, List<tb_ActiveClassDays> days, List<tb_ActiveClassBays> bays)
        {
            ctx.tb_ActiveClass.InsertOnSubmit(info);
            ctx.SubmitChanges();

            foreach (tb_ActiveClassDays day in days)
            {
                day.activecs_ID = info.activecs_ID;
            }
            dayBll.Add(days);

            bayBll.Add(bays);
        }

        public void Update(tb_ActiveClass info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_ActiveClass configInfo = ctx.tb_ActiveClass.SingleOrDefault<tb_ActiveClass>(d => d.activecs_ID == id);
            if (configInfo != null)
            {
                ctx.tb_ActiveClass.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_ActiveClass> GetAll()
        {
            List<tb_ActiveClass> list = new List<tb_ActiveClass>();

            list = ctx.GetTable<tb_ActiveClass>().ToList<tb_ActiveClass>();
            return list;
        }

        public List<tb_ActiveClass> GetByLocation(Guid locationID)
        {
            List<tb_ActiveClass> list = new List<tb_ActiveClass>();
            list = (from c in ctx.tb_ActiveClass
                                   where c.loca_ID == locationID
                                   select c).ToList<tb_ActiveClass>();
            return list;
        }
        
        public List<tb_ActiveClass> GetByTeacher(Guid teacherID)
        {
            List<tb_ActiveClass> list = new List<tb_ActiveClass>();
            list = (from c in ctx.tb_ActiveClass
                    where c.activecs_Teacher == teacherID
                    select c).ToList<tb_ActiveClass>();
            return list;
        }

        public List<tb_ActiveClass> GetByClassID(Guid classID)
        {
            List<tb_ActiveClass> list = new List<tb_ActiveClass>();
            list = (from c in ctx.tb_ActiveClass
                    where c.class_ID == classID
                    select c).ToList<tb_ActiveClass>();
            return list;
        }

        public List<tb_ActiveClass> GetByClassLevel(Guid levelID)
        {
            List<tb_ActiveClass> list = new List<tb_ActiveClass>();
            list = (from c in ctx.tb_ActiveClass
                    where c.level_ID == levelID
                    select c).ToList<tb_ActiveClass>();
            return list;
        }

        public tb_ActiveClass GetByID(Guid id)
        {
            tb_ActiveClass dataInfo = ctx.tb_ActiveClass.SingleOrDefault<tb_ActiveClass>(d => d.activecs_ID == id);
            return dataInfo;
        }

        public List<tb_ActiveClass> GetByStudentID(Guid studentID)
        {
            List<tb_ActiveClass> list = new List<tb_ActiveClass>();
            list = (from c in ctx.tb_ActiveClass
                        join cl in ctx.tb_StudentBooks
                        on c.activecs_ID equals cl.activecs_ID
                    where cl.stud_ID == studentID
                    select c).ToList<tb_ActiveClass>();
            return list;
        }

        public List<tb_ActiveClass> GetStudentCurrentPast(Guid studentID)
        {
            List<tb_ActiveClass> list = new List<tb_ActiveClass>();
            list = (from c in ctx.tb_ActiveClass
                    join cl in ctx.tb_StudentBooks
                    on c.activecs_ID equals cl.activecs_ID
                    where cl.stud_ID == studentID && (!cl.studch_IsWithdrawal.HasValue || !cl.studch_IsWithdrawal.Value || ((cl.studch_WidthdrawalData.Value).CompareTo(DateTime.Today) >= 0))
                    select c).Distinct().ToList<tb_ActiveClass>();
            return list;
        }

        public List<tb_ActiveClass> GetStudentWithdrawaled(Guid studentID)
        {
            List<tb_ActiveClass> list = new List<tb_ActiveClass>();
            list = (from c in ctx.tb_ActiveClass
                    join cl in ctx.tb_StudentBooks
                    on c.activecs_ID equals cl.activecs_ID
                    where cl.stud_ID == studentID 
                    && cl.studch_IsWithdrawal.HasValue 
                    && cl.studch_IsWithdrawal.Value 
                    && cl.studch_WidthdrawalData.Value.CompareTo(DateTime.Today) < 0
                    select c).Distinct().ToList<tb_ActiveClass>();
            return list;
        }

        public List<tb_ActiveClass> GetAllWithdrawaledRequest()
        {
            List<tb_ActiveClass> list = new List<tb_ActiveClass>();
            list = (from c in ctx.tb_ActiveClass
                    join cl in ctx.tb_StudentBooks
                    on c.activecs_ID equals cl.activecs_ID
                    where cl.studch_IsWithdrawal == true
                    orderby cl.studch_WidthdrawalData descending 
                    select c).ToList<tb_ActiveClass>();
            return list;
        }

        public List<tb_ActiveClass> GetTodayClasses(DateTime today, Guid locationID)
        {
            List<tb_ActiveClass> listActiveClass = new List<tb_ActiveClass>();

            listActiveClass = (from cl in ctx.tb_ActiveClass
                                   join c in ctx.tb_ActiveClassDays
                                        on cl.activecs_ID equals c.activecs_ID
                                   where cl.activecs_StartDay <= today && (cl.activecs_EndDay == null || cl.activecs_EndDay >= today) && c.activeDay_Day == today.DayOfWeek.ToString() && cl.loca_ID == locationID
                                   orderby c.activeDay_StartTime
                                   select cl).ToList<tb_ActiveClass>();
            return listActiveClass;
        }

        public List<tb_ActiveClass> GetTodayClasses(DateTime today, Guid locationID, Guid teacherID)
        {
            List<tb_ActiveClass> listActiveClass = new List<tb_ActiveClass>();

            listActiveClass = (from cl in ctx.tb_ActiveClass
                               join c in ctx.tb_ActiveClassDays
                                    on cl.activecs_ID equals c.activecs_ID
                               where cl.activecs_StartDay <= today && (cl.activecs_EndDay == null || cl.activecs_EndDay >= today) && c.activeDay_Day == today.DayOfWeek.ToString() && cl.loca_ID == locationID && cl.activecs_Teacher == teacherID
                               orderby c.activeDay_StartTime
                               select cl).ToList<tb_ActiveClass>();
            return listActiveClass;
        }

        public List<tb_ActiveClass> ValidAvaiBay(DateTime startDay, string DayOfWeek, TimeSpan startTime, TimeSpan endTime, Guid bayID)
        {
            List<tb_ActiveClass> listActiveClass = new List<tb_ActiveClass>();

            listActiveClass = (from c in ctx.tb_ActiveClass
                               join cl in ctx.tb_ActiveClassDays
                                    on c.activecs_ID equals cl.activecs_ID
                                    join ct in ctx.tb_ActiveClassBays
                                    on cl.activeDay_ID equals ct.activeDay_ID
                               where c.activecs_StartDay <= startDay && (c.activecs_EndDay == null || c.activecs_EndDay >= startDay) && cl.activeDay_Day == DayOfWeek && cl.activeDay_StartTime < endTime && cl.activeDay_EndTime > startTime && ct.Bay_ID == bayID
                               select c).ToList<tb_ActiveClass>();
            return listActiveClass;
        }
    }
}
