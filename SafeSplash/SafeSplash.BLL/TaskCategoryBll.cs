﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class TaskCategoryBll
    {
         protected SafeSplashDBDataContext ctx = null;
        public TaskCategoryBll()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public TaskCategoryBll(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }

        public void Add(tb_TaskCategory info)
        {
            ctx.tb_TaskCategory.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_TaskCategory info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(int id)
        {
            tb_TaskCategory configInfo = ctx.tb_TaskCategory.SingleOrDefault<tb_TaskCategory>(d => d.taskCategory_ID == id);
            if (configInfo != null)
            {
                ctx.tb_TaskCategory.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_TaskCategory> GetAll()
        {
            List<tb_TaskCategory> list = new List<tb_TaskCategory>();

            list = ctx.GetTable<tb_TaskCategory>().ToList<tb_TaskCategory>();
            return list;
        }

        public tb_TaskCategory GetByID(int id)
        {
            tb_TaskCategory dataInfo = ctx.tb_TaskCategory.SingleOrDefault<tb_TaskCategory>(d => d.taskCategory_ID == id);
            return dataInfo;
        }
    }
}
