﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class ClientBillingBll
    {
        protected SafeSplashDBDataContext ctx = null;

        public ClientBillingBll()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public ClientBillingBll(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }

        public void Add(tb_ClientBilling billingInfo)
        {
            billingInfo.clientBill_CardNumber = "";
            billingInfo.clientBill_SecurityCode = "";
            ctx.tb_ClientBillings.InsertOnSubmit(billingInfo);
            ctx.SubmitChanges();
        }

        public void Update(tb_ClientBilling info)
        {
            info.clientBill_CardNumber = "";
            info.clientBill_SecurityCode = "";
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_ClientBilling billingInfo = ctx.tb_ClientBillings.SingleOrDefault<tb_ClientBilling>(d => d.clientBill_ID == id);
            if (billingInfo != null)
            {
                ctx.tb_ClientBillings.DeleteOnSubmit(billingInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_ClientBilling> GetAll()
        {
            List<tb_ClientBilling> list = new List<tb_ClientBilling>();

            list = ctx.GetTable<tb_ClientBilling>().ToList<tb_ClientBilling>();
            return list;
        }

        public tb_ClientBilling GetByID(Guid id)
        {
            tb_ClientBilling dataInfo = ctx.tb_ClientBillings.SingleOrDefault<tb_ClientBilling>(d => d.clientBill_ID == id);
            return dataInfo;
        }

        public tb_ClientBilling GetByClientID(Guid clientID)
        {
            tb_ClientBilling dataInfo = ctx.tb_ClientBillings.SingleOrDefault<tb_ClientBilling>(d => d.client_ID == clientID);
            return dataInfo;
        }
    }
}