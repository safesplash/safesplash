﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using AKMII.SafeSplash.EntityNew;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class EmployeeBll
    {
        protected AKMII.SafeSplash.Entity.SafeSplashDBDataContext ctx = null;
        protected AKMII.SafeSplash.EntityNew.SafeSplashDBDataContext ctxNew = null;
        protected InstructorCertificationBll certBll = null;

        public EmployeeBll()
        {
            ctx = new AKMII.SafeSplash.Entity.SafeSplashDBDataContext();
            ctxNew = new AKMII.SafeSplash.EntityNew.SafeSplashDBDataContext();
            certBll = new InstructorCertificationBll();
        }

        public EmployeeBll(string connection)
        {
            ctx = new AKMII.SafeSplash.Entity.SafeSplashDBDataContext(connection);
            ctxNew = new AKMII.SafeSplash.EntityNew.SafeSplashDBDataContext(connection);
            certBll = new InstructorCertificationBll(connection);
        }

        public void Add(AKMII.SafeSplash.EntityNew.tb_Employee info)
        {
            ctxNew.tb_Employees.InsertOnSubmit(info);
            ctxNew.SubmitChanges();
        }

        public void Add(AKMII.SafeSplash.Entity.tb_Employee info, List<AKMII.SafeSplash.Entity.tb_InstructorCertification> certs)
        {
            ctx.tb_Employees.InsertOnSubmit(info);
            ctx.SubmitChanges();

            foreach (AKMII.SafeSplash.Entity.tb_InstructorCertification cert in certs)
            {
                cert.empl_ID = info.empl_ID;
            }
            certBll.Add(certs);
        }

        public void Update(AKMII.SafeSplash.Entity.tb_Employee info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            AKMII.SafeSplash.Entity.tb_Employee configInfo = ctx.tb_Employees.SingleOrDefault<AKMII.SafeSplash.Entity.tb_Employee>(d => d.empl_ID == id);
            //ctx.tb_Employees.DeleteOnSubmit(configInfo);
            configInfo.empl_IsDeleted = true;
            ctx.SubmitChanges();
        }

        public List<AKMII.SafeSplash.EntityNew.tb_Employee> GetAll()
        {
            return GetAll(false);
        }


        public List<AKMII.SafeSplash.EntityNew.AspNetUser> GetAllUsers()
        {
            AKMII.SafeSplash.EntityNew.SafeSplashDBDataContext ctxNew = new AKMII.SafeSplash.EntityNew.SafeSplashDBDataContext();

            List<AKMII.SafeSplash.EntityNew.AspNetUser> list = new List<AKMII.SafeSplash.EntityNew.AspNetUser>();

            list = (from c in ctxNew.AspNetUsers select c).ToList<AKMII.SafeSplash.EntityNew.AspNetUser>();

            return list; 
            
        }


        public List<AKMII.SafeSplash.EntityNew.tb_Employee> GetAll(bool includeDeletedEmployees)
        {
            List<AKMII.SafeSplash.EntityNew.tb_Employee> list = new List<AKMII.SafeSplash.EntityNew.tb_Employee>();


            // list = (from c in ctxNew.tb_Employees orderby c.empl_FirstName select c).ToList<AKMII.SafeSplash.EntityNew.tb_Employee>();
            
            list = (from c in ctxNew.tb_Employees
                    where (includeDeletedEmployees ? c.empl_IsDeleted == c.empl_IsDeleted : c.empl_IsDeleted == null || c.empl_IsDeleted.Value == false)
                    orderby c.empl_FirstName
                    select c).ToList<AKMII.SafeSplash.EntityNew.tb_Employee>();
            return list;
        }

        public AKMII.SafeSplash.Entity.tb_Employee GetByID(Guid id)
        {
            AKMII.SafeSplash.Entity.tb_Employee dataInfo = ctx.tb_Employees.SingleOrDefault<AKMII.SafeSplash.Entity.tb_Employee>(d => d.empl_ID == id);
            return dataInfo;
        }

        public AKMII.SafeSplash.Entity.tb_Employee GetByName(string firstName, string lastName)
        {
            AKMII.SafeSplash.Entity.tb_Employee dataInfo = ctx.tb_Employees.SingleOrDefault<AKMII.SafeSplash.Entity.tb_Employee>(d => d.empl_FirstName == firstName && d.empl_LastName == lastName);
            return dataInfo;
        }

        public AKMII.SafeSplash.Entity.tb_Employee QueryByName(string firstName, string lastName)
        {
            List<AKMII.SafeSplash.Entity.tb_Employee> list = new List<AKMII.SafeSplash.Entity.tb_Employee>();

            list = (from c in ctx.tb_Employees
                    where c.empl_FirstName == firstName && c.empl_LastName.StartsWith(lastName)
                    select c).ToList<AKMII.SafeSplash.Entity.tb_Employee>();
            if (list != null && list.Count > 0)
            {
                return list[0];
            }
            else
            {
                return null;
            }
        }

        public AKMII.SafeSplash.Entity.tb_Employee GetByUserID(Guid userID)
        {
            AKMII.SafeSplash.Entity.tb_Employee dataInfo = ctx.tb_Employees.SingleOrDefault<AKMII.SafeSplash.Entity.tb_Employee>(d => d.user_ID == userID);
            return dataInfo;
        }

        public List<AKMII.SafeSplash.Entity.tb_Employee> GetByLocationID(Guid locationID)
        {
            return GetByLocationID(locationID, false);
        }

        public List<AKMII.SafeSplash.Entity.tb_Employee> GetByLocationID(Guid locationID, bool includeDeletedEmployees)
        {
            List<AKMII.SafeSplash.Entity.tb_Employee> list = new List<AKMII.SafeSplash.Entity.tb_Employee>();

            list = (from c in ctx.tb_Employees
                    join cl in ctx.tb_EmployeeLocations
                    on c.empl_ID equals cl.empl_ID
                    where cl.loca_ID == locationID && (includeDeletedEmployees ? c.empl_IsDeleted == c.empl_IsDeleted : c.empl_IsDeleted == null || c.empl_IsDeleted.Value == false)
                    orderby c.empl_FirstName
                    select c).ToList<AKMII.SafeSplash.Entity.tb_Employee>();
            return list;
        }

        public List<AKMII.SafeSplash.Entity.tb_Employee> GetByLocationID(Guid[] locationIDs)
        {
            return GetByLocationID(locationIDs, false);
        }

        public List<AKMII.SafeSplash.Entity.tb_Employee> GetByLocationID(Guid[] locationIDs, bool includeDeletedEmployees)
        {
            List<AKMII.SafeSplash.Entity.tb_Employee> list = new List<AKMII.SafeSplash.Entity.tb_Employee>();

            list = (from c in ctx.tb_Employees
                    join cl in ctx.tb_EmployeeLocations
                    on c.empl_ID equals cl.empl_ID
                    //where locationIDs.Contains((Guid)cl.loca_ID)
                    where locationIDs.Contains((Guid)cl.loca_ID) && (includeDeletedEmployees ? c.empl_IsDeleted == c.empl_IsDeleted : c.empl_IsDeleted == null || c.empl_IsDeleted.Value == false)
                    select c).ToList<AKMII.SafeSplash.Entity.tb_Employee>();

            return list;
        }

        public string GetNewEmployeeCode()
        {
            string maxCode = (from c in ctx.tb_Employees select c.empl_Code).Max<string>();
            string newCode = string.Empty;

            if (string.IsNullOrEmpty(maxCode))
            {
                newCode = "000001";
            }
            else
            {
                newCode = (int.Parse(maxCode) + 1).ToString();

                for (int i = 6 - newCode.Length; i > 0; i--)
                {
                    newCode = string.Concat("0", newCode);
                }
            }
            return newCode;
        }

        public List<AKMII.SafeSplash.Entity.tb_Employee> QueryEmployeeByName(string Name)
        {
            if (!String.IsNullOrEmpty(Name))
            {
                List<AKMII.SafeSplash.Entity.tb_Employee> list = new List<AKMII.SafeSplash.Entity.tb_Employee>();
                string[] splitName = Name.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (splitName.Length == 2)
                {
                    list = (from c in ctx.tb_Employees
                            where c.empl_FirstName.Contains(splitName[0]) || c.empl_LastName.Contains(splitName[1])
                            select c).ToList<AKMII.SafeSplash.Entity.tb_Employee>();
                }
                else
                {

                    list = (from c in ctx.tb_Employees
                            where c.empl_FirstName.Contains(Name) || c.empl_LastName.Contains(Name)
                            select c).ToList<AKMII.SafeSplash.Entity.tb_Employee>();
                }
                return list;
            }
            else
            {
                return null;
            }
        }

        public List<AKMII.SafeSplash.Entity.tb_Employee> QueryEmployeeByNameALocation(Guid locationID, string Name)
        {
            List<AKMII.SafeSplash.Entity.tb_Employee> list = new List<AKMII.SafeSplash.Entity.tb_Employee>();
            if (!String.IsNullOrEmpty(Name))
            {
                string[] splitName = Name.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (splitName.Length == 2)
                {
                    list = (from c in ctx.tb_Employees
                            join cl in ctx.tb_EmployeeLocations
                            on c.empl_ID equals cl.empl_ID
                            where cl.loca_ID == locationID && (c.empl_FirstName.Contains(splitName[0]) || c.empl_LastName.Contains(splitName[1]))
                            select c).ToList<AKMII.SafeSplash.Entity.tb_Employee>();
                }
                else
                {

                    list = (from c in ctx.tb_Employees
                            join cl in ctx.tb_EmployeeLocations
                            on c.empl_ID equals cl.empl_ID
                            where cl.loca_ID == locationID && (c.empl_FirstName.Contains(Name) || c.empl_LastName.Contains(Name))
                            select c).ToList<AKMII.SafeSplash.Entity.tb_Employee>();
                }
            }
            else
            {
                list = (from c in ctx.tb_Employees
                        join cl in ctx.tb_EmployeeLocations
                        on c.empl_ID equals cl.empl_ID
                        where cl.loca_ID == locationID
                        select c).ToList<AKMII.SafeSplash.Entity.tb_Employee>();
            }
            return list;
        }

        public List<AKMII.SafeSplash.Entity.tb_Employee> GetCurrentLocationInstructors(Guid[] locationIDs)
        {
            List<AKMII.SafeSplash.Entity.tb_Employee> list = new List<AKMII.SafeSplash.Entity.tb_Employee>();

            list = (from clo in ctx.tb_EmployeeLocations
                    join c in ctx.tb_Employees
                    on clo.empl_ID equals c.empl_ID
                    join cl in ctx.aspnet_Users
                    on c.user_ID equals cl.UserId
                    join ct in ctx.aspnet_UsersInRoles
                    on cl.UserId equals ct.UserId
                    join cc in ctx.aspnet_Roles
                    on ct.RoleId equals cc.RoleId
                    where locationIDs.Contains((Guid)clo.loca_ID) && c.empl_Type == "Instructor"
                    orderby c.empl_FirstName
                    select c).Distinct().ToList<AKMII.SafeSplash.Entity.tb_Employee>();

            return list;
        }

        public List<AKMII.SafeSplash.Entity.tb_Employee> GetLocationEmployees(Guid[] locationIDs)
        {
            List<AKMII.SafeSplash.Entity.tb_Employee> list = new List<AKMII.SafeSplash.Entity.tb_Employee>();

            list = (from clo in ctx.tb_EmployeeLocations
                    join c in ctx.tb_Employees
                    on clo.empl_ID equals c.empl_ID
                    join cl in ctx.aspnet_Users
                    on c.user_ID equals cl.UserId
                    join ct in ctx.aspnet_UsersInRoles
                    on cl.UserId equals ct.UserId
                    join cc in ctx.aspnet_Roles
                    on ct.RoleId equals cc.RoleId
                    where locationIDs.Contains((Guid)clo.loca_ID)
                    orderby c.empl_FirstName
                    select c).Distinct().ToList<AKMII.SafeSplash.Entity.tb_Employee>();

            return list;
        }

        public List<AKMII.SafeSplash.Entity.tb_Employee> GetCurrentLocationManagers(Guid[] locationIDs)
        {
            List<AKMII.SafeSplash.Entity.tb_Employee> list = new List<AKMII.SafeSplash.Entity.tb_Employee>();

            list = (from clo in ctx.tb_EmployeeLocations
                    join c in ctx.tb_Employees
                    on clo.empl_ID equals c.empl_ID
                    join cl in ctx.aspnet_Users
                    on c.user_ID equals cl.UserId
                    join ct in ctx.aspnet_UsersInRoles
                    on cl.UserId equals ct.UserId
                    join cc in ctx.aspnet_Roles
                    on ct.RoleId equals cc.RoleId
                    where locationIDs.Contains((Guid)clo.loca_ID) && cc.RoleName == "FacilityManager"
                    select c).Distinct().ToList<AKMII.SafeSplash.Entity.tb_Employee>();

            return list;
        }

        public List<AKMII.SafeSplash.Entity.tb_Employee> GetCorpAdmins()
        {
            List<AKMII.SafeSplash.Entity.tb_Employee> list = new List<AKMII.SafeSplash.Entity.tb_Employee>();

            list = (from c in ctx.tb_Employees
                    join cl in ctx.aspnet_Users
                    on c.user_ID equals cl.UserId
                    join ct in ctx.aspnet_UsersInRoles
                    on cl.UserId equals ct.UserId
                    join cc in ctx.aspnet_Roles
                    on ct.RoleId equals cc.RoleId
                    where cc.RoleName == "CorpAdmin"
                    select c).Distinct().ToList<AKMII.SafeSplash.Entity.tb_Employee>();

            return list;
        }

        public bool DeleteEmployee(Guid empl_ID, out string errorMsg)
        {
            errorMsg = string.Empty;

            AKMII.SafeSplash.Entity.tb_Employee employee = GetByID(empl_ID);
            if (employee != null)
            {
                List<AKMII.SafeSplash.Entity.tb_ActiveClass> activeClass = new List<AKMII.SafeSplash.Entity.tb_ActiveClass>();
                activeClass = (from c in ctx.tb_ActiveClass
                               where (c.activecs_Teacher == empl_ID || c.activecs_Assistant == empl_ID)
                                  && (c.activecs_EndDay == null || c.activecs_EndDay.Value.CompareTo(DateTime.Today) >= 0)
                               select c).ToList<AKMII.SafeSplash.Entity.tb_ActiveClass>();
                if (activeClass != null && activeClass.Count > 0)
                {
                    errorMsg = string.Format("{0} {1} cannot be deleted because they are a teacher or assistant for at least one active class.", employee.empl_FirstName, employee.empl_LastName);
                    return false;
                }
                else
                {
                    try
                    {
                        Delete(employee.empl_ID);
                        return true;
                    }
                    catch (Exception exc)
                    {
                        errorMsg = string.Format("An error occurred deleting employee: {0}", exc.Message);
                        return false;
                    }
                }
            }
            else
            {
                errorMsg = "Employee not found.";
                return false;
            }
        }

        public static List<string> EmployeeAccountsWithRestrictedPermissions()
        {
            return new List<string>() { "atodd", "thamen", "samantha", "cgeary", "jbrainard" };
        }

        public static List<string> EmployeeAccountsWhoCanManageMarketSegments()
        {
            return new List<string>() { "kgerrard", "tbackes", "hadderly", "fbaadmin" };
        }

        public static List<string> EmployeeAccountsWhoCanManageInstructorSegments()
        {
            return new List<string>() { "kgerrard", "tbackes", "hadderly", "fbaadmin" };
        }

        public static List<string> EmployeeAccountsWhoCanManageReportPermissions()
        {
            return new List<string>() { "kgerrard", "fbaadmin" };
        }

        public static List<string> EmployeesWhoCanAddClassesToShoppingCart()
        {
            return new List<string>() { "kgerrard", "fbaadmin" };
        }
    }
}
