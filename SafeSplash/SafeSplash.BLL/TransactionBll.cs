﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Linq;
using AKMII.SafeSplash.Entity;

namespace AKMII.SafeSplash.BLL
{
    public class TransactionBll
    {
        protected SafeSplashDBDataContext ctx = null;

        public TransactionBll()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public void Add(tb_Transaction info)
        {
            ctx.tb_Transactions.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Add(List<tb_Transaction> infos)
        {
            ctx.tb_Transactions.InsertAllOnSubmit<tb_Transaction>(infos);
            ctx.SubmitChanges();
        }

        public List<tb_Transaction> GetAll()
        {
            List<tb_Transaction> list = new List<tb_Transaction>();

            list = ctx.GetTable<tb_Transaction>().ToList<tb_Transaction>();
            return list;
        }
    }
}
