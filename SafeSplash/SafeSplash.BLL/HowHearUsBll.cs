﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class HowHearUsBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_HowHearUs info)
        {
            ctx.tb_HowHearUs.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_HowHearUs info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(tb_HowHearUs configInfo)
        {
            if (configInfo != null)
            {
                ctx.tb_HowHearUs.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public void Delete(Guid id)
        {
            tb_HowHearUs configInfo = ctx.tb_HowHearUs.SingleOrDefault<tb_HowHearUs>(d => d.howhear_ID == id);
            Delete(configInfo);
        }

        public List<tb_HowHearUs> GetAll()
        {
            List<tb_HowHearUs> list = new List<tb_HowHearUs>();

            list = (from c in ctx.tb_HowHearUs
                    orderby c.howhear_Order
                    select c).ToList<tb_HowHearUs>();
            return list;
        }

        public tb_HowHearUs GetByID(Guid id)
        {
            tb_HowHearUs dataInfo = ctx.tb_HowHearUs.SingleOrDefault<tb_HowHearUs>(d => d.howhear_ID == id);
            return dataInfo;
        }
    }
}
