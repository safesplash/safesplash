﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class EmployeeAvailabilityBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_EmployeeAvailability info)
        {
            ctx.tb_EmployeeAvailability.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_EmployeeAvailability info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_EmployeeAvailability configInfo = ctx.tb_EmployeeAvailability.SingleOrDefault<tb_EmployeeAvailability>(d => d.emplab_ID == id);
            if (configInfo != null)
            {
                ctx.tb_EmployeeAvailability.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_EmployeeAvailability> GetAll()
        {
            List<tb_EmployeeAvailability> list = new List<tb_EmployeeAvailability>();

            list = ctx.GetTable<tb_EmployeeAvailability>().ToList<tb_EmployeeAvailability>();
            return list;
        }

        public tb_EmployeeAvailability GetByID(Guid id)
        {
            tb_EmployeeAvailability dataInfo = ctx.tb_EmployeeAvailability.SingleOrDefault<tb_EmployeeAvailability>(d => d.emplab_ID == id);
            return dataInfo;
        }
    }
}
