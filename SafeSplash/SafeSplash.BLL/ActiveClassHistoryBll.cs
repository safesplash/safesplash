﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class ActiveClassHistoryBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_ActiveClassHistory info)
        {
            ctx.tb_ActiveClassHistories.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Add(List<tb_ActiveClassHistory> infos)
        {
            ctx.tb_ActiveClassHistories.InsertAllOnSubmit<tb_ActiveClassHistory>(infos);
            ctx.SubmitChanges();
        }

        public void Update(tb_ActiveClassHistory info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_ActiveClassHistory configInfo = ctx.tb_ActiveClassHistories.SingleOrDefault<tb_ActiveClassHistory>(d => d.activecsHistory_ID == id);
            if (configInfo != null)
            {
                ctx.tb_ActiveClassHistories.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_ActiveClassHistory> GetAll()
        {
            List<tb_ActiveClassHistory> list = new List<tb_ActiveClassHistory>();

            list = ctx.GetTable<tb_ActiveClassHistory>().ToList<tb_ActiveClassHistory>();
            return list;
        }

        public tb_ActiveClassHistory GetByID(Guid id)
        {
            tb_ActiveClassHistory dataInfo = ctx.tb_ActiveClassHistories.SingleOrDefault<tb_ActiveClassHistory>(d => d.activecsHistory_ID == id);
            return dataInfo;
        }

        public List<tb_ActiveClassHistory> GetByActiveClassID(Guid activeClassID)
        {
            List<tb_ActiveClassHistory> listActiveClassDays = new List<tb_ActiveClassHistory>();

            listActiveClassDays = (from c in ctx.tb_ActiveClassHistories
                                   where c.activecs_ID == activeClassID
                                   select c).ToList<tb_ActiveClassHistory>();
            return listActiveClassDays;
        }
    }
}
