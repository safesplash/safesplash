﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class ClientLocationBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_ClientLocation info)
        {
            ctx.tb_ClientLocation.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_ClientLocation info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_ClientLocation configInfo = ctx.tb_ClientLocation.SingleOrDefault<tb_ClientLocation>(d => d.clientloca_ID == id);
            if (configInfo != null)
            {
                ctx.tb_ClientLocation.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public void Delete(Guid clientID, Guid locationID)
        {
            tb_ClientLocation configInfo = ctx.tb_ClientLocation.SingleOrDefault<tb_ClientLocation>(d => d.client_ID == clientID && d.loca_ID == locationID);
            if (configInfo != null)
            {
                ctx.tb_ClientLocation.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_ClientLocation> GetAll()
        {
            List<tb_ClientLocation> list = new List<tb_ClientLocation>();

            list = ctx.GetTable<tb_ClientLocation>().ToList<tb_ClientLocation>();
            return list;
        }

        public tb_ClientLocation GetByID(Guid id)
        {
            tb_ClientLocation dataInfo = ctx.tb_ClientLocation.SingleOrDefault<tb_ClientLocation>(d => d.clientloca_ID == id);
            return dataInfo;
        }
        public bool ExistLocation(Guid location)
        {
            tb_ClientLocation dataInfo = ctx.tb_ClientLocation.SingleOrDefault<tb_ClientLocation>(d => d.clientloca_ID == location);
            return (dataInfo == null);
        }

        public List<tb_ClientLocation> GetClientLocations(Guid clientID)
        {
            List<tb_ClientLocation> list = new List<tb_ClientLocation>();

            list = (from i in ctx.tb_ClientLocation
                    where i.client_ID == clientID
                    select i).ToList<tb_ClientLocation>();

            return list; 
        }
    }
}
