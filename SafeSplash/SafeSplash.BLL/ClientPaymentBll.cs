﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq; 

namespace AKMII.SafeSplash.BLL
{
    public class ClientPaymentBll
    {
        protected SafeSplashDBDataContext ctx = null;

        public ClientPaymentBll()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public ClientPaymentBll(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }

        public void Add(tb_ClientPayment info)
        {
            ctx.tb_ClientPayments.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public List<tb_ClientPayment> GetByClientBillID(Guid clientBillID)
        {
            List<tb_ClientPayment> list = new List<tb_ClientPayment>();

            list = (from cp in ctx.tb_ClientPayments
                        where cp.clientBill_ID == clientBillID
                        select cp).ToList<tb_ClientPayment>();

            return list;
        }

        public tb_ClientPayment GetByID(Guid paymentID)
        {
            tb_ClientPayment dataInfo = ctx.tb_ClientPayments.SingleOrDefault<tb_ClientPayment>(d => d.clientPayment_ID == paymentID);
            return dataInfo;
        }

        public void Update(tb_ClientPayment info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }
    }
}
