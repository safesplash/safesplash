﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Linq;
using AKMII.SafeSplash.Entity;

namespace AKMII.SafeSplash.BLL
{
    public class BayBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_Bay info)
        {
            ctx.tb_Bay.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Add(List<tb_Bay> infos)
        {
            ctx.tb_Bay.InsertAllOnSubmit<tb_Bay>(infos);
            ctx.SubmitChanges();
        }

        public void Update(tb_Bay info)
        {
            //ctx.tb_Bay.Attach(configInfo);
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_Bay configInfo = ctx.tb_Bay.SingleOrDefault<tb_Bay>(d => d.bay_ID == id);
            if (configInfo != null)
            {
                ctx.tb_Bay.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public void Delete(List<tb_Bay> infos)
        {
            if (infos != null)
            {
                ctx.tb_Bay.DeleteAllOnSubmit(infos);
                ctx.SubmitChanges();
            }
        }

        public List<tb_Bay> GetAll()
        {
            List<tb_Bay> list = new List<tb_Bay>();

            list = ctx.GetTable<tb_Bay>().ToList<tb_Bay>();
            return list;
        }

        public tb_Bay GetByID(Guid id)
        {
            tb_Bay dataInfo = ctx.tb_Bay.SingleOrDefault<tb_Bay>(d => d.bay_ID == id);
            return dataInfo;
        }

        public tb_Bay GetByPoolAName(Guid poolID, string bayName)
        {
            tb_Bay dataInfo = ctx.tb_Bay.SingleOrDefault<tb_Bay>(d => d.pool_ID == poolID && d.bay_Name == bayName);
            return dataInfo;
        }

        public List<tb_Bay> GetBaysByPoolID(Guid poolID)
        {
            List<tb_Bay> listBay = new List<tb_Bay>();

            listBay = (from bays in ctx.tb_Bay where bays.pool_ID == poolID select bays).ToList<tb_Bay>();

            return listBay;
        }

        public List<tb_Bay> GetBaysByLocationID(Guid locationID)
        {
            List<tb_Bay> listBay = new List<tb_Bay>();

            listBay = (from c in ctx.tb_Bay
                       join cl in ctx.tb_Pool
                           on c.pool_ID equals cl.pool_ID
                       where cl.loca_ID == locationID
                       select c).ToList<tb_Bay>();
            return listBay;
        }

        public bool ExistPool(Guid poolID)
        {
            List<tb_Bay> listBay = GetBaysByPoolID(poolID);
            if (listBay == null || listBay.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
