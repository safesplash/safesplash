﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using AKMII.SafeSplash.Entity;
using System.IO;

namespace AKMII.SafeSplash.BLL
{
    public class UserConfigDataBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_UserConfigData info)
        {
            ctx.tb_UserConfigData.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_UserConfigData info)
        {
            StreamWriter sw = new StreamWriter("D:\\WorkSpace\\Personal\\SafeSplash\\Dev\\SafeSplash\\SafeSplash.BLL\\log.txt", true);
            ctx.Log = sw;

            //ctx.tb_UserConfigData.Attach(configInfo);
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();

            sw.Close();
        }

        public void Delete(Guid id)
        {
            tb_UserConfigData configInfo = ctx.tb_UserConfigData.SingleOrDefault<tb_UserConfigData>(d => d.userdata_ID == id);
            if (configInfo != null)
            {
                ctx.tb_UserConfigData.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_UserConfigData> GetAll()
        {
            List<tb_UserConfigData> list = new List<tb_UserConfigData>();

            list = ctx.GetTable<tb_UserConfigData>().ToList<tb_UserConfigData>();
            return list;
        }

        public tb_UserConfigData GetByID(Guid id)
        {
            tb_UserConfigData dataInfo = ctx.tb_UserConfigData.SingleOrDefault<tb_UserConfigData>(d => d.userdata_ID == id);
            return dataInfo;
        }

    }
}
