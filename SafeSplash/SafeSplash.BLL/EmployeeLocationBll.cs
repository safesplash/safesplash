﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using AKMII.SafeSplash.EntityNew;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class EmployeeLocationBll
    {
        protected AKMII.SafeSplash.Entity.SafeSplashDBDataContext ctx = null;
        protected AKMII.SafeSplash.EntityNew.SafeSplashDBDataContext ctxNew = null;
        protected InstructorCertificationBll certBll = null;

        public EmployeeLocationBll()
        {
            ctx = new AKMII.SafeSplash.Entity.SafeSplashDBDataContext();
            ctxNew = new AKMII.SafeSplash.EntityNew.SafeSplashDBDataContext();
            certBll = new InstructorCertificationBll();
        }

        public EmployeeLocationBll(string connection)
        {
            ctx = new AKMII.SafeSplash.Entity.SafeSplashDBDataContext(connection);
            ctxNew = new AKMII.SafeSplash.EntityNew.SafeSplashDBDataContext(connection);
            certBll = new InstructorCertificationBll(connection);
        }

        public void Add(AKMII.SafeSplash.EntityNew.tb_EmployeeLocation info)
        {
            ctxNew.tb_EmployeeLocations.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Add(List<AKMII.SafeSplash.EntityNew.tb_EmployeeLocation> infos)
        {
            ctxNew.tb_EmployeeLocations.InsertAllOnSubmit<AKMII.SafeSplash.EntityNew.tb_EmployeeLocation>(infos);
            ctxNew.SubmitChanges();
        }

        public void Delete(AKMII.SafeSplash.Entity.tb_EmployeeLocation configInfo)
        {
            if (configInfo != null)
            {
                ctx.tb_EmployeeLocations.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<AKMII.SafeSplash.Entity.tb_EmployeeLocation> GetByEmplID(Guid empl_ID)
        {
            List<AKMII.SafeSplash.Entity.tb_EmployeeLocation> list = new List<AKMII.SafeSplash.Entity.tb_EmployeeLocation>();
            list = (from c in ctx.tb_EmployeeLocations
                    where c.empl_ID == empl_ID
                    select c).ToList<AKMII.SafeSplash.Entity.tb_EmployeeLocation>();
            return list;
        }

        public Guid[] GetEmplocaID(Guid empl_ID)
        {
            List<AKMII.SafeSplash.Entity.tb_EmployeeLocation> list = GetByEmplID(empl_ID);
            if (list == null || list.Count == 0)
            {
                return null;
            }
            else
            {
                Guid[] locaIDs = new Guid[list.Count];
                for (int i = 0; i < list.Count; i++)
                {
                    AKMII.SafeSplash.Entity.tb_EmployeeLocation emplLocation = list[i];
                    locaIDs[i] = (Guid)emplLocation.loca_ID;
                }
                return locaIDs;
            }
        }

        public List<AKMII.SafeSplash.EntityNew.tb_Location> GetLocationsByEmplID(Guid empl_ID)
        {
            List<AKMII.SafeSplash.EntityNew.tb_Location> list = new List<AKMII.SafeSplash.EntityNew.tb_Location>();
            // filter out Littleton, CO from locations list 
            // customers wanted it deleted but too many database constraints and too much history would be lost            
            list = (from c in ctxNew.tb_EmployeeLocations
                    join cl in ctxNew.tb_Locations
                    on c.loca_ID equals cl.loca_ID
                    //where (c.empl_ID == empl_ID) && (c.loca_ID.ToString() != "A231067E-266C-4BF1-A393-414002D494C3")
                    where ((c.empl_ID == empl_ID) && c.tb_Location.loca_IsActive == true)
                    orderby cl.loca_Name
                    select cl).ToList<AKMII.SafeSplash.EntityNew.tb_Location>();
            return list;
        }
    }
}
