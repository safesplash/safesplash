﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class DSAInfoBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_DSAInfo info)
        {
            ctx.tb_DSAInfo.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_DSAInfo info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_DSAInfo configInfo = ctx.tb_DSAInfo.SingleOrDefault<tb_DSAInfo>(d => d.dsa_ID == id);
            if (configInfo != null)
            {
                ctx.tb_DSAInfo.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_DSAInfo> GetAll()
        {
            List<tb_DSAInfo> list = new List<tb_DSAInfo>();

            list = ctx.GetTable<tb_DSAInfo>().ToList<tb_DSAInfo>();
            return list;
        }

        public tb_DSAInfo GetByID(Guid id)
        {
            tb_DSAInfo dataInfo = ctx.tb_DSAInfo.SingleOrDefault<tb_DSAInfo>(d => d.dsa_ID == id);
            return dataInfo;
        }
    }
}
