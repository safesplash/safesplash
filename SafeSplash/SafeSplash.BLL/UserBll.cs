﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using AKMII.SafeSplash.EntityNew;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class UserBll
    {
        protected AKMII.SafeSplash.Entity.SafeSplashDBDataContext ctx = new AKMII.SafeSplash.Entity.SafeSplashDBDataContext();
        protected AKMII.SafeSplash.EntityNew.SafeSplashDBDataContext ctxNew = new AKMII.SafeSplash.EntityNew.SafeSplashDBDataContext();

        public Guid GetUserIDByUserName(string userName)
        {
            string parsedUserName = userName.Contains("|") ? userName.Substring(userName.LastIndexOf("|") + 1) : userName;
            //aspnet_Users user = ctx.aspnet_Users.SingleOrDefault<aspnet_Users>(d => d.UserName == userName.Trim());
            aspnet_Users user = ctx.aspnet_Users.SingleOrDefault<aspnet_Users>(d => d.UserName == parsedUserName.Trim());
            return user.UserId;
        }

        public aspnet_Users GetUserByID(Guid userID)
        {
            aspnet_Users user = ctx.aspnet_Users.SingleOrDefault<aspnet_Users>(d => d.UserId == userID);
            return user;
        }

        public aspnet_Users GetUserByName(string userName)
        {
            aspnet_Users user = ctx.aspnet_Users.SingleOrDefault<aspnet_Users>(d => d.UserName == userName.Trim());
            return user;
        }

        public List<aspnet_Users> FindUsersByName(string userName)
        {
            List<aspnet_Users> list = new List<aspnet_Users>();

            list = (from i in ctx.aspnet_Users
                    where i.UserName == userName.Trim()
                    select i).ToList<aspnet_Users>();
            return list; 
        }

        public AKMII.SafeSplash.Entity.aspnet_Membership GetMemberShipByUserID(Guid userID)
        {
            AKMII.SafeSplash.Entity.aspnet_Membership membership = ctx.aspnet_Membership.SingleOrDefault<AKMII.SafeSplash.Entity.aspnet_Membership>(d => d.UserId == userID);
            return membership;
        }
        public List<aspnet_Users> GetAllUsers()
        {
            List<aspnet_Users> list = new List<aspnet_Users>();

            list = ctx.GetTable<aspnet_Users>().ToList<aspnet_Users>();
            return list;
        }

        public List<aspnet_Users> GetAllEmployeeAccount()
        {
            List<aspnet_Users> list = new List<aspnet_Users>();
            list = (from u in ctx.aspnet_Users
                    join uir in ctx.aspnet_UsersInRoles
                    on u.UserId equals uir.UserId
                    join r in ctx.aspnet_Roles
                    on uir.RoleId equals r.RoleId
                    where r.RoleName != "Client"
                    select u).Distinct().ToList<aspnet_Users>();

            return list;
        }

        public string GetQuestion(Guid userID)
        {
            AKMII.SafeSplash.Entity.aspnet_Membership membership = ctx.aspnet_Membership.SingleOrDefault<AKMII.SafeSplash.Entity.aspnet_Membership>(d => d.UserId == userID);
            if (membership != null)
                return membership.PasswordQuestion;
            else
                return "";
        }

        public void UpdateQuestionAnswer(Guid userID, string newAnswer)
        {
            AKMII.SafeSplash.Entity.aspnet_Membership membership = ctx.aspnet_Membership.SingleOrDefault<AKMII.SafeSplash.Entity.aspnet_Membership>(d => d.UserId == userID);
            if (membership != null)
            {
                membership.PasswordAnswer = newAnswer;
                ctx.Refresh(RefreshMode.KeepCurrentValues, membership);
                ctx.SubmitChanges();
            }
        }

        public void SaveCopyPassword (Guid userID, string password)
        {
            AKMII.SafeSplash.Entity.aspnet_Membership membership = ctx.aspnet_Membership.SingleOrDefault<AKMII.SafeSplash.Entity.aspnet_Membership>(d => d.UserId == userID);
            if (membership != null)
            {
                membership.MobilePIN = password;
                ctx.Refresh(RefreshMode.KeepCurrentValues, membership);
                ctx.SubmitChanges();
            }
        }

        public string GetCopyPassword(Guid userID)
        {
            AKMII.SafeSplash.Entity.aspnet_Membership membership = ctx.aspnet_Membership.SingleOrDefault<AKMII.SafeSplash.Entity.aspnet_Membership>(d => d.UserId == userID);
            if (membership != null)
            {
                return membership.MobilePIN;
            }
            else
            {
                return "";
            }
        }

        public string GetQuestionAnswer(Guid userID)
        {
            AKMII.SafeSplash.Entity.aspnet_Membership membership = ctx.aspnet_Membership.SingleOrDefault<AKMII.SafeSplash.Entity.aspnet_Membership>(d => d.UserId == userID);
            if (membership != null)
                return membership.PasswordAnswer;
            else
                return "";
        }

        public string GetUserNameByID(Guid userID)
        {
            aspnet_Users user = ctx.aspnet_Users.SingleOrDefault<aspnet_Users>(d => d.UserId == userID);
            if (user != null)
            {
                return user.UserName;
            }
            else
                return "";
        }

        public bool UserExisted(string userName)
        {
            // This is tricky in th enew system, as they may be a user in tb_client or tb_Employee  and aspnet_users or aspnetuser
            // for now, lets only look in the new system.  
            // RAB  Need to identify how to port from one system to another if we decide to paralel.
            int count = (from c in ctxNew.AspNetUsers
                         where c.UserName == userName.Trim()
                         select c).ToList<AKMII.SafeSplash.EntityNew.AspNetUser>().Count;

            if (count >0)
                return true;
            else
                return false;
        }

        public bool IsFirstLogin(string userName)
        {
            Guid userID = this.GetUserIDByUserName(userName.Trim());

            AKMII.SafeSplash.Entity.aspnet_Membership membership = ctx.aspnet_Membership.SingleOrDefault<AKMII.SafeSplash.Entity.aspnet_Membership>(d => d.UserId == userID);
            if (membership != null)
            {
                return membership.PasswordQuestion == "Default";
            }
            else
                return true;
        }

        public AKMII.SafeSplash.Entity.vw_GetUserInfoByUserId GetUserInfoByUserID(Guid userId)
        {
            AKMII.SafeSplash.Entity.vw_GetUserInfoByUserId userInfo = (from c in ctx.vw_GetUserInfoByUserId
                                               where c.UserId == userId
                                                                       select c).SingleOrDefault<AKMII.SafeSplash.Entity.vw_GetUserInfoByUserId>();

            return userInfo;
        }

        public void ChangeUserStatus(Guid userID, bool isActive)
        {
            AKMII.SafeSplash.Entity.aspnet_Membership membership = ctx.aspnet_Membership.SingleOrDefault<AKMII.SafeSplash.Entity.aspnet_Membership>(d => d.UserId == userID);
            
            if (membership != null)
            {
                membership.IsApproved = isActive;

                ctx.Refresh(RefreshMode.KeepCurrentValues, membership);
                ctx.SubmitChanges();
            }
        }
    }
}
