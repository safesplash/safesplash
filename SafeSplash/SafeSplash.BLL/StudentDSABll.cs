﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class StudentDSABll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_StudentDSA info)
        {
            ctx.tb_StudentDSA.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_StudentDSA info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_StudentDSA configInfo = ctx.tb_StudentDSA.SingleOrDefault<tb_StudentDSA>(d => d.studdsa_ID == id);
            if (configInfo != null)
            {
                ctx.tb_StudentDSA.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_StudentDSA> GetAll()
        {
            List<tb_StudentDSA> list = new List<tb_StudentDSA>();

            list = ctx.GetTable<tb_StudentDSA>().ToList<tb_StudentDSA>();
            return list;
        }

        public tb_StudentDSA GetByID(Guid id)
        {
            tb_StudentDSA dataInfo = ctx.tb_StudentDSA.SingleOrDefault<tb_StudentDSA>(d => d.studdsa_ID == id);
            return dataInfo;
        }
    }
}
