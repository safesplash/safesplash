﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using AKMII.SafeSplash.EntityNew;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class LocationBll
    {
        protected AKMII.SafeSplash.Entity.SafeSplashDBDataContext ctx = new AKMII.SafeSplash.Entity.SafeSplashDBDataContext();
        protected AKMII.SafeSplash.EntityNew.SafeSplashDBDataContext ctxNew = new AKMII.SafeSplash.EntityNew.SafeSplashDBDataContext();

        public void Add(AKMII.SafeSplash.Entity.tb_Location info)
        {
            ctx.tb_Locations.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(AKMII.SafeSplash.Entity.tb_Location info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            AKMII.SafeSplash.Entity.tb_Location configInfo = ctx.tb_Locations.SingleOrDefault<AKMII.SafeSplash.Entity.tb_Location>(d => d.loca_ID == id);
            if (configInfo != null)
            {
                ctx.tb_Locations.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<AKMII.SafeSplash.EntityNew.tb_Location> GetAll()
        {
            List<AKMII.SafeSplash.EntityNew.tb_Location> list = new List<AKMII.SafeSplash.EntityNew.tb_Location>();
            //// filter out Littleton, CO from locations list 
            //// customers wanted it deleted but too many database constraints and too much history would be lost
            ////list = ctx.GetTable<AKMII.SafeSplash.Entity.tb_Location>().Where(location => !location.loca_ID.Equals("A231067E-266C-4BF1-A393-414002D494C3")).OrderBy(location => location.loca_Name).ToList<AKMII.SafeSplash.Entity.tb_Location>();
            list = ctxNew.GetTable<AKMII.SafeSplash.EntityNew.tb_Location>().Where(l => l.loca_IsActive == true).OrderBy(location => location.loca_Name).ToList<AKMII.SafeSplash.EntityNew.tb_Location>();
            return list;
            //return GetAll(false);
        }

        public List<AKMII.SafeSplash.EntityNew.tb_Location> GetAll(bool includeInactiveLocations)
        {
            List<AKMII.SafeSplash.EntityNew.tb_Location> list = new List<AKMII.SafeSplash.EntityNew.tb_Location>();
            if (includeInactiveLocations)
            {
                list = ctxNew.GetTable<AKMII.SafeSplash.EntityNew.tb_Location>().OrderBy(location => location.loca_Name).ToList<AKMII.SafeSplash.EntityNew.tb_Location>();
            }
            else
            {
                list = ctxNew.GetTable<AKMII.SafeSplash.EntityNew.tb_Location>().Where(l => l.loca_IsActive == true).OrderBy(location => location.loca_Name).ToList<AKMII.SafeSplash.EntityNew.tb_Location>();
            }
            return list;
        }
               

        public List<AKMII.SafeSplash.EntityNew.tb_Location> GetPeakPricingLocations()
        {
            // for now only Lone Tree, Aurora and Parker should see peak pricing
            //List<Guid> peakPricingLocationGuids = new List<Guid>() { new Guid("DEE19A02-FC73-44A4-AF40-2FAA8300EA63"), new Guid("7F22897A-F6E5-4BE2-8833-AAA7C57CDE89"), new Guid("82130240-FB1F-440B-B0D7-41722E28B5E9") };
            return GetAll().ToList<AKMII.SafeSplash.EntityNew.tb_Location>();
        }

        public Guid[] GetAllIDs()
        {
            List<AKMII.SafeSplash.EntityNew.tb_Location> list = GetAll();
            if (list == null || list.Count == 0)
            {
                return null;
            }
            else
            {
                Guid[] guidIDs = new Guid[list.Count];
                for (int i = 0; i < list.Count; i++)
                {
                    guidIDs[i] = list[i].loca_ID;
                }
                return guidIDs;
            }
        }

        public AKMII.SafeSplash.Entity.tb_Location GetByID(Guid id)
        {
            AKMII.SafeSplash.Entity.tb_Location dataInfo = ctx.tb_Locations.SingleOrDefault<AKMII.SafeSplash.Entity.tb_Location>(d => d.loca_ID == id);
            return dataInfo;
        }

        public AKMII.SafeSplash.Entity.tb_Location GetByName(string locaName)
        {
            AKMII.SafeSplash.Entity.tb_Location dataInfo = ctx.tb_Locations.SingleOrDefault<AKMII.SafeSplash.Entity.tb_Location>(d => d.loca_Name == locaName);
            return dataInfo;
        }
    }
}
