﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class StudentAttendanceBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_StudentAttendance info)
        {
            ctx.tb_StudentAttendance.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_StudentAttendance info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_StudentAttendance configInfo = ctx.tb_StudentAttendance.SingleOrDefault<tb_StudentAttendance>(d => d.studAtte_ID == id);
            if (configInfo != null)
            {

                ctx.tb_StudentAttendance.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public void Delete(List<tb_StudentAttendance> infos)
        {
            if (infos != null)
            {
                ctx.tb_StudentAttendance.DeleteAllOnSubmit(infos);
                ctx.SubmitChanges();
            }
        }

        public List<tb_StudentAttendance> GetAll()
        {
            List<tb_StudentAttendance> list = new List<tb_StudentAttendance>();

            list = ctx.GetTable<tb_StudentAttendance>().ToList<tb_StudentAttendance>();
            return list;
        }

        public tb_StudentAttendance GetByID(Guid id)
        {
            tb_StudentAttendance dataInfo = ctx.tb_StudentAttendance.SingleOrDefault<tb_StudentAttendance>(d => d.studAtte_ID == id);
            return dataInfo;
        }

        public List<tb_StudentAttendance> GetByStudent(Guid studID)
        {
            List<tb_StudentAttendance> list = new List<tb_StudentAttendance>();
            list = (from c in ctx.tb_StudentAttendance
                    where c.stud_ID == studID
                    select c).ToList<tb_StudentAttendance>();
            return list;
        }

        public tb_StudentAttendance GetAttendStatus(Guid activeDayID, Guid studentID, DateTime attendDate)
        {
            tb_StudentAttendance dataInfo = ctx.tb_StudentAttendance.SingleOrDefault<tb_StudentAttendance>(d => d.activeDay_ID == activeDayID && d.stud_ID == studentID && d.studAtte_Date == attendDate);
            return dataInfo;
        }

        public tb_StudentAttendance GetAttendance(Guid studID, DateTime date, TimeSpan timeStart, TimeSpan timeEnd)
        {
            tb_StudentAttendance dataInfo = ctx.tb_StudentAttendance.SingleOrDefault<tb_StudentAttendance>(d => d.studAtte_Date == date && 
                d.stud_ID == studID && d.studAtte_From == timeStart && d.studAtte_To == timeEnd);
            return dataInfo;
        }
    }
}
