﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Linq;
using AKMII.SafeSplash.Entity;

namespace AKMII.SafeSplash.BLL
{
    public class OrderBll
    {
        protected SafeSplashDBDataContext ctx = null;

        public OrderBll()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public OrderBll(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }
        public void Add(tb_Order info)
        {
            ctx.tb_Order.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Add(List<tb_Order> infos)
        {
            ctx.tb_Order.InsertAllOnSubmit<tb_Order>(infos);
            ctx.SubmitChanges();
        }

        public List<tb_Order> GetAll()
        {
            List<tb_Order> list = new List<tb_Order>();

            list = ctx.GetTable<tb_Order>().ToList<tb_Order>();
            return list;
        }

        public List<tb_Order> GetByClient(Guid clientID)
        {
            List<tb_Order> list = new List<tb_Order>();

            list = (from c in ctx.tb_Order
                    where c.client_ID == clientID
                    select c).ToList<tb_Order>();
            return list;
        }

        public void Delete(List<tb_Order> infos)
        {
            if (infos != null)
            {
                ctx.tb_Order.DeleteAllOnSubmit(infos);
                ctx.SubmitChanges();
            }
        }
    }
}
