﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;
using SafeSplash.DataAccess.ActiveClass;
using SafeSplash.DataAccess.NHibernate;

namespace AKMII.SafeSplash.BLL
{
    public class ActiveClassDaysBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();
        private IActiveClassInstructorDataAccess repository = new ActiveClassInstructorDataAccess();

        public void Add(tb_ActiveClassDays info)
        {
            ctx.tb_ActiveClassDays.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Add(List<tb_ActiveClassDays> infos)
        {
            ctx.tb_ActiveClassDays.InsertAllOnSubmit<tb_ActiveClassDays>(infos);
            ctx.SubmitChanges();
        }

        public void Update(tb_ActiveClassDays info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_ActiveClassDays configInfo = ctx.tb_ActiveClassDays.SingleOrDefault<tb_ActiveClassDays>(d => d.activeDay_ID == id);
            if (configInfo != null)
            {
                ctx.tb_ActiveClassDays.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_ActiveClassDays> GetAll()
        {
            List<tb_ActiveClassDays> list = new List<tb_ActiveClassDays>();

            list = ctx.GetTable<tb_ActiveClassDays>().ToList<tb_ActiveClassDays>();
            return list;
        }

        public tb_ActiveClassDays GetByID(Guid id)
        {
            tb_ActiveClassDays dataInfo = ctx.tb_ActiveClassDays.SingleOrDefault<tb_ActiveClassDays>(d => d.activeDay_ID == id);
            return dataInfo;
        }

        public List<tb_ActiveClassDays> GetByActiveClassID(Guid activeClassID)
        {
            List<tb_ActiveClassDays> listActiveClassDays = new List<tb_ActiveClassDays>();

            listActiveClassDays = (from c in ctx.tb_ActiveClassDays
                                   where c.activecs_ID == activeClassID
                                   select c).ToList<tb_ActiveClassDays>();
            return listActiveClassDays;
        }

        public List<tb_ActiveClassDays> GetByBayID(Guid BayID)
        {
            List<tb_ActiveClassDays> listActiveClassDays = new List<tb_ActiveClassDays>();

            listActiveClassDays = (from c in ctx.tb_ActiveClassDays
                                   join cl in ctx.tb_ActiveClassBays
                                        on c.activeDay_ID equals cl.activeDay_ID
                                   where cl.Bay_ID == BayID
                                   orderby c.activeDay_StartTime
                                   select c).ToList<tb_ActiveClassDays>();
            return listActiveClassDays;
        }

        public List<tb_ActiveClassDays> GetByLocationID(Guid locationID)
        {
            List<tb_ActiveClassDays> listActiveClassDays = new List<tb_ActiveClassDays>();

            listActiveClassDays = (from c in ctx.tb_ActiveClassDays
                                   join cl in ctx.tb_ActiveClass
                                        on c.activecs_ID equals cl.activecs_ID
                                   where cl.loca_ID == locationID
                                   orderby c.activeDay_StartTime
                                   select c).ToList<tb_ActiveClassDays>();
            return listActiveClassDays;
        }

        public List<tb_ActiveClassDays> GetMyTodayClasses(Guid teacherID, DateTime date)
        {
            List<tb_ActiveClassDays> listActiveClassDays = new List<tb_ActiveClassDays>();

            listActiveClassDays = (from c in ctx.tb_ActiveClassDays
                                   join cl in ctx.tb_ActiveClass
                                        on c.activecs_ID equals cl.activecs_ID
                                   where cl.activecs_Teacher == teacherID && c.activeDay_Day == date.DayOfWeek.ToString()
                                   && (cl.activecs_EndDay == null || ((DateTime)cl.activecs_EndDay).CompareTo(date) >= 0)
                                   orderby c.activeDay_StartTime
                                   select c).ToList<tb_ActiveClassDays>();
            return listActiveClassDays;
        }

        public List<tb_ActiveClassDays> GetTodayClasses(DateTime today, Guid[] locationIDs)
        {
            List<tb_ActiveClassDays> listActiveClassDays = new List<tb_ActiveClassDays>();

            listActiveClassDays = (from c in ctx.tb_ActiveClassDays
                                   join cl in ctx.tb_ActiveClass
                                        on c.activecs_ID equals cl.activecs_ID
                                   where cl.activecs_StartDay <= today && (cl.activecs_EndDay == null || cl.activecs_EndDay >= today) && c.activeDay_Day == today.DayOfWeek.ToString() && locationIDs.Contains((Guid)cl.loca_ID)
                                   orderby c.activeDay_StartTime
                                   select c).ToList<tb_ActiveClassDays>();
            return listActiveClassDays;
        }

        public List<vw_Class> GetAllTodayClasses(DateTime today, Guid[] locationIDs)
        {
            List<vw_Class> listActiveClassDays = new List<vw_Class>();

            listActiveClassDays = (from c in ctx.vw_Classes
                                   where c.activecs_StartDay <= today && (c.activecs_EndDay == null || c.activecs_EndDay >= today) && c.activeDay_Day == today.DayOfWeek.ToString() && locationIDs.Contains((Guid)c.loca_ID)
                                   orderby c.activeDay_StartTime
                                   select c).ToList<vw_Class>();
            return listActiveClassDays;
        }

        public List<tb_ActiveClassDays> GetTodayClasses(DateTime today, Guid locationID, Guid teacherID)
        {
            List<tb_ActiveClassDays> listActiveClassDays = new List<tb_ActiveClassDays>();

            listActiveClassDays = (from c in ctx.tb_ActiveClassDays
                                   join cl in ctx.tb_ActiveClass
                                        on c.activecs_ID equals cl.activecs_ID
                                   where cl.activecs_StartDay <= today && (cl.activecs_EndDay == null || cl.activecs_EndDay >= today) && c.activeDay_Day == today.DayOfWeek.ToString() && cl.loca_ID == locationID && cl.activecs_Teacher == teacherID
                                   orderby c.activeDay_StartTime
                                   select c).ToList<tb_ActiveClassDays>();
            return listActiveClassDays;
        }

        public List<vw_Class> GetAllTodayClasses(DateTime today, Guid locationID, Guid teacherID)
        {
            List<vw_Class> listActiveClassDays = new List<vw_Class>();

            listActiveClassDays = (from c in ctx.vw_Classes
                                   where c.activecs_StartDay <= today && (c.activecs_EndDay == null || c.activecs_EndDay >= today) && c.activeDay_Day == today.DayOfWeek.ToString() && c.loca_ID == locationID && c.empl_ID == teacherID
                                   orderby c.activeDay_StartTime
                                   select c).ToList<vw_Class>();
            return listActiveClassDays;
        }

        public List<tb_ActiveClassDays> GetTodayClasses(DateTime today)
        {
            List<tb_ActiveClassDays> listActiveClassDays = new List<tb_ActiveClassDays>();

            listActiveClassDays = (from c in ctx.tb_ActiveClassDays
                                   join cl in ctx.tb_ActiveClass
                                        on c.activecs_ID equals cl.activecs_ID
                                   where cl.activecs_StartDay <= today && (cl.activecs_EndDay == null || cl.activecs_EndDay >= today) && c.activeDay_Day == today.DayOfWeek.ToString()
                                   orderby c.activeDay_StartTime
                                   select c).ToList<tb_ActiveClassDays>();
            return listActiveClassDays;
        }


        public List<tb_ActiveClassDays> GetTodayClasses(Guid locationID, DateTime date, Guid teacherID)
        {

            //List<tb_ActiveClassDays> list = new List<tb_ActiveClassDays>();
            //list = (from c in ctx.tb_ActiveClassDays
            //        join cl in ctx.tb_ActiveClass
            //        on c.activecs_ID equals cl.activecs_ID
            //        where cl.loca_ID == locationID && cl.activecs_Teacher == teacherID && c.activeDay_Day == date.DayOfWeek.ToString()
            //        && (cl.activecs_EndDay == null || ((DateTime)cl.activecs_EndDay).CompareTo(date) >= 0)
            //        orderby c.activeDay_StartTime
            //        select c).ToList<tb_ActiveClassDays>();
            //return list;

            List<tb_ActiveClassDays> list = new List<tb_ActiveClassDays>();
            list = (from c in ctx.tb_ActiveClassDays
                    join cl in ctx.tb_ActiveClass
                    on c.activecs_ID equals cl.activecs_ID
                    //where cl.loca_ID == locationID && cl.activecs_Teacher == teacherID && c.activeDay_Day == date.DayOfWeek.ToString()
                    where cl.loca_ID == locationID && c.activeDay_Day == date.DayOfWeek.ToString()
                    && (cl.activecs_EndDay == null || ((DateTime)cl.activecs_EndDay).CompareTo(date) >= 0)
                    orderby c.activeDay_StartTime
                    select c).ToList<tb_ActiveClassDays>();
            //return list;

            List<tb_ActiveClassDays> updatedInstructorsList = SetInstructors(list, date);

            return updatedInstructorsList.Where(acd => acd.InstructorId == teacherID).ToList<tb_ActiveClassDays>();


            //IEnumerable<ActiveClassInstructorDataObject> instructorHistories = GetActiveClassInstructorHistoriesAsOfDate(date);
            //List<Guid> instructorHistoryIds = instructorHistories.Select(ih => ih.InstructorId).Distinct<Guid>().ToList<Guid>();
            //List<Guid> instructorIds = new List<Guid>() { teacherID };
            //instructorIds.AddRange(instructorHistoryIds);
            //instructorIds = instructorIds.Distinct<Guid>().ToList<Guid>();
            //List<Guid> classIds = instructorHistories.Select(ih => ih.ActiveClassId).Distinct<Guid>().ToList<Guid>();            

            //List<tb_ActiveClassDays> list = new List<tb_ActiveClassDays>();
            //list = (from c in ctx.tb_ActiveClassDays
            //        join cl in ctx.tb_ActiveClass
            //        on c.activecs_ID equals cl.activecs_ID
            //        where cl.loca_ID == locationID && (cl.activecs_Teacher == teacherID || classIds.Contains(cl.activecs_ID)) && c.activeDay_Day == date.DayOfWeek.ToString()
            //        && (cl.activecs_EndDay == null || ((DateTime)cl.activecs_EndDay).CompareTo(date) >= 0)
            //        orderby c.activeDay_StartTime
            //        select c).ToList<tb_ActiveClassDays>();

            //List<tb_ActiveClassDays> filteredList = new List<tb_ActiveClassDays>();

            //foreach (var acd in list)
            //{
            //    ActiveClassInstructorDataObject obj = instructorHistories.Where(ih => ih.ActiveClassId == acd.tb_ActiveClass.activecs_ID).FirstOrDefault();

            //    // if no history records found for this class & the active class day teacher matches the selected teacher then add it to the results
            //    if (obj == null && acd.tb_ActiveClass.activecs_Teacher == teacherID)
            //    {
            //        filteredList.Add(acd);
            //        continue;
            //    }

            //    // if history record found for this class for this date & history record matches the selected teacher then add it to the results
            //    if (obj != null && obj.InstructorId == teacherID)
            //    {
            //        filteredList.Add(acd);
            //        continue;
            //    }              
            //}           

            //return filteredList;           
        }

        private List<tb_ActiveClassDays> SetInstructors(List<tb_ActiveClassDays> classes, DateTime asOfDate)
        {
            List<tb_ActiveClassDays> updatedClasses = new List<tb_ActiveClassDays>();
            foreach (var cls in classes)
            {
                ActiveClassInstructorDataObject history = repository.GetInstructorHistoryAsOfDate(cls.activecs_ID.Value, asOfDate);
                cls.InstructorId = (history != null) ? history.InstructorId : cls.tb_ActiveClass.activecs_Teacher.Value;
                updatedClasses.Add(cls);
            }            

            return updatedClasses;
        }

        //public Guid GetActiveClassInstructorIdAsOfDate(Guid activeClassId, DateTime asOfDate)
        //{
        //    Guid instructorId = Guid.Empty;
        //    bool instructorFound = false;
        //    //repository.
        //    //IEnumerable<ActiveClassInstructorDataObject> histories = GetActiveClassInstructorHistoriesForClass(activeClassId);
        //    //if (histories != null && histories.Count() > 0)
        //    //{
        //    //    foreach (var hist in histories.OrderBy(h => h.BeginDate))
        //    //    {
        //    //        if (hist.EndDate.HasValue)
        //    //        {
        //    //            if (asOfDate.CompareTo(hist.BeginDate.Date) >= 0 && asOfDate.CompareTo(hist.EndDate.Value.Date) <= 0)
        //    //            {
        //    //                instructorId = hist.InstructorId;
        //    //                instructorFound = true;
        //    //                break;
        //    //            }
        //    //        }
        //    //        else
        //    //        {
        //    //            if (asOfDate.CompareTo(hist.BeginDate.Date) >= 0)
        //    //            {
        //    //                instructorId = hist.InstructorId;
        //    //                instructorFound = true;
        //    //                break;
        //    //            }
        //    //        }
        //    //    }
        //    //}

        //    if (!instructorFound)
        //    {
        //        instructorId = GetInstructorForClassId(activeClassId);
        //    }

        //    return instructorId;
        //}

        //private Guid GetInstructorForClassId(Guid classId)
        //{
        //    Guid instructorId = Guid.Empty;
        //    ActiveClassBll acBll = new ActiveClassBll();
        //    tb_ActiveClass ac = acBll.GetByID(classId);
        //    if (ac != null && ac.activecs_Teacher.HasValue) { instructorId = ac.activecs_Teacher.Value; }
        //    return instructorId;
        //}

        //public IEnumerable<ActiveClassInstructorDataObject> GetActiveClassInstructorHistoriesForInstructorAsOfDate(Guid instructorId, DateTime asOfDate)
        //{
        //    List<ActiveClassInstructorDataObject> instructorHistories = new List<ActiveClassInstructorDataObject>();
        //    IEnumerable<ActiveClassInstructorDataObject> dataObjects = repository.GetInstructorHistoryForInstructor(instructorId);
        //    foreach (var dataObject in dataObjects)
        //    {
        //        if (dataObject.EndDate.HasValue)
        //        {
        //            if (asOfDate.CompareTo(dataObject.BeginDate) >= 0 && asOfDate.CompareTo(dataObject.EndDate) <= 0)
        //            {
        //                instructorHistories.Add(dataObject);
        //            }
        //        }
        //        else
        //        {
        //            if (asOfDate.CompareTo(dataObject.BeginDate) >= 0)
        //            {
        //                instructorHistories.Add(dataObject);
        //            }
        //        }
        //    }
        //    return instructorHistories;
        //}

        //public IEnumerable<ActiveClassInstructorDataObject> GetActiveClassInstructorHistoriesAsOfDate(DateTime asOfDate)
        //{
        //    return repository.GetInstructorHistoriesAsOfDate(asOfDate);
        //}

        public List<tb_ActiveClassDays> GetStudentCurrentPast(Guid studentID)
        {
            List<tb_ActiveClassDays> list = new List<tb_ActiveClassDays>();
            list = (from c in ctx.tb_ActiveClassDays
                    join cl in ctx.tb_StudentBooks
                    on c.activecs_ID equals cl.activecs_ID
                    where cl.stud_ID == studentID && (!cl.studch_IsWithdrawal.HasValue || !cl.studch_IsWithdrawal.Value || ((cl.studch_WidthdrawalData.Value).CompareTo(DateTime.Today) >= 0))
                    select c).Distinct().ToList<tb_ActiveClassDays>();
            return list;
        }

        public List<tb_ActiveClassDays> GetStudentWithdrawaled(Guid studentID)
        {
            List<tb_ActiveClassDays> list = new List<tb_ActiveClassDays>();
            list = (from c in ctx.tb_ActiveClassDays
                    join cl in ctx.tb_StudentBooks
                    on c.activecs_ID equals cl.activecs_ID
                    where cl.stud_ID == studentID && (cl.studch_IsWithdrawal.HasValue && cl.studch_IsWithdrawal.Value && (cl.studch_WidthdrawalData.Value).CompareTo(DateTime.Today) < 0)
                    select c).Distinct().ToList<tb_ActiveClassDays>();
            return list;
        }
    }
}
