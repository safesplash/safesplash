﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class MonthlyReportBll
    {
        protected SafeSplashDBDataContext ctx = null;

        public MonthlyReportBll()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public MonthlyReportBll(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }

        public int GetMonthStartSportsFilled(int Year, int Month)
        {
            DateTime currentDate = new DateTime(Year, Month, 1).AddMonths(1).AddSeconds(-1);
            DateTime dtFirstDayofMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
            // Class Spots Filled
            List<tb_StudentBooks> studentBooks = new List<tb_StudentBooks>();

            studentBooks = (from c in ctx.tb_StudentBooks
                            where ((DateTime)c.studch_JoinData).CompareTo(dtFirstDayofMonth) <= 0 && (c.studch_IsWithdrawal == null || c.studch_IsWithdrawal == false || (c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofMonth) > 0))
                            select c).Distinct().ToList<tb_StudentBooks>();
            if (studentBooks != null)
                return totalSpots(studentBooks);
            else
                return 0;
        }

        public tb_MonthlyWithdrawalReport GetMWR(Guid locaID, Guid reasonID, int Year, int Month)
        {
            DateTime currentDate = new DateTime(Year, Month, 1).AddMonths(1).AddSeconds(-1);
            DateTime dtFirstDayofMonth = new DateTime(currentDate.Year, currentDate.Month, 1);

            // Spots Withdraw
            List<tb_StudentBooks> SpotsWithdrawAll = new List<tb_StudentBooks>();
            if (locaID == Guid.Empty)
            {
                SpotsWithdrawAll = (from c in ctx.tb_StudentBooks
                                    where c.studch_IsWithdrawal != null && c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) <= 0 && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofMonth) >= 0
                                    select c).Distinct().ToList<tb_StudentBooks>();
            }
            else
            {
                SpotsWithdrawAll = (from c in ctx.tb_StudentBooks
                                    join cl in ctx.tb_ActiveClass
                                    on c.activecs_ID equals cl.activecs_ID
                                    where cl.loca_ID == locaID && (c.studch_IsWithdrawal != null && c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) <= 0 && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofMonth) >= 0)
                                    select c).Distinct().ToList<tb_StudentBooks>();
            }
            int iSpotsWithdrawAll = totalSpots(SpotsWithdrawAll);
            
            WithdrawalReasonBll wrbll = new WithdrawalReasonBll();
            tb_WithdrawalReason reason = wrbll.GetByID(reasonID);
            if (reason != null)
            {
                tb_MonthlyWithdrawalReport monthlyWithdrawalReport = new tb_MonthlyWithdrawalReport();
                monthlyWithdrawalReport.mwr_ID = Guid.NewGuid();
                // Spots Withdraw
                List<tb_StudentBooks> SpotsWithdrawList = new List<tb_StudentBooks>();
                if (locaID == Guid.Empty)
                {
                    SpotsWithdrawList = (from c in ctx.tb_StudentBooks
                                         where c.reason_ID == reason.reason_ID && c.studch_IsWithdrawal != null && c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) <= 0 && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofMonth) >= 0
                                         select c).Distinct().ToList<tb_StudentBooks>();
                }
                else
                {
                    SpotsWithdrawList = (from c in ctx.tb_StudentBooks
                                         join cl in ctx.tb_ActiveClass
                                            on c.activecs_ID equals cl.activecs_ID
                                         where cl.loca_ID == locaID && (c.reason_ID == reason.reason_ID && c.studch_IsWithdrawal != null && c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) <= 0 && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofMonth) >= 0)
                                         select c).Distinct().ToList<tb_StudentBooks>();
                }
                int iSpotsWithdraw = totalSpots(SpotsWithdrawList);
                monthlyWithdrawalReport.mwr_Date = currentDate;
                monthlyWithdrawalReport.mwr_SpotsWithdrawn = iSpotsWithdraw;
                if (iSpotsWithdrawAll == 0)
                {
                    monthlyWithdrawalReport.mwr_PercentSpotsWithdrawn = 0;
                }
                else
                {
                    monthlyWithdrawalReport.mwr_PercentSpotsWithdrawn = (decimal)iSpotsWithdraw / (decimal)iSpotsWithdrawAll;
                }
                monthlyWithdrawalReport.reason_ID = reason.reason_ID;
                return monthlyWithdrawalReport;
            }
            return null;
        }

        public int GetMonthlyWithdrawalNumber(Guid locaID, int Year, int Month)
        {
            DateTime currentDate = new DateTime(Year, Month, 1).AddMonths(1).AddSeconds(-1);
            DateTime dtFirstDayofMonth = new DateTime(currentDate.Year, currentDate.Month, 1);

            // Spots Withdraw
            List<tb_StudentBooks> SpotsWithdrawAll = new List<tb_StudentBooks>();
            if (locaID == Guid.Empty)
            {
                SpotsWithdrawAll = (from c in ctx.tb_StudentBooks
                                    where c.studch_IsWithdrawal != null && c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) <= 0 && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofMonth) >= 0
                                    select c).Distinct().ToList<tb_StudentBooks>();
            }
            else
            {
                SpotsWithdrawAll = (from c in ctx.tb_StudentBooks
                                    join cl in ctx.tb_ActiveClass
                                    on c.activecs_ID equals cl.activecs_ID
                                    where cl.loca_ID == locaID && (c.studch_IsWithdrawal != null && c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) <= 0 && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofMonth) >= 0)
                                    select c).Distinct().ToList<tb_StudentBooks>();
            }
            int iSpotsWithdrawAll = totalSpots(SpotsWithdrawAll);
            return iSpotsWithdrawAll;
        }
        public void CreateMontyWithdrawalReport(int Year, int Month)
        {
            DateTime currentDate = new DateTime(Year, Month, 1).AddMonths(1).AddSeconds(-1);
            DateTime dtFirstDayofMonth = new DateTime(currentDate.Year, currentDate.Month, 1);

            // Spots Withdraw
                List<tb_StudentBooks> SpotsWithdrawAll = new List<tb_StudentBooks>();

                SpotsWithdrawAll = (from c in ctx.tb_StudentBooks
                                     where c.studch_IsWithdrawal != null && c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) <= 0 && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofMonth) >= 0
                                     select c).Distinct().ToList<tb_StudentBooks>();
                int iSpotsWithdrawAll = SpotsWithdrawAll.Count;

            // Get withdrawal Reason List
            List<tb_WithdrawalReason> WithdrawReasonList = new List<tb_WithdrawalReason>();
            WithdrawReasonList = ctx.GetTable<tb_WithdrawalReason>().ToList<tb_WithdrawalReason>();
            if (WithdrawReasonList != null)
            {
                foreach (tb_WithdrawalReason reason in WithdrawReasonList)
                {
                    tb_MonthlyWithdrawalReport monthlyWithdrawalReport = new tb_MonthlyWithdrawalReport();
                    monthlyWithdrawalReport.mwr_ID = Guid.NewGuid();
                    // Spots Withdraw
                    List<tb_StudentBooks> SpotsWithdrawList = new List<tb_StudentBooks>();

                    SpotsWithdrawList = (from c in ctx.tb_StudentBooks
                                         where c.reason_ID == reason.reason_ID && c.studch_IsWithdrawal != null && c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) <= 0 && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofMonth) >= 0
                                         select c).Distinct().ToList<tb_StudentBooks>();
                    int iSpotsWithdraw = SpotsWithdrawList.Count;
                    monthlyWithdrawalReport.mwr_Date = currentDate;
                    monthlyWithdrawalReport.mwr_SpotsWithdrawn = iSpotsWithdraw;
                    monthlyWithdrawalReport.mwr_PercentSpotsWithdrawn = (decimal)iSpotsWithdraw / (decimal)iSpotsWithdrawAll;
                    monthlyWithdrawalReport.reason_ID = reason.reason_ID;

                    ctx.tb_MonthlyWithdrawalReports.InsertOnSubmit(monthlyWithdrawalReport);
                    ctx.SubmitChanges();
                }
            }
        }

        public tb_MonthlyEnrollReport GetMER(Guid locaID, int Year, int Month)
        {
            DateTime currentDate = new DateTime(Year, Month, 1).AddMonths(1).AddSeconds(-1);
            DateTime dtFirstDayofMonth = new DateTime(currentDate.Year, currentDate.Month, 1);

            // Class Spots Filled
            List<tb_StudentBooks> studentBooks = new List<tb_StudentBooks>();
            if (locaID == Guid.Empty)
            {
                studentBooks = (from c in ctx.tb_StudentBooks
                                where ((DateTime)c.studch_JoinData).CompareTo(currentDate) <= 0 && (c.studch_IsWithdrawal == null || c.studch_IsWithdrawal == false || (c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) > 0))
                                select c).Distinct().ToList<tb_StudentBooks>();
            }
            else
            {
                studentBooks = (from c in ctx.tb_StudentBooks
                                join cl in ctx.tb_ActiveClass
                                on c.activecs_ID equals cl.activecs_ID
                                where cl.loca_ID == locaID && (((DateTime)c.studch_JoinData).CompareTo(currentDate) <= 0 && (c.studch_IsWithdrawal == null || c.studch_IsWithdrawal == false || (c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) > 0)))
                                select c).Distinct().ToList<tb_StudentBooks>();
            }
            int iClassSportsFilled = totalSpots(studentBooks);
            // Families Enrolled
            List<tb_Client> clients = new List<tb_Client>();
            if (locaID == Guid.Empty)
            {
                clients = (from c in ctx.tb_Clients
                           join cb in ctx.tb_ClientStudent
                               on c.client_ID equals cb.client_ID
                           join cl in ctx.tb_StudentBooks
                           on cb.stud_ID equals cl.stud_ID
                           where ((DateTime)cl.studch_JoinData).CompareTo(currentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(currentDate) > 0))
                           select c).Distinct().ToList<tb_Client>();
            }
            else
            {
                clients = (from c in ctx.tb_Clients
                           join cb in ctx.tb_ClientStudent
                               on c.client_ID equals cb.client_ID
                           join cl in ctx.tb_StudentBooks
                           on cb.stud_ID equals cl.stud_ID
                           join cx in ctx.tb_ActiveClass
                           on cl.activecs_ID equals cx.activecs_ID
                           where cx.loca_ID == locaID && (((DateTime)cl.studch_JoinData).CompareTo(currentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(currentDate) > 0)))
                           select c).Distinct().ToList<tb_Client>();
            }
            int iFamiliesEnrolled = clients.Count;

            decimal dSpotsFilledPerFamily = iFamiliesEnrolled == 0 ? 0 : (decimal)iClassSportsFilled / (decimal)iFamiliesEnrolled;

            // Spots Per Child

            List<tb_ClientStudent> clientStudents = new List<tb_ClientStudent>();

            if (locaID == Guid.Empty)
            {
                clientStudents = (from c in ctx.tb_ClientStudent
                                  join cl in ctx.tb_StudentBooks
                                  on c.stud_ID equals cl.stud_ID
                                  where ((DateTime)cl.studch_JoinData).CompareTo(currentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(currentDate) > 0))
                                  select c).Distinct().ToList<tb_ClientStudent>();
            }
            else
            {
                clientStudents = (from c in ctx.tb_ClientStudent
                                  join cl in ctx.tb_StudentBooks
                                  on c.stud_ID equals cl.stud_ID
                                  join cx in ctx.tb_ActiveClass
                                    on cl.activecs_ID equals cx.activecs_ID
                                  where cx.loca_ID == locaID && (((DateTime)cl.studch_JoinData).CompareTo(currentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(currentDate) > 0)))
                                  select c).Distinct().ToList<tb_ClientStudent>();
            }
            int iStudentFilled = clientStudents.Count;

            decimal dSportsPerChild = iStudentFilled == 0 ? 0 : (decimal)iClassSportsFilled / (decimal)iStudentFilled;

            // % of Capacity
            List<tb_ActiveClass> activeClassList = new List<tb_ActiveClass>();
            if (locaID == Guid.Empty)
            {
                activeClassList = (from c in ctx.tb_ActiveClass
                                   join cl in ctx.tb_StudentBooks
                                      on c.activecs_ID equals cl.activecs_ID
                                   where ((DateTime)cl.studch_JoinData).CompareTo(currentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(currentDate) > 0))
                                   select c).Distinct().ToList<tb_ActiveClass>();
            }
            else
            {
                activeClassList = (from c in ctx.tb_ActiveClass
                                   join cl in ctx.tb_StudentBooks
                                      on c.activecs_ID equals cl.activecs_ID
                                   join cx in ctx.tb_ActiveClass
                                   on cl.activecs_ID equals cx.activecs_ID
                                   where  cx.loca_ID == locaID && (((DateTime)cl.studch_JoinData).CompareTo(currentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(currentDate) > 0)))
                                   select c).Distinct().ToList<tb_ActiveClass>();
            }
            int iCapacity = 0;
            if (activeClassList != null)
            {
                foreach (tb_ActiveClass activeClass in activeClassList)
                {

                    tb_Class classType = ctx.tb_Class.SingleOrDefault<tb_Class>(d => d.class_ID == (Guid)activeClass.class_ID);

                    if (classType != null && classType.class_StudentCapacity != null)
                    {
                        if (classType.class_IsPrivate.Value)
                        {
                            iCapacity += (classType.class_StudentCapacity.Value * 4);
                        }
                        else if (classType.class_IsSemiPrivate.Value)
                        {
                            iCapacity += (classType.class_StudentCapacity.Value * 2);
                        }
                        else
                        {
                            iCapacity += classType.class_StudentCapacity.Value;
                        }
                    }
                }
            }
            decimal dPrecentCapacity = iCapacity == 0 ? 0 : (decimal)iClassSportsFilled / (decimal)iCapacity;

            // New Spots Filled

            List<tb_StudentBooks> currentMonthBooks = new List<tb_StudentBooks>();
            if (locaID == Guid.Empty)
            {
                currentMonthBooks = (from c in ctx.tb_StudentBooks
                                     where ((DateTime)c.studch_JoinData).CompareTo(dtFirstDayofMonth) >= 0 && ((DateTime)c.studch_JoinData).CompareTo(currentDate) <= 0 && (c.studch_IsWithdrawal == null || c.studch_IsWithdrawal == false || (c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) > 0))
                                     select c).Distinct().ToList<tb_StudentBooks>();
            }
            else
            {
                currentMonthBooks = (from c in ctx.tb_StudentBooks
                                     join cl in ctx.tb_ActiveClass
                                        on c.activecs_ID equals cl.activecs_ID
                                     where cl.loca_ID == locaID && (((DateTime)c.studch_JoinData).CompareTo(dtFirstDayofMonth) >= 0 && ((DateTime)c.studch_JoinData).CompareTo(currentDate) <= 0 && (c.studch_IsWithdrawal == null || c.studch_IsWithdrawal == false || (c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) > 0)))
                                     select c).Distinct().ToList<tb_StudentBooks>();
            }
            int iNewSpotsFilled = currentMonthBooks.Count;

            // New Regisgtrations
            List<tb_Client> clientsList = new List<tb_Client>();
            if (locaID == Guid.Empty)
            {
                clientsList = (from c in ctx.tb_Clients
                               where c.client_CreatedDate != null && ((DateTime)c.client_CreatedDate).CompareTo(dtFirstDayofMonth) >= 0 && ((DateTime)c.client_CreatedDate).CompareTo(currentDate) <= 0
                               select c).Distinct().ToList<tb_Client>();
            }
            else
            {
                clientsList = (from c in ctx.tb_Clients
                               join cl in ctx.tb_ClientLocation
                               on c.client_ID equals cl.client_ID
                               where cl.loca_ID == locaID && (c.client_CreatedDate != null && ((DateTime)c.client_CreatedDate).CompareTo(dtFirstDayofMonth) >= 0 && ((DateTime)c.client_CreatedDate).CompareTo(currentDate) <= 0)
                               select c).Distinct().ToList<tb_Client>();
            }
            int iNewRegisgtrations = clientsList.Count;

            // # of RIP
            List<tb_Client> RIPClientsList = new List<tb_Client>();
            List<tb_Client> seRIPClientList = new List<tb_Client>();
            if (locaID == Guid.Empty)
            {
                RIPClientsList = (from c in ctx.tb_Clients
                                  where c.client_CreatedDate != null && ((DateTime)c.client_CreatedDate).CompareTo(currentDate) <= 0 && (c.client_IsCompleteRegister == null || c.client_IsCompleteRegister == false)
                                  select c).Distinct().ToList<tb_Client>();
                seRIPClientList = (from c in ctx.tb_Clients
                                   join mbr in ctx.aspnet_Membership
                                   on c.user_ID equals mbr.UserId
                                   where mbr.CreateDate > currentDate
                                   select c).Distinct().ToList<tb_Client>();
            }
            else
            {
                RIPClientsList = (from c in ctx.tb_Clients
                                  join cl in ctx.tb_ClientLocation
                                    on c.client_ID equals cl.client_ID
                                  where cl.loca_ID == locaID && (c.client_CreatedDate != null && ((DateTime)c.client_CreatedDate).CompareTo(currentDate) <= 0 && (c.client_IsCompleteRegister == null || c.client_IsCompleteRegister == false))
                                  select c).Distinct().ToList<tb_Client>();
                seRIPClientList = (from c in ctx.tb_Clients
                                   join mbr in ctx.aspnet_Membership
                                   on c.user_ID equals mbr.UserId
                                   join cl1 in ctx.tb_ClientLocation
                                   on c.client_ID equals cl1.client_ID
                                   where mbr.CreateDate > currentDate && cl1.loca_ID == locaID
                                   select c).Distinct().ToList<tb_Client>();
            }
            
            int iRIP = 0;
            if (RIPClientsList != null && seRIPClientList != null)
            {
                iRIP = RIPClientsList.Count + seRIPClientList.Count;
            }

            // Spots Withdraw
            List<tb_StudentBooks> SpotsWithdrawList = new List<tb_StudentBooks>();
            if (locaID == Guid.Empty)
            {
                SpotsWithdrawList = (from c in ctx.tb_StudentBooks
                                     where c.studch_IsWithdrawal != null && c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) <= 0 && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofMonth) >= 0
                                     select c).Distinct().ToList<tb_StudentBooks>();
            }
            else
            {
                SpotsWithdrawList = (from c in ctx.tb_StudentBooks
                                     join cl in ctx.tb_ActiveClass
                                        on c.activecs_ID equals cl.activecs_ID
                                     where cl.loca_ID == locaID && (c.studch_IsWithdrawal != null && c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) <= 0 && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofMonth) >= 0)
                                     select c).Distinct().ToList<tb_StudentBooks>();
            }
            int iSpotsWithdraw = SpotsWithdrawList.Count;

            // % of Spots Withdrawn
            decimal dPrecentSportsWithdrawn = iClassSportsFilled == 0 ? 0 : (decimal)iSpotsWithdraw / (decimal)iClassSportsFilled;

            tb_MonthlyEnrollReport monthlyEnrollReport = new tb_MonthlyEnrollReport();
            monthlyEnrollReport.mer_ID = Guid.NewGuid();
            monthlyEnrollReport.mer_Date = currentDate;
            monthlyEnrollReport.mer_ClassSpotsFilled = iClassSportsFilled;
            monthlyEnrollReport.mer_FamiliesEnrolled = iFamiliesEnrolled;
            monthlyEnrollReport.mer_SpotsFilledPerFamily = dSpotsFilledPerFamily;
            monthlyEnrollReport.mer_SpotsPerChild = dSportsPerChild;
            monthlyEnrollReport.mer_PercentCapacity = dPrecentCapacity;
            monthlyEnrollReport.mer_NewSpotsFilled = iNewSpotsFilled;
            monthlyEnrollReport.mer_NewRegisgtrations = iNewRegisgtrations;
            monthlyEnrollReport.mer_RIP = iRIP;
            monthlyEnrollReport.mer_SpotsWithdrawn = iSpotsWithdraw;
            monthlyEnrollReport.mer_PercentSpotsWithdrawn = dPrecentSportsWithdrawn;
            return monthlyEnrollReport;
        }

        public tb_MonthlyEnrollReport GetMonthlyEnrollmentReport(List<string> locaIds, int Year, int Month, tb_MonthlyEnrollReport prev)
        {
            DateTime currentDate = new DateTime(Year, Month, 1).AddMonths(1).AddSeconds(-1);
            DateTime dtFirstDayofMonth = new DateTime(currentDate.Year, currentDate.Month, 1);

            // Class Spots Filled
            List<tb_StudentBooks> studentBooks = new List<tb_StudentBooks>();
            if (locaIds != null && locaIds.Count == 1 && string.IsNullOrEmpty(locaIds[0]))
            {
                studentBooks = (from client in ctx.tb_Clients
                                join student in ctx.tb_ClientStudent on client.client_ID equals student.client_ID
                                join studentbook in ctx.tb_StudentBooks on student.stud_ID equals studentbook.stud_ID
                                join active in ctx.tb_ActiveClass on studentbook.activecs_ID equals active.activecs_ID
                                where
                                (!active.activecs_EndDay.HasValue || active.activecs_EndDay.Value.Date >= dtFirstDayofMonth.Date)
                                && studentbook.studch_JoinData.Value.Date.CompareTo(currentDate.Date) <= 0
                                && (!studentbook.studch_IsWithdrawal.HasValue || !studentbook.studch_IsWithdrawal.Value || studentbook.studch_WidthdrawalData.Value.Date.CompareTo(dtFirstDayofMonth.Date) > 0)
                                select studentbook).Distinct().ToList<tb_StudentBooks>();
            }
            else
            {
                //studentBooks = (from c in ctx.tb_StudentBooks
                //                join cl in ctx.tb_ActiveClass
                //                on c.activecs_ID equals cl.activecs_ID
                //                where locaIds.Contains(cl.loca_ID.ToString())
                //                && c.studch_JoinData.Value.CompareTo(currentDate) <= 0 
                //                && (!c.studch_IsWithdrawal.HasValue || !c.studch_IsWithdrawal.Value || c.studch_WidthdrawalData.Value.CompareTo(currentDate) > 0)
                //                select c).Distinct().ToList<tb_StudentBooks>();
                studentBooks = (from client in ctx.tb_Clients
                                join student in ctx.tb_ClientStudent on client.client_ID equals student.client_ID
                                join studentbook in ctx.tb_StudentBooks on student.stud_ID equals studentbook.stud_ID
                                join active in ctx.tb_ActiveClass on studentbook.activecs_ID equals active.activecs_ID
                                where locaIds.Contains(active.loca_ID.ToString())
                                && (!active.activecs_EndDay.HasValue || active.activecs_EndDay.Value.Date >= dtFirstDayofMonth.Date)
                                && studentbook.studch_JoinData.Value.Date.CompareTo(currentDate.Date) <= 0
                                && (!studentbook.studch_IsWithdrawal.HasValue || !studentbook.studch_IsWithdrawal.Value || studentbook.studch_WidthdrawalData.Value.Date.CompareTo(dtFirstDayofMonth.Date) > 0)
                                select studentbook).Distinct().ToList<tb_StudentBooks>();
            }
            int iClassSportsFilled = totalSpots(studentBooks);
            // Families Enrolled
            List<tb_Client> clients = new List<tb_Client>();
            if (locaIds != null && locaIds.Count == 1 && string.IsNullOrEmpty(locaIds[0]))
            {
                clients = (from c in ctx.tb_Clients
                           join cb in ctx.tb_ClientStudent on c.client_ID equals cb.client_ID
                           join cl in ctx.tb_StudentBooks on cb.stud_ID equals cl.stud_ID
                           join active in ctx.tb_ActiveClass on cl.activecs_ID equals active.activecs_ID
                           where 
                           (!active.activecs_EndDay.HasValue || active.activecs_EndDay.Value.Date >= dtFirstDayofMonth.Date)
                           && cl.studch_JoinData.HasValue && cl.studch_JoinData.Value.Date.CompareTo(currentDate.Date) <= 0 
                           && (!cl.studch_IsWithdrawal.HasValue || cl.studch_IsWithdrawal.Value || cl.studch_WidthdrawalData.Value.Date.CompareTo(dtFirstDayofMonth.Date) >= 0)
                           select c).Distinct().ToList<tb_Client>();
            }
            else
            {
                clients = (from c in ctx.tb_Clients
                           join cb in ctx.tb_ClientStudent on c.client_ID equals cb.client_ID
                           join cl in ctx.tb_StudentBooks on cb.stud_ID equals cl.stud_ID
                           join cx in ctx.tb_ActiveClass on cl.activecs_ID equals cx.activecs_ID
                           where 
                           locaIds.Contains(cx.loca_ID.ToString()) 
                           && (!cx.activecs_EndDay.HasValue || cx.activecs_EndDay.Value.Date >= dtFirstDayofMonth.Date) 
                           && cl.studch_JoinData.Value.Date.CompareTo(currentDate.Date) <= 0 
                           && (!cl.studch_IsWithdrawal.HasValue || cl.studch_IsWithdrawal.Value || cl.studch_WidthdrawalData.Value.Date.CompareTo(dtFirstDayofMonth.Date) >= 0)
                           select c).Distinct().ToList<tb_Client>();
            }
            int iFamiliesEnrolled = clients.Count;

            decimal dSpotsFilledPerFamily = iFamiliesEnrolled == 0 ? 0 : (decimal)iClassSportsFilled / (decimal)iFamiliesEnrolled;

            // Spots Per Child

            //List<tb_ClientStudent> clientStudents = new List<tb_ClientStudent>();

            //if (locaIds != null && locaIds.Count == 1 && string.IsNullOrEmpty(locaIds[0]))
            //{
            //    clientStudents = (from c in ctx.tb_ClientStudent
            //                      join cl in ctx.tb_StudentBooks
            //                      on c.stud_ID equals cl.stud_ID
            //                      where ((DateTime)cl.studch_JoinData).CompareTo(currentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(currentDate) > 0))
            //                      select c).Distinct().ToList<tb_ClientStudent>();
            //}
            //else
            //{
            //    clientStudents = (from c in ctx.tb_ClientStudent
            //                      join cl in ctx.tb_StudentBooks
            //                      on c.stud_ID equals cl.stud_ID
            //                      join cx in ctx.tb_ActiveClass
            //                        on cl.activecs_ID equals cx.activecs_ID
            //                      where locaIds.Contains(cx.loca_ID.ToString()) && (((DateTime)cl.studch_JoinData).CompareTo(currentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(currentDate) > 0)))
            //                      select c).Distinct().ToList<tb_ClientStudent>();
            //}
            //int iStudentFilled = clientStudents.Count;
            int iStudentFilled = GetStudentNumber(studentBooks);

            decimal dSportsPerChild = iStudentFilled == 0 ? 0 : (decimal)iClassSportsFilled / (decimal)iStudentFilled;

            // % of Capacity
            List<tb_ActiveClass> activeClassList = new List<tb_ActiveClass>();
            if (locaIds != null && locaIds.Count == 1 && string.IsNullOrEmpty(locaIds[0]))
            {
                //activeClassList = (from c in ctx.tb_ActiveClass
                //                   join cl in ctx.tb_StudentBooks
                //                      on c.activecs_ID equals cl.activecs_ID
                //                   where ((DateTime)cl.studch_JoinData).CompareTo(currentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(currentDate) > 0))
                //                   select c).Distinct().ToList<tb_ActiveClass>();
                activeClassList = (from active in ctx.tb_ActiveClass
                                   where 
                                   (!active.activecs_EndDay.HasValue || active.activecs_EndDay.Value.Date >= dtFirstDayofMonth.Date)
                                   && (active.activecs_StartDay.HasValue && active.activecs_StartDay.Value.Date <= currentDate.Date)
                                   select active).Distinct().ToList<tb_ActiveClass>();
            }
            else
            {
                //activeClassList = (from c in ctx.tb_ActiveClass
                //                   join cl in ctx.tb_StudentBooks
                //                      on c.activecs_ID equals cl.activecs_ID
                //                   join cx in ctx.tb_ActiveClass
                //                   on cl.activecs_ID equals cx.activecs_ID
                //                   where locaIds.Contains(cx.loca_ID.ToString()) && (((DateTime)cl.studch_JoinData).CompareTo(currentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(currentDate) > 0)))
                //                   select c).Distinct().ToList<tb_ActiveClass>();
                activeClassList = (from active in ctx.tb_ActiveClass
                                   where
                                   (!active.activecs_EndDay.HasValue || active.activecs_EndDay.Value.Date >= dtFirstDayofMonth.Date)
                                   && (active.activecs_StartDay.HasValue && active.activecs_StartDay.Value.Date <= currentDate.Date)
                                   && locaIds.Contains(active.loca_ID.ToString())
                                   select active).Distinct().ToList<tb_ActiveClass>();
            }
            int iCapacity = 0;
            if (activeClassList != null)
            {
                foreach (tb_ActiveClass activeClass in activeClassList)
                {

                    tb_Class classType = ctx.tb_Class.SingleOrDefault<tb_Class>(d => d.class_ID == (Guid)activeClass.class_ID);

                    if (classType != null && classType.class_StudentCapacity != null)
                    {
                        if (classType.class_IsPrivate.Value)
                        {
                            iCapacity += (classType.class_StudentCapacity.Value * 4);
                        }
                        else if (classType.class_IsSemiPrivate.Value)
                        {
                            iCapacity += (classType.class_StudentCapacity.Value * 2);
                        }
                        else
                        {
                            iCapacity += classType.class_StudentCapacity.Value;
                        }
                    }
                }
            }
            decimal dPrecentCapacity = 0;
            if (iCapacity != 0) dPrecentCapacity = ((decimal)iCapacity - (decimal)iClassSportsFilled) / (decimal)iCapacity;

            // New Spots Filled

            List<tb_StudentBooks> currentMonthBooks = new List<tb_StudentBooks>();
            //if (locaIds != null && locaIds.Count == 1 && string.IsNullOrEmpty(locaIds[0]))
            //{
                //currentMonthBooks = (from c in ctx.tb_StudentBooks
                //                     where ((DateTime)c.studch_JoinData).CompareTo(dtFirstDayofMonth) >= 0 && ((DateTime)c.studch_JoinData).CompareTo(currentDate) <= 0 && (c.studch_IsWithdrawal == null || c.studch_IsWithdrawal == false || (c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) > 0))
                //                     select c).Distinct().ToList<tb_StudentBooks>();
                currentMonthBooks = (from studentbook in studentBooks
                                     where studentbook.studch_JoinData.Value.Date >= dtFirstDayofMonth.Date
                                     select studentbook).ToList<tb_StudentBooks>();
            //}
            //else
            //{
                //currentMonthBooks = (from c in ctx.tb_StudentBooks
                //                     join cl in ctx.tb_ActiveClass
                //                        on c.activecs_ID equals cl.activecs_ID
                //                     where locaIds.Contains(cl.loca_ID.ToString()) && (((DateTime)c.studch_JoinData).CompareTo(dtFirstDayofMonth) >= 0 && ((DateTime)c.studch_JoinData).CompareTo(currentDate) <= 0 && (c.studch_IsWithdrawal == null || c.studch_IsWithdrawal == false || (c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) > 0)))
                //                     select c).Distinct().ToList<tb_StudentBooks>();

            //}
            int iNewSpotsFilled = currentMonthBooks.Count;

            // New Regisgtrations
            List<tb_Client> clientsList = new List<tb_Client>();
            if (locaIds != null && locaIds.Count == 1 && string.IsNullOrEmpty(locaIds[0]))
            {
                //clientsList = (from c in ctx.tb_Clients
                //               where c.client_CreatedDate != null && ((DateTime)c.client_CreatedDate).CompareTo(dtFirstDayofMonth) >= 0 && ((DateTime)c.client_CreatedDate).CompareTo(currentDate) <= 0
                //               select c).Distinct().ToList<tb_Client>();
                clientsList = (from client in ctx.tb_Clients
                               join clientlocation in ctx.tb_ClientLocation on client.client_ID equals clientlocation.client_ID
                               where
                               client.client_CreatedDate.HasValue
                               && client.client_CreatedDate.Value.Date >= dtFirstDayofMonth.Date
                               && client.client_CreatedDate.Value.Date <= currentDate.Date
                               select client).Distinct().ToList<tb_Client>();
            }
            else
            {
                //clientsList = (from c in ctx.tb_Clients
                //               join cl in ctx.tb_ClientLocation
                //               on c.client_ID equals cl.client_ID
                //               where locaIds.Contains(cl.loca_ID.ToString()) && (c.client_CreatedDate != null && ((DateTime)c.client_CreatedDate).CompareTo(dtFirstDayofMonth) >= 0 && ((DateTime)c.client_CreatedDate).CompareTo(currentDate) <= 0)
                //               select c).Distinct().ToList<tb_Client>();
                clientsList = (from client in ctx.tb_Clients
                               join clientlocation in ctx.tb_ClientLocation on client.client_ID equals clientlocation.client_ID
                               where
                               client.client_CreatedDate.HasValue
                               && client.client_CreatedDate.Value.Date >= dtFirstDayofMonth.Date
                               && client.client_CreatedDate.Value.Date <= currentDate.Date
                               && locaIds.Contains(clientlocation.loca_ID.Value.ToString())
                               select client).Distinct().ToList<tb_Client>();
            }
            int iNewRegisgtrations = clientsList.Count;

            // # of RIP
            //List<tb_Client> RIPClientsList = new List<tb_Client>();
            List<tb_ClientStudent> RIPClientsList = new List<tb_ClientStudent>();
            if (locaIds != null && locaIds.Count == 1 && string.IsNullOrEmpty(locaIds[0]))
            {
                //RIPClientsList = (from c in ctx.tb_Clients
                //                  where c.client_CreatedDate != null && ((DateTime)c.client_CreatedDate).CompareTo(currentDate) <= 0 && (c.client_IsCompleteRegister == null || c.client_IsCompleteRegister == false)
                //                  select c).Distinct().ToList<tb_Client>();
                RIPClientsList = (from client in ctx.tb_Clients
                                  join student in ctx.tb_ClientStudent on client.client_ID equals student.client_ID
                                  where 
                                  client.client_CreatedDate.HasValue
                                  && client.client_CreatedDate.Value.Date <= currentDate.Date
                                  && (client.client_IsCompleteRegister.HasValue || !client.client_IsCompleteRegister.Value)
                                  select student).Distinct().ToList<tb_ClientStudent>();
            }
            else
            {
                //RIPClientsList = (from c in ctx.tb_Clients
                //                  join cl in ctx.tb_ClientLocation
                //                    on c.client_ID equals cl.client_ID
                //                  where locaIds.Contains(cl.loca_ID.ToString()) && (c.client_CreatedDate != null && ((DateTime)c.client_CreatedDate).CompareTo(currentDate) <= 0 && (c.client_IsCompleteRegister == null || c.client_IsCompleteRegister == false))
                //                  select c).Distinct().ToList<tb_Client>();
                RIPClientsList = (from client in ctx.tb_Clients
                                  join student in ctx.tb_ClientStudent on client.client_ID equals student.client_ID
                                  join location in ctx.tb_ClientLocation on client.client_ID equals location.client_ID
                                  where
                                  client.client_CreatedDate.HasValue
                                  && client.client_CreatedDate.Value.Date <= currentDate.Date
                                  && (client.client_IsCompleteRegister.HasValue || !client.client_IsCompleteRegister.Value)
                                  && locaIds.Contains(location.loca_ID.ToString())
                                  select student).Distinct().ToList<tb_ClientStudent>();
            }

            int iRIP = 0;
            if (RIPClientsList != null)
            {
                iRIP = RIPClientsList.Count;
            }

            // Spots Withdraw
            List<tb_StudentBooks> SpotsWithdrawList = new List<tb_StudentBooks>();
            if (locaIds != null && locaIds.Count == 1 && string.IsNullOrEmpty(locaIds[0]))
            {
                SpotsWithdrawList = (from client in ctx.tb_Clients
                                     join student in ctx.tb_ClientStudent on client.client_ID equals student.stud_ID
                                     join studentbook in ctx.tb_StudentBooks on student.stud_ID equals studentbook.stud_ID
                                     join active in ctx.tb_ActiveClass on studentbook.activecs_ID equals active.activecs_ID
                                     where
                                     (!active.activecs_EndDay.HasValue || active.activecs_EndDay.Value.Date >= dtFirstDayofMonth.Date)
                                     && studentbook.studch_IsWithdrawal.HasValue 
                                     && studentbook.studch_IsWithdrawal.Value 
                                     && studentbook.studch_WidthdrawalData.Value.Date <= currentDate.Date
                                     && studentbook.studch_WidthdrawalData.Value.Date >= dtFirstDayofMonth.Date
                                     select studentbook).Distinct().ToList<tb_StudentBooks>();
                    //(from c in ctx.tb_StudentBooks
                    //                 where c.studch_IsWithdrawal != null && c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) <= 0 && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofMonth) >= 0
                    //                 select c).Distinct().ToList<tb_StudentBooks>();
            }
            else
            {
                //SpotsWithdrawList = (from c in ctx.tb_StudentBooks
                //                     join cl in ctx.tb_ActiveClass
                //                        on c.activecs_ID equals cl.activecs_ID
                //                     where locaIds.Contains(cl.loca_ID.ToString()) && (c.studch_IsWithdrawal != null && c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) <= 0 && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofMonth) >= 0)
                //                     select c).Distinct().ToList<tb_StudentBooks>();
                SpotsWithdrawList = (from client in ctx.tb_Clients
                                     join student in ctx.tb_ClientStudent on client.client_ID equals student.client_ID
                                     join studentbook in ctx.tb_StudentBooks on student.stud_ID equals studentbook.stud_ID
                                     join active in ctx.tb_ActiveClass on studentbook.activecs_ID equals active.activecs_ID
                                     where
                                     (!active.activecs_EndDay.HasValue || active.activecs_EndDay.Value.Date >= dtFirstDayofMonth.Date)
                                     && studentbook.studch_IsWithdrawal.HasValue
                                     && studentbook.studch_IsWithdrawal.Value
                                     && studentbook.studch_WidthdrawalData.Value.Date <= currentDate.Date
                                     && studentbook.studch_WidthdrawalData.Value.Date >= dtFirstDayofMonth.Date
                                     && locaIds.Contains(active.loca_ID.ToString())
                                     select studentbook).Distinct().ToList<tb_StudentBooks>();
            }
            int iSpotsWithdraw = SpotsWithdrawList.Count;

            // % of Spots Withdrawn
            decimal dPrecentSportsWithdrawn = iClassSportsFilled == 0 ? 0 : (decimal)iSpotsWithdraw / (decimal)iClassSportsFilled;

            if (prev != null)
            {
                iNewSpotsFilled = prev.mer_ClassSpotsFilled.Value - prev.mer_SpotsWithdrawn.Value;
                iNewSpotsFilled = iClassSportsFilled - iNewSpotsFilled;
            }

            tb_MonthlyEnrollReport monthlyEnrollReport = new tb_MonthlyEnrollReport();
            monthlyEnrollReport.mer_ID = Guid.NewGuid();
            monthlyEnrollReport.mer_Date = currentDate;
            monthlyEnrollReport.mer_ClassSpotsFilled = iClassSportsFilled;
            monthlyEnrollReport.mer_FamiliesEnrolled = iFamiliesEnrolled;
            monthlyEnrollReport.mer_SpotsFilledPerFamily = dSpotsFilledPerFamily;
            monthlyEnrollReport.mer_SpotsPerChild = dSportsPerChild;
            monthlyEnrollReport.mer_PercentCapacity = dPrecentCapacity;
            monthlyEnrollReport.mer_NewSpotsFilled = iNewSpotsFilled;
            monthlyEnrollReport.mer_NewRegisgtrations = iNewRegisgtrations;
            monthlyEnrollReport.mer_RIP = iRIP;
            monthlyEnrollReport.mer_SpotsWithdrawn = iSpotsWithdraw;
            monthlyEnrollReport.mer_PercentSpotsWithdrawn = dPrecentSportsWithdrawn;
            return monthlyEnrollReport;
        }
        protected int GetStudentNumber(IEnumerable<tb_StudentBooks> books)
        {
            int total = 0;
            List<Guid> studentIds = books.Select(book => book.stud_ID.Value).Distinct().ToList<Guid>();
            total = studentIds.Count;
            return total;
        }
        public void CreateMontyEnrollReport(int Year, int Month)
        {
            DateTime currentDate = new DateTime(Year, Month, 1).AddMonths(1).AddSeconds(-1);
            DateTime dtFirstDayofMonth = new DateTime(currentDate.Year, currentDate.Month, 1);

            tb_MonthlyEnrollReport existingMonthReport = ctx.tb_MonthlyEnrollReports.SingleOrDefault<tb_MonthlyEnrollReport>(d => d.mer_Date == currentDate);
            if (existingMonthReport == null)
            {
                // Class Spots Filled
                List<tb_StudentBooks> studentBooks = new List<tb_StudentBooks>();

                studentBooks = (from c in ctx.tb_StudentBooks
                                where ((DateTime)c.studch_JoinData).CompareTo(currentDate) <= 0 && (c.studch_IsWithdrawal == null || c.studch_IsWithdrawal == false || (c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) > 0))
                                select c).Distinct().ToList<tb_StudentBooks>();
                int iClassSportsFilled = studentBooks.Count;

                // Families Enrolled
                List<tb_Client> clients = new List<tb_Client>();

                clients = (from c in ctx.tb_Clients
                           join cb in ctx.tb_ClientStudent
                               on c.client_ID equals cb.client_ID
                           join cl in ctx.tb_StudentBooks
                           on cb.stud_ID equals cl.stud_ID
                           where ((DateTime)cl.studch_JoinData).CompareTo(currentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(currentDate) > 0))
                           select c).Distinct().ToList<tb_Client>();
                int iFamiliesEnrolled = clients.Count;

                decimal dSpotsFilledPerFamily = iFamiliesEnrolled == 0 ? 0 : (decimal)iClassSportsFilled / (decimal)iFamiliesEnrolled;

                // Spots Per Child

                List<tb_ClientStudent> clientStudents = new List<tb_ClientStudent>();

                clientStudents = (from c in ctx.tb_ClientStudent
                                  join cl in ctx.tb_StudentBooks
                                  on c.stud_ID equals cl.stud_ID
                                  where ((DateTime)cl.studch_JoinData).CompareTo(currentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(currentDate) > 0))
                                  select c).Distinct().ToList<tb_ClientStudent>();
                int iStudentFilled = clientStudents.Count;

                decimal dSportsPerChild = iStudentFilled == 0 ? 0 : (decimal)iClassSportsFilled / (decimal)iStudentFilled;

                // % of Capacity
                List<tb_ActiveClass> activeClassList = new List<tb_ActiveClass>();

                activeClassList = (from c in ctx.tb_ActiveClass
                                   join cl in ctx.tb_StudentBooks
                                      on c.activecs_ID equals cl.activecs_ID
                                   where ((DateTime)cl.studch_JoinData).CompareTo(currentDate) <= 0 && (cl.studch_IsWithdrawal == null || cl.studch_IsWithdrawal == false || (cl.studch_IsWithdrawal == true && ((DateTime)cl.studch_WidthdrawalData).CompareTo(currentDate) > 0))
                                   select c).Distinct().ToList<tb_ActiveClass>();
                int iCapacity = 0;
                if (activeClassList != null)
                {
                    foreach (tb_ActiveClass activeClass in activeClassList)
                    {

                        tb_Class classType = ctx.tb_Class.SingleOrDefault<tb_Class>(d => d.class_ID == (Guid)activeClass.class_ID);

                        if (classType != null && classType.class_StudentCapacity != null)
                        {
                            iCapacity += (int)classType.class_StudentCapacity;
                        }
                    }
                }
                decimal dPrecentCapacity =  iCapacity == 0 ? 0 : (decimal)iClassSportsFilled / (decimal)iCapacity;

                // New Spots Filled

                List<tb_StudentBooks> currentMonthBooks = new List<tb_StudentBooks>();

                currentMonthBooks = (from c in ctx.tb_StudentBooks
                                     where ((DateTime)c.studch_JoinData).CompareTo(dtFirstDayofMonth) >= 0 && ((DateTime)c.studch_JoinData).CompareTo(currentDate) <= 0 && (c.studch_IsWithdrawal == null || c.studch_IsWithdrawal == false || (c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) > 0))
                                     select c).Distinct().ToList<tb_StudentBooks>();
                int iNewSpotsFilled = currentMonthBooks.Count;

                // New Regisgtrations
                List<tb_Client> clientsList = new List<tb_Client>();

                clientsList = (from c in ctx.tb_Clients
                               where c.client_CreatedDate != null && ((DateTime)c.client_CreatedDate).CompareTo(dtFirstDayofMonth) >= 0 && ((DateTime)c.client_CreatedDate).CompareTo(currentDate) <= 0
                               select c).Distinct().ToList<tb_Client>();
                int iNewRegisgtrations = clientsList.Count;

                // # of RIP
                List<tb_Client> RIPClientsList = new List<tb_Client>();

                RIPClientsList = (from c in ctx.tb_Clients
                                  where c.client_CreatedDate != null && ((DateTime)c.client_CreatedDate).CompareTo(dtFirstDayofMonth) >= 0 && ((DateTime)c.client_CreatedDate).CompareTo(currentDate) <= 0 && (c.client_IsCompleteRegister == null || c.client_IsCompleteRegister == false)
                                  select c).Distinct().ToList<tb_Client>();
                int iRIP = RIPClientsList.Count;

                // Spots Withdraw
                List<tb_StudentBooks> SpotsWithdrawList = new List<tb_StudentBooks>();

                SpotsWithdrawList = (from c in ctx.tb_StudentBooks
                                     where c.studch_IsWithdrawal != null && c.studch_IsWithdrawal == true && ((DateTime)c.studch_WidthdrawalData).CompareTo(currentDate) <= 0 && ((DateTime)c.studch_WidthdrawalData).CompareTo(dtFirstDayofMonth) >= 0
                                     select c).Distinct().ToList<tb_StudentBooks>();
                int iSpotsWithdraw = SpotsWithdrawList.Count;

                // % of Spots Withdrawn
                decimal dPrecentSportsWithdrawn = iClassSportsFilled == 0 ? 0 : (decimal)iSpotsWithdraw / (decimal)iClassSportsFilled;

                tb_MonthlyEnrollReport monthlyEnrollReport = new tb_MonthlyEnrollReport();
                monthlyEnrollReport.mer_ID = Guid.NewGuid();
                monthlyEnrollReport.mer_Date = currentDate;
                monthlyEnrollReport.mer_ClassSpotsFilled = iClassSportsFilled;
                monthlyEnrollReport.mer_FamiliesEnrolled = iFamiliesEnrolled;
                monthlyEnrollReport.mer_SpotsFilledPerFamily = dSpotsFilledPerFamily;
                monthlyEnrollReport.mer_SpotsPerChild = dSportsPerChild;
                monthlyEnrollReport.mer_PercentCapacity = dPrecentCapacity;
                monthlyEnrollReport.mer_NewSpotsFilled = iNewSpotsFilled;
                monthlyEnrollReport.mer_NewRegisgtrations = iNewRegisgtrations;
                monthlyEnrollReport.mer_RIP = iRIP;
                monthlyEnrollReport.mer_SpotsWithdrawn = iSpotsWithdraw;
                monthlyEnrollReport.mer_PercentSpotsWithdrawn = dPrecentSportsWithdrawn;

                ctx.tb_MonthlyEnrollReports.InsertOnSubmit(monthlyEnrollReport);
                ctx.SubmitChanges();
            }
        }

        public tb_MonthlyEnrollReport GetMonthlyReport(int Year, int Month)
        {
            DateTime currentDate = new DateTime(Year, Month, 1).AddMonths(1).AddSeconds(-1);
            tb_MonthlyEnrollReport report = ctx.tb_MonthlyEnrollReports.SingleOrDefault<tb_MonthlyEnrollReport>(d => d.mer_Date == currentDate);
            return report;
        }

        public tb_MonthlyWithdrawalReport GetMonthlyReport(Guid reason_ID, int Year, int Month)
        {
            DateTime currentDate = new DateTime(Year, Month, 1).AddMonths(1).AddSeconds(-1);
            List<tb_MonthlyWithdrawalReport> reportList = new List<tb_MonthlyWithdrawalReport>();

            reportList = (from c in ctx.tb_MonthlyWithdrawalReports
                          where c.mwr_Date == currentDate && c.reason_ID == reason_ID
                          select c).ToList<tb_MonthlyWithdrawalReport>();
            if (reportList != null && reportList.Count > 0)
            {
                return reportList[0];
            }
            else
            {
                return null;
            }
        }

        public int totalSpots(IEnumerable<tb_StudentBooks> books)
        {
            int iClassSportsFilled = 0;
            if (books != null)
            {
                foreach (tb_StudentBooks stbk in books)
                {
                    List<tb_Class> clas = (from acs in ctx.tb_ActiveClass
                                           join css in ctx.tb_Class
                                           on acs.class_ID equals css.class_ID
                                           where acs.activecs_ID == stbk.activecs_ID.Value
                                           select css).Distinct().ToList<tb_Class>();
                    if (clas != null && clas.Count > 0)
                    {
                        tb_Class cls = clas[0];
                        if (cls.class_IsPrivate.Value)
                        {
                            iClassSportsFilled += 4;
                        }
                        else if (cls.class_IsSemiPrivate.Value)
                        {
                            iClassSportsFilled += 2;
                        }
                        else
                        {
                            iClassSportsFilled++;
                        }
                    }
                }
            }
            return iClassSportsFilled;
        }
    }
}
