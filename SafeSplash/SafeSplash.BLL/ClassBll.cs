﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class ClassBll
    {
        protected SafeSplashDBDataContext ctx = null;

        public ClassBll()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public ClassBll(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }

        public void Add(tb_Class info)
        {
            ctx.tb_Class.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_Class info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_Class configInfo = ctx.tb_Class.SingleOrDefault<tb_Class>(d => d.class_ID == id);
            if (configInfo != null)
            {
                ctx.tb_Class.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_Class> GetAll()
        {
            List<tb_Class> list = new List<tb_Class>();

            list = ctx.GetTable<tb_Class>().ToList<tb_Class>();
            return list;
        }

        public tb_Class GetByID(Guid id)
        {
            tb_Class dataInfo = ctx.tb_Class.SingleOrDefault<tb_Class>(d => d.class_ID == id);
            return dataInfo;
        }

        public tb_Class GetByName(string classTypeName)
        {
            tb_Class dataInfo = ctx.tb_Class.SingleOrDefault<tb_Class>(d => d.class_Name == classTypeName);
            return dataInfo;
        }

        public tb_Class GetByActiveClassID(Guid activeClassID)
        {
            tb_Class clients = new tb_Class();

            clients = (from c in ctx.tb_Class
                       join cl in ctx.tb_ActiveClass
                           on c.class_ID equals cl.class_ID
                       where cl.activecs_ID == activeClassID
                       select c).SingleOrDefault();
            return clients;
        }

    }
}
