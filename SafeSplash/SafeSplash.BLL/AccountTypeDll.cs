﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class AccountTypeDll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_AccountType info)
        {
            ctx.tb_AccountType.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_AccountType info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_AccountType configInfo = ctx.tb_AccountType.SingleOrDefault<tb_AccountType>(d => d.accounttp_ID == id);
            if (configInfo != null)
            {
                ctx.tb_AccountType.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_AccountType> GetAll()
        {
            List<tb_AccountType> list = new List<tb_AccountType>();

            list = ctx.GetTable<tb_AccountType>().ToList<tb_AccountType>();
            return list;
        }

        public tb_AccountType GetByID(Guid id)
        {
            tb_AccountType dataInfo = ctx.tb_AccountType.SingleOrDefault<tb_AccountType>(d => d.accounttp_ID == id);
            return dataInfo;
        }
    }
}
