﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class FacilityTypeBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_FacilityType info)
        {
            ctx.tb_FacilityType.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_FacilityType info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(int id)
        {
            tb_FacilityType configInfo = ctx.tb_FacilityType.SingleOrDefault<tb_FacilityType>(d => d.facilityType_ID == id);
            if (configInfo != null)
            {
                ctx.tb_FacilityType.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_FacilityType> GetAll()
        {
            return ctx.GetTable<tb_FacilityType>().ToList<tb_FacilityType>();
        }

        public tb_FacilityType GetByID(int id)
        {
            tb_FacilityType dataInfo = ctx.tb_FacilityType.SingleOrDefault<tb_FacilityType>(d => d.facilityType_ID == id);
            return dataInfo;
        }        
    }
}
