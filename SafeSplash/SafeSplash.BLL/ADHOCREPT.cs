﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using AKMII.SafeSplash.BLL;
using System.Data.Linq;
using System.Data;

namespace AKMII.SafeSplash.BLL
{
    public class ADHOCREPT
    {
        protected SafeSplashDBDataContext ctx = null;

        public ADHOCREPT()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public ADHOCREPT(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }

        public List<vw_ClientEnrollSummary> GetEnrolledClients(DateTime startDate, DateTime endDate)
        {
            List<vw_ClientEnrollSummary> clients = new List<vw_ClientEnrollSummary>();

            clients = (from c in ctx.vw_ClientEnrollSummaries
                       where ((DateTime)c.studch_JoinData).CompareTo(startDate) >= 0 && ((DateTime)c.studch_JoinData).CompareTo(endDate) <= 0
                       orderby c.client_ID, c.studch_JoinData descending
                       select c).ToList<vw_ClientEnrollSummary>();
            return clients;
        }
        public List<vw_ClientEnrollSummary> GetEnrolledClientsWithLocationFilter(DateTime startDate, DateTime endDate, Guid loca_ID)
        {
            List<vw_ClientEnrollSummary> clients = new List<vw_ClientEnrollSummary>();

            clients = (from c in ctx.vw_ClientEnrollSummaries
                       where ((DateTime)c.studch_JoinData).CompareTo(startDate) >= 0 && ((DateTime)c.studch_JoinData).CompareTo(endDate) <= 0 && c.loca_ID == loca_ID
                       orderby c.client_ID, c.studch_JoinData descending
                       select c).ToList<vw_ClientEnrollSummary>();
            return clients;
        }

        //public List<vw_ClientEnrollSummary> GetEnrolledClients(DateTime startDate, DateTime endDate, List<string> local_id, bool isCorpAdmin)
        //public List<vw_ClientEnrollSummary> GetEnrolledClients(DateTime startDate, DateTime endDate, List<string> locationIds, bool isCorpAdmin)
        //public List<vw_ClientEnrollSummary> GetEnrolledClients(DateTime startDate, DateTime endDate, List<string> locationIds)
        //{
        //    List<vw_ClientEnrollSummary> clientEnrollRept = null;
        //    //if (isCorpAdmin && local_id != null && local_id.Count > 0)
        //    //{
        //    //    clientEnrollRept = GetEnrolledClients(startDate, endDate, local_id[0]);
        //    //}
        //    //else
        //    //{
        //    //    clientEnrollRept = GetEnrolledClients(startDate, endDate, local_id);
        //    //}
        //    clientEnrollRept = GetEnrolledClients(startDate, endDate, locationIds);
        //    return clientEnrollRept;
        //}

        public List<vw_ClientEnrollSummary> GetEnrolledClients(DateTime startDate, DateTime endDate, List<string> localIds)
        {
            //List<vw_ClientEnrollSummary> clients = null;
            //List<string> days = buildDayRange(startDate, endDate);

            //if (localIds != null)
            //{
            //    clients = (from c in ctx.vw_ClientEnrollSummaries
            //               where c.studch_JoinData.HasValue && c.studch_JoinData.Value <= endDate
            //               && days.Contains(c.activeDay_Day.ToLower())
            //               && (!c.studch_IsWithdrawal.HasValue || !c.studch_IsWithdrawal.Value||c.studch_WidthdrawalData.Value>=startDate)
            //               && localIds.Contains(c.loca_ID.Value.ToString())
            //               orderby c.client_ID, c.studch_JoinData descending
            //               select c).ToList<vw_ClientEnrollSummary>();
            //}
            //return clients;

            List<vw_ClientEnrollSummary> clients = new List<vw_ClientEnrollSummary>();
            List<string> days = buildDayRange(startDate, endDate);

            clients = (from c in ctx.vw_ClientEnrollSummaries
                       where localIds.Contains(c.loca_ID.Value.ToString())
                       && days.Contains(c.activeDay_Day.ToLower())
                       && (!c.activecs_EndDay.HasValue || c.activecs_EndDay.Value.Date >= startDate.Date)
                       && c.studch_JoinData.Value.Date.CompareTo(endDate.Date) <= 0
                       && (!c.studch_IsWithdrawal.HasValue || !c.studch_IsWithdrawal.Value || c.studch_WidthdrawalData.Value.Date.CompareTo(startDate.Date) >= 0)
                       orderby c.client_ID, c.studch_JoinData descending
                       select c).ToList<vw_ClientEnrollSummary>();
            return clients;

        }

        //public List<vw_ClientInfoSummary> GetStudentBirthdays(List<int> months, List<string> locationIds)
        //{
        //    List<vw_ClientInfoSummary> clients = new List<vw_ClientInfoSummary>();

        //    clients = (from c in ctx.vw_ClientInfoSummaries
        //               where locationIds.Contains(c.loca_ID.Value.ToString())
        //               && (!c.studch_IsWithdrawal.HasValue || !c.studch_IsWithdrawal.Value || c.studch_WidthdrawalData.Value.Date.CompareTo(DateTime.Today) >= 0)
        //               && months.Contains(c.stud_DOB.Value.Month)
        //               orderby c.stud_DOB.Value.DayOfYear, c.stud_ID, c.stud_LastName, c.stud_FirstName ascending
        //               select c).ToList<vw_ClientInfoSummary>();

        //    return clients;
        //}

        public List<vw_ClientEnrollSummary> GetStudentBirthdays(List<int> months, List<string> locationIds)
        {
            List<vw_ClientEnrollSummary> clients = new List<vw_ClientEnrollSummary>();

            clients = (from c in ctx.vw_ClientEnrollSummaries
                       where locationIds.Contains(c.loca_ID.Value.ToString())
                       && (!c.activecs_EndDay.HasValue || c.activecs_EndDay.Value.Date >= DateTime.Today)
                       && c.studch_JoinData.Value.Date.CompareTo(DateTime.Today) <= 0
                       && (!c.studch_IsWithdrawal.HasValue || !c.studch_IsWithdrawal.Value || c.studch_WidthdrawalData.Value.Date.CompareTo(DateTime.Today) >= 0)
                       && months.Contains(c.stud_DOB.Value.Month)
                       orderby c.stud_DOB.Value.DayOfYear, c.stud_ID, c.stud_LastName, c.stud_FirstName ascending
                       select c).ToList<vw_ClientEnrollSummary>();

            return clients;
        }



        private List<string> buildDayRange(DateTime dtFrom, DateTime dtTo)
        {

            DateTime dtstep = dtFrom;
            List<string> days = new List<string>();

            int count = 0;
            while (dtstep <= dtTo && count < 7)
            {
                string day = dtstep.DayOfWeek.ToString().ToLower();
                if (!days.Contains(day)) days.Add(day);

                dtstep = dtstep.AddDays(1);
                count++;
            }

            return days;
        }

        public List<vw_ClientEnrollSummary> GetEnrolledClients(DateTime startDate, DateTime endDate, string local_id)
        {
            List<vw_ClientEnrollSummary> clients = new List<vw_ClientEnrollSummary>();
            List<string> days = buildDayRange(startDate, endDate);

            clients = (from c in ctx.vw_ClientEnrollSummaries
                       where
                       (string.Equals(local_id, "") || string.Equals(local_id, c.loca_ID.Value.ToString()))
                       && days.Contains(c.activeDay_Day.ToLower())
                       && (!c.activecs_EndDay.HasValue || c.activecs_EndDay.Value.Date >= startDate.Date)
                       && c.studch_JoinData.Value.Date.CompareTo(endDate.Date) <= 0
                       && (!c.studch_IsWithdrawal.HasValue || !c.studch_IsWithdrawal.Value || c.studch_WidthdrawalData.Value.Date.CompareTo(startDate.Date) >= 0)
                       orderby c.client_ID, c.studch_JoinData descending
                       select c).ToList<vw_ClientEnrollSummary>();
            return clients;
        }
        public List<vw_ActiveClass> GetActiveClasses(DateTime fromDate, DateTime toDate, string[] weekDaysList)
        {
            List<vw_ActiveClass> classes = new List<vw_ActiveClass>();

            classes = (from c in ctx.vw_ActiveClasses
                       where ((DateTime)c.activecs_StartDay).CompareTo(toDate) <= 0 && (c.activecs_EndDay == null || ((DateTime)c.activecs_EndDay).CompareTo(fromDate) >= 0) && weekDaysList.Contains(c.activeDay_Day)
                       orderby c.activecs_StartDay descending
                       select c).ToList<vw_ActiveClass>();


            return classes;

        }
        protected List<vw_ActiveClass> GetActiveClassesForRept(DateTime fromDate, DateTime endDate, bool isCropAdmin, List<string> locaIds)
        {
            List<vw_ActiveClass> activeClasses = null;
            if (isCropAdmin && locaIds != null && locaIds.Count == 1)
            {
                activeClasses = (from active in ctx.vw_ActiveClasses
                                 where active.activecs_StartDay.HasValue && active.activecs_StartDay.Value <= endDate
                                 && (!active.activecs_EndDay.HasValue || active.activecs_EndDay.Value >= fromDate)
                                 && (string.IsNullOrEmpty(locaIds[0]) || (active.loca_ID.HasValue && active.loca_ID.Value.ToString() == locaIds[0]))
                                 select active).Distinct().ToList<vw_ActiveClass>();
            }
            else
            {
                activeClasses = (from active in ctx.vw_ActiveClasses
                                 where active.activecs_StartDay.HasValue && active.activecs_StartDay.Value <= endDate
                                 && (!active.activecs_EndDay.HasValue || active.activecs_EndDay.Value >= fromDate)
                                 && locaIds.Contains(active.loca_ID.Value.ToString())
                                 select active).Distinct().ToList<vw_ActiveClass>();
            }
            return activeClasses;
        }
        protected Dictionary<vw_ActiveClass, int> ImportActiveClassStudentsInfo(List<vw_ActiveClass> actives, DateTime day)
        {
            Dictionary<vw_ActiveClass, int> res = null;
            if (actives != null)
            {
                StudentBookBll studentBookBll = new StudentBookBll();
                res = new Dictionary<vw_ActiveClass, int>();
                List<tb_ClientStudent> studBooksList = null;
                foreach (vw_ActiveClass active in actives)
                {
                    studBooksList = studentBookBll.GetStudentsByBooks(day, active.activecs_ID);
                    if (studBooksList != null)
                    {
                        res.Add(active, studBooksList.Count);
                    }
                }
            }
            return res;
        }

        public DataTable GetActiveClass(DateTime fromDate, DateTime toDate, List<string> locaIds, bool isCropAdmin)
        {
            //List<vw_ActiveClass> classes = new List<vw_ActiveClass>();
            DataTable allClasses = new DataTable();
            DataColumn col0 = new DataColumn("datetime");
            DataColumn col1 = new DataColumn("class_Name");
            DataColumn col2 = new DataColumn("level_Name");
            DataColumn col3 = new DataColumn("activeDay_Day");
            DataColumn col4 = new DataColumn("class_StudentCapacity");
            DataColumn col5 = new DataColumn("class_starttime");
            DataColumn col6 = new DataColumn("instructor");
            DataColumn col7 = new DataColumn("studentsCount");

            allClasses.Columns.Add(col0);
            allClasses.Columns.Add(col1);
            allClasses.Columns.Add(col2);
            allClasses.Columns.Add(col3);
            allClasses.Columns.Add(col4);
            allClasses.Columns.Add(col5);
            allClasses.Columns.Add(col6);
            allClasses.Columns.Add(col7);

            //Table<vw_ActiveClass> allActiveClasses = ctx.vw_ActiveClasses;

            if (fromDate.CompareTo(toDate) <= 0)
            {
                List<vw_ActiveClass> allActiveClasses = GetActiveClassesForRept(fromDate, toDate, isCropAdmin, locaIds);
                Dictionary<vw_ActiveClass, int> studentsInfo = ImportActiveClassStudentsInfo(allActiveClasses, DateTime.Today);
                for (int i = 0; fromDate.AddDays(i).CompareTo(toDate) <= 0; i++)
                {
                    DateTime tmp = fromDate.AddDays(i);
                    foreach (vw_ActiveClass var in allActiveClasses)
                    {
                        if (tmp.DayOfWeek.ToString().Equals(var.activeDay_Day))
                        {
                            if (var.activecs_StartDay.HasValue && var.activecs_StartDay.Value.CompareTo(tmp) <= 0)
                            {
                                if (!var.activecs_EndDay.HasValue || var.activecs_EndDay.Value.CompareTo(tmp) >= 0)
                                {
                                    DataRow dr = allClasses.NewRow();
                                    dr["datetime"] = tmp.ToString("MM/dd/yyy");
                                    dr["class_Name"] = var.class_Name;
                                    dr["level_Name"] = var.level_Name;
                                    dr["activeDay_Day"] = var.activeDay_Day;
                                    dr["class_StudentCapacity"] = var.class_StudentCapacity.Value;
                                    dr["class_starttime"] = DateTime.MinValue.Add(var.activeDay_StartTime.Value).ToString("hh:mm tt");

                                    dr["instructor"] = string.Format("{0} {1}", var.empl_FirstName, var.empl_LastName);
                                    //StudentBookBll studentBookBll = new StudentBookBll();
                                    //List<tb_ClientStudent> studBooksList = studentBookBll.GetStudentsByBooks(DateTime.Now, var.activecs_ID);
                                    //dr["studentsCount"] = studBooksList.Count;
                                    if (studentsInfo.ContainsKey(var))
                                    {
                                        dr["studentsCount"] = studentsInfo[var];
                                    }
                                    else
                                    {
                                        dr["studentsCount"] = 0;
                                    }
                                    allClasses.Rows.Add(dr);
                                }
                            }
                        }
                    }

                }

                //classes = (from c in ctx.vw_ActiveClasses
                //           where ((DateTime)c.activecs_StartDay).CompareTo(toDate) <= 0 && (c.activecs_EndDay == null || ((DateTime)c.activecs_EndDay).CompareTo(fromDate) >= 0) && weekDaysList.Contains(c.activeDay_Day)
                //           orderby c.activecs_StartDay descending
                //           select c).ToList<vw_ActiveClass>();

            }
            //return classes;
            return allClasses;
        }

        //public IEnumerable<ActiveClass> GetActiveClasses(DateTime fromDate, DateTime toDate, List<string> locaIds, bool isCropAdmin)
        //{
        //    List<ActiveClass> classes = new List<ActiveClass>();

        //    if (fromDate.CompareTo(toDate) <= 0)
        //    {
        //        List<vw_ActiveClass> allActiveClasses = GetActiveClassesForRept(fromDate, toDate, isCropAdmin, locaIds);
        //        Dictionary<vw_ActiveClass, int> studentsInfo = ImportActiveClassStudentsInfo(allActiveClasses, DateTime.Today);
        //        for (int i = 0; fromDate.AddDays(i).CompareTo(toDate) <= 0; i++)
        //        {
        //            DateTime tmp = fromDate.AddDays(i);
        //            foreach (vw_ActiveClass var in allActiveClasses)
        //            {
        //                if (tmp.DayOfWeek.ToString().Equals(var.activeDay_Day))
        //                {
        //                    if (var.activecs_StartDay.HasValue && var.activecs_StartDay.Value.CompareTo(tmp) <= 0)
        //                    {
        //                        if (!var.activecs_EndDay.HasValue || var.activecs_EndDay.Value.CompareTo(tmp) >= 0)
        //                        {
        //                            //DataRow dr = allClasses.NewRow();
        //                            //dr["datetime"] = tmp.ToString("MM/dd/yyy");
        //                            //dr["class_Name"] = var.class_Name;
        //                            //dr["level_Name"] = var.level_Name;
        //                            //dr["activeDay_Day"] = var.activeDay_Day;
        //                            //dr["class_StudentCapacity"] = var.class_StudentCapacity.Value;
        //                            //dr["class_starttime"] = DateTime.MinValue.Add(var.activeDay_StartTime.Value).ToString("hh:mm tt");

        //                            //dr["instructor"] = string.Format("{0} {1}", var.empl_FirstName, var.empl_LastName);
        //                            //if (studentsInfo.ContainsKey(var))
        //                            //{
        //                            //    dr["studentsCount"] = studentsInfo[var];
        //                            //}
        //                            //else
        //                            //{
        //                            //    dr["studentsCount"] = 0;
        //                            //}
        //                            //allClasses.Rows.Add(dr);

        //                            ActiveClass activeClass = new ActiveClass();
        //                            activeClass.Date = tmp;
        //                            activeClass.Name = var.class_Name;
        //                            activeClass.LevelName = var.level_Name;
        //                            activeClass.DayOfWeek = var.activeDay_Day;
        //                            activeClass.Capacity = var.class_StudentCapacity.Value;
        //                            activeClass.StartTime = DateTime.MinValue.Add(var.activeDay_StartTime.Value).ToString("hh:mm tt");
        //                            activeClass.InstructorName = string.Format("{0} {1}", var.empl_FirstName, var.empl_LastName);
        //                            activeClass.StudentCount = (studentsInfo.ContainsKey(var)) ? studentsInfo[var] : 0;
        //                            classes.Add(activeClass);
        //                        }
        //                    }
        //                }
        //            }

        //        }
        //    }



        //    return classes;

        //    //DataTable allClasses = new DataTable();
        //    //DataColumn col0 = new DataColumn("datetime");
        //    //DataColumn col1 = new DataColumn("class_Name");
        //    //DataColumn col2 = new DataColumn("level_Name");
        //    //DataColumn col3 = new DataColumn("activeDay_Day");
        //    //DataColumn col4 = new DataColumn("class_StudentCapacity");
        //    //DataColumn col5 = new DataColumn("class_starttime");
        //    //DataColumn col6 = new DataColumn("instructor");
        //    //DataColumn col7 = new DataColumn("studentsCount");

        //    //allClasses.Columns.Add(col0);
        //    //allClasses.Columns.Add(col1);
        //    //allClasses.Columns.Add(col2);
        //    //allClasses.Columns.Add(col3);
        //    //allClasses.Columns.Add(col4);
        //    //allClasses.Columns.Add(col5);
        //    //allClasses.Columns.Add(col6);
        //    //allClasses.Columns.Add(col7);

        //    ////Table<vw_ActiveClass> allActiveClasses = ctx.vw_ActiveClasses;

        //    //if (fromDate.CompareTo(toDate) <= 0)
        //    //{
        //    //    List<vw_ActiveClass> allActiveClasses = GetActiveClassesForRept(fromDate, toDate, isCropAdmin, locaIds);
        //    //    Dictionary<vw_ActiveClass, int> studentsInfo = ImportActiveClassStudentsInfo(allActiveClasses, DateTime.Today);
        //    //    for (int i = 0; fromDate.AddDays(i).CompareTo(toDate) <= 0; i++)
        //    //    {
        //    //        DateTime tmp = fromDate.AddDays(i);
        //    //        foreach (vw_ActiveClass var in allActiveClasses)
        //    //        {
        //    //            if (tmp.DayOfWeek.ToString().Equals(var.activeDay_Day))
        //    //            {
        //    //                if (var.activecs_StartDay.HasValue && var.activecs_StartDay.Value.CompareTo(tmp) <= 0)
        //    //                {
        //    //                    if (!var.activecs_EndDay.HasValue || var.activecs_EndDay.Value.CompareTo(tmp) >= 0)
        //    //                    {
        //    //                        DataRow dr = allClasses.NewRow();
        //    //                        dr["datetime"] = tmp.ToString("MM/dd/yyy");
        //    //                        dr["class_Name"] = var.class_Name;
        //    //                        dr["level_Name"] = var.level_Name;
        //    //                        dr["activeDay_Day"] = var.activeDay_Day;
        //    //                        dr["class_StudentCapacity"] = var.class_StudentCapacity.Value;
        //    //                        dr["class_starttime"] = DateTime.MinValue.Add(var.activeDay_StartTime.Value).ToString("hh:mm tt");

        //    //                        dr["instructor"] = string.Format("{0} {1}", var.empl_FirstName, var.empl_LastName);
        //    //                        //StudentBookBll studentBookBll = new StudentBookBll();
        //    //                        //List<tb_ClientStudent> studBooksList = studentBookBll.GetStudentsByBooks(DateTime.Now, var.activecs_ID);
        //    //                        //dr["studentsCount"] = studBooksList.Count;
        //    //                        if (studentsInfo.ContainsKey(var))
        //    //                        {
        //    //                            dr["studentsCount"] = studentsInfo[var];
        //    //                        }
        //    //                        else
        //    //                        {
        //    //                            dr["studentsCount"] = 0;
        //    //                        }
        //    //                        allClasses.Rows.Add(dr);
        //    //                    }
        //    //                }
        //    //            }
        //    //        }

        //    //    }

        //    //    //classes = (from c in ctx.vw_ActiveClasses
        //    //    //           where ((DateTime)c.activecs_StartDay).CompareTo(toDate) <= 0 && (c.activecs_EndDay == null || ((DateTime)c.activecs_EndDay).CompareTo(fromDate) >= 0) && weekDaysList.Contains(c.activeDay_Day)
        //    //    //           orderby c.activecs_StartDay descending
        //    //    //           select c).ToList<vw_ActiveClass>();

        //    //}
        //    ////return classes;
        //    //return allClasses;
        //}

        public DataTable GetActiveClassesWithLocationFilter(DateTime fromDate, DateTime toDate, Guid loca_ID)
        {
            //List<vw_ActiveClass> classes = new List<vw_ActiveClass>();

            //classes = (from c in ctx.vw_ActiveClasses
            //           where ((DateTime)c.activecs_StartDay).CompareTo(toDate) <= 0 && (c.activecs_EndDay == null || ((DateTime)c.activecs_EndDay).CompareTo(fromDate) >= 0) && weekDaysList.Contains(c.activeDay_Day) && c.loca_ID.HasValue && c.loca_ID == loca_ID
            //           orderby c.activecs_StartDay descending
            //           select c).ToList<vw_ActiveClass>();
            //return classes;
            DataTable allClasses = new DataTable();
            DataColumn col0 = new DataColumn("datetime");
            DataColumn col1 = new DataColumn("class_Name");
            DataColumn col2 = new DataColumn("level_Name");
            DataColumn col3 = new DataColumn("activeDay_Day");
            DataColumn col4 = new DataColumn("class_StudentCapacity");
            DataColumn col5 = new DataColumn("class_starttime");
            DataColumn col6 = new DataColumn("instructor");
            DataColumn col7 = new DataColumn("studentsCount");

            allClasses.Columns.Add(col0);
            allClasses.Columns.Add(col1);
            allClasses.Columns.Add(col2);
            allClasses.Columns.Add(col3);
            allClasses.Columns.Add(col4);
            allClasses.Columns.Add(col5);
            allClasses.Columns.Add(col6);
            allClasses.Columns.Add(col7);


            Table<vw_ActiveClass> allActiveClasses = ctx.vw_ActiveClasses;
            if (fromDate.CompareTo(toDate) <= 0)
            {
                for (int i = 0; fromDate.AddDays(i).CompareTo(toDate) <= 0; i++)
                {
                    DateTime tmp = fromDate.AddDays(i);
                    foreach (vw_ActiveClass var in allActiveClasses)
                    {
                        if (tmp.DayOfWeek.ToString().Equals(var.activeDay_Day) && var.loca_ID.HasValue && var.loca_ID == loca_ID)
                        {
                            if (var.activecs_StartDay.HasValue && var.activecs_StartDay.Value.CompareTo(tmp) <= 0)
                            {
                                if (!var.activecs_EndDay.HasValue || var.activecs_EndDay.Value.CompareTo(tmp) >= 0)
                                {
                                    DataRow dr = allClasses.NewRow();
                                    dr["datetime"] = tmp.ToString("MM/dd/yyy");
                                    dr["class_Name"] = var.class_Name;
                                    dr["level_Name"] = var.level_Name;
                                    dr["activeDay_Day"] = var.activeDay_Day;
                                    dr["class_StudentCapacity"] = var.class_StudentCapacity.Value;
                                    dr["class_starttime"] = DateTime.MinValue.Add(var.activeDay_StartTime.Value).ToString("hh:mm tt");

                                    dr["instructor"] = string.Format("{0} {1}", var.empl_FirstName, var.empl_LastName);
                                    StudentBookBll studentBookBll = new StudentBookBll();
                                    List<tb_ClientStudent> studBooksList = studentBookBll.GetStudentsByBooks(DateTime.Now, var.activecs_ID);
                                    dr["studentsCount"] = studBooksList.Count;
                                    allClasses.Rows.Add(dr);
                                }
                            }
                        }
                    }

                }
            }
            return allClasses;
        }

        public List<vw_ActiveClass> GetActiveClassesByType(DateTime fromDate, DateTime toDate, List<string> locaIds, string[] weekDaysList)
        {
            List<vw_ActiveClass> classes = new List<vw_ActiveClass>();
            if (locaIds != null)
            {
                if (locaIds.Count == 1 && !string.IsNullOrEmpty(locaIds[0]))
                {
                    classes = (from c in ctx.vw_ActiveClasses
                               where
                               c.activecs_StartDay.HasValue
                               && c.activecs_StartDay.Value.CompareTo(toDate) <= 0
                               && (!c.activecs_EndDay.HasValue || c.activecs_EndDay.Value.CompareTo(fromDate) >= 0)
                               && locaIds.Contains(c.loca_ID.Value.ToString())
                               && weekDaysList.Contains(c.activeDay_Day)
                               orderby c.activecs_StartDay descending
                               select c).ToList<vw_ActiveClass>();
                }
            }
            return classes;
        }
        public List<vw_ActiveClass> GetActiveClasses(DateTime fromDate, DateTime toDate, Guid localID, string[] weekDaysList)
        {
            List<vw_ActiveClass> classes = new List<vw_ActiveClass>();

            classes = (from c in ctx.vw_ActiveClasses
                       where ((DateTime)c.activecs_StartDay).CompareTo(toDate) <= 0 && (c.activecs_EndDay == null || ((DateTime)c.activecs_EndDay).CompareTo(fromDate) >= 0) && c.loca_ID == localID && weekDaysList.Contains(c.activeDay_Day)
                       orderby c.activecs_StartDay descending
                       select c).ToList<vw_ActiveClass>();
            return classes;
        }

        public List<vw_instructorSubstitution> GetInstructorSub(DateTime fromDate, DateTime toDate, List<string> locaIds)
        {
            List<vw_instructorSubstitution> instrSub = null;
            if (locaIds != null)
            {
                if (locaIds.Count == 1 && string.IsNullOrEmpty(locaIds[0]))
                {
                    instrSub = (from c in ctx.vw_instructorSubstitutions
                                where
                                c.instsub_Date.HasValue
                                && c.instsub_Date.Value.CompareTo(fromDate) >= 0
                                && c.instsub_Date.Value.CompareTo(toDate) <= 0
                                orderby c.instsub_Date descending
                                select c).ToList<vw_instructorSubstitution>();
                }
                else
                {
                    instrSub = (from c in ctx.vw_instructorSubstitutions
                                where
                                c.instsub_Date.HasValue
                                && c.instsub_Date.Value.CompareTo(fromDate) >= 0
                                && c.instsub_Date.Value.CompareTo(toDate) <= 0
                                && locaIds.Contains(c.loca_ID.Value.ToString())
                                orderby c.instsub_Date descending
                                select c).ToList<vw_instructorSubstitution>();
                }
            }
            return instrSub;
        }



        public List<vw_ClientInfoSummary> GetClientRegistration(DateTime fromDate, DateTime toDate, List<string> locaIds)
        {
            DateTime reportFromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
            DateTime reportToDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);

            List<vw_ClientInfoSummary> clientSub = new List<vw_ClientInfoSummary>();
            if (locaIds != null)
            {
                if (locaIds.Count == 1 && string.IsNullOrEmpty(locaIds[0]))
                {
                    clientSub = (from c in ctx.vw_ClientInfoSummaries
                                 where c.client_CreatedDate.HasValue
                                 && c.client_CreatedDate.Value.CompareTo(reportFromDate) >= 0
                                 && c.client_CreatedDate.Value.CompareTo(reportToDate) <= 0
                                 orderby c.loca_ID, c.client_CreatedDate descending, c.client_ID, c.stud_ID, c.studch_JoinData descending
                                 select c).ToList<vw_ClientInfoSummary>();
                }
                else
                {
                    clientSub = (from c in ctx.vw_ClientInfoSummaries
                                 where c.client_CreatedDate.HasValue
                                 && c.client_CreatedDate.Value.CompareTo(reportFromDate) >= 0
                                 && c.client_CreatedDate.Value.CompareTo(reportToDate) <= 0
                                 && locaIds.Contains(c.loca_ID.Value.ToString())
                                 orderby c.loca_ID, c.client_CreatedDate descending, c.client_ID, c.stud_ID, c.studch_JoinData descending
                                 select c).ToList<vw_ClientInfoSummary>();
                }
            }
            return clientSub;
        }
        public List<vw_ClientInfoSummary> GetClientRegistrationWithLocationFilter(DateTime fromDate, DateTime toDate, Guid loca_ID)
        {
            List<vw_ClientInfoSummary> clientSub = new List<vw_ClientInfoSummary>();

            clientSub = (from c in ctx.vw_ClientInfoSummaries
                         where ((DateTime)c.client_CreatedDate).CompareTo(fromDate) >= 0 && ((DateTime)c.client_CreatedDate).CompareTo(toDate) <= 0 && c.loca_ID.HasValue && loca_ID == c.loca_ID
                         orderby c.loca_ID, c.client_CreatedDate descending, c.client_ID, c.stud_ID, c.studch_JoinData descending
                         select c).ToList<vw_ClientInfoSummary>();
            return clientSub;
        }
    }
}
