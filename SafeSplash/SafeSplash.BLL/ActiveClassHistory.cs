﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class ActiveClassHistory
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_ActiveClassHistory info)
        {
            ctx.tb_ActiveClassHistories.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Add(List<tb_ActiveClassHistory> infos)
        {
            ctx.tb_ActiveClassHistories.InsertAllOnSubmit<tb_ActiveClassHistory>(infos);
            ctx.SubmitChanges();
        }
        public tb_ActiveClassHistory GetClassLevelByIdAndDate(Guid activeClassId, DateTime date)
        {
            tb_ActiveClassHistory history = null;
            IEnumerable<tb_ActiveClassHistory> historys = from hty in ctx.tb_ActiveClassHistories
                                                          where hty.activecs_ID.HasValue
                                                          && hty.activecs_ID.Value == activeClassId
                                                          && hty.activecsHistory_Created.HasValue
                                                          && hty.activecsHistory_Created.Value <= date
                                                          && hty.activecsHistory_Column == "level_ID"                                                          
                                                          select hty;
            if (historys != null)
            {
                foreach (tb_ActiveClassHistory his in historys)
                {
                    if (history == null)
                    {
                        history = his;
                    }
                    if (history.activecsHistory_Created.Value < his.activecsHistory_Created.Value)
                    {
                        history = his;
                    }
                }
            }
            return history;
        }
        public List<tb_ActiveClassHistory> GetClassLevelChange(Guid activeClassID)
        {
            List<tb_ActiveClassHistory> listActiveClassDays = new List<tb_ActiveClassHistory>();

            listActiveClassDays = (from c in ctx.tb_ActiveClassHistories
                                   where c.activecs_ID == activeClassID && c.activecsHistory_Column == "level_ID" && c.activecsHistory_NewValue != c.activecsHistory_OldValue
                                   orderby c.activecsHistory_Created descending
                                   select c).ToList<tb_ActiveClassHistory>();
            return listActiveClassDays;
        }

        public List<tb_ActiveClassHistory> GetClassLevelChange(Guid[] activeClassIDs)
        {
            List<tb_ActiveClassHistory> listActiveClassDays = new List<tb_ActiveClassHistory>();

            listActiveClassDays = (from c in ctx.tb_ActiveClassHistories
                                   where activeClassIDs.Contains((Guid)c.activecs_ID) && c.activecsHistory_Column == "level_ID" && c.activecsHistory_NewValue != c.activecsHistory_OldValue
                                   orderby c.activecsHistory_Created descending
                                   select c).ToList<tb_ActiveClassHistory>();
            return listActiveClassDays;
        }

        public List<tb_ActiveClassHistory> GetClassLevelChangeForStudent(Guid studentID)
        {
            List<tb_ActiveClassHistory> listActiveClassDays = new List<tb_ActiveClassHistory>();

            listActiveClassDays = (from c in ctx.tb_ActiveClassHistories
                                   join stuBook in ctx.tb_StudentBooks
                                   on c.activecs_ID equals stuBook.activecs_ID
                                   where studentID == stuBook.stud_ID && c.activecsHistory_Created.Value > stuBook.studch_JoinData.Value && (!stuBook.studch_IsWithdrawal.HasValue || !stuBook.studch_IsWithdrawal.Value || c.activecsHistory_Created.Value < stuBook.studch_WidthdrawalData.Value) && (c.activecsHistory_Column == "level_ID" || c.activecsHistory_Column == "activecs_Teacher") && c.activecsHistory_NewValue != c.activecsHistory_OldValue
                                   orderby c.activecsHistory_Created descending
                                   select c).Distinct().ToList<tb_ActiveClassHistory>();
            return listActiveClassDays;
        }

        public List<tb_ActiveClassHistory> GetStudentBeginLevels(Guid studentID)
        {
            List<tb_ActiveClassHistory> listActiveClassDays = new List<tb_ActiveClassHistory>();
            List<tb_ActiveClassHistory> temp = new List<tb_ActiveClassHistory>();


            Table< tb_ActiveClassHistory> classHistory = ctx.tb_ActiveClassHistories;
            List<tb_StudentBooks> stuBooks = new List<tb_StudentBooks>();

            stuBooks = (from c in ctx.tb_StudentBooks
                        where studentID == c.stud_ID.Value && c.studch_JoinData.Value <= DateTime.Today.AddDays(1).AddSeconds(-1)
                        select c).ToList<tb_StudentBooks>();
            foreach (tb_StudentBooks book in stuBooks)
            {
                temp = (from c1 in classHistory
                        where c1.activecs_ID == book.activecs_ID && c1.activecsHistory_Created.Value <= book.studch_JoinData.Value && c1.activecsHistory_Column == "level_ID" && c1.activecsHistory_NewValue != c1.activecsHistory_OldValue
                        select c1).Distinct().ToList<tb_ActiveClassHistory>();
                if (temp != null && temp.Count > 0)
                {
                    tb_ActiveClassHistory his = null;
                    foreach (tb_ActiveClassHistory ty in temp)
                    {
                        if (his == null)
                            his = ty;
                        if (his.activecsHistory_Created.Value < ty.activecsHistory_Created.Value)
                            his = ty;
                    }
                    his.activecsHistory_Created = book.studch_JoinData;
                    //his.activecsHistory_NewValue = string.Format("{0}(start new class)", his.activecsHistory_NewValue);
                    listActiveClassDays.Add(his);
                }
            }
            
            
            return listActiveClassDays;
        }


        public tb_ActiveClassHistory GetTeacherChange(Guid changeSetID)
        {
            tb_ActiveClassHistory dataInfo = ctx.tb_ActiveClassHistories.SingleOrDefault<tb_ActiveClassHistory>(d => d.activecsHistory_ChangeSet == changeSetID && d.activecsHistory_Column == "activecs_Teacher");
            return dataInfo;
        }
    }
}
