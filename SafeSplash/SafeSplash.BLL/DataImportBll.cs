﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class DataImportBll
    {
        protected SafeSplashDBDataContext ctx = null;

        public DataImportBll()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public DataImportBll(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }

        public List<ss_ListFamiliesExport> GetAllFamilies()
        {
            List<ss_ListFamiliesExport> clients = new List<ss_ListFamiliesExport>();

            clients = (from c in ctx.ss_ListFamiliesExports
                       select c).ToList<ss_ListFamiliesExport>();
            return clients;
        }

        public List<ss_ListAllStudentsExport> GetStudent(string family, string name)
        {
            List<ss_ListAllStudentsExport> clients = new List<ss_ListAllStudentsExport>();

            clients = (from c in ctx.ss_ListAllStudentsExports
                       where c.Family == family && c.Student_s_First_Name == name
                       select c).ToList<ss_ListAllStudentsExport>();
            return clients;
        }

        public List<ss_ListClassesExport> GetAllClasses()
        {
            List<ss_ListClassesExport> clients = new List<ss_ListClassesExport>();

            clients = (from c in ctx.ss_ListClassesExports
                       select c).ToList<ss_ListClassesExport>();
            return clients;
        }
         
        public List<ss_ListInstructorExport> GetAllInstructors()
        {
            List<ss_ListInstructorExport> clients = new List<ss_ListInstructorExport>();

            clients = (from c in ctx.ss_ListInstructorExports
                       select c).ToList<ss_ListInstructorExport>();
            return clients;
        }
    }
}
