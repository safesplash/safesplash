﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.EntityNew;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class PortUsersBLL
    {
        
        protected AKMII.SafeSplash.EntityNew.SafeSplashDBDataContext ctxNew = new AKMII.SafeSplash.EntityNew.SafeSplashDBDataContext();

        public PortUsersBLL ( )
        {

        }

        public List<aspnet_User> GetAllOldSystemUsers()
        {            
            List<AKMII.SafeSplash.EntityNew.aspnet_User> list = new List< aspnet_User>();

            list = (from c in ctxNew.aspnet_Users select c).ToList<aspnet_User>();

            return list; 
        }

        public aspnet_UsersInRole GetOldUserRole( Guid UserId)
        {
            return ctxNew.aspnet_UsersInRoles.SingleOrDefault<aspnet_UsersInRole>( r => r.UserId == UserId);
        }

        public tb_Employee GetEmployeeByUserID(Guid userID)
        {
            tb_Employee dataInfo = ctxNew.tb_Employees.SingleOrDefault<tb_Employee>(d => d.user_ID == userID);
            return dataInfo;
        }

        public tb_Client GetClientByUserID(Guid userID)
        {
            tb_Client dataInfo = ctxNew.tb_Clients.SingleOrDefault<tb_Client>(d => d.user_ID == userID);
            return dataInfo;
        }


        public List<AspNetUser> GetAllNewSystemUsers()
        {
            EmployeeBll empBLL = new EmployeeBll();

            return empBLL.GetAllUsers();            
        }

        public List<tb_Employee> GetEmployees()
        {
            EmployeeBll empBLL = new EmployeeBll();

            return empBLL.GetAll();
        }

        public List<tb_Client> GetClients()
        {
            ///RAB convert to ClientBLL once converted
            List<tb_Client> list = new List<tb_Client>();

            list = ctxNew.GetTable<tb_Client>().OrderBy(c => c.client_LastName).ThenBy(c => c.client_FirstName).ToList<tb_Client>();
            return list;
        }


    }
}
