﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class InstructorSubstituteBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_InstructorSubstitute info)
        {
            ctx.tb_InstructorSubstitute.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_InstructorSubstitute info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_InstructorSubstitute configInfo = ctx.tb_InstructorSubstitute.SingleOrDefault<tb_InstructorSubstitute>(d => d.instsub_ID == id);
            if (configInfo != null)
            {
                ctx.tb_InstructorSubstitute.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public void Delete(tb_InstructorSubstitute info)
        {
            if (info != null)
            {
                ctx.tb_InstructorSubstitute.DeleteOnSubmit(info);
                ctx.SubmitChanges();
            }
        }

        public tb_InstructorSubstitute GetByClassADate(Guid activeClassID, DateTime dtDate)
        {
            tb_InstructorSubstitute configInfo = ctx.tb_InstructorSubstitute.SingleOrDefault<tb_InstructorSubstitute>(d => d.activecs_ID == activeClassID && d.instsub_Date == dtDate);
            return configInfo;
        }
    }
}
