﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class PoolBll
    {
        public Facility Facility
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();
        protected BayBll bayBll = new BayBll();

        public void Add(tb_Pool info)
        {
            ctx.tb_Pool.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Add(tb_Pool info, List<tb_Bay> bays)
        {
            ctx.tb_Pool.InsertOnSubmit(info);
            ctx.SubmitChanges();

            foreach (tb_Bay bay in bays)
            {
                bay.pool_ID = info.pool_ID;
            }
            bayBll.Add(bays);
        }

        public void Update(tb_Pool info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_Pool configInfo = ctx.tb_Pool.SingleOrDefault<tb_Pool>(d => d.pool_ID == id);
            if (configInfo != null)
            {
                ctx.tb_Pool.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_Pool> GetAll()
        {
            List<tb_Pool> list = new List<tb_Pool>();

            list = ctx.GetTable<tb_Pool>().ToList<tb_Pool>();
            return list;
        }

        public tb_Pool GetByID(Guid id)
        {
            tb_Pool dataInfo = ctx.tb_Pool.SingleOrDefault<tb_Pool>(d => d.pool_ID == id);
            return dataInfo;
        }

        public tb_Pool GetByLocationAName(Guid localID, string poolName)
        {
            tb_Pool dataInfo = ctx.tb_Pool.SingleOrDefault<tb_Pool>(d => d.loca_ID == localID && d.pool_Name == poolName);
            return dataInfo;
        }

        public List<tb_Pool> GetByLocationID(Guid localID)
        {
            List<tb_Pool> listPool = new List<tb_Pool>();

            listPool = (from c in ctx.tb_Pool
                       where c.loca_ID == localID
                       select c).ToList<tb_Pool>();
            return listPool;
        }
    }
}
