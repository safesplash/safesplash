﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class TaskBll
    {
         protected SafeSplashDBDataContext ctx = null;
        public TaskBll()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public TaskBll(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }

        public void Add(tb_Task info)
        {
            ctx.tb_Task.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_Task info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_Task configInfo = ctx.tb_Task.SingleOrDefault<tb_Task>(d => d.task_ID == id);
            if (configInfo != null)
            {
                ctx.tb_Task.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_Task> GetAll()
        {
            List<tb_Task> list = new List<tb_Task>();

            list = ctx.GetTable<tb_Task>().ToList<tb_Task>();
            return list;
        }

        public tb_Task GetByID(Guid id)
        {
            tb_Task dataInfo = ctx.tb_Task.SingleOrDefault<tb_Task>(d => d.task_ID == id);
            return dataInfo;
        }

        public List<tb_Task> GetTasksForEmployee(Guid employeeId)
        {
            return GetTasksForEmployee(employeeId, string.Empty);
        }

        public List<tb_Task> GetTasksForEmployee(Guid employeeId, string status)
        {
            List<tb_Task> list = new List<tb_Task>();
            list = (from c in ctx.tb_Task
                    where (c.task_AssignedTo.Contains(employeeId.ToString()) || c.task_Recipient.Contains(employeeId.ToString())) && (!string.IsNullOrEmpty(status) || c.task_Status.Trim().ToUpper() == status.Trim().ToUpper())
                    select c).ToList<tb_Task>();
            return list;
        }

        public List<tb_Task> GetOpenTasksForEmployee(string emplId)
        {
           List<tb_Task> list = new List<tb_Task>();
            list = (from c in ctx.tb_Task
                    where (c.task_AssignedTo.Contains(emplId) || c.task_Recipient.Contains(emplId)) && c.task_Status != "Closed"
                    orderby c.task_CreatedTime descending
                    select c).ToList<tb_Task>();
            return list;
        }

        public List<tb_Task> GetClosedTasksForEmployee(string emplId)
        {
            List<tb_Task> list = new List<tb_Task>();
            list = (from c in ctx.tb_Task
                    where (c.task_AssignedTo.Contains(emplId) || c.task_Recipient.Contains(emplId)) && c.task_Status == "Closed"
                    select c).ToList<tb_Task>();
            return list;
        }

        public List<tb_Task> GetTasksCreatedByEmployee(Guid emplID)
        {
            List<tb_Task> list = new List<tb_Task>();
            list = (from c in ctx.tb_Task
                    where c.task_Creator == emplID
                    select c).ToList<tb_Task>();
            return list;
        }       
    }
}
