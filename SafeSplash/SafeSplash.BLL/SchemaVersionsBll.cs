﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class SchemaVersionsBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(aspnet_SchemaVersions info)
        {
            ctx.aspnet_SchemaVersions.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }
    }
}
