﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class WithdrawalReasonBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_WithdrawalReason info)
        {
            ctx.tb_WithdrawalReasons.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_WithdrawalReason info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_WithdrawalReason configInfo = ctx.tb_WithdrawalReasons.Single<tb_WithdrawalReason>(d => d.reason_ID == id);
            configInfo.reason_Active = false;
            ctx.Refresh(RefreshMode.KeepCurrentValues, configInfo);
            ctx.SubmitChanges();
        }
        public List<tb_WithdrawalReason> GetAll(bool isActive)
        { 
            List<tb_WithdrawalReason> activeReasons = null;
            if (isActive)
            {

                activeReasons = (from reason in ctx.tb_WithdrawalReasons
                                 where reason.reason_Active
                                 orderby reason.reason_Name
                                 select reason).Distinct().ToList<tb_WithdrawalReason>();
            }
            else
            {
                activeReasons = (from reason in ctx.tb_WithdrawalReasons
                                 where !reason.reason_Active
                                 orderby reason.reason_Name
                                 select reason).Distinct().ToList<tb_WithdrawalReason>();
            }
            return activeReasons;
        }
        public List<tb_WithdrawalReason> GetAll()
        {
            List<tb_WithdrawalReason> list = new List<tb_WithdrawalReason>();

            list = ctx.GetTable<tb_WithdrawalReason>().ToList<tb_WithdrawalReason>();
            return list;
        }

        public List<tb_WithdrawalReason> GetActiveReasons()
        {
            List<tb_WithdrawalReason> list = new List<tb_WithdrawalReason>();

            list = (from c in ctx.tb_WithdrawalReasons
                       where c.reason_Active != false
                    select c).ToList<tb_WithdrawalReason>();
            return list;
        }

        public tb_WithdrawalReason GetByID(Guid id)
        {
            tb_WithdrawalReason dataInfo = ctx.tb_WithdrawalReasons.Single<tb_WithdrawalReason>(d => d.reason_ID == id);
            return dataInfo;
        }

        public bool Existing(string reason_Name)
        {
            tb_WithdrawalReason dataInfo = ctx.tb_WithdrawalReasons.SingleOrDefault<tb_WithdrawalReason>(d => d.reason_Name == reason_Name);
            return (!(dataInfo == null));
        }
    }
}
