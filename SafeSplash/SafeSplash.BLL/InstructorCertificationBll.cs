﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class InstructorCertificationBll
    {
        protected SafeSplashDBDataContext ctx = null;

        public InstructorCertificationBll()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public InstructorCertificationBll(string connection)
        {
            ctx = new SafeSplashDBDataContext(connection);
        }

        public void Add(tb_InstructorCertification info)
        {
            ctx.tb_InstructorCertification.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Add(List<tb_InstructorCertification> infos)
        {
            ctx.tb_InstructorCertification.InsertAllOnSubmit<tb_InstructorCertification>(infos);
            ctx.SubmitChanges();
        }

        public void Update(tb_InstructorCertification info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_InstructorCertification configInfo = ctx.tb_InstructorCertification.SingleOrDefault<tb_InstructorCertification>(d => d.Instcf_ID == id);
            if (configInfo != null)
            {
                ctx.tb_InstructorCertification.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_InstructorCertification> GetAll()
        {
            List<tb_InstructorCertification> list = new List<tb_InstructorCertification>();

            list = ctx.GetTable<tb_InstructorCertification>().ToList<tb_InstructorCertification>();
            return list;
        }

        public tb_InstructorCertification GetByID(Guid id)
        {
            tb_InstructorCertification dataInfo = ctx.tb_InstructorCertification.SingleOrDefault<tb_InstructorCertification>(d => d.Instcf_ID == id);
            return dataInfo;
        }

        public List<vw_InstructorCertInfo> GetByEmployeeID(Guid emplID)
        {
            List<vw_InstructorCertInfo> list = new List<vw_InstructorCertInfo>();

            list = (from i in ctx.vw_InstructorCertInfo
                    where i.empl_ID == emplID
                    select i).ToList<vw_InstructorCertInfo>();

            return list;
        }

        public tb_InstructorCertification GetByUserCert(Guid user_ID, Guid cert_ID)
        {
            tb_InstructorCertification dataInfo = ctx.tb_InstructorCertification.SingleOrDefault<tb_InstructorCertification>(d => d.empl_ID == user_ID && d.cert_ID == cert_ID);
            return dataInfo;
        }

        public List<tb_InstructorCertification> GetExpiringCert(int days)
        {
            List<tb_InstructorCertification> list = new List<tb_InstructorCertification>();

            list = (from i in ctx.tb_InstructorCertification
                    where i.Instcf_ExpirationDate != null && ((DateTime)i.Instcf_ExpirationDate).CompareTo(DateTime.Today.AddDays(days)) > 0
                    orderby i.empl_ID
                    select i).ToList<tb_InstructorCertification>();

            return list;
        }

        public List<tb_InstructorCertification> GetExpiringCert(Guid[] locationIDs, int days)
        {
            List<tb_InstructorCertification> list = new List<tb_InstructorCertification>();

            list = (from i in ctx.tb_InstructorCertification
                    join cl in ctx.tb_Employees
                    on i.empl_ID equals cl.empl_ID
                    join ct in ctx.tb_EmployeeLocations
                    on cl.empl_ID equals ct.empl_ID
                    where locationIDs.Contains((Guid)ct.loca_ID) && i.Instcf_ExpirationDate != null && ((DateTime)i.Instcf_ExpirationDate).CompareTo(DateTime.Today.AddDays(days)) > 0
                    orderby i.empl_ID
                    select i).ToList<tb_InstructorCertification>();

            return list;
        }
    }
}
