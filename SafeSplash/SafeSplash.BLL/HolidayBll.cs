﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class HolidayBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_Holiday info)
        {
            ctx.tb_Holiday.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_Holiday info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(Guid id)
        {
            tb_Holiday configInfo = ctx.tb_Holiday.SingleOrDefault<tb_Holiday>(d => d.holi_ID == id);
            if (configInfo != null)
            {
                ctx.tb_Holiday.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_Holiday> GetAll()
        {
            List<tb_Holiday> list = new List<tb_Holiday>();

            list = ctx.GetTable<tb_Holiday>().ToList<tb_Holiday>();
            return list;
        }

        public tb_Holiday GetByID(Guid id)
        {
            tb_Holiday dataInfo = ctx.tb_Holiday.SingleOrDefault<tb_Holiday>(d => d.holi_ID == id);
            return dataInfo;
        }
    }
}
