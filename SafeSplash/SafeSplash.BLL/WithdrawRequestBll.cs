﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class WithdrawRequestBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_WithdrawalRequest info)
        {
            ctx.tb_WithdrawalRequests.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Update(tb_WithdrawalRequest info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(tb_WithdrawalRequest info)
        {
            ctx.tb_WithdrawalRequests.DeleteOnSubmit(info);
            ctx.SubmitChanges();
        }

        public tb_WithdrawalRequest GetByID(Guid id)
        {
            tb_WithdrawalRequest dataInfo = ctx.tb_WithdrawalRequests.SingleOrDefault<tb_WithdrawalRequest>(d => d.withdraw_ID == id);
            return dataInfo;
        }

        public List<tb_WithdrawalRequest> GetByStatus(string status)
        {
            List<tb_WithdrawalRequest> listBay = new List<tb_WithdrawalRequest>();

            listBay = (from c in ctx.tb_WithdrawalRequests
                       where c.withdraw_Status == status
                       select c).ToList<tb_WithdrawalRequest>();
            return listBay;
        }

        public List<tb_WithdrawalRequest> GetByLocaAStatus(Guid[] locationIDs, string status)
        {
            List<tb_WithdrawalRequest> listBay = new List<tb_WithdrawalRequest>();

            listBay = (from c in ctx.tb_WithdrawalRequests
                       join cl in ctx.tb_StudentBooks
                       on c.studch_ID equals cl.stud_ID
                       join cx in ctx.tb_ActiveClass
                       on cl.activecs_ID equals cx.activecs_ID
                       where locationIDs.Contains((Guid)cx.loca_ID) && c.withdraw_Status == status
                       select c).ToList<tb_WithdrawalRequest>();
            return listBay;
        }
    }
}
