﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using System.Data.Linq;

namespace AKMII.SafeSplash.BLL
{
    public class StateBll
    {
        protected SafeSplashDBDataContext ctx = new SafeSplashDBDataContext();

        public void Add(tb_State info)
        {
            ctx.tb_State.InsertOnSubmit(info);
            ctx.SubmitChanges();
        }

        public void Add(List<tb_State> infos)
        {
            ctx.tb_State.InsertAllOnSubmit<tb_State>(infos);
            ctx.SubmitChanges();
        }



        public void ClearTable()
        {
            List<tb_State> list = new List<tb_State>();

            list = ctx.GetTable<tb_State>().ToList<tb_State>();
            ctx.tb_State.DeleteAllOnSubmit<tb_State>(list);
            ctx.SubmitChanges();
        }

        public void Update(tb_State info)
        {
            ctx.Refresh(RefreshMode.KeepCurrentValues, info);
            ctx.SubmitChanges();
        }

        public void Delete(string code)
        {
            tb_State configInfo = ctx.tb_State.SingleOrDefault<tb_State>(d => d.state_Code == code);
            if (configInfo != null)
            {
                ctx.tb_State.DeleteOnSubmit(configInfo);
                ctx.SubmitChanges();
            }
        }

        public List<tb_State> GetAll()
        {
            List<tb_State> list = new List<tb_State>();

            list = ctx.GetTable<tb_State>().ToList<tb_State>();
            return list;
        }

        public tb_State GetByID(string code)
        {
            tb_State dataInfo = ctx.tb_State.SingleOrDefault<tb_State>(d => d.state_Code == code);
            return dataInfo;
        }
    }
}
