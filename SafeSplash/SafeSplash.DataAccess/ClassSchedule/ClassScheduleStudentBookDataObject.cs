﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.ClassSchedule
{
    [Serializable]
    public class ClassScheduleStudentBookDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid ClassId { get; set; }
        public virtual Guid StudentId { get; set; }
        public virtual ClassScheduleClassDataObject Class { get; set; }
        public virtual DateTime? StudentJoinDate { get; set; }
        public virtual DateTime? StudentWithdrawalDate { get; set; }
        public virtual bool? IsWithdrawn { get; set; }
        public virtual ClassScheduleClientStudentDataObject Student { get; set; }
    }
}
