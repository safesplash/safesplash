﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.ClassSchedule
{
    [Serializable]
    public class ClassScheduleClassDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid LocationId { get; set; }
        public virtual string LevelName { get; set; }
        public virtual int StudentCapacity { get; set; }
        public virtual string DayOfWeek { get; set; }
        public virtual DateTime BeginDate { get; set; }
        public virtual DateTime EndDate { get; set; }
        public virtual TimeSpan StartTime { get; set; }
        public virtual TimeSpan EndTime { get; set; }        
        //public virtual DateTime? StudentJoinDate { get; set; }
        //public virtual DateTime? StudentWithdrawalDate { get; set; }
        //public virtual bool? StudentIsWithdrawn { get; set; }
        //public virtual string StudentFirstName { get; set; }
        //public virtual string StudentLastName { get; set; }
        //public virtual DateTime StudentDateOfBirth { get; set; }
        //public virtual IEnumerable<ClassScheduleStudentBookDataObject> StudentBooks { get; set; }
    }
}
