﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.ClassSchedule
{
    [Serializable]
    public class ClassScheduleDataObject
    {
        public virtual Guid ActiveClassId { get; set; }
        public virtual Guid ClassId { get; set; }
        public virtual Guid LocationId { get; set; }
        public virtual Guid LevelId { get; set; }
        public virtual string LevelName { get; set; }
        public virtual string ClassName { get; set; }
        public virtual DateTime ClassStartDate { get; set; }
        public virtual DateTime? ClassEndDate { get; set; }
        public virtual DateTime ClassDate { get; set; }
        public virtual TimeSpan ClassStartTime { get; set; }
        public virtual TimeSpan ClassEndTime { get; set; }
        public virtual int StudentCapacity { get; set; }
        public virtual int SpotsRemaining { get; set; }
        public virtual decimal AverageStudentAge { get; set; }
        public virtual Guid InstructorId { get; set; }
        public virtual string InstructorFirstName { get; set; }
        public virtual string InstructorLastName { get; set; }
        public virtual string InstructorPrimarySegment { get; set; }
        public virtual string InstructorSecondarySegment { get; set; }
        public virtual decimal ClassPrice { get; set; }

    }
}
