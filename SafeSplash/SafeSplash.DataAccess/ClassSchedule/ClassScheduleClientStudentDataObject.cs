﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.ClassSchedule
{
    [Serializable]
    public class ClassScheduleClientStudentDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid StudentId { get; set; }
        public virtual IEnumerable<ClassScheduleStudentBookDataObject> StudentBooks { get; set; }
    }
}
