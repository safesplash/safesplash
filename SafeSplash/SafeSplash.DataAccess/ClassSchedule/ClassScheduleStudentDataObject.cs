﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.ClassSchedule
{
    [Serializable]
    public class ClassScheduleStudentDataObject
    {
        public virtual Guid StudentBookId { get; set; }
        public virtual Guid ActiveClassId { get; set; }
        public virtual Guid StudentId { get; set; }
        public virtual DateTime? JoinDate { get; set; }
        public virtual bool IsWithdrawn { get; set; }
        public virtual DateTime? WithdrawalDate { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual bool Gender { get; set; }
        public virtual DateTime DateOfBirth { get; set; }
    }
}
