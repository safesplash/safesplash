﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.ClassSchedule
{
    public interface IClassScheduleDataAccess
    {
        //IEnumerable<ClassScheduleClassDataObject> GetClasses(DateTime beginDate, DateTime endDate, Guid locationId, string dayOfWeek);
        //IEnumerable<ClassScheduleStudentBookDataObject> GetStudentsBooked(IEnumerable<Guid> classIds, DateTime beginDate, DateTime endDate);

        IEnumerable<ClassScheduleStudentDataObject> GetStudentsBooked(IEnumerable<Guid> classIds, DateTime beginDate, DateTime endDate);

        IEnumerable<ClassScheduleDataObject> GetClassesForClassSchedule(Guid locationId, DateTime asOfDate, string dayOfWeek);

        //IEnumerable<ClassScheduleDataObject> GetClassesForClassSchedule(Guid locationId, DateTime asOfDate, string dayOfWeek, IEnumerable<Guid> classIds, IEnumerable<Guid> levelIds, Guid instructorId, int instructorSegmentId);
    }
}
