﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.PaymentGateway
{
    [Serializable]
    public class ClientBillingMethodsDataObject
    {
        public virtual int clientBillingMethodID { get; set; }
        public virtual Guid clientID { get; set; }
        public virtual string cardType { get; set; }
        public virtual string cardNumber { get; set; }
        public virtual DateTime expDate { get; set; }
        public virtual string expMonth { get; set; }
        public virtual string expYear { get; set; }
        public virtual string firstName { get; set; }
        public virtual string lastName { get; set; }
        public virtual string cardAddress { get; set; }
        public virtual string cardCity { get; set; }
        public virtual string cardState { get; set; }
        public virtual string cardZip { get; set; }
        public virtual string cardCountry { get; set; }
        public virtual string cardHolder { get; set; }
        public virtual string cardAddress2 { get; set; }
        public virtual string clientACHID { get; set; }
        public virtual string paymentMethodID { get; set; }
        public virtual string type { get; set; }
        public virtual string CheckTRN { get; set; }
        public virtual string CheckNumber { get; set; }
        public virtual string CheckType { get; set; }
        public virtual bool SameAddress { get; set; }
    }
}
