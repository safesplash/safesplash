using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.PaymentGateway
{
    public interface IPaymentGatewayDataAccess
    {
        PaymentGatewaySettlementsDataObject SavePaymentGatewaySettlement(PaymentGatewaySettlementsDataObject settlement);
        ClientBillingMethodsDataObject SaveClientBillingMethodsDataObject(ClientBillingMethodsDataObject billing);
        IEnumerable<ClientBillingMethodsDataObject> GetClientBillingMethodsByClientID(Guid clientID);
        List<ClientSettlementDataObject> GetSettlementsLocal(string location, DateTime fromDate, DateTime toDate);
        void SavePaymentCustomerDataObject(paymentCustomerDataObject customer);
        bool CheckDupe(int customerID, string customerToken);
        void UpdateCustomer(string customerToken, string defaultPaymethodToken, string accountID, string status, string postalCode, string region, string street_line1, string street_line2, string locality, string email_address, string phone_number, string settlementPath, string transactionPath, string paymethodPath, string addressPath, string selfPath, DateTime lastUpdated);
        bool CheckTransactionDupe(string transactionID);
        bool CheckPaymethodDupe(string paymethodToken);
        bool CheckSettlementDupe(string settlementID);
        void AddTransaction(ClientTransactionDataObject transaction);
        void AddSettlement(ClientSettlementDataObject settlement);
        int AddPaymethod(ClientPaymethodDataObject paymethod);
    }
}
