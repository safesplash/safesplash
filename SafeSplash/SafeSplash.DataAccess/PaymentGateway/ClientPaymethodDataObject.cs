﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.PaymentGateway
{
    public class ClientPaymethodDataObject
    {
        public int client_paymethodID { get; set; }
        public Guid client_ID { get; set; }
        public bool procurement_card { get; set; }
        public string card_type { get; set; }
        public int expire_year { get; set; }
        public int expire_month { get; set; }
        public string masked_account_number { get; set; }
        public string name_on_card { get; set; }
        public string notes { get; set; }
        public string label { get; set; }
        public string customer_token { get; set; }
        public string location_id { get; set; }
        public string account_id { get; set; }
        public string paymethod_token { get; set; }
        public string routing_number { get; set; }
        public string account_type { get; set; }
    }
}
