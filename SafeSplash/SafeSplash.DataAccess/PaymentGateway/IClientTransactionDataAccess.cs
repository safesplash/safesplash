using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.Client;

namespace SafeSplash.DataAccess.PaymentGateway
{
    public interface IClientTransactionDataAccess
    {
        ClientTransactionDataObject SaveClientTransaction(ClientTransactionDataObject transaction);
        ClientPaymethodDataObject SaveClientPaymethod(ClientPaymethodDataObject paymethod);
        bool CheckTransactionDupe(string transactionID);
        bool checkPaymethodDupe(string paymethod_token);
        ClientMatch GetClientIDByCustomerID(int CustomerID);
        List<paymentCustomer> getClients();
        void updateCustomer(string customerToken);
    }
}