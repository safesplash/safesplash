﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.PaymentGateway
{
    [Serializable]
    public class ClientTransactionDataObject
    {
        public virtual int clientTransactionID { get; set; }
        public virtual Guid clientID { get; set; }
        public virtual int customerID { get; set; }
        public virtual string forteTransactionID { get; set; }
        public virtual string transactionType { get; set; }
        public virtual decimal amount { get; set; }
        public virtual DateTime dateCreated { get; set; }
        public virtual bool batched { get; set; }
        public virtual string batchID { get; set; }
        public virtual DateTime dateBatched { get; set; }
        public virtual string cardNumber { get; set; }
        public virtual string cardType { get; set; }
        public virtual string cardExp { get; set; }
        public virtual string locationID { get; set; }
        public virtual string merchantID { get; set; }
        public virtual string customerToken { get; set; }
        public virtual string accountID { get; set; }
        public virtual string firstName { get; set; }
        public virtual string lastName { get; set; }
        public virtual string authCode { get; set; }
        public virtual string enteredBy { get; set; }
        public virtual string paymethodToken { get; set; }
    }
}
