﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.PaymentGateway
{
    public class paymentCustomerDataObject
    {
        public virtual int paymentCustomerID { get; set; }
        public virtual int customerID { get; set; }
        public virtual string customerToken { get; set; }
        public virtual string firstName { get; set; }
        public virtual string lastName { get; set; }
        public virtual string lastUpdated { get; set; }
        public virtual string locationID { get; set; }
    }

    public class paymentCustomer
    {
        public int paymentCustomerID { get; set; }
        public Guid client_ID { get; set; }
        public int customerID { get; set; }
        public string customerToken { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string locationID { get; set; }
        public string defaultPaymethodToken { get; set; }
        public string accountID { get; set; }
        public string status { get; set; }
        public string postalCode { get; set; }
        public string region { get; set; }
        public string street_line1 { get; set; }
        public string street_line2 { get; set; }
        public string locality { get; set; }
        public string email_address { get; set; }
        public string phone_number { get; set; }
        public string settlementPath { get; set; }
        public string transactionPath { get; set; }
        public string paymethodPath { get; set; }
        public string addressPath { get; set; }
        public string selfPath { get; set; }
        public DateTime lastUpdated { get; set; }
    }
}
