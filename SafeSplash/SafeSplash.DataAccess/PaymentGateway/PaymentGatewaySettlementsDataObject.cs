﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.PaymentGateway
{
    [Serializable]
    public class PaymentGatewaySettlementsDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual string SettlementID { get; set; }
        public virtual string TransactionID { get; set; }
        public virtual long CustomerID { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string Method { get; set; }
        public virtual string Type { get; set; }
    }
}
