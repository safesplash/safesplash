﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.PaymentGateway
{
    public class ClientSettlementDataObject
    {
        public int client_settlementID { get; set; }
        public Guid client_ID { get; set; }
        public string settlementID { get; set; }
        public string transactionID { get; set; }
        public string customerToken { get; set; }
        public string customerID { get; set; }
        public DateTime settle_date { get; set; }
        public string settle_type { get; set; }
        public string settle_response_code { get; set; }
        public decimal settle_amount { get; set; }
        public string method { get; set; }
    }
}
