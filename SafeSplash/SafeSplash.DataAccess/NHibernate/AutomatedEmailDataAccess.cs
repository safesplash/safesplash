﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.NHibernate
{
    public interface IAutomatedEmailDataAccess
    {
        IAutomatedEmailDataObject Get(int id);
        IAutomatedEmailDataObject Save(IAutomatedEmailDataObject email);
    }

    public interface IAutomatedEmailDataObject
    {
        int Id { get; set; }
        string Name { get; set; }
        string Subject { get; set; }
        string Content { get; set; }
        string Cc { get; set; }
        string Bcc { get; set; }        
    }

    public class AutomatedEmailDataObject : IAutomatedEmailDataObject
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Subject { get; set; }
        public virtual string Content { get; set; }
        public virtual string Cc { get; set; }
        public virtual string Bcc { get; set; }
    }

    public class AutomatedEmailDataAccess : IAutomatedEmailDataAccess
    {
        public IAutomatedEmailDataObject Get(int id)
        {
            return NHibernateUtil.Get(new AutomatedEmailDataObject(), id);
        }

        public IAutomatedEmailDataObject Save(IAutomatedEmailDataObject email)
        {
            return NHibernateUtil.Save(email);
        }
    }
}
