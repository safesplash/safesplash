﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.NHibernate;
using NHibernate;
using NHibernate.Criterion;

namespace SafeSplash.DataAccess.NHibernate
{
    public interface IEmployeeActionDataAccess
    {
        IEmployeeActionDataObject Save(IEmployeeActionDataObject action);
        //IEnumerable<IEmployeeActionDataObject> GetActions(DateTime startDate, DateTime endDate);
        //IEnumerable<IEmployeeActionDataObject> GetActions(Guid employeeId, DateTime startDate, DateTime endDate);
        IEnumerable<IEmployeeActionDataObject> GetActions(Guid? employeeId, string action, DateTime startDate, DateTime endDate);
    }

    public interface IEmployeeActionDataObject
    {
        Guid Id { get; set; }
        Guid EmployeeId { get; set; }
        string Action { get; set; }
        DateTime Date { get; set; }
        string Description { get; set; }
        string Value { get; set; }
    }

    public class EmployeeActionDataObject : IEmployeeActionDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid EmployeeId { get; set; }
        public virtual string Action { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string Description { get; set; }
        public virtual string Value { get; set; }
    }

    public class EmployeeActionDataAccess : IEmployeeActionDataAccess
    {
        public IEmployeeActionDataObject Save(IEmployeeActionDataObject action)
        {
            return NHibernateUtil.Save(action);
        }

        //public IEnumerable<IEmployeeActionDataObject> GetActions(DateTime startDate, DateTime endDate)
        //{
        //    using (ISession session = NHibernateUtil.OpenSession())
        //    {
        //        using (var transaction = session.BeginTransaction())
        //        {
        //            IEnumerable<IEmployeeActionDataObject> actions = session.CreateCriteria<IEmployeeActionDataObject>().Add(Restrictions.Ge("Date", startDate)).Add(Restrictions.Le("Date", endDate)).List<IEmployeeActionDataObject>();
        //            return actions;
        //        }

        //    }
        //}

        //public IEnumerable<IEmployeeActionDataObject> GetActions(Guid employeeId, DateTime startDate, DateTime endDate)
        //{
        //    using (ISession session = NHibernateUtil.OpenSession())
        //    {
        //        using (var transaction = session.BeginTransaction())
        //        {
        //            IEnumerable<IEmployeeActionDataObject> actions = session.CreateCriteria<IEmployeeActionDataObject>().Add(Restrictions.Eq("EmployeeId", employeeId)).Add(Restrictions.Ge("Date", startDate)).Add(Restrictions.Le("Date", endDate)).List<IEmployeeActionDataObject>();
        //            return actions;
        //        }

        //    }
        //}

        public IEnumerable<IEmployeeActionDataObject> GetActions(Guid? employeeId, string action, DateTime startDate, DateTime endDate)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    //IEnumerable<IEmployeeActionDataObject> actions = session.CreateCriteria<IEmployeeActionDataObject>().Add(Restrictions.Eq("EmployeeId", employeeId)).Add(Restrictions.Ge("Date", startDate)).Add(Restrictions.Le("Date", endDate)).List<IEmployeeActionDataObject>();
                    ICriteria criteria = session.CreateCriteria<IEmployeeActionDataObject>().Add(Restrictions.Ge("Date", startDate)).Add(Restrictions.Le("Date", endDate));
                    if (employeeId.HasValue)
                    {
                        criteria.Add(Restrictions.Eq("EmployeeId", employeeId.Value));
                    }
                    if (!string.IsNullOrEmpty(action))
                    {
                        criteria.Add(Restrictions.Eq("Action", action));
                    }
                    IEnumerable<IEmployeeActionDataObject> actions = criteria.List<IEmployeeActionDataObject>();
                    return actions;
                }

            }
        }
    }
}
