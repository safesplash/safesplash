﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;

namespace SafeSplash.DataAccess.NHibernate
{
    [Serializable]
    public class LocationPriceDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid LocationId { get; set; }
        public virtual decimal Price { get; set; }
        public virtual DateTime BeginDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual string Comment { get; set; }
        public virtual IEnumerable<ClassPriceProRateDataObject> ProRates { get; set; }
    }

    [Serializable]
    public class ClassPriceDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid ClassId { get; set; }
        public virtual decimal Price { get; set; }
        public virtual DateTime BeginDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual string Comment { get; set; }
    }

    [Serializable]
    public class LocationClassPriceDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid LocationId { get; set; }
        public virtual Guid ClassId { get; set; }
        public virtual decimal Price { get; set; }
        public virtual DateTime BeginDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual string Comment { get; set; }
        public virtual IEnumerable<ClassPriceProRateDataObject> ProRates { get; set; }
    }

    [Serializable]
    public class ActiveClassPriceDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid ActiveClassId { get; set; }
        public virtual decimal Price { get; set; }
        public virtual DateTime BeginDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual string Comment { get; set; }
        public virtual IEnumerable<ClassPriceProRateDataObject> ProRates { get; set; }
    }

    [Serializable]
    public class ClassPriceProRateDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid? ActiveClassPriceId { get; set; }
        public virtual Guid? LocationClassPriceId { get; set; }
        public virtual Guid? LocationPriceId { get; set; }
        public virtual int StudentNumber { get; set; }
        public virtual int ClassesMissed { get; set; }
        public virtual decimal Price { get; set; }
        public virtual string Comment { get; set; }
    }

    [Serializable]
    public class ClassPricingProRatesValidationResult
    {
        public bool ValidationSuccessful { get; set; }
        public string ValidationMessage { get; set; }
    }

    [Serializable]
    public class LocationFixedDiscountDataObject
    {
        public virtual int Id { get; set; }
        public virtual Guid LocationId { get; set; }
        public virtual DateTime BeginDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual decimal Amount { get; set; }
    }

    [Serializable]
    public class LocationPercentageDiscountDataObject
    {
        public virtual int Id { get; set; }
        public virtual Guid LocationId { get; set; }
        public virtual DateTime BeginDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual decimal Percentage { get; set; }
    }

    public interface IClassPricingDataAccess
    {
        // Active Class Pricings
        ActiveClassPriceDataObject GetActiveClassPricing(Guid id);
        IEnumerable<ActiveClassPriceDataObject> GetActiveClassPricings(Guid activeClassId);
        IEnumerable<ClassPriceProRateDataObject> GetActiveClassPricingProRates(Guid activeClassPricingId);
        ActiveClassPriceDataObject GetActiveClassPricingForDate(Guid activeClassId, DateTime asOfDate);
        ActiveClassPriceDataObject SaveActiveClassPricing(ActiveClassPriceDataObject classPricing);
        void DeleteActiveClassPricing(ActiveClassPriceDataObject classPricing);

        // Location Pricings
        LocationPriceDataObject GetLocationPricing(Guid id);
        IEnumerable<LocationPriceDataObject> GetLocationPricings(Guid locationId);
        IEnumerable<ClassPriceProRateDataObject> GetLocationPricingProRates(Guid locationPricingId);
        LocationPriceDataObject GetLocationPricingForDate(Guid locationId, DateTime asOfDate);
        LocationPriceDataObject SaveLocationPricing(LocationPriceDataObject locationPricing);
        void DeleteLocationPricing(LocationPriceDataObject locationPricing);

        // Location Class Pricings
        LocationClassPriceDataObject GetLocationClassPricing(Guid id);
        IEnumerable<LocationClassPriceDataObject> GetLocationClassPricings(Guid locationId);
        IEnumerable<LocationClassPriceDataObject> GetLocationClassPricings(Guid locationId, Guid classId);
        IEnumerable<ClassPriceProRateDataObject> GetLocationClassPricingProRates(Guid locationClassPricingId);
        LocationClassPriceDataObject GetLocationClassPricingForDate(Guid locationId, Guid classId, DateTime asOfDate);
        LocationClassPriceDataObject SaveLocationClassPricing(LocationClassPriceDataObject locationClassPricing);
        void DeleteLocationClassPricing(LocationClassPriceDataObject locationClassPricing);

        ClassPricingProRatesValidationResult ValidateClassPriceProRates(IEnumerable<ClassPriceProRateDataObject> proRates);
        ClassPriceProRateDataObject GetClassPriceProRate(Guid id);
        ClassPriceProRateDataObject SaveClassPriceProRate(ClassPriceProRateDataObject classPriceProRate);
        void DeleteClassPriceProRate(ClassPriceProRateDataObject classPriceProRate);

        // Discounts
        IEnumerable<LocationFixedDiscountDataObject> GetAllLocationFixedDiscounts();
        IEnumerable<LocationPercentageDiscountDataObject> GetAllLocationPercentageDiscounts();
    }

    public class ClassPricingDataAccess : IClassPricingDataAccess
    {
        public IEnumerable<LocationPriceDataObject> GetLocationPricings(Guid locationId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    IEnumerable<LocationPriceDataObject> pricings = session.CreateCriteria<LocationPriceDataObject>().Add(Restrictions.Eq("LocationId", locationId)).List<LocationPriceDataObject>();
                    return pricings;
                }
            }
        }

        public LocationPriceDataObject GetLocationPricingForDate(Guid locationId, DateTime asOfDate)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<LocationPriceDataObject>();
                    criteria.Add(Restrictions.Eq("LocationId", locationId));
                    criteria.Add(Restrictions.Or(Restrictions.And(Restrictions.Le("BeginDate", asOfDate), Restrictions.Ge("EndDate", asOfDate)), Restrictions.And(Restrictions.Le("BeginDate", asOfDate), Restrictions.IsNull("EndDate"))));
                    LocationPriceDataObject pricing = criteria.UniqueResult<LocationPriceDataObject>();
                    if (pricing != null)
                    {
                        IEnumerable<ClassPriceProRateDataObject> proRates = GetLocationPricingProRates(pricing.Id);
                        if (proRates != null)
                        {
                            pricing.ProRates = proRates;
                        }
                    }
                    return pricing;
                }
            }
        }

        public IEnumerable<ActiveClassPriceDataObject> GetActiveClassPricings(Guid activeClassId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    IEnumerable<ActiveClassPriceDataObject> pricings = session.CreateCriteria<ActiveClassPriceDataObject>().Add(Restrictions.Eq("ActiveClassId", activeClassId)).List<ActiveClassPriceDataObject>();
                    List<ActiveClassPriceDataObject> pricingsList = new List<ActiveClassPriceDataObject>();
                    foreach (var pdo in pricings)
                    {
                        ActiveClassPriceDataObject acp = new ActiveClassPriceDataObject();
                        acp = NHibernateUtil.Get(acp, pdo.Id);
                        if (acp != null)
                        {
                            IEnumerable<ClassPriceProRateDataObject> proRates = GetActiveClassPricingProRates(pdo.Id);
                            if (proRates != null)
                            {
                                acp.ProRates = proRates;
                            }
                        }
                        pricingsList.Add(acp);
                    }

                    return pricingsList;
                }
            }
        }

        public ActiveClassPriceDataObject GetActiveClassPricingForDate(Guid activeClassId, DateTime asOfDate)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<ActiveClassPriceDataObject>();
                    criteria.Add(Restrictions.Eq("ActiveClassId", activeClassId));
                    criteria.Add(Restrictions.Or(Restrictions.And(Restrictions.Le("BeginDate", asOfDate), Restrictions.Ge("EndDate", asOfDate)), Restrictions.And(Restrictions.Le("BeginDate", asOfDate), Restrictions.IsNull("EndDate"))));
                    ActiveClassPriceDataObject pricing = criteria.UniqueResult<ActiveClassPriceDataObject>();
                    if (pricing != null)
                    {
                        IEnumerable<ClassPriceProRateDataObject> proRates = GetActiveClassPricingProRates(pricing.Id);
                        if (proRates != null)
                        {
                            pricing.ProRates = proRates;
                        }
                    }
                    return pricing;
                }
            }
        }

        public LocationPriceDataObject GetLocationPricing(Guid id)
        {
            LocationPriceDataObject lp = new LocationPriceDataObject();
            lp = NHibernateUtil.Get(lp, id);
            if (lp != null)
            {
                IEnumerable<ClassPriceProRateDataObject> proRates = GetLocationPricingProRates(id);
                if (proRates != null)
                {
                    lp.ProRates = proRates;
                }
            }
            return lp;
        }


        public LocationPriceDataObject SaveLocationPricing(LocationPriceDataObject locationPricing)
        {
            return NHibernateUtil.Save(locationPricing);
        }


        public ActiveClassPriceDataObject SaveActiveClassPricing(ActiveClassPriceDataObject classPricing)
        {
            return NHibernateUtil.Save(classPricing);
        }

        public ActiveClassPriceDataObject GetActiveClassPricing(Guid id)
        {
            ActiveClassPriceDataObject acp = new ActiveClassPriceDataObject();
            acp = NHibernateUtil.Get(acp, id);
            if (acp != null)
            {
                IEnumerable<ClassPriceProRateDataObject> proRates = GetActiveClassPricingProRates(id);
                if (proRates != null)
                {
                    acp.ProRates = proRates;
                }
            }
            return acp;
        }


        public void DeleteActiveClassPricing(ActiveClassPriceDataObject classPricing)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    foreach (var proRate in classPricing.ProRates)
                    {
                        DeleteClassPriceProRate(proRate);
                    }
                    NHibernateUtil.Delete(classPricing);
                }
            }
        }


        public IEnumerable<LocationClassPriceDataObject> GetLocationClassPricings(Guid locationId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    IEnumerable<LocationClassPriceDataObject> pricings = session.CreateCriteria<LocationClassPriceDataObject>().Add(Restrictions.Eq("LocationId", locationId)).List<LocationClassPriceDataObject>();
                    return pricings;
                }
            }
        }

        public IEnumerable<LocationClassPriceDataObject> GetLocationClassPricings(Guid locationId, Guid classId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    IEnumerable<LocationClassPriceDataObject> pricings = session.CreateCriteria<LocationClassPriceDataObject>().Add(Restrictions.Eq("LocationId", locationId)).Add(Restrictions.Eq("ClassId", classId)).List<LocationClassPriceDataObject>();
                    return pricings;
                }
            }
        }

        public LocationClassPriceDataObject GetLocationClassPricingForDate(Guid locationId, Guid classId, DateTime asOfDate)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<LocationClassPriceDataObject>();
                    criteria.Add(Restrictions.Eq("LocationId", locationId));
                    criteria.Add(Restrictions.Eq("ClassId", classId));
                    criteria.Add(Restrictions.Or(Restrictions.And(Restrictions.Le("BeginDate", asOfDate), Restrictions.Ge("EndDate", asOfDate)), Restrictions.And(Restrictions.Le("BeginDate", asOfDate), Restrictions.IsNull("EndDate"))));
                    LocationClassPriceDataObject pricing = criteria.UniqueResult<LocationClassPriceDataObject>();
                    if (pricing != null)
                    {
                        IEnumerable<ClassPriceProRateDataObject> proRates = GetLocationClassPricingProRates(pricing.Id);
                        if (proRates != null)
                        {
                            pricing.ProRates = proRates;
                        }
                    }
                    return pricing;
                }
            }
        }

        public LocationClassPriceDataObject SaveLocationClassPricing(LocationClassPriceDataObject locationClassPricing)
        {
            return NHibernateUtil.Save(locationClassPricing);
        }


        public LocationClassPriceDataObject GetLocationClassPricing(Guid id)
        {
            //return NHibernateUtil.Get(new LocationClassPriceDataObject(), id);
            LocationClassPriceDataObject lcp = new LocationClassPriceDataObject();
            lcp = NHibernateUtil.Get(lcp, id);
            if (lcp != null)
            {
                IEnumerable<ClassPriceProRateDataObject> proRates = GetLocationClassPricingProRates(id);
                if (proRates != null)
                {
                    lcp.ProRates = proRates;
                }
            }
            return lcp;
        }


        public void DeleteLocationClassPricing(LocationClassPriceDataObject locationClassPricing)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    foreach (var proRate in locationClassPricing.ProRates)
                    {
                        DeleteClassPriceProRate(proRate);
                    }
                    NHibernateUtil.Delete(locationClassPricing);
                }
            }
        }


        public IEnumerable<ClassPriceProRateDataObject> GetLocationPricingProRates(Guid locationPriceId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<ClassPriceProRateDataObject>();
                    criteria.Add(Restrictions.Eq("LocationPriceId", locationPriceId));
                    return criteria.List<ClassPriceProRateDataObject>();
                }
            }
        }

        public ClassPriceProRateDataObject SaveClassPriceProRate(ClassPriceProRateDataObject classPriceProRate)
        {
            return NHibernateUtil.Save<ClassPriceProRateDataObject>(classPriceProRate);
        }


        public IEnumerable<ClassPriceProRateDataObject> GetActiveClassPricingProRates(Guid activeClassPricingId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<ClassPriceProRateDataObject>();
                    criteria.Add(Restrictions.Eq("ActiveClassPriceId", activeClassPricingId));
                    return criteria.List<ClassPriceProRateDataObject>();
                }
            }
        }


        public ClassPricingProRatesValidationResult ValidateClassPriceProRates(IEnumerable<ClassPriceProRateDataObject> proRates)
        {
            ClassPricingProRatesValidationResult result = new ClassPricingProRatesValidationResult()
            {
                ValidationSuccessful = true,
                ValidationMessage = "Success"
            };

            List<ClassPriceProRateDataObject> validatedProRates = new List<ClassPriceProRateDataObject>();
            foreach (var proRate in proRates)
            {
                if (validatedProRates.Where(pr => pr.StudentNumber == proRate.StudentNumber && pr.ClassesMissed == proRate.ClassesMissed).Count() > 0)
                {
                    result.ValidationSuccessful = false;
                    result.ValidationMessage = string.Format("This update would cause a conflict. A pro rate record already exists for student number {0} and classes missed {1}", proRate.StudentNumber, proRate.ClassesMissed);
                }
                else
                {
                    validatedProRates.Add(proRate);
                }
            }

            return result;

        }


        public ClassPriceProRateDataObject GetClassPriceProRate(Guid id)
        {
            return NHibernateUtil.Get(new ClassPriceProRateDataObject(), id);
        }


        public void DeleteClassPriceProRate(ClassPriceProRateDataObject classPriceProRate)
        {
            NHibernateUtil.Delete<ClassPriceProRateDataObject>(classPriceProRate);
        }


        public IEnumerable<ClassPriceProRateDataObject> GetLocationClassPricingProRates(Guid locationClassPricingId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<ClassPriceProRateDataObject>();
                    criteria.Add(Restrictions.Eq("LocationClassPriceId", locationClassPricingId));
                    return criteria.List<ClassPriceProRateDataObject>();
                }
            }
        }


        public void DeleteLocationPricing(LocationPriceDataObject locationPricing)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    foreach (var proRate in locationPricing.ProRates)
                    {
                        DeleteClassPriceProRate(proRate);
                    }
                    NHibernateUtil.Delete(locationPricing);
                }
            }
        }


        public IEnumerable<LocationFixedDiscountDataObject> GetAllLocationFixedDiscounts()
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<LocationFixedDiscountDataObject>();
                IList<LocationFixedDiscountDataObject> allDiscounts = criteria.List<LocationFixedDiscountDataObject>();
                return allDiscounts;
            }
        }

        public IEnumerable<LocationPercentageDiscountDataObject> GetAllLocationPercentageDiscounts()
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<LocationPercentageDiscountDataObject>();
                IList<LocationPercentageDiscountDataObject> allDiscounts = criteria.List<LocationPercentageDiscountDataObject>();
                return allDiscounts;
            }
        }
    }
}
