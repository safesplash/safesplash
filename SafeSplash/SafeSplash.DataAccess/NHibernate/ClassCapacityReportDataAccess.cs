﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace SafeSplash.DataAccess.NHibernate
{
    public interface IClassCapacityReportDataAccess
    {
        IEnumerable<ClassCapacityReportDataObject> GetClassCapacityReports(DateTime fromDate, DateTime toDate, Guid locationId, Guid? instructorId);
    }

    public class ClassCapacityReportDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual string ClassName { get; set; }
        public virtual string LevelName { get; set; }
        public virtual string StartTime { get; set; }
        public virtual string Day { get; set; }
        public virtual Guid InstructorId { get; set; }
        public virtual int Capacity { get; set; }
        public virtual int StudentsEnrolled { get; set; }
    }


    public class ClassCapacityReportDataAccess : IClassCapacityReportDataAccess
    {
        public IEnumerable<ClassCapacityReportDataObject> GetClassCapacityReports(DateTime fromDate, DateTime toDate, Guid locationId, Guid? instructorId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                IQuery query = session.GetNamedQuery("GetClassCapacityReports");
                query.SetDateTime("FromDate", fromDate);
                query.SetDateTime("ToDate", toDate);
                query.SetString("LocationId", locationId.ToString());
                IList<ClassCapacityReportDataObject> results = query.List<ClassCapacityReportDataObject>();
                IList<ClassCapacityReportDataObject> filteredResults = results;
                if (instructorId.HasValue)
                {
                    filteredResults = results.Where(r => r.InstructorId == instructorId.Value).ToList<ClassCapacityReportDataObject>();
                }
                //return results.FirstOrDefault();
                return filteredResults;
            }
        }
    }

     //ICriteria criteria = session.CreateCriteria<IEmployeeActionDataObject>().Add(Restrictions.Ge("Date", startDate)).Add(Restrictions.Le("Date", endDate));
     //               if (employeeId.HasValue)
     //               {
     //                   criteria.Add(Restrictions.Eq("EmployeeId", employeeId.Value));
     //               }
     //               if (!string.IsNullOrEmpty(action))
     //               {
     //                   criteria.Add(Restrictions.Eq("Action", action));
     //               }
     //               IEnumerable<IEmployeeActionDataObject> actions = criteria.List<IEmployeeActionDataObject>();
     //               return actions;
}
