﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;

namespace SafeSplash.DataAccess.NHibernate
{
    public interface IWeeklyEnrollmentDataAccess
    {
        WeeklyEnrollmentDataObject SaveWeeklyEnrollment(WeeklyEnrollmentDataObject weeklyEnrollment);
        IEnumerable<WeeklyEnrollmentDataObject> GetWeeklyEnrollments(Guid locationId, DateTime fromDate, DateTime toDate);
    }

    public class WeeklyEnrollmentDataAccess : IWeeklyEnrollmentDataAccess
    {
        public WeeklyEnrollmentDataObject SaveWeeklyEnrollment(WeeklyEnrollmentDataObject weeklyEnrollment)
        {
            return NHibernateUtil.Save(weeklyEnrollment);
        }


        public IEnumerable<WeeklyEnrollmentDataObject> GetWeeklyEnrollments(Guid locationId, DateTime fromDate, DateTime toDate)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<WeeklyEnrollmentDataObject>();
                    criteria.Add(Restrictions.Eq("LocationId", locationId));
                    criteria.Add(Restrictions.Ge("FromDate", fromDate)).Add(Restrictions.Le("ToDate", toDate));                    
                    IEnumerable<WeeklyEnrollmentDataObject> enrollments = criteria.List<WeeklyEnrollmentDataObject>();
                    return enrollments;
                }

            }
        }
    }

    public class WeeklyEnrollmentDataObject
    {
        public virtual int Id { get; set; }
        public virtual Guid LocationId { get; set; }
        public virtual DateTime FromDate { get; set; }
        public virtual DateTime ToDate { get; set; }
        public virtual int StudentsEnrolled { get; set; }
        public virtual int PrivateSpotsFilled { get; set; }
        public virtual int SemiPrivateSpotsFilled { get; set; }
        public virtual int Capacity { get; set; }
        public virtual int StudentsWithdrawn { get; set; }
        public virtual int PrivateSpotsWithdrawn { get; set; }
        public virtual int SemiPrivateSpotsWithdrawn { get; set; }
        public virtual int RIP { get; set; }
        public virtual DateTime DateUpdated { get; set; }
    }
}
