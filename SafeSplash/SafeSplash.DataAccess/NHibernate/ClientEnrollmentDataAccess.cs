﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace SafeSplash.DataAccess.NHibernate
{
    public interface IClientEnrollmentDataAccess
    {
        IEnumerable<RevenueEnrollmentDataObject> GetClientEnrollmentForRevenueReport(Guid locationId, DateTime beginDateRange, DateTime endDateRange);
    }

    [Serializable]
    public class ClientEnrollmentDataAccess : IClientEnrollmentDataAccess
    {
        public IEnumerable<RevenueEnrollmentDataObject> GetClientEnrollmentForRevenueReport(Guid locationId, DateTime beginDateRange, DateTime endDateRange)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                IQuery query = session.GetNamedQuery("GetClientEnrollmentForRevenueReport");
                query.SetString("LocationId", locationId.ToString());
                query.SetDateTime("FromDate", beginDateRange);
                query.SetDateTime("ToDate", endDateRange);
                IList<RevenueEnrollmentDataObject> results = query.List<RevenueEnrollmentDataObject>();
                return results;
            }
        }
    }

    [Serializable]
    public class RevenueEnrollmentDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid ClientId { get; set; }
        public virtual string ClientFirstName { get; set; }
        public virtual string ClientLastName { get; set; }
        public virtual Guid StudentId { get; set; }
        public virtual string StudentFirstName { get; set; }
        public virtual string StudentLastName { get; set; }
        public virtual DateTime StudentJoinDate { get; set; }
        public virtual bool? StudentIsWithdrawn { get; set; }
        public virtual DateTime? StudentWithdrawalDate { get; set; }
        public virtual DateTime StudentEarliestEnrolledDate { get; set; }
        public virtual DateTime ClassStartDate { get; set; }
        public virtual DateTime ClassEndDate { get; set; }
        public virtual Guid ClassId { get; set; }
        public virtual Guid ClassTypeId { get; set; }
        public virtual string ClassDay { get; set; }
        public virtual Guid LocationId { get; set; }
        public virtual int? ClientBillingAchId { get; set; }
    }
}
