﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.Client;
using NHibernate;
using NHibernate.Criterion;
using SafeSplash.DataAccess.Sql;

namespace SafeSplash.DataAccess.NHibernate
{
    [Serializable]
    public class ClientDataAccess : IClientDataAccess
    {
        public bool checkDupe(ClientMatch client)
        {
            int count = 0;
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("safesplash_checkDupe", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@clientID", client.clientID);
                    count = (int)cmd.ExecuteScalar();

                }
            }
            if (count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveStudentTrial(StudentTrialDataObject studentTrial)
        {
            NHibernateUtil.Save<StudentTrialDataObject>(studentTrial);
        }

        public void setNoUpdate(Guid clientID)
        {
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("setNoUpdate", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@client_id", clientID);
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public void setModified(Guid clientID)
        {
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("setModified", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@client_id", clientID);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public ClientMatch getClientMatchByCustomerID(int customerID)
        {
            ClientMatch thisMatch = new ClientMatch();
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("getClientMapByCustomerID", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@customerID", customerID);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            thisMatch.clientID = new Guid(dr["client_id"].ToString());
                            thisMatch.customerID = customerID;
                            thisMatch.locationID = Convert.ToInt32(dr["loca_MerchantID"]);
                        }
                    }
                      
                }
            }
            return thisMatch;
        }
        public List<ClientMatch> GetClientMatches()
        {
            List<ClientMatch> results = new List<ClientMatch>();
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();

                using (SqlCommand cmd = new SqlCommand("safesplash_getClientMatch", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ClientMatch thisMatch = new ClientMatch()
                            {
                                clientID = new Guid(dr["client_ID"].ToString()),
                                customerID = Convert.ToInt32(dr["clientBill_ACHID"].ToString()),
                                locationID = Convert.ToInt32(dr["loca_MerchantID"])
                            };
                            results.Add(thisMatch);
                        }
                            
                    }
                }
            }
            return results;
        }

        public void UpdateClientMatch(ClientMatch client)
        {
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();

                using (SqlCommand cmd = new SqlCommand("safesplash_updateClientMatch", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@clientID", client.clientID);
                    cmd.Parameters.AddWithValue("@customerID", client.customerID);
                    cmd.Parameters.AddWithValue("@locationID", client.locationID);
                    cmd.ExecuteNonQuery();
                }
            }
        }


        public IEnumerable<ClientComment> GetClientComments(Guid clientId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    return session.CreateCriteria<ClientComment>().Add(Restrictions.Eq("ClientId", clientId)).List<ClientComment>();
                }
            }
        }

        public ClientComment GetClientComment(Guid id)
        {
            return NHibernateUtil.Get<ClientComment>(new ClientComment(), id);
        }


        public ClientComment SaveClientComment(ClientComment comment)
        {
            return NHibernateUtil.Save<ClientComment>(comment);
        }

        public void DeleteClientComment(ClientComment comment)
        {
            NHibernateUtil.Delete<ClientComment>(comment);
        }
    }
}
