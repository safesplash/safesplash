﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;

namespace SafeSplash.DataAccess.NHibernate
{
    //public interface ILocationDataAccess
    //{
    //    IEnumerable<Location> GetReportUserLocations(Guid employeeId);
    //}


    //public class LocationDataAccess : ILocationDataAccess
    //{

    //    public IEnumerable<Location> GetReportUserLocations(Guid employeeId)
    //    {
    //        using (ISession session = NHibernateUtil.OpenSession())
    //        {
    //            ICriteria criteria = session.CreateCriteria<ReportUserLocation>().Add(Restrictions.Eq("EmployeeId", employeeId));
    //            return criteria.List<ReportUserLocation>().Select(rul => rul.Location);
    //        }
    //    }
    //}

    [Serializable]
    public class Location
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        //public virtual string Address { get; set; }
        //public virtual string City { get; set; }
        //public virtual string State { get; set; }
        //public virtual string Zip { get; set; }
        public virtual bool IsOwned { get; set; }
        public virtual bool IsLeased { get; set; }
        //public virtual string LessorName { get; set; }
        //public virtual DateTime? LeaseStartDate { get; set; }
        //public virtual DateTime? LeaseEndDate { get; set; }
        //public virtual int? MerchantId { get; set; }
        //public virtual string APILoginId { get; set; }
        //public virtual string SecureTransferKey { get; set; }
        //public virtual string ProcessingPassword { get; set; }
        public virtual DateTime? OpenDate { get; set; }
    }

    //[Serializable]
    //public class ReportUserLocation
    //{
    //    public virtual Guid Id { get; set; }
    //    public virtual Guid EmployeeId { get; set; }
    //    public virtual Location Location { get; set; }
    //}
}
