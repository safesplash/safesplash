﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.Dto.Membership;
using NHibernate;
using NHibernate.Criterion;

namespace SafeSplash.DataAccess.NHibernate
{
    public class MembershipDataAccess
    {
        public AspNetMembershipUser Get(Guid id)
        {
            return NHibernateUtil.Get(new AspNetMembershipUser(), id);
        }

        public IEnumerable<AspNetMembershipUser> FindUsers(string userName, string email)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<AspNetMembershipUser>();
                    if (!string.IsNullOrEmpty(userName))
                    {
                        criteria.Add(Restrictions.Like("UserName", string.Format("%{0}%", userName.Trim().ToUpper()).ToUpper().Trim()));
                    }
                    if (!string.IsNullOrEmpty(email))
                    {
                        criteria.Add(Restrictions.Like("Email", string.Format("%{0}%", email.Trim().ToUpper()).Trim().ToUpper()));
                    }
                    IEnumerable<AspNetMembershipUser> users = criteria.List<AspNetMembershipUser>();
                    return users;
                }
            }
        }

        public void UnlockUserAccount(string userName)
        {
            string applicationName = "/";
            using (ISession session = NHibernateUtil.OpenSession())
            {
                IQuery query = session.CreateSQLQuery("EXEC [dbo].[aspnet_Membership_UnlockUser] @ApplicationName=:applicationName, @UserName=:userName");
                query.SetString("applicationName", applicationName);
                query.SetString("userName", userName.Trim());
                query.UniqueResult();
            }
        }
    }
}
