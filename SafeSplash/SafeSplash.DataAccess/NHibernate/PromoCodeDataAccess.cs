﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;

namespace SafeSplash.DataAccess.NHibernate
{
    public interface IPromoCodeDataAccess
    {
        IPromoCodeDataObject GetPromoCodeById(Guid id);
        IEnumerable<IPromoCodeDataObject> GetAllPromoCodes();
        IEnumerable<IPromoCodeDataObject> GetPromoCodeByIds(IEnumerable<Guid> ids);
        IPromoCodeRedemptionDataObject GetPromoCodeRedemptionById(Guid id);
        IPromoCodeRedemptionDataObject SavePromoCodeRedemption(IPromoCodeRedemptionDataObject redemption);
        void DeletePromoCodeRedemption(IPromoCodeRedemptionDataObject redemption);
        IEnumerable<IPromoCodeRedemptionDataObject> FindPromoCodeRedemptions(Guid? clientId, Guid? locationId, IEnumerable<Guid> promoCodeIds, DateTime? beginDateRange, DateTime? endDateRange);
        IEnumerable<IPromoCodeRedemptionDataObject> FindPromoCodeRedemptionsByPromoCodeId(Guid id);
        IPromoCodeRedemptionRequestDataObject SavePromoCodeRedemptionClientRequest(IPromoCodeRedemptionRequestDataObject request);
        IEnumerable<IPromoCodeRedemptionRequestDataObject> FindPromoCodeRedemptionRequestsByClientId(Guid clientId);
        IEnumerable<PromoCodeRedemptionSummaryDataObject> GetPromoCodeUsageSummary(DateTime asOfDate);
        IEnumerable<PromoCodeUsageSummaryReportDataObject> GetPromoCodeUsageReportSummary(DateTime beginDateRange, DateTime endDateRange, decimal amount);
    }

    public interface IPromoCodeDataObject
    {
        Guid Id { get; set; }
        string Code { get; set; }
        string Description { get; set; }
        decimal? Amount { get; set; }
        bool? IsCancelled { get; set; }
    }


    public interface IPromoCodeRedemptionDataObject
    {
        Guid Id { get; set; }
        Guid PromoCodeId { get; set; }
        Guid ClientId { get; set; }
        Guid LocationId { get; set; }
        Guid? EmployeeId { get; set; }
        decimal AmountBegin { get; set; }
        decimal AmountRedeemed { get; set; }
        decimal AmountRemaining { get; set; }
        DateTime DateEntered { get; set; }
        DateTime? DateRedeemed { get; set; }
        string Comment { get; set; }
    }

    public interface IPromoCodeRedemptionRequestDataObject
    {
        Guid Id { get; set; }
        Guid ClientId { get; set; }
        Guid LocationId { get; set; }
        string Code { get; set; }
        DateTime DateRequested { get; set; }
    }

    public class PromoCodeDataObject : IPromoCodeDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal? Amount { get; set; }
        public virtual bool? IsCancelled { get; set; }
    }

    public class PromoCodeRedemptionDataObject : IPromoCodeRedemptionDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid PromoCodeId { get; set; }
        public virtual Guid ClientId { get; set; }
        public virtual Guid LocationId { get; set; }
        public virtual Guid? EmployeeId { get; set; }
        public virtual decimal AmountBegin { get; set; }
        public virtual decimal AmountRedeemed { get; set; }
        public virtual decimal AmountRemaining { get; set; }
        public virtual DateTime DateEntered { get; set; }
        public virtual DateTime? DateRedeemed { get; set; }
        public virtual string Comment { get; set; }
    }

    public class PromoCodeRedemptionRequestDataObject : IPromoCodeRedemptionRequestDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid ClientId { get; set; }
        public virtual Guid LocationId { get; set; }
        public virtual string Code { get; set; }
        public virtual DateTime DateRequested { get; set; }
    }

    public class PromoCodeRedemptionSummaryDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid PromoCodeId { get; set; }
        public virtual string PromoCode { get; set; }
        public virtual decimal PromoCodeAmount { get; set; }
        public virtual decimal AmountUsed { get; set; }
    }

    public class PromoCodeUsageSummaryReportDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid PromoCodeId { get; set; }
        public virtual string PromoCode { get; set; }
        public virtual Guid LocationId { get; set; }
        public virtual string LocationName { get; set; }
        public virtual decimal PromoCodeRetailValue { get; set; }
        public virtual decimal PromoCodePaidBusinessValue { get; set; }
        public virtual decimal PromoCodeMaxBusinessValue { get; set; }
        public virtual decimal PromoCodeMaxBusinessRevenue { get; set; }
        public virtual decimal PromoCodeRetailRedeemed { get; set; }
        public virtual decimal PromoCodePercentRedeemed { get; set; }
        public virtual decimal PromoCodeNetRevenueToCompany { get; set; }
        public virtual decimal PromoCodeNetCostToCompany { get; set; }
    }

    public class PromoCodeDataAccess : IPromoCodeDataAccess
    {
        public IPromoCodeDataObject GetPromoCodeById(Guid id)
        {
            return NHibernateUtil.Get<IPromoCodeDataObject>(id);
        }

        public IEnumerable<IPromoCodeDataObject> GetAllPromoCodes()
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    return session.CreateCriteria<IPromoCodeDataObject>().List<IPromoCodeDataObject>();
                }
            }
        }

        public IEnumerable<IPromoCodeDataObject> GetPromoCodeByIds(IEnumerable<Guid> ids)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<IPromoCodeRedemptionDataObject>();
                    criteria.Add(Restrictions.In("Code", ids.ToArray<Guid>()));
                    IEnumerable<IPromoCodeDataObject> codes = criteria.List<IPromoCodeDataObject>();
                    return codes;
                }
            }
        }

        public IPromoCodeRedemptionDataObject GetPromoCodeRedemptionById(Guid id)
        {
            return NHibernateUtil.Get<IPromoCodeRedemptionDataObject>(id);
        }

        public IPromoCodeRedemptionDataObject SavePromoCodeRedemption(IPromoCodeRedemptionDataObject redemption)
        {
            return NHibernateUtil.Save<IPromoCodeRedemptionDataObject>(redemption);
        }

        public void DeletePromoCodeRedemption(IPromoCodeRedemptionDataObject redemption)
        {
            NHibernateUtil.Delete<IPromoCodeRedemptionDataObject>(redemption);
        }

        public IEnumerable<IPromoCodeRedemptionDataObject> FindPromoCodeRedemptions(Guid? clientId, Guid? locationId, IEnumerable<Guid> promoCodeIds, DateTime? beginDateRange, DateTime? endDateRange)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<IPromoCodeRedemptionDataObject>();
                    if (beginDateRange.HasValue)
                    {
                        criteria.Add(Restrictions.Ge("DateRedeemed", beginDateRange.Value));
                    }
                    if (endDateRange.HasValue)
                    {
                        criteria.Add(Restrictions.Le("DateRedeemed", endDateRange.Value));
                    }
                    if (clientId.HasValue)
                    {
                        criteria.Add(Restrictions.Eq("ClientId", clientId.Value));
                    }
                    if (locationId.HasValue)
                    {
                        criteria.Add(Restrictions.Eq("LocationId", locationId.Value));
                    }
                    if (promoCodeIds != null && promoCodeIds.Count() > 0)
                    {
                        criteria.Add(Restrictions.In("PromoCodeId", promoCodeIds.ToArray<Guid>()));
                    }
                    IEnumerable<IPromoCodeRedemptionDataObject> redemptions = criteria.List<IPromoCodeRedemptionDataObject>();
                    return redemptions;
                }
            }
        }

        public IEnumerable<IPromoCodeRedemptionDataObject> FindPromoCodeRedemptionsByPromoCodeId(Guid id)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<IPromoCodeRedemptionDataObject>();
                    criteria.Add(Restrictions.Eq("PromoCodeId", id));
                    IEnumerable<IPromoCodeRedemptionDataObject> redemptions = criteria.List<IPromoCodeRedemptionDataObject>();
                    return redemptions;
                }

            }
        }


        public IPromoCodeRedemptionRequestDataObject SavePromoCodeRedemptionClientRequest(IPromoCodeRedemptionRequestDataObject request)
        {
            return NHibernateUtil.Save<IPromoCodeRedemptionRequestDataObject>(request);
        }

        public IEnumerable<IPromoCodeRedemptionRequestDataObject> FindPromoCodeRedemptionRequestsByClientId(Guid clientId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<IPromoCodeRedemptionRequestDataObject>();
                    criteria.Add(Restrictions.Eq("ClientId", clientId));
                    IEnumerable<IPromoCodeRedemptionRequestDataObject> requests = criteria.List<IPromoCodeRedemptionRequestDataObject>();
                    return requests;
                }

            }
        }


        public IEnumerable<PromoCodeRedemptionSummaryDataObject> GetPromoCodeUsageSummary(DateTime asOfDate)
        {
             using (ISession session = NHibernateUtil.OpenSession())
            {
                IQuery query = session.GetNamedQuery("GetPromoCodeRedemptionSummary");
                query.SetDateTime("AsOfDate", asOfDate);
                IList<PromoCodeRedemptionSummaryDataObject> results = query.List<PromoCodeRedemptionSummaryDataObject>();
                return results;
            }
        }


        public IEnumerable<PromoCodeUsageSummaryReportDataObject> GetPromoCodeUsageReportSummary(DateTime beginDateRange, DateTime endDateRange, decimal amount)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                IQuery query = session.GetNamedQuery("GetPromoCodeRedemptionSummaryReport");
                query.SetDateTime("BeginDateRange", beginDateRange);
                query.SetDateTime("EndDateRange", endDateRange);
                query.SetDecimal("Amount", amount);
                IList<PromoCodeUsageSummaryReportDataObject> results = query.List<PromoCodeUsageSummaryReportDataObject>();
                return results;
            }
        }
    }
}
