﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.Tasks;
using NHibernate;
using NHibernate.Criterion;
using SafeSplash.DataAccess.Groups;

namespace SafeSplash.DataAccess.NHibernate
{
    public class TaskDataAccess : ITaskDataAccess
    {
        public TaskDataObject GetTask(Guid id)
        {
            return NHibernateUtil.Get<TaskDataObject>(id);
        }

        public IEnumerable<TaskDataObject> GetTasksAssignedToEmployee(Guid employeeId)
        {
            IEmployeeGroupsDataAccess GroupsRepository = new NHibernate.EmployeeGroupsRepository();
            IEnumerable<EmployeeGroupDataObject> employeeGroups = GroupsRepository.GetGroupsEmployeeBelongsTo(employeeId);
            List<Guid> employeeGroupIds = employeeGroups.Select(eg => eg.Id).ToList<Guid>();
            //IEnumerable<TaskRecipientDataObject> recipients =
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    
                    IEnumerable<TaskRecipientDataObject> recipients = session.CreateCriteria<TaskRecipientDataObject>().Add(Restrictions.In("EmployeeGroupId", employeeGroupIds)).List<TaskRecipientDataObject>();
                    List<Guid> taskIds = recipients.Select(r => r.TaskId).ToList<Guid>();

                    //IEnumerable<TaskDataObject> tasks = session.CreateCriteria<TaskDataObject>().Add(Restrictions.Like("AssignedTo", string.Format("%{0}%", employeeId.ToString()))).List<TaskDataObject>();

                    IEnumerable<TaskDataObject> tasks = session.CreateCriteria<TaskDataObject>().Add(Restrictions.Or(Restrictions.In("Id", taskIds),Restrictions.Like("AssignedTo", string.Format("%{0}%", employeeId.ToString())))).List<TaskDataObject>();

                    //IEnumerable<TaskDataObject> tasks = session.CreateCriteria<TaskDataObject>().Add(Restrictions.Like("AssignedTo", string.Format("%{0}%", employeeId.ToString()))).List<TaskDataObject>();

                    ////tasks.Where(t => t.AssignedToEmployeeGroups.SelectMany(eg => eg.Employees.SelectMany<EmployeeGroupEmployeeDataObject>(e => e.Employee.Id == employeeId)));
                    //tasks.SelectMany(t => t.AssignedToEmployeeGroups).SelectMany(eg => eg.Employees).Where(e => e.Employee.Id == employeeId);


                    ////.CreateCriteria("categories")
                    ////       .Add(NHibernate.Criterion.Restrictions.IdEq(category.ID))
                    ////       .List();
                    return tasks;
                }
            }
        }


        public TaskDataObject AssignTaskToEmployeeGroup(TaskDataObject task, EmployeeGroupDataObject group)
        {
            TaskRecipientDataObject recipient = new TaskRecipientDataObject();
            recipient.TaskId = task.Id;
            recipient.EmployeeGroupId = group.Id;
            recipient = NHibernateUtil.Save<TaskRecipientDataObject>(recipient);
            return GetTask(task.Id);
        }


        //public IEnumerable<EmployeeGroupDataObject> GetGroupsAssignedToTask(TaskDataObject task, IEnumerable<string> recipientTypes)
        //{
        //    using (ISession session = NHibernateUtil.OpenSession())
        //    {
        //        using (var transaction = session.BeginTransaction())
        //        {
        //            IEnumerable<TaskRecipientDataObject> groups = session.CreateCriteria<TaskRecipientDataObject>().Add(Restrictions.Eq("TaskId", task.Id)).List<TaskDataObject>();



        //            //.CreateCriteria("categories")
        //            //       .Add(NHibernate.Criterion.Restrictions.IdEq(category.ID))
        //            //       .List();
        //            return tasks;
        //        }
        //    }
        //}


        public TaskDataObject RemoveEmployeeGroupFromTask(TaskDataObject task, EmployeeGroupDataObject group)
        {
            //TaskRecipientDataObject recipient = new TaskRecipientDataObject();
            //recipient.TaskId = task.Id;
            //recipient.EmployeeGroupId = group.Id;
            //NHibernateUtil.Delete<TaskRecipientDataObject>(recipient);
            //return GetTask(task.Id);

            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    IEnumerable<TaskRecipientDataObject> recipients = session.CreateCriteria<TaskRecipientDataObject>().Add(Restrictions.Eq("TaskId", task.Id)).Add(Restrictions.Eq("EmployeeGroupId", group.Id)).List<TaskRecipientDataObject>();
                    foreach (var recipient in recipients)
                    {
                        NHibernateUtil.Delete<TaskRecipientDataObject>(recipient);
                    }
                }
            }

            return GetTask(task.Id);
        }       
        
    }
}
