﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;

namespace SafeSplash.DataAccess.NHibernate
{
    public interface IReportsDataAccess
    {
        ReportType GetReportTypeById(int id);
        IEnumerable<ReportType> GetAllReportTypes();
        IEnumerable<ReportUser> GetReportsForEmployee(Guid employeeId);
        IEnumerable<ReportUserLocation> GetReportUserLocations(Guid employeeId);
        ReportUser SaveReportUser(ReportUser reportUser);
        ReportUserLocation SaveReportUserLocation(ReportUserLocation reportUserLocation);
        void DeleteReportUser(ReportUser reportUser);
        void DeleteReportUserLocation(ReportUserLocation reportUserLocation);
    }

    [Serializable]
    public class ReportsDataAccess : IReportsDataAccess
    {
        public ReportType GetReportTypeById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ReportType> GetAllReportTypes()
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<ReportType>();
                IList<ReportType> allReportTypes = criteria.List<ReportType>();
                return allReportTypes;
            }
        }

        public IEnumerable<ReportUser> GetReportsForEmployee(Guid employeeId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<ReportUser>().Add(Restrictions.Eq("EmployeeId", employeeId));
                IList<ReportUser> reportUsers = criteria.List<ReportUser>();
                //return reportUsers.Select(ru => ru.ReportType);
                return reportUsers;
            }
        }


        public IEnumerable<ReportUserLocation> GetReportUserLocations(Guid employeeId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<ReportUserLocation>().Add(Restrictions.Eq("EmployeeId", employeeId));
                IList<ReportUserLocation> reportUserLocations = criteria.List<ReportUserLocation>();
                return reportUserLocations;
            }
        }


        public ReportUser SaveReportUser(ReportUser reportUser)
        {
            return NHibernateUtil.Save(reportUser);
        }

        public ReportUserLocation SaveReportUserLocation(ReportUserLocation reportUserLocation)
        {
            return NHibernateUtil.Save(reportUserLocation);
        }

        public void DeleteReportUser(ReportUser reportUser)
        {
            NHibernateUtil.Delete(reportUser);
        }

        public void DeleteReportUserLocation(ReportUserLocation reportUserLocation)
        {
            NHibernateUtil.Delete(reportUserLocation);
        }
    }

    [Serializable]
    public class ReportType
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual int SortOrder { get; set; }
    }

    [Serializable]
    public class ReportUser
    {
        public virtual Guid Id { get; set; }
        public virtual Guid EmployeeId { get; set; }
        public virtual ReportType ReportType { get; set; }
    }

    [Serializable]
    public class ReportUserLocation
    {
        public virtual Guid Id { get; set; }
        public virtual Guid EmployeeId { get; set; }
        public virtual Guid LocationId { get; set; }
    }
}
