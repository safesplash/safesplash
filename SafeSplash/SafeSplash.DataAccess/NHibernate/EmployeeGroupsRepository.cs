﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.Groups;
using NHibernate;
using NHibernate.Criterion;

namespace SafeSplash.DataAccess.NHibernate
{
    [Serializable]
    public class EmployeeGroupsRepository : IEmployeeGroupsDataAccess
    {
        public EmployeeGroupDataObject SaveEmployeeGroup(EmployeeGroupDataObject group)
        {
            return NHibernateUtil.Save<EmployeeGroupDataObject>(group);
        }

        public EmployeeGroupDataObject GetEmployeeGroup(Guid id)
        {
            return NHibernateUtil.Get<EmployeeGroupDataObject>(id);
        }

        public void DeleteEmployeeGroup(EmployeeGroupDataObject group)
        {
            NHibernateUtil.Delete<EmployeeGroupDataObject>(group);
        }

        public IEnumerable<EmployeeGroupDataObject> GetAllEmployeeGroups()
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<EmployeeGroupDataObject>();
                IList<EmployeeGroupDataObject> allEmployeeGroups = criteria.List<EmployeeGroupDataObject>();
                return allEmployeeGroups;
            }
        }

        public IEnumerable<EmployeeGroupDataObject> FindGroups(string groupName)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<EmployeeGroupDataObject>().Add(Restrictions.Like("Name", string.Format("%{0}%", groupName)));
                IList<EmployeeGroupDataObject> employeeGroups = criteria.List<EmployeeGroupDataObject>();
                return employeeGroups;
            }
        }

        public EmployeeGroupEmployeeDataObject SaveEmployeeGroupEmployee(EmployeeGroupEmployeeDataObject groupEmployee)
        {
            return NHibernateUtil.Save<EmployeeGroupEmployeeDataObject>(groupEmployee);
        }

        public EmployeeGroupEmployeeDataObject GetEmployeeGroupEmployee(Guid id)
        {
            return NHibernateUtil.Get<EmployeeGroupEmployeeDataObject>(id);
        }

        public void DeleteEmployeeGroupEmployee(EmployeeGroupEmployeeDataObject groupEmployee)
        {
            NHibernateUtil.Delete<EmployeeGroupEmployeeDataObject>(groupEmployee);
        }

        public EmployeeGroupDataObject AddEmployeeToGroup(EmployeeGroupDataObject group, Employee.EmployeeDataObject employee)
        {
            EmployeeGroupEmployeeDataObject groupEmployee = new EmployeeGroupEmployeeDataObject() { Group = group, Employee = employee };
            if (group.Employees == null || group.Employees.Where(e => e.Employee.Id == employee.Id).Count() < 1)
            {
                groupEmployee = NHibernateUtil.Save<EmployeeGroupEmployeeDataObject>(groupEmployee);
            }
            return GetEmployeeGroup(group.Id);
        }

        public EmployeeGroupDataObject RemoveEmployeeFromGroup(EmployeeGroupDataObject group, Employee.EmployeeDataObject employee)
        {
            EmployeeGroupEmployeeDataObject groupEmployee = group.Employees.Where(e => e.Employee.Id == employee.Id).FirstOrDefault();
            if (groupEmployee != null)
            {
                NHibernateUtil.Delete<EmployeeGroupEmployeeDataObject>(groupEmployee);
            }
            return GetEmployeeGroup(group.Id);
        }

        public IEnumerable<EmployeeGroupEmployeeDataObject> GetEmployeesInGroup(EmployeeGroupDataObject group)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria groupEmployeesCriteria = session.CreateCriteria<EmployeeGroupEmployeeDataObject>().Add(Restrictions.Eq("Group.Id", group.Id));
                IList<EmployeeGroupEmployeeDataObject> groupEmployees = groupEmployeesCriteria.List<EmployeeGroupEmployeeDataObject>();
                return groupEmployees;
            }
        }

        public IEnumerable<EmployeeGroupDataObject> GetAllGroupsForEmployee(Employee.EmployeeDataObject employee)
        {
            //using (ISession session = NHibernateUtil.OpenSession())
            //{
            //    ICriteria groupEmployeesCriteria = session.CreateCriteria<EmployeeGroupEmployeeDataObject>().Add(Restrictions.Like("EmployeeId", employee.Id));
            //    IList<EmployeeGroupEmployeeDataObject> groupEmployees = groupEmployeesCriteria.List<EmployeeGroupEmployeeDataObject>();
            //    //ICriteria groupCriteria = session.CreateCriteria<EmployeeGroupDataObject>().Add(Restrictions.In("Id", groupEmployees.Select(eg => eg.GroupId).ToList<Guid>()));
            //    //ICriteria groupCriteria = session.CreateCriteria<EmployeeGroupDataObject>().Add(Restrictions.In("Id", groupEmployees.Select(eg => eg.GroupId).ToList<Guid>()));
            //    IList<EmployeeGroupDataObject> employeeGroups = groupCriteria.List<EmployeeGroupDataObject>();
            //    return employeeGroups;
            //}

            throw new NotImplementedException();
        }





        public IEnumerable<EmployeeGroupDataObject> GetGroupsEmployeeBelongsTo(Guid employeeId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria groupEmployeesCriteria = session.CreateCriteria<EmployeeGroupEmployeeDataObject>().Add(Restrictions.Eq("Employee.Id", employeeId));
                IList<EmployeeGroupEmployeeDataObject> groupEmployees = groupEmployeesCriteria.List<EmployeeGroupEmployeeDataObject>();
                return groupEmployees.Select(eg => eg.Group).Distinct<EmployeeGroupDataObject>();
            }
        }
    }
}
