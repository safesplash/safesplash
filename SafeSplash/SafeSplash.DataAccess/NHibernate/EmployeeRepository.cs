﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.Employee;
using NHibernate;
using NHibernate.Criterion;

namespace SafeSplash.DataAccess.NHibernate
{
    [Serializable]
    public class EmployeeRepository : IEmployeeDataAccess
    {
        public EmployeeDataObject GetEmployee(Guid id)
        {
            return NHibernateUtil.Get<EmployeeDataObject>(id);
        }

        public IEnumerable<EmployeeDataObject> GetEmployees(IEnumerable<Guid> employeeIds)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<EmployeeDataObject>().Add(Restrictions.In("Id", employeeIds.ToList<Guid>()));
                IList<EmployeeDataObject> employees = criteria.List<EmployeeDataObject>();
                return employees;
            }
        }
    }
}
