﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace SafeSplash.DataAccess.NHibernate
{
    public interface IMonthlyEnrollmentReportDataAccess
    {
        IMonthlyEnrollmentReportDataObject GetMonthlyEnrollmentReport(DateTime fromDate, DateTime toDate, Guid locationId);
    }

    public interface IMonthlyEnrollmentReportDataObject
    {
        int StudentsEnrolled { get; set; }
        int PrivateSpotsFilled { get; set; }
        int SemiPrivateSpotsFilled { get; set; }
        int FamiliesEnrolled { get; set; }
        int Capacity { get; set; }
        int FamiliesBilled { get; set; }
        int OutstandingAccounts { get; set; }
        int NewSpotsFilled { get; set; }
        int NewRegistrations { get; set; }
        int RegistrationsInProgress { get; set; }
        int StudentsWithdrawn  { get; set; }
        int PrivateSpotsWithdrawn  { get; set; }
        int SemiPrivateSpotsWithdrawn { get; set; }
    }

    public class MonthlyEnrollmentReportDataObject : IMonthlyEnrollmentReportDataObject
    {
        public virtual int StudentsEnrolled { get; set; }
        public virtual int PrivateSpotsFilled { get; set; }
        public virtual int SemiPrivateSpotsFilled { get; set; }
        public virtual int FamiliesEnrolled { get; set; }
        public virtual int Capacity { get; set; }
        public virtual int FamiliesBilled { get; set; }
        public virtual int OutstandingAccounts { get; set; }
        public virtual int NewSpotsFilled { get; set; }
        public virtual int NewRegistrations { get; set; }
        public virtual int RegistrationsInProgress { get; set; }
        public virtual int StudentsWithdrawn { get; set; }
        public virtual int PrivateSpotsWithdrawn { get; set; }
        public virtual int SemiPrivateSpotsWithdrawn { get; set; }
    }

    public class MonthlyEnrollmentReportDataAccess : IMonthlyEnrollmentReportDataAccess
    {
        public IMonthlyEnrollmentReportDataObject GetMonthlyEnrollmentReport(DateTime fromDate, DateTime toDate, Guid locationId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                IQuery query = session.GetNamedQuery("GetMonthlyEnrollment");
                query.SetDateTime("FromDate", fromDate);
                query.SetDateTime("ToDate", toDate);
                query.SetString("LocationId", locationId.ToString());
                IList<MonthlyEnrollmentReportDataObject> results = query.List<MonthlyEnrollmentReportDataObject>();
                return results.FirstOrDefault();
            }
        }
    }
}
