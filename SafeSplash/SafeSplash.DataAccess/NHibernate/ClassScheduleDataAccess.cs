﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.ClassSchedule;
using NHibernate;
using NHibernate.Criterion;

namespace SafeSplash.DataAccess.NHibernate
{
    [Serializable]
    public class ClassScheduleDataAccess : IClassScheduleDataAccess
    {

        public IEnumerable<ClassScheduleStudentDataObject> GetStudentsBooked(IEnumerable<Guid> classIds, DateTime beginDate, DateTime endDate)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<ClassScheduleStudentDataObject>()
                        .Add(Restrictions.In("ActiveClassId", classIds.ToList()))
                .Add(Restrictions.Or(Restrictions.And(Restrictions.Le("JoinDate", endDate), Restrictions.IsNull("IsWithdrawn")), Restrictions.And(Restrictions.Eq("IsWithdrawn", true), Restrictions.Ge("WithdrawalDate", beginDate))));
               
                return criteria.List<ClassScheduleStudentDataObject>();
            }
        }


        public IEnumerable<ClassScheduleDataObject> GetClassesForClassSchedule(Guid locationId, DateTime asOfDate, string dayOfWeek)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                IQuery query = session.GetNamedQuery("GetClassesForClassSchedule");
                query.SetString("LocationId", locationId.ToString());
                query.SetDateTime("AsOfDate", asOfDate);
                query.SetString("DayOfWeek", dayOfWeek);
                IList<ClassScheduleDataObject> results = query.List<ClassScheduleDataObject>();
                return results;
            }
        }
       
    }
}
