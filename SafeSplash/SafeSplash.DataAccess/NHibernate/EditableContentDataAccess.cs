﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.NHibernate
{
    public interface IEditableContentDataAccess
    {
        IEditableContentDataObject Get(int id);
        IEditableContentDataObject Save(IEditableContentDataObject contentObject);
    }

    public interface IEditableContentDataObject
    {
        int Id { get; set; }
        string Description { get; set; }
        string Name { get; set; }
        string Value { get; set; }
    }

    public class EditableContentDataObject : IEditableContentDataObject
    {
        public virtual int Id { get; set; }
        public virtual string Description { get; set; }
        public virtual string Name { get; set; }
        public virtual string Value { get; set; }
    }

    public class EditableContentDataAccess : IEditableContentDataAccess
    {
        public IEditableContentDataObject Get(int id)
        {
            return NHibernateUtil.Get(new EditableContentDataObject(), id);
        }

        public IEditableContentDataObject Save(IEditableContentDataObject contentObject)
        {
            return NHibernateUtil.Save(contentObject);
        }
    }
}
