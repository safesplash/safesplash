﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using SafeSplash.DataAccess.PaymentGateway;
using SafeSplash.Dto.Membership;
using SafeSplash.DataAccess.Sql;
using System.Data.SqlClient;

namespace SafeSplash.DataAccess.NHibernate
{
    [Serializable]
    public class PaymentGatewayDataAccess : IPaymentGatewayDataAccess
    {
        public PaymentGatewaySettlementsDataObject SavePaymentGatewaySettlement(PaymentGatewaySettlementsDataObject settlement)
        {
            return NHibernateUtil.Save<PaymentGatewaySettlementsDataObject>(settlement);
        }

        public ClientBillingMethodsDataObject SaveClientBillingMethodsDataObject(ClientBillingMethodsDataObject billing)
        {
            return NHibernateUtil.Save<ClientBillingMethodsDataObject>(billing);
        }

        public bool CheckPaymethodDupe(string paymethodToken)
        {
            int returnID = 0;
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("checkPaymethodDupe", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@paymethod_token", paymethodToken);
                    returnID = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            if (returnID == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool CheckTransactionDupe(string transactionID)
        {
            int returnID = 0;
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("checkTransactionDupe", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@forteTransactionID", transactionID);
                    returnID = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            if (returnID == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool CheckSettlementDupe(string settlementID)
        {
            int returnID = 0;
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("checkSettlementDupe", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@settlementID", settlementID);
                    returnID = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            if (returnID == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void AddTransaction(ClientTransactionDataObject trans)
        {
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("addTransaction", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@clientID", trans.clientID);
                    cmd.Parameters.AddWithValue("@customerID", trans.customerID);
                    cmd.Parameters.AddWithValue("@forteTransactionID", trans.forteTransactionID);
                    cmd.Parameters.AddWithValue("@transactionType", trans.transactionType);
                    cmd.Parameters.AddWithValue("@amount", trans.amount);
                    cmd.Parameters.AddWithValue("@dateCreated", trans.dateCreated);
                    cmd.Parameters.AddWithValue("@batched", 1);
                    cmd.Parameters.AddWithValue("@batchID", "0");
                    cmd.Parameters.AddWithValue("@dateBatched", trans.dateCreated);
                    cmd.Parameters.AddWithValue("@cardNumber", trans.cardNumber);
                    cmd.Parameters.AddWithValue("@cardType", trans.cardType);
                    cmd.Parameters.AddWithValue("@cardExp", trans.cardExp);
                    cmd.Parameters.AddWithValue("@locationID", trans.locationID);
                    cmd.Parameters.AddWithValue("@merchantID", trans.merchantID);
                    cmd.Parameters.AddWithValue("@customerToken", trans.customerToken);
                    cmd.Parameters.AddWithValue("@accountID", trans.accountID);
                    cmd.Parameters.AddWithValue("@firstName", trans.firstName);
                    cmd.Parameters.AddWithValue("@lastName", trans.lastName);
                    cmd.Parameters.AddWithValue("@authCode", trans.authCode);
                    cmd.Parameters.AddWithValue("@enteredBy", trans.enteredBy);
                    cmd.Parameters.AddWithValue("@paymethodToken", trans.paymethodToken ?? String.Empty);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void AddSettlement(ClientSettlementDataObject set)
        {
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("addSettlement", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@client_ID", set.client_ID);
                    cmd.Parameters.AddWithValue("@settlementID", set.settlementID);
                    cmd.Parameters.AddWithValue("@transactionID", set.transactionID);
                    cmd.Parameters.AddWithValue("@customerToken", set.customerToken);
                    cmd.Parameters.AddWithValue("@customerID", set.customerID);
                    cmd.Parameters.AddWithValue("@settle_date", set.settle_date);
                    cmd.Parameters.AddWithValue("@settle_type", set.settle_type);
                    cmd.Parameters.AddWithValue("@settle_response_code", set.settle_response_code);
                    cmd.Parameters.AddWithValue("@settle_amount", set.settle_amount);
                    cmd.Parameters.AddWithValue("@method", set.method);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public int AddPaymethod(ClientPaymethodDataObject pay)
        {
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("addPaymethod", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@client_ID", pay.client_ID);
                    cmd.Parameters.AddWithValue("@procurement_card", pay.procurement_card);
                    cmd.Parameters.AddWithValue("@card_type", pay.card_type);
                    cmd.Parameters.AddWithValue("@expire_year", pay.expire_year);
                    cmd.Parameters.AddWithValue("@expire_month", pay.expire_month);
                    cmd.Parameters.AddWithValue("@masked_account_number", pay.masked_account_number);
                    cmd.Parameters.AddWithValue("@name_on_card", pay.name_on_card);
                    cmd.Parameters.AddWithValue("@notes", pay.notes);
                    cmd.Parameters.AddWithValue("@label", pay.label);
                    cmd.Parameters.AddWithValue("@customer_token", pay.customer_token);
                    cmd.Parameters.AddWithValue("@location_id", pay.location_id);
                    cmd.Parameters.AddWithValue("@account_id", pay.account_id);
                    cmd.Parameters.AddWithValue("@paymethod_token", pay.paymethod_token);
                    cmd.Parameters.AddWithValue("@routing_number", pay.routing_number ?? string.Empty);
                    cmd.Parameters.AddWithValue("@account_type", pay.account_type ?? string.Empty);
                    int ach = Convert.ToInt32(cmd.ExecuteScalar());
                    return ach;
                }
            }
        }
        public void SavePaymentCustomerDataObject(paymentCustomerDataObject customer)
        {
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("insertPaymentCustomer", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@customerID", customer.customerID);
                    cmd.Parameters.AddWithValue("@customerToken", customer.customerToken);
                    cmd.Parameters.AddWithValue("@firstName", customer.firstName);
                    cmd.Parameters.AddWithValue("@lastName", customer.lastName);
                    cmd.Parameters.AddWithValue("@locationID", customer.locationID);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<ClientSettlementDataObject> GetSettlementsLocal(string locationID, DateTime fromDate, DateTime toDate)
        {
            List<ClientSettlementDataObject> settlementReturn = new List<ClientSettlementDataObject>();
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("GetSettlementsByLocationAndDate", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@locationID", Convert.ToInt32(locationID));
                    cmd.Parameters.AddWithValue("@fromDate", fromDate);
                    cmd.Parameters.AddWithValue("@toDate", toDate);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            int thisCustomerID = 0;
                            if (dr["customerID"] != null)
                            {
                                thisCustomerID = Convert.ToInt32(dr["customerID"]);
                            }
                            Guid thisClientID = new Guid();
                            if (dr["client_ID"] != null)
                            {
                                thisClientID = new Guid(dr["client_ID"].ToString());
                            }

                            ClientSettlementDataObject thisSettle = new ClientSettlementDataObject()
                            {
                                client_settlementID = Convert.ToInt32(dr["client_settlementID"]),
                                client_ID = thisClientID,
                                settlementID = dr["settlementID"].ToString(),
                                transactionID = dr["transactionID"].ToString(),
                                customerToken = dr["customerToken"].ToString(),
                                customerID = thisCustomerID.ToString(),
                                settle_date = Convert.ToDateTime(dr["settle_date"]),
                                settle_type = dr["settle_type"].ToString() ?? String.Empty,
                                settle_response_code = dr["settle_response_code"].ToString() ?? String.Empty,
                                settle_amount = Convert.ToDecimal(dr["settle_amount"]),
                                method = dr["method"].ToString() ?? String.Empty
                            };

                            settlementReturn.Add(thisSettle);
                        }
                    }
                }
            }
            return settlementReturn;
        }

        public IEnumerable<ClientBillingMethodsDataObject> GetClientBillingMethodsByClientID(Guid clientID)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<ClientBillingMethodsDataObject>();
                    criteria.Add(Restrictions.Eq("clientID", clientID));
                    IEnumerable<ClientBillingMethodsDataObject> billMethods = criteria.List<ClientBillingMethodsDataObject>();
                    return billMethods;
                }
            }
        }

        public void UpdateCustomer(string customerToken, string defaultPaymethodToken, string accountID, string status, string postalCode, string region, string street_line1, string street_line2, string locality, string email_address, string phone_number, string settlementPath, string transactionPath, string paymethodPath, string addressPath, string selfPath, DateTime lastUpdated)
        {
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("updatePaymentCustomer", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@customerToken", customerToken);
                    cmd.Parameters.AddWithValue("@defaultPaymethodToken", defaultPaymethodToken ?? String.Empty);
                    cmd.Parameters.AddWithValue("@accountID", accountID ?? String.Empty);
                    cmd.Parameters.AddWithValue("@status", status);
                    cmd.Parameters.AddWithValue("@postalCode", postalCode);
                    cmd.Parameters.AddWithValue("@region", region);
                    cmd.Parameters.AddWithValue("@street_line1", street_line1);
                    cmd.Parameters.AddWithValue("@street_line2", street_line2 ?? String.Empty);
                    cmd.Parameters.AddWithValue("@locality", locality);
                    cmd.Parameters.AddWithValue("@email_address", email_address ?? String.Empty);
                    cmd.Parameters.AddWithValue("@phone_number", phone_number ?? String.Empty);
                    cmd.Parameters.AddWithValue("@settlementPath", settlementPath);
                    cmd.Parameters.AddWithValue("@transactionPath", transactionPath);
                    cmd.Parameters.AddWithValue("@paymethodPath", paymethodPath);
                    cmd.Parameters.AddWithValue("@addressPath", addressPath);
                    cmd.Parameters.AddWithValue("@selfPath", selfPath);
                    cmd.Parameters.AddWithValue("@lastUpdated", lastUpdated);
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public bool CheckDupe(int customerID, string customerToken)
        {
            int returnID = 0;
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("checkPaymentCustomer", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@customerID", customerID);
                    cmd.Parameters.AddWithValue("@customerToken", customerToken);
                    returnID = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            if (returnID == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
