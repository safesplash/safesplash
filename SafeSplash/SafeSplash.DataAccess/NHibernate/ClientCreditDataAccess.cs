﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.NHibernate;
using NHibernate;
using NHibernate.Criterion;

namespace SafeSplash.DataAccess.NHibernate
{
    public interface IClientCreditDataAccess
    {
        IClientCreditDataObject Get(Guid id);
        IClientCreditDataObject Save(IClientCreditDataObject credit);
        void Delete(IClientCreditDataObject credit);
        IEnumerable<IClientCreditDataObject> GetClientCredits(Guid clientId);
        IEnumerable<IClientCreditDataObject> GetClientCredits(IEnumerable<Guid> clientIds, DateTime fromDate, DateTime toDate);
    }

    public interface IClientCreditDataObject
    {
        Guid Id { get; set; }
        Guid ClientId { get; set; }
        string Amount { get; set; }
        string Comment { get; set; }
        bool? IsUsed { get; set; }
        DateTime? DateUsed { get; set; }
        string EmployeeDescription { get; set; }
        Guid EmployeeId { get; set; }
    }

    public class ClientCreditDataObject : IClientCreditDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid ClientId { get; set; }
        public virtual string Amount { get; set; }
        public virtual string Comment { get; set; }
        public virtual bool? IsUsed { get; set; }
        public virtual DateTime? DateUsed { get; set; }
        public virtual string EmployeeDescription { get; set; }
        public virtual Guid EmployeeId { get; set; }
    }

    public class ClientCreditDataAccess : IClientCreditDataAccess
    {
        public IClientCreditDataObject Get(Guid id)
        {
            return NHibernateUtil.Get(new ClientCreditDataObject(), id);
        }

        public IClientCreditDataObject Save(IClientCreditDataObject credit)
        {
            return NHibernateUtil.Save(credit);
        }

        public void Delete(IClientCreditDataObject credit)
        {
            NHibernateUtil.Delete(credit);
        }

        public IEnumerable<IClientCreditDataObject> GetClientCredits(Guid clientId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    IEnumerable<IClientCreditDataObject> credits = session.CreateCriteria<IClientCreditDataObject>().Add(Restrictions.Eq("ClientId", clientId)).List<IClientCreditDataObject>();
                    return credits;
                }
            }
        }

        public IEnumerable<IClientCreditDataObject> GetClientCredits(IEnumerable<Guid> clientIds, DateTime fromDate, DateTime toDate)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    IEnumerable<IClientCreditDataObject> credits = session.CreateCriteria<IClientCreditDataObject>().Add(Restrictions.In("ClientId", clientIds.ToList())).Add(Restrictions.Ge("DateUsed", fromDate)).Add(Restrictions.Le("DateUsed", toDate)).List<IClientCreditDataObject>();
                    return credits;
                }
            }
        }
    }
}
