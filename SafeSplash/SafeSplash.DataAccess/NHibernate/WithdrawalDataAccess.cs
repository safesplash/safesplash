﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace SafeSplash.DataAccess.NHibernate
{
    public interface IWithdrawalDataAccess
    {
        IEnumerable<IWithdrawalReasonDataObject> GetAllWithdrawalReasons();
        IEnumerable<IClassWithdrawalDataObject> GetWithdrawals(DateTime fromDate, DateTime toDate, Guid locationId);
    }

    public interface IClassWithdrawalDataObject
    {
        //Guid LocationId { get; set; }
        //DateTime? WithdrawalDate { get; set; }
        string Reason { get; set; }
        string ClassName { get; set; }
    }

    public interface IWithdrawalReasonDataObject
    {
        Guid Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        bool Active { get; set; }
    }

    public class ClassWithdrawalDataObject : IClassWithdrawalDataObject
    {
        //public virtual Guid LocationId { get; set; }
        //public virtual DateTime? WithdrawalDate { get; set; }
        public virtual string Reason { get; set; }
        public virtual string ClassName { get; set; }
    }

    public class WithdrawalReasonDataObject : IWithdrawalReasonDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual bool Active { get; set; }
    }


    public class WithdrawalDataAccess : IWithdrawalDataAccess
    {
        public IEnumerable<IWithdrawalReasonDataObject> GetAllWithdrawalReasons()
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                IEnumerable<IWithdrawalReasonDataObject> reasons = session.CreateCriteria<WithdrawalReasonDataObject>().List<IWithdrawalReasonDataObject>();
                return reasons;
            }
        }

        public IEnumerable<IClassWithdrawalDataObject> GetWithdrawals(DateTime fromDate, DateTime toDate, Guid locationId)
        {
            //using (ISession session = NHibernateUtil.OpenSession())
            //{
            //    //using (var transaction = session.BeginTransaction())
            //    //{
            //        IEnumerable<IClassWithdrawalDataObject> withdrawals = session.CreateCriteria<IClassWithdrawalDataObject>().Add(Restrictions.Eq("LocationId", locationId)).Add(Restrictions.Ge("WithdrawalDate", fromDate)).Add(Restrictions.Le("WithdrawalDate", toDate)).List<IClassWithdrawalDataObject>();
            //        return withdrawals;
            //    //}

            //}

            using (ISession session = NHibernateUtil.OpenSession())
            {
                IQuery query = session.GetNamedQuery("GetWithdrawals");
                query.SetDateTime("FromDate", fromDate);
                query.SetDateTime("ToDate", toDate);
                query.SetString("LocationId", locationId.ToString());
                IList<IClassWithdrawalDataObject> results = query.List<IClassWithdrawalDataObject>();
                return results;
            }
        }
    }
}
