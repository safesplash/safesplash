﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.ActiveClass;
using NHibernate;
using NHibernate.Criterion;

namespace SafeSplash.DataAccess.NHibernate
{
    [Serializable]
    public class ActiveClassInstructorDataAccess : IActiveClassInstructorDataAccess
    {
        public IEnumerable<ActiveClassInstructorDataObject> GetInstructorHistoryForClass(Guid activeClassId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    IEnumerable<ActiveClassInstructorDataObject> instructors = session.CreateCriteria<ActiveClassInstructorDataObject>().Add(Restrictions.Eq("ActiveClassId", activeClassId)).List<ActiveClassInstructorDataObject>();
                    return instructors;
                }
            }
        }

        public ActiveClassInstructorDataObject Save(ActiveClassInstructorDataObject activeClassInstructor)
        {
            return NHibernateUtil.Save<ActiveClassInstructorDataObject>(activeClassInstructor);
        }


        public ActiveClassInstructorDataObject Get(Guid id)
        {
            return NHibernateUtil.Get<ActiveClassInstructorDataObject>(id);
        }

        public void Delete(ActiveClassInstructorDataObject activeClassInstructor)
        {
            NHibernateUtil.Delete<ActiveClassInstructorDataObject>(activeClassInstructor);
        }


        public IEnumerable<ActiveClassInstructorDataObject> GetInstructorHistoryForInstructor(Guid instructorId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    IEnumerable<ActiveClassInstructorDataObject> instructors = session.CreateCriteria<ActiveClassInstructorDataObject>().Add(Restrictions.Eq("InstructorId", instructorId)).List<ActiveClassInstructorDataObject>();
                    return instructors;
                }
            }
        }


        public IEnumerable<ActiveClassInstructorDataObject> GetInstructorHistoriesAsOfDate(DateTime asOfDate)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    IEnumerable<ActiveClassInstructorDataObject> instructors = session.CreateCriteria<ActiveClassInstructorDataObject>().Add(Restrictions.Or(Restrictions.And(Restrictions.Le("BeginDate", asOfDate), Restrictions.Ge("EndDate", asOfDate)), Restrictions.And(Restrictions.Le("BeginDate", asOfDate), Restrictions.IsNull("EndDate")))).List<ActiveClassInstructorDataObject>();
                    return instructors;
                }
            }
        }


        public ActiveClassInstructorDataObject GetInstructorHistoryAsOfDate(Guid activeClassId, DateTime asOfDate)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    //ActiveClassInstructorDataObject instructorHistory = session.CreateCriteria<ActiveClassInstructorDataObject>().Add(Restrictions.Or(Restrictions.And(Restrictions.Le("BeginDate", asOfDate), Restrictions.Ge("EndDate", asOfDate)), Restrictions.And(Restrictions.Le("BeginDate", asOfDate), Restrictions.IsNull("EndDate")))).UniqueResult<ActiveClassInstructorDataObject>();
                    ActiveClassInstructorDataObject instructorHistory = session.CreateCriteria<ActiveClassInstructorDataObject>().Add(Restrictions.And(Restrictions.Eq("ActiveClassId", activeClassId), (Restrictions.Or(Restrictions.And(Restrictions.Le("BeginDate", asOfDate), Restrictions.Ge("EndDate", asOfDate)), Restrictions.And(Restrictions.Le("BeginDate", asOfDate), Restrictions.IsNull("EndDate")))))).UniqueResult<ActiveClassInstructorDataObject>();
                    return instructorHistory;
                }
            }
        }
    }
}
