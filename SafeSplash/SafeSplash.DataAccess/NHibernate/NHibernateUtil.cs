﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Cfg;
using SafeSplash.Dto.Membership;

namespace SafeSplash.DataAccess.NHibernate
{
    public class NHibernateUtil
    {
        //private const string CurrentSessionKey = "nhibernate.current_session";
        //private static readonly ISessionFactory sessionFactory;
        //private static ISession currentSession;


        //static NHibernateUtil()
        //{
        //    var configuration = new Configuration();
        //    configuration.Configure(typeof(NHibernateUtil).Assembly, "SafeSplash.DataAccess.NHibernate.hibernate.cfg.xml");
        //    configuration.AddAssembly(typeof(NHibernateUtil).Assembly.FullName);
        //    sessionFactory = configuration.BuildSessionFactory();
        //}

        //public static ISession GetCurrentSession()
        //{
        //    if (currentSession == null)
        //    {
        //        currentSession = sessionFactory.OpenSession();
        //        //context.Items[CurrentSessionKey] = currentSession;
        //    }

        //    if (!currentSession.IsOpen) { currentSession = sessionFactory.OpenSession(); }

        //    return currentSession;
        //}

        //public static ISession OpenSession()
        //{
        //    //return SessionFactory.OpenSession();
        //    return GetCurrentSession();
        //}

        //public static void CloseSession()
        //{
        //    //HttpContext context = HttpContext.Current;
        //    //ISession currentSession = context.Items[CurrentSessionKey] as ISession;

        //    if (currentSession == null)
        //    {
        //        // No current session
        //        return;
        //    }

        //    currentSession.Close();
        //    //context.Items.Remove(CurrentSessionKey);
        //}

        //public static void CloseSessionFactory()
        //{
        //    if (sessionFactory != null)
        //    {
        //        sessionFactory.Close();
        //    }
        //}

        //public static T Get<T>(object id)
        //{
        //    using (ISession session = NHibernateUtil.GetCurrentSession())
        //    {
        //        return session.Get<T>(id);
        //    }
        //}

        //public static T Get<T>(T instance, object id)
        //{
        //    using (ISession session = NHibernateUtil.GetCurrentSession())
        //    {
        //        return session.Get<T>(id);
        //    }
        //}

        //public static T Save<T>(T instance)
        //{
        //    using (ISession session = NHibernateUtil.GetCurrentSession())
        //    {
        //        using (var transaction = session.BeginTransaction())
        //        {
        //            session.SaveOrUpdate(instance);
        //            transaction.Commit();
        //            return instance;
        //        }                
        //    }
        //}

        //public static void Delete<T>(T instance)
        //{
        //    using (ISession session = NHibernateUtil.OpenSession())
        //    {
        //        using (var transaction = session.BeginTransaction())
        //        {
        //            session.Delete(instance);
        //            transaction.Commit();
        //        }               
        //    }
        //}





        private static ISessionFactory sessionFactory;

        private static ISessionFactory SessionFactory
        {
            get
            {
                if (sessionFactory == null)
                {
                    var configuration = new Configuration();
                    configuration.Configure(typeof(NHibernateUtil).Assembly, "SafeSplash.DataAccess.NHibernate.hibernate.cfg.xml");
                    configuration.AddAssembly(typeof(NHibernateUtil).Assembly.FullName);
                    sessionFactory = configuration.BuildSessionFactory();
                }
                return sessionFactory;
            }
        }

        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }

        public static ISession GetCurrentSession()
        {
            return SessionFactory.GetCurrentSession();
        }

        public static void Delete<T>(T instance)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    session.Delete(instance);
                    transaction.Commit();
                }
            }
        }

        public static T Get<T>(T instance, object id)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                return session.Get<T>(id);
            }
        }

        public static T Get<T>(object id)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                return session.Get<T>(id);
            }
        }

        public static T Save<T>(T instance)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    session.SaveOrUpdate(instance);
                    transaction.Commit();
                    return instance;
                }
            }
        }
    }
}
