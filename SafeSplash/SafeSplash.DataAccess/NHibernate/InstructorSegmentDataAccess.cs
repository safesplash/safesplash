﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.InstructorSegment;
using NHibernate;

namespace SafeSplash.DataAccess.NHibernate
{
    [Serializable]
    public class InstructorSegmentDataAccess : IInstructorSegmentDataAccess
    {
        public InstructorSegmentDataObject GetInstructorSegment(int id)
        {
            //return NHibernateUtil.Get<InstructorSegmentDataObject>(new InstructorSegmentDataObject(), id);
            return NHibernateUtil.Get<InstructorSegmentDataObject>(id);
        }

        public InstructorSegmentDataObject SaveInstructorSegment(InstructorSegmentDataObject instructorSegment)
        {
            return NHibernateUtil.Save<InstructorSegmentDataObject>(instructorSegment);
        }

        public void DeleteInstructorSegment(InstructorSegmentDataObject instructorSegment)
        {
            NHibernateUtil.Delete<InstructorSegmentDataObject>(instructorSegment);
        }

        public IEnumerable<InstructorSegmentDataObject> GetAllInstructorSegments()
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<InstructorSegmentDataObject>();
                IList<InstructorSegmentDataObject> allInstructorSegments = criteria.List<InstructorSegmentDataObject>();
                return allInstructorSegments;
            }
        }
    }
}
