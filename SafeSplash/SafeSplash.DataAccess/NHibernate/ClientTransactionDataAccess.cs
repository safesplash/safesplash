﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using SafeSplash.DataAccess.ClassSchedule;
using SafeSplash.DataAccess.Client;
using SafeSplash.DataAccess.PaymentGateway;
using System.Data.SqlClient;
using SafeSplash.DataAccess.Sql;

namespace SafeSplash.DataAccess.NHibernate
{
    [Serializable]
    public class ClientTransactionDataAccess : IClientTransactionDataAccess
    {
        public ClientTransactionDataObject SaveClientTransaction(ClientTransactionDataObject transaction)
        {
            return NHibernateUtil.Save<ClientTransactionDataObject>(transaction);
        }

        public ClientPaymethodDataObject SaveClientPaymethod(ClientPaymethodDataObject paymethod)
        {
            return NHibernateUtil.Save<ClientPaymethodDataObject>(paymethod);
        }

        public void updateCustomer(string customerToken)
        {
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("updateCustomer", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@customerToken", customerToken);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<paymentCustomer> getClients()
        {
            List<paymentCustomer> customers = new List<paymentCustomer>();
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("getClients", cn))
                {
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            paymentCustomer thisCust = new paymentCustomer()
                            {
                                paymentCustomerID = Convert.ToInt32(dr["paymentCustomerID"]),
                                client_ID = new Guid(dr["client_ID"].ToString()),
                                customerID = Convert.ToInt32(dr["customerID"]),
                                customerToken = dr["customerToken"].ToString(),
                                firstName = dr["firstName"].ToString(),
                                lastName = dr["lastName"].ToString(),
                                locationID = dr["locationID"].ToString(),
                                defaultPaymethodToken = dr["defaultPaymethodToken"].ToString(),
                                accountID = dr["accountID"].ToString(),
                                status = dr["status"].ToString(),
                                postalCode = dr["postalCode"].ToString(),
                                region = dr["region"].ToString(),
                                street_line1 = dr["street_line1"].ToString(),
                                street_line2 = dr["street_line2"].ToString(),
                                locality = dr["locality"].ToString(),
                                email_address = dr["email_address"].ToString(),
                                phone_number = dr["phone_number"].ToString(),
                                settlementPath = dr["settlementPath"].ToString(),
                                transactionPath = dr["transactionPath"].ToString(),
                                paymethodPath = dr["paymethodPath"].ToString(),
                                addressPath = dr["addressPath"].ToString(),
                                selfPath = dr["selfPath"].ToString(),
                                lastUpdated = Convert.ToDateTime(dr["lastUpdated"])
                            };
                            customers.Add(thisCust);
                        }
                    }
                }
            }
            return customers;
        }

        public ClientMatch GetClientIDByCustomerID(int customerID)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<ClientMatch>()
                    .Add(Restrictions.Eq("customerID", customerID));

                return criteria.List<ClientMatch>().FirstOrDefault();
            }
        }
        public bool checkPaymethodDupe(string paymethod_token)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria =
                    session.CreateCriteria<ClientPaymethodDataObject>()
                        .Add(Restrictions.Eq("paymethod_token", paymethod_token));

                return criteria.List<ClientPaymethodDataObject>().Any();
            }
        }
        public bool CheckTransactionDupe(string transactionID)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<ClientTransactionDataObject>()
                    .Add(Restrictions.Eq("forteTransactionID", transactionID));

                return criteria.List<ClientTransactionDataObject>().Any();
            }
        }
    }
}
