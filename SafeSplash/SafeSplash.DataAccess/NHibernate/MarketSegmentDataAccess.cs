﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.MarketSegment;
using NHibernate;

namespace SafeSplash.DataAccess.NHibernate
{
    [Serializable]
    public class MarketSegmentDataAccess : IMarketSegmentDataAccess
    {
        public MarketSegmentDataObject GetMarketSegment(int id)
        {
            return NHibernateUtil.Get<MarketSegmentDataObject>(new MarketSegmentDataObject(), id);
        }

        public MarketSegmentDataObject SaveMarketSegment(MarketSegmentDataObject marketSegment)
        {
            return NHibernateUtil.Save<MarketSegmentDataObject>(marketSegment);
        }

        public void DeleteMarketSegment(MarketSegmentDataObject marketSegment)
        {
            NHibernateUtil.Delete<MarketSegmentDataObject>(marketSegment);
        }

        public IEnumerable<MarketSegmentDataObject> GetAllMarketSegments()
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<MarketSegmentDataObject>();
                IList<MarketSegmentDataObject> allMarketSegments = criteria.List<MarketSegmentDataObject>();
                return allMarketSegments;
            }
        }

        public MarketSegmentQuestionDataObject GetMarketSegmentQuestion(int id)
        {
            return NHibernateUtil.Get<MarketSegmentQuestionDataObject>(new MarketSegmentQuestionDataObject(), id);
        }

        public MarketSegmentQuestionDataObject SaveMarketSegmentQuestion(MarketSegmentQuestionDataObject question)
        {
            //return NHibernateUtil.Save<MarketSegmentQuestionDataObject>(question);
            //MarketSegmentQuestionDataObject savedQuestion = NHibernateUtil.Save<MarketSegmentQuestionDataObject>(question);
            //foreach (var answer in savedQuestion.Answers)
            //{
            //    answer.Question = savedQuestion;
            //    answer = SaveMarketSegmentAnswer(answer);
            //}
            //return savedQuestion;
            return NHibernateUtil.Save<MarketSegmentQuestionDataObject>(question);
        }

        public void DeleteMarketSegmentQuestion(MarketSegmentQuestionDataObject question)
        {
            NHibernateUtil.Delete<MarketSegmentQuestionDataObject>(question);
        }

        public IEnumerable<MarketSegmentQuestionDataObject> GetAllMarketSegmentQuestions()
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                ICriteria criteria = session.CreateCriteria<MarketSegmentQuestionDataObject>();
                IList<MarketSegmentQuestionDataObject> allMarketSegmentQuestions = criteria.List<MarketSegmentQuestionDataObject>();
                return allMarketSegmentQuestions;
            }
        }

        public MarketSegmentAnswerDataObject GetMarketSegmentAnswer(int id)
        {
            return NHibernateUtil.Get<MarketSegmentAnswerDataObject>(new MarketSegmentAnswerDataObject(), id);
        }

        public MarketSegmentAnswerDataObject SaveMarketSegmentAnswer(MarketSegmentAnswerDataObject answer)
        {
            return NHibernateUtil.Save<MarketSegmentAnswerDataObject>(answer);
        }       

        public void DeleteMarketSegmentAnswer(MarketSegmentAnswerDataObject answer)
        {
            NHibernateUtil.Delete<MarketSegmentAnswerDataObject>(answer);
        }
    }
}
