﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace SafeSplash.DataAccess.NHibernate
{
    public interface IWeeklyEnrollmentReportDataAccess
    {
        IWeeklyEnrollmentReportDataObject GetWeeklyEnrollmentReport(DateTime fromDate, DateTime toDate, Guid locationId);
    }

    public interface IWeeklyEnrollmentReportDataObject
    {
        int StudentsEnrolled { get; set; }
        int PrivateSpotsFilled { get; set; }
        int SemiPrivateSpotsFilled { get; set; }
        int Capacity { get; set; }
        int RegistrationsInProgress { get; set; }
        int StudentsWithdrawn { get; set; }
        int PrivateSpotsWithdrawn { get; set; }
        int SemiPrivateSpotsWithdrawn { get; set; }
    }

    public class WeeklyEnrollmentReportDataObject : IWeeklyEnrollmentReportDataObject
    {
        public virtual int StudentsEnrolled { get; set; }
        public virtual int PrivateSpotsFilled { get; set; }
        public virtual int SemiPrivateSpotsFilled { get; set; }
        public virtual int Capacity { get; set; }
        public virtual int RegistrationsInProgress { get; set; }
        public virtual int StudentsWithdrawn { get; set; }
        public virtual int PrivateSpotsWithdrawn { get; set; }
        public virtual int SemiPrivateSpotsWithdrawn { get; set; }
    }


    public class WeeklyEnrollmentReportDataAccess : IWeeklyEnrollmentReportDataAccess
    {
        public IWeeklyEnrollmentReportDataObject GetWeeklyEnrollmentReport(DateTime fromDate, DateTime toDate, Guid locationId)
        {
            using (ISession session = NHibernateUtil.OpenSession())
            {
                IQuery query = session.GetNamedQuery("GetWeeklyEnrollment");
                query.SetDateTime("FromDate", fromDate);
                query.SetDateTime("ToDate", toDate);
                query.SetString("LocationId", locationId.ToString());
                IList<WeeklyEnrollmentReportDataObject> results = query.List<WeeklyEnrollmentReportDataObject>();
                return results.FirstOrDefault();
            }
        }
    }
}
