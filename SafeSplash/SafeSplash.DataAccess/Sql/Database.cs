﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SafeSplash.DataAccess.Sql
{
    public class Database
    {
        #region Connection String

        private static string _connectionString;
        public static string ConnectionString
        {
            get
            {
                if (_connectionString == null)
                {
                    ConnectionStringSettings setting = ConfigurationManager.ConnectionStrings["SafeSplashPortalConnectionString"];
                    if (setting == null)
                        throw new Exception("Connection String Setting missing for SafeSplash Data Access.");

                    _connectionString = setting.ConnectionString;
                }

                return _connectionString;
            }
        }

        #endregion
    }
}
