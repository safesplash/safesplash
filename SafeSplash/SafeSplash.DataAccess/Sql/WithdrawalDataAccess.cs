﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace SafeSplash.DataAccess.Sql
{
    public interface IWithdrawalDataAccess
    {
        IEnumerable<IClassWithdrawalDataObject> GetWithdrawals(DateTime fromDate, DateTime toDate, Guid locationId);
    }

    public interface IClassWithdrawalDataObject
    {
        string Reason { get; set; }
        string ClassName { get; set; }
    }

    public class ClassWithdrawalDataObject : IClassWithdrawalDataObject
    {
        public virtual string Reason { get; set; }
        public virtual string ClassName { get; set; }
    }

    public class WithdrawalDataAccess : IWithdrawalDataAccess
    {

        public IEnumerable<IClassWithdrawalDataObject> GetWithdrawals(DateTime fromDate, DateTime toDate, Guid locationId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@FromDate", fromDate));
            parameters.Add(new SqlParameter("@ToDate", toDate));
            parameters.Add(new SqlParameter("@LocationId", locationId));
            IEnumerable<IClassWithdrawalDataObject> withdrawals = ExecuteReaderWithdrawal("safesplash_Withdrawals", parameters);
            return withdrawals;
        }


        private IEnumerable<IClassWithdrawalDataObject> ExecuteReaderWithdrawal(string storedProcedure, List<SqlParameter> parameters)
        {

            List<IClassWithdrawalDataObject> results = new List<IClassWithdrawalDataObject>();

            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();

                using (SqlCommand cmd = new SqlCommand(storedProcedure, cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    foreach (SqlParameter p in parameters)
                        cmd.Parameters.AddWithValue(p.ParameterName, p.Value);
                    
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                            results.Add(FillWithdrawalDataObject(dr));
                    }
                }
            }

            return results;
        }

        private IClassWithdrawalDataObject FillWithdrawalDataObject(SqlDataReader reader)
        {
            IClassWithdrawalDataObject obj = new ClassWithdrawalDataObject();
            //obj.Reason = reader.GetString("Reason");
            //obj.ClassName = reader.GetString("ClassName");
            obj.Reason = reader.GetString(0);
            obj.ClassName = reader.GetString(1);
            return obj;
        }

    }
       
}
