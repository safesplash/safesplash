﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.Sql;

namespace SafeSplash.DataAccess.Location
{
    public class LocationDataAccess : ILocationDataAccess
    {
        public List<Guid> getLocationGuidsByGroupID(int groupID)
        {
            List<Guid> thisReturn = new List<Guid>();
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("safesplash_getLocationsByGroupID", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@groupID", groupID);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            thisReturn.Add(new Guid(dr["loca_id"].ToString()));
                        }
                    }
                }
            }
            return thisReturn;
        }

        public void updateLocation(int locationID)
        {
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("safesplash_updateLocation", cn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@LocationID", locationID);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public List<int> getLocation()
        {
            List<int> returnIDs = new List<int>();
            using (SqlConnection cn = new SqlConnection(Database.ConnectionString))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("safesplash_getLocation", cn))
                {
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            returnIDs.Add(Convert.ToInt32(dr["loca_MerchantID"]));
                        }
                    }
                }
            }
            return returnIDs;
        }
    }
}