using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace SafeSplash.DataAccess.Location
{
    public interface ILocationDataAccess
    {
        List<Guid> getLocationGuidsByGroupID(int groupID);
        List<int> getLocation();
        void updateLocation(int locationID);
    } 
}
