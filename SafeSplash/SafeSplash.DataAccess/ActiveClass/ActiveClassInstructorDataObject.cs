﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.ActiveClass
{
    [Serializable]
    public class ActiveClassInstructorDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid ActiveClassId { get; set; }
        public virtual Guid InstructorId { get; set; }
        public virtual DateTime BeginDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual string Comment { get; set; }
        public virtual Guid LastUpdatedById { get; set; }
    }

}
