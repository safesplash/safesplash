﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.ActiveClass
{
    public interface IActiveClassInstructorDataAccess
    {
        ActiveClassInstructorDataObject Save(ActiveClassInstructorDataObject activeClassInstructor);
        ActiveClassInstructorDataObject Get(Guid id);
        void Delete(ActiveClassInstructorDataObject activeClassInstructor);
        IEnumerable<ActiveClassInstructorDataObject> GetInstructorHistoryForClass(Guid activeClassId);
        IEnumerable<ActiveClassInstructorDataObject> GetInstructorHistoryForInstructor(Guid instructorId);
        IEnumerable<ActiveClassInstructorDataObject> GetInstructorHistoriesAsOfDate(DateTime asOfDate);
        ActiveClassInstructorDataObject GetInstructorHistoryAsOfDate(Guid activeClassId, DateTime asOfDate);
    }

}
