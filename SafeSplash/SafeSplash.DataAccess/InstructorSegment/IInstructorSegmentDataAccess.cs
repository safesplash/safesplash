﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.InstructorSegment
{
    public interface IInstructorSegmentDataAccess
    {
        InstructorSegmentDataObject GetInstructorSegment(int id);
        InstructorSegmentDataObject SaveInstructorSegment(InstructorSegmentDataObject instructorSegment);
        void DeleteInstructorSegment(InstructorSegmentDataObject instructorSegment);
        IEnumerable<InstructorSegmentDataObject> GetAllInstructorSegments();
    }
}
