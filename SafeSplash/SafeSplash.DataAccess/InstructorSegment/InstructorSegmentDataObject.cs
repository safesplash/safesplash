﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.InstructorSegment
{
    [Serializable]
    public class InstructorSegmentDataObject
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int Number { get; set; }
        public virtual string Description { get; set; }
        public virtual string Color { get; set; }
    }
}
