﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.Employee
{
    public interface IEmployeeDataAccess
    {
        EmployeeDataObject GetEmployee(Guid id);
        IEnumerable<EmployeeDataObject> GetEmployees(IEnumerable<Guid> employeeIds);
    }
}
