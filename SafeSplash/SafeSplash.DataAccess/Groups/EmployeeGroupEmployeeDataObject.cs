﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.Employee;

namespace SafeSplash.DataAccess.Groups
{
    [Serializable]
    public class EmployeeGroupEmployeeDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual EmployeeGroupDataObject Group { get; set; }
        public virtual EmployeeDataObject Employee { get; set; }

        #region Equals

        public override bool Equals(object obj)
        {
            if ((obj == null) || !obj.GetType().Equals(this.GetType())) { return false; }
            EmployeeGroupEmployeeDataObject other = (EmployeeGroupEmployeeDataObject)obj;
            return Id.Equals(((EmployeeGroupEmployeeDataObject)obj).Id);
        }

        public virtual bool Equals(EmployeeGroupEmployeeDataObject other)
        {
            if (other == null) { return false; }
            return Id.Equals(other.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        #endregion
    }
}
