﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.Employee;

namespace SafeSplash.DataAccess.Groups
{
    public interface IEmployeeGroupsDataAccess
    {
        EmployeeGroupDataObject SaveEmployeeGroup(EmployeeGroupDataObject group);
        EmployeeGroupDataObject GetEmployeeGroup(Guid id);
        void DeleteEmployeeGroup(EmployeeGroupDataObject group);
        IEnumerable<EmployeeGroupDataObject> GetAllEmployeeGroups();
        IEnumerable<EmployeeGroupDataObject> FindGroups(string groupName);
        EmployeeGroupDataObject AddEmployeeToGroup(EmployeeGroupDataObject group, Employee.EmployeeDataObject employee);
        EmployeeGroupDataObject RemoveEmployeeFromGroup(EmployeeGroupDataObject group, Employee.EmployeeDataObject employee);
        IEnumerable<EmployeeGroupDataObject> GetGroupsEmployeeBelongsTo(Guid employeeId);
        //EmployeeGroupEmployeeDataObject SaveEmployeeGroupEmployee(EmployeeGroupEmployeeDataObject groupEmployee);
        //void DeleteEmployeeGroupEmployee(EmployeeGroupEmployeeDataObject groupEmployee);
        //EmployeeGroupEmployeeDataObject GetEmployeeGroupEmployee(Guid id);
        //void DeleteEmployeeGroupEmployee(EmployeeGroupEmployeeDataObject groupEmployee);
        //void AddEmployeeToGroup(EmployeeGroupDataObject group, Employee.EmployeeDataObject employee);
        //void RemoveEmployeeFromGroup(EmployeeGroupDataObject group, Employee.EmployeeDataObject employee);
        //IEnumerable<EmployeeGroupEmployeeDataObject> SaveEmployeesInGroup(EmployeeGroupDataObject group, IEnumerable<Employee.EmployeeDataObject> employees);
        //IEnumerable<EmployeeGroupEmployeeDataObject> GetEmployeesInGroup(EmployeeGroupDataObject group);
        //IEnumerable<EmployeeGroupDataObject> GetAllGroupsForEmployee(EmployeeDataObject employee);        
    }
}
