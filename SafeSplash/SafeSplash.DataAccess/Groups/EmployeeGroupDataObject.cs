﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.Groups
{
    [Serializable]
    public class EmployeeGroupDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual IEnumerable<EmployeeGroupEmployeeDataObject> Employees { get; set; }
        
        public virtual IEnumerable<Guid> GetEmployeeIds()
        {
            return Employees != null ? Employees.Select(e => e.Employee.Id).ToList<Guid>() : new List<Guid>();
        }
        
    }
}
