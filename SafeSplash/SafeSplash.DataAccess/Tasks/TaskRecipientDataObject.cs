﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.Employee;
using SafeSplash.DataAccess.Groups;

namespace SafeSplash.DataAccess.Tasks
{
    [Serializable]
    public class TaskRecipientDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual Guid TaskId { get; set; }
        public virtual Guid? EmployeeId { get; set; }
        public virtual Guid? EmployeeGroupId { get; set; }
        public virtual string RecipientType { get; set; }
    }
}
