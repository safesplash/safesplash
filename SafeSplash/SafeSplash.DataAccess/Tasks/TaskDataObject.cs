﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.Employee;
using SafeSplash.DataAccess.Groups;

namespace SafeSplash.DataAccess.Tasks
{
    [Serializable]
    public class TaskDataObject
    {
        public virtual Guid Id { get; set; }
        public virtual string Subject { get; set; }
        public virtual string Content { get; set; }
        public virtual string AssignedTo { get; set; }
        //public virtual IEnumerable<EmployeeDataObject> AssignedToEmployees { get; set; }
        public virtual IEnumerable<EmployeeGroupDataObject> AssignedToEmployeeGroups { get; set; }
    }
}
