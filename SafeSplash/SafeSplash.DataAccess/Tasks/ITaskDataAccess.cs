﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.Groups;

namespace SafeSplash.DataAccess.Tasks
{
    public interface ITaskDataAccess
    {
        TaskDataObject GetTask(Guid id);
        IEnumerable<TaskDataObject> GetTasksAssignedToEmployee(Guid employeeId);
        TaskDataObject AssignTaskToEmployeeGroup(TaskDataObject task, EmployeeGroupDataObject group);
        TaskDataObject RemoveEmployeeGroupFromTask(TaskDataObject task, EmployeeGroupDataObject group);
        //IEnumerable<TaskDataObject> GetTasksAssignedTo
        //IEnumerable<EmployeeGroupDataObject> GetGroupsAssignedToTask(TaskDataObject task, IEnumerable<string> recipientTypes);
    }
}
