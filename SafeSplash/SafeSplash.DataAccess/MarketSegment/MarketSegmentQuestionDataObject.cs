﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.MarketSegment
{
    [Serializable]
    public class MarketSegmentQuestionDataObject
    {
        private List<MarketSegmentAnswerDataObject> answers = new List<MarketSegmentAnswerDataObject>();

        public virtual int Id { get; set; }
        public virtual int Number { get; set; }
        public virtual string Text { get; set; }
        public virtual IEnumerable<MarketSegmentAnswerDataObject> Answers { get { return answers; } set { answers = value.ToList<MarketSegmentAnswerDataObject>(); } }
        public virtual bool IsActive { get; set; }
        
        public virtual void AddAnswer(MarketSegmentAnswerDataObject answer)
        {
            answer.Question = this;
            answers.Add(answer);
        }

        public virtual void RemoveAnswer(MarketSegmentAnswerDataObject answer)
        {
            answers.Remove(answer);
        }
    }
}
