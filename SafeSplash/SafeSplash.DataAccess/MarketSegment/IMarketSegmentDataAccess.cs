﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.MarketSegment
{
    public interface IMarketSegmentDataAccess
    {
        MarketSegmentDataObject GetMarketSegment(int id);
        MarketSegmentDataObject SaveMarketSegment(MarketSegmentDataObject marketSegment);
        void DeleteMarketSegment(MarketSegmentDataObject marketSegment);
        IEnumerable<MarketSegmentDataObject> GetAllMarketSegments();
        MarketSegmentQuestionDataObject GetMarketSegmentQuestion(int id);
        MarketSegmentQuestionDataObject SaveMarketSegmentQuestion(MarketSegmentQuestionDataObject question);
        void DeleteMarketSegmentQuestion(MarketSegmentQuestionDataObject question);
        IEnumerable<MarketSegmentQuestionDataObject> GetAllMarketSegmentQuestions();
        MarketSegmentAnswerDataObject GetMarketSegmentAnswer(int id);
        MarketSegmentAnswerDataObject SaveMarketSegmentAnswer(MarketSegmentAnswerDataObject answer);
        void DeleteMarketSegmentAnswer(MarketSegmentAnswerDataObject answer);
    }
}
