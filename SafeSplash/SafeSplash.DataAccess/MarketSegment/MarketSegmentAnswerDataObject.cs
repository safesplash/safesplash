﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.MarketSegment
{
    [Serializable]
    public class MarketSegmentAnswerDataObject
    {
        public virtual int Id { get; set; }
        public virtual MarketSegmentQuestionDataObject Question { get; set; }
        public virtual MarketSegmentDataObject Segment { get; set; }
        public virtual string Letter { get; set; }
        public virtual string Text { get; set; }
        public virtual bool IsActive { get; set; }
    }
}
