﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.Client
{
    public interface IClientDataAccess
    {
        ClientComment GetClientComment(Guid id);
        ClientComment SaveClientComment(ClientComment comment);
        void DeleteClientComment(ClientComment comment);
        IEnumerable<ClientComment> GetClientComments(Guid clientId);
        List<ClientMatch> GetClientMatches();
        void UpdateClientMatch(ClientMatch client);
        bool checkDupe(ClientMatch client);
        ClientMatch getClientMatchByCustomerID(int customerID);
        void setModified(Guid clientID);
        void setNoUpdate(Guid clientID);
        void SaveStudentTrial(StudentTrialDataObject studentTrial);
    }
}
