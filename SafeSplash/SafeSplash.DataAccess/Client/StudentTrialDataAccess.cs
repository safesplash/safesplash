﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.Client
{
    [Serializable]
    public class StudentTrialDataObject
    {
        public virtual int studentTrialID { get; set; }
        public virtual DateTime trialDate { get; set; }
        public virtual Guid stud_ID { get; set; }
        public virtual Guid loca_ID { get; set; }
    }
}
