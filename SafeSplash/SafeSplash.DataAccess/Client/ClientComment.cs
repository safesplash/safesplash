﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.Client
{
    [Serializable]
    public class ClientComment
    {
        public virtual Guid Id { get; set; }
        public virtual Guid ClientId { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string Content { get; set; }
        //public virtual Guid EmployeeId { get; set; }
        public virtual string EmployeeFirstName { get; set; }
        public virtual string EmployeeLastName { get; set; }       
        public virtual string EmployeeName { get { return string.Format("{0}, {1}", EmployeeLastName, EmployeeFirstName); } }
    }
}
