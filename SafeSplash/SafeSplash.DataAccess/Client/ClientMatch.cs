﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.DataAccess.Client
{
    public class ClientMatch
    {
        public Guid clientID { get; set; }
        public int customerID { get; set; }
        public int locationID { get; set; }
    }
}
