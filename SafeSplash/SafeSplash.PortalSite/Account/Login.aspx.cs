﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using SafeSplash.PortalSite.Models;
using System.Collections.Generic;

namespace SafeSplash.PortalSite.Account
{
    public partial class Login : Page
    {
        private string ReturnUrl 
        {
            get {
                string returnUrl = Request.QueryString["ReturnUrl"];
                if (string.IsNullOrEmpty(returnUrl))
                {
                    returnUrl = "/Content/SS/MyTasks.aspx";
                }

                return returnUrl;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // RegisterHyperLink.NavigateUrl = "Register";
            // Enable this once you have account confirmation enabled for password reset functionality
            //ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            
            OpenAuthLogin.ReturnUrl = ReturnUrl;
            // var returnUrl = HttpUtility.UrlEncode(returnUrl);
            //if (!String.IsNullOrEmpty(HttpUtility.UrlEncode(ReturnUrl)))
            //{
                //  RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            //}
        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Validate the user password
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                // This doen't count login failures towards account lockout
                // To enable password failures to trigger lockout, change to shouldLockout: true
                var result = signinManager.PasswordSignIn(Email.Text, Password.Text, RememberMe.Checked, shouldLockout: false);

                if (result == SignInStatus.Success)
                {
                    // Validate Roles
                    AspNetUser newUser = manager.FindByName(Email.Text);
                    if (newUser != null)
                    {
                        var roles = manager.GetRoles(newUser.Id);

                        // // // OR // // // 
                        
                        bool inRole = manager.IsInRole(newUser.Id, "Client");
                    }
                }


                switch (result)
                {
                    case SignInStatus.Success:
                        IdentityHelper.RedirectToReturnUrl( ReturnUrl, Response); // Request.QueryString["ReturnUrl"], Response);
                        break;
                    case SignInStatus.LockedOut:
                        Response.Redirect("/Account/Lockout");
                        break;
                    case SignInStatus.RequiresVerification:
                        Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}", 
                                                        ReturnUrl, // Request.QueryString["ReturnUrl"],
                                                        RememberMe.Checked),
                                          true);
                        break;
                    case SignInStatus.Failure:
                    default:
                        FailureText.Text = "Invalid login attempt";
                        ErrorMessage.Visible = true;
                        break;
                }
            }
        }
    }
}