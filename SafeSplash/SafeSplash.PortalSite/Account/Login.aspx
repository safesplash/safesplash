﻿<%@ Page Title="Log in" Language="C#" MasterPageFile="~/SafeSplashDefault.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SafeSplash.PortalSite.Account.Login" Async="true" %>

<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="PlaceHolderMain">
    <%--<h2><%: Title %>.!</h2>--%>

    <%--<div class="row">--%>
    <table border="0" style="font-size: 9pt; font-family: Arial, Helvetica, sans-serif;">
        <tr>
            <td style="vertical-align: middle; text-align: center;">
                <asp:Image runat="server" ImageUrl="~/Layouts/Shares/Images/logo-1.png" alt="" border="0" Style="vertical-align: middle; text-align: center;" />
            </td>
            <td>
                <section id="loginForm">
                    <div class="form-horizontal" style="border: 1px solid #616EA2; width: 400px; font-size: 9pt; font-family: Arial, Helvetica, sans-serif; margin: 10px; padding: 10px; border-radius: 5px;">
                        <span style="text-align: center; margin: 20px;">Please enter the 
								Email Address we have on file for you (or your 
								Login ID) and your Password, then click the <b>Log In</b> button.</span>
                        <hr />
                        <asp:PlaceHolder runat="server" ID="ErrorMessage" Visible="false">
                            <p class="text-danger">
                                <asp:Literal runat="server" ID="FailureText" />
                            </p>
                        </asp:PlaceHolder>
                        <div class="row">
                            <div class="col-md-3">
                                <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">User&nbspName:</asp:Label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="Email" CssClass="form-control"  />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                    CssClass="text-danger" ErrorMessage="The email field is required." />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">Password:</asp:Label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="text-danger" ErrorMessage="The password field is required." />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <div class="checkbox">
                                    <asp:CheckBox runat="server" ID="RememberMe" />
                                    <asp:Label runat="server" AssociatedControlID="RememberMe">Remember me?</asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button runat="server" BackColor="#276ed9" Width="200px" ForeColor="White" OnClick="LogIn" Text="Log in" CssClass="btn btn-default" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <a class="NavLink" href="javascript:" onclick="window.location.href='clients/FindPassword.aspx'; return false;">I forgot my password</a>
                            </div>
                        </div>
                    </div>
                    <%--<p>
                    <asp:HyperLink runat="server" ID="RegisterHyperLink" ViewStateMode="Disabled">Register as a new user</asp:HyperLink>
                </p>--%>
                    <p>
                        <%-- Enable this once you have account confirmation enabled for password reset functionality
                    <asp:HyperLink runat="server" ID="ForgotPasswordHyperLink" ViewStateMode="Disabled">Forgot your password?</asp:HyperLink>
                        --%>
                    </p>
                </section>
            </td>


            <%--</div>--%>
        </tr>
    </table>
    <div style="visibility: hidden">
        <section id="socialLoginForm">
            <uc:OpenAuthProviders runat="server" ID="OpenAuthLogin" Visible="false" />
        </section>
    </div>
</asp:Content>
