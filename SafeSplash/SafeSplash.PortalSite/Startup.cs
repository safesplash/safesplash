﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SafeSplash.PortalSite.Startup))]
namespace SafeSplash.PortalSite
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
