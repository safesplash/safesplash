﻿using System;
using System.Data;
using System.Collections;
//using Microsoft.SharePoint;
//using Microsoft.SharePoint.WebControls;
using AKMII.SafeSplash.Entity;
using AKMII.SafeSplash.BLL;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using SafeSplash.ObjectModel;
using SafeSplash.ObjectModel.Class;

// Added These
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using SafeSplash.PortalSite.Models;


namespace SafeSplash.PortalSite.Layouts.Clients
{
    public partial class Register : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.errorMsg.Visible = false;
            this.errorMsg.Text = "";
            if (!Page.IsPostBack)
            {
                InitControls();

                if (Session["reg_Client"] != null)
                {
                    tb_Client tbClient = (tb_Client)Session["reg_Client"];
                    LoadClientInfo(tbClient);
                }
                else
                {
                    this.referTitle.Style.Clear();
                    this.referTitle.Style.Add("display", "none");
                    this.TextWhoReferred.Style.Clear();
                    this.TextWhoReferred.Style.Add("display", "none");
                }

                // testing data...
                //this.TextFirstName.Value = "Mike";
                //this.TextLastName.Value = "Rohrbach";
                //this.TextEmail.Value = "mikerohrbach@yahoo.com";
                //this.textPrimaryPhone.Text = "360-459-4522";
                //this.textSecondPhone.Text = string.Empty;
                //this.TextSpouseName.Value = string.Empty;
                //this.TextAddress.Value = "4715 82nd Ave SE";
                //this.TextZip.Value = "98501";
                //this.TextCity.Value = "Olympia";
                //this.ddlState.SelectedValue = "WA";
                //this.textEmergency.Text = string.Empty;
                //this.txbEmergencyPhone.Text = string.Empty;
                //this.ddlAboutUs.SelectedValue = "Google";
                //radio24HourFitness1.Checked = true;
                //radio24HourFitness1.Text = "Yes";
                ////this.listBoxLocation.SelectedValue = "Henderson, NV";
                //this.listBoxLocation.Items[0].Selected = true;
                ////radioWaiver1.Checked = true;
                ////radioPolicies1.Checked = true;
                ////radioBilling1.Checked = true;
                ////////////////////////////////////////////
            }
        }

        private void LoadClientInfo(tb_Client client)
        {
            if (client != null)
            {
                this.TextFirstName.Value = client.client_FirstName;
                this.TextLastName.Value = client.client_LastName;
                this.TextEmail.Value = client.client_Email;
                this.textPrimaryPhone.Text = client.client_PrimaryPhone;
                this.textSecondPhone.Text = client.client_SecondaryPhone;
                this.TextSpouseName.Value = client.client_SpouseName;
                this.TextAddress.Value = client.client_Address;
                this.TextZip.Value = client.client_Zip;
                this.TextCity.Value = client.client_City;
                this.ddlState.SelectedValue = client.client_State;
                this.textEmergency.Text = client.client_EmergencyContact;
                this.txbEmergencyPhone.Text = client.client_EmergencyPhone;
                this.ddlAboutUs.SelectedValue = client.client_HowHearUs;
                this.TextTwentyFourHourFitness.Text = client.client_IsInterestedInTwentyFourHourFitnessMembership.ToString();
                if (client.client_HowHearUs == "Referral")
                {
                    this.referTitle.Style.Clear();
                    this.referTitle.Style.Add("display", "block");
                    this.TextWhoReferred.Style.Clear();
                    this.TextWhoReferred.Style.Add("display", "block");
                    this.TextWhoReferred.Text = client.client_WhoReferred;
                }
                else
                {
                    this.referTitle.Style.Clear();
                    this.referTitle.Style.Add("display", "none");
                    this.TextWhoReferred.Style.Clear();
                    this.TextWhoReferred.Style.Add("display", "none");
                }
                this.listBoxLocation.SelectedValue = Session["reg_Client_loca"].ToString();
            }
        }

        protected void btnSumit_Click(object sender, EventArgs e)
        {
            //string policiesWarning = "To register you must read the policies and procedures listed at the bottom and mark them as yes to indicate you have read them.";
            if (this.listBoxLocation.SelectedIndex == -1)
            {
                errorMsg.Text += "please select one location you are interested in. <br/>";
            }
            ClientBll clientBll = new ClientBll();
            List<tb_Client> dupUser = clientBll.GetByEmail(this.TextEmail.Value.Trim());
            if (dupUser != null && dupUser.Count > 0)
            {
                errorMsg.Text += "You are already in our registration system. To help you book a class quickly, please call us at 303-799-1885";
            }
            //if (!radioWaiver1.Checked)
            //{
            //    errorMsg.Text += policiesWarning + "<br/>";
            //}
            //else if (!this.radioPolicies1.Checked)
            //{
            //    errorMsg.Text += policiesWarning + "<br/>";
            //}
            //else if (!this.radioBilling1.Checked)
            //{
            //    errorMsg.Text += policiesWarning + "<br/>";
            //}
            if (errorMsg.Text.Length > 0)
            {
                errorMsg.Visible = true;
                return;
            }
            tb_Client clientInfo;
            if (Session["reg_Client"] != null)
            {
                clientInfo = (tb_Client)Session["reg_Client"];
            }
            else
            {
                clientInfo = new tb_Client();
            }
            clientInfo.client_ID = Guid.NewGuid();
            clientInfo.client_Code = clientBll.GetNewClientCode();
            clientInfo.client_FirstName = TextFirstName.Value;
            clientInfo.client_LastName = TextLastName.Value;
            clientInfo.client_Email = TextEmail.Value;
            clientInfo.client_PrimaryPhone = textPrimaryPhone.Text;
            clientInfo.client_SecondaryPhone = textSecondPhone.Text;
            clientInfo.client_SpouseName = TextSpouseName.Value;
            clientInfo.client_Address = TextAddress.Value;
            clientInfo.client_Zip = TextZip.Value;
            clientInfo.client_City = TextCity.Value;
            clientInfo.client_State = ddlState.SelectedValue;
            clientInfo.client_EmergencyContact = textEmergency.Text;
            clientInfo.client_EmergencyPhone = txbEmergencyPhone.Text;
            clientInfo.client_HowHearUs = ddlAboutUs.SelectedValue;
            if (clientInfo.client_HowHearUs == "Referral")
            {
                clientInfo.client_WhoReferred = TextWhoReferred.Text;
            }
            clientInfo.client_Status = ClientStatus.prospect.ToString();
            clientInfo.client_IsInterestedInTwentyFourHourFitnessMembership = (String.IsNullOrEmpty(TextTwentyFourHourFitness.Text) || TextTwentyFourHourFitness.Text.Equals("0")) ? false : true;
            clientInfo.client_CreatedDate = DateTime.Now;
            clientInfo.client_IsCompleteRegister = false;
            clientInfo.client_fitnessMember = (fitnessMember2.Checked && !fitnessMember1.Checked) ? false : true;
            clientInfo.client_promoOptIn = promos.Checked;
            Session["reg_Client"] = clientInfo;
            Session["reg_Client_loca"] = this.listBoxLocation.SelectedValue.Split('|')[0];
            //Response.Redirect("StudentsList.aspx");
            //Response.Redirect("MarketSegmentQuestions.aspx");
            Response.Redirect("AcceptAgreements.aspx");
        }

        private void InitControls()
        {
            ControlInitialization.DDL_State(this.ddlState, true);

            LocationBll locaBll = new LocationBll();
            List<AKMII.SafeSplash.EntityNew.tb_Location> listLoca = locaBll.GetAll();

            foreach (var loc in listLoca)
            {
                listBoxLocation.Items.Add(new ListItem(loc.loca_Name, loc.loca_ID + "|" + loc.facilitytype_ID));
            }

            ControlInitialization.DDL_HowHear(ddlAboutUs, true);
        }
    }
}
