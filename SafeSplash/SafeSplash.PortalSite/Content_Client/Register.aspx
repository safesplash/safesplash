﻿
<%@ Page Title="Register"  Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="SafeSplash.PortalSite.Layouts.Clients.Register" MasterPageFile="~/SafeSplashDefault.Master" Async="true" %>

<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%--<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>--%>


<asp:Content ID="PageHead" ContentPlaceHolderID="SPNavigation" runat="server">
    <link type="text/css" href="/_layouts/JQuery/css/tab/jquery-ui-1.8.5.custom.css" rel="stylesheet" />
    <link type="text/css" href="/_layouts/Shares/css/buttons.css" rel="stylesheet" />    
    <script type="text/javascript" src="/_layouts/JQuery/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="/_layouts/JQuery/js/jquery-ui-1.8.5.custom.min.js"></script>
    <script type="text/javascript">
        var PhoneSeparator = "-";
        var checkPoints = new Array(3, 7);
        var keyDelete = 46;
        var keyBackspace = 8;
        var ignoreKeys = new Array(keyDelete, keyBackspace);

        (function ($) {
            $(document).ready(function () {
                method2();
            });
        })(jQuery);

        function method2() {
            $('.PhoneControl').keyup(
		function () {
		    var str = $(this).val();
		    //str = str.replace(PhoneSeparator, "");
		    str = str.replace(new RegExp(PhoneSeparator, "gm"), "");
		    $(checkPoints).each(
				function () {
				    if (str.length > this) {
				        if (str.charAt(this) != PhoneSeparator) {
				            str = str.substring(0, this) + PhoneSeparator + str.substring(this);
				        }
				    }
				}
			);
		    $(this).val(str);
		}
	);

        }

        function method1() {
            $('.PhoneControl').keydown(
			function (event) {
			    var str = $(this).val();
			    if ($.inArray(str.length, checkPoints) > -1 &&
					$.inArray(event.keyCode, ignoreKeys) == -1) {
			        str += PhoneSeparator;
			        $(this).val(str);
			    }

			}

		);
            $('.PhoneControl').keyup(
			function (event) {
			    var str = $(this).val();
			    $(checkPoints).each(
					function () {
					    if (str.length > this) {
					        if (str.charAt(this) != PhoneSeparator) {
					            str = str.substring(0, this) + PhoneSeparator + str.substring(this);
					        }
					    }
					}
				)
			    $(this).val(str);
			}
		)


        }
        function cancelInfo() {
            window.location = "clearsession.aspx?page=/_layouts/login.aspx";
        }

        function OnChange(dropdown) {
            var myindex = dropdown.selectedIndex;
            var SelValue = dropdown.options[myindex].value;
            if (SelValue == "Referral") {
                document.getElementById("<%=this.referTitle.ClientID %>").style.display = 'block';
                document.getElementById("<%=this.TextWhoReferred.ClientID %>").style.display = 'block';
            }
            else {
                document.getElementById("<%=this.referTitle.ClientID %>").style.display = 'none';
                document.getElementById("<%=this.TextWhoReferred.ClientID %>").style.display = 'none';
            }
            return true;
        }        
    </script>
    <style type="text/css">
        .tdSpacer { width: 5px; }
    </style>
</asp:Content>
<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <script type="text/javascript">
        function SetTwentyFourHourFitnessValue(value) {
            document.getElementById('<%=this.TextTwentyFourHourFitness.ClientID%>').value = value;
        }

        function IsValidTwentyFourHourFitnessValue(source, args) {
            if (args.Value.length > 0)
                args.IsValid = true;
            else
                args.IsValid = false;
        }

        function IsReferalEntered(sender, args) {
            args.IsValid = true;
            var dropdown = document.getElementById("<%=this.ddlAboutUs.ClientID %>");
            var myindex = dropdown.selectedIndex;
            var selectedValue = dropdown.options[myindex].value;
            if (selectedValue == "Referral") {
                if (document.getElementById("<%=this.TextWhoReferred.ClientID %>").value.length > 0) {
                    args.IsValid = true;
                }
                else {
                    args.IsValid = false;
                }   
            }
        }
        function locationChange(value) {
            var selected = value.split("|")[1];
            if (selected == 2) {
                $("#memberInfo").html("Are you currently a member of 24 Hour Fitness?");
                $("#memberInfo").show();
                $("#memberInfoRadio").show();
                $("#promoOptIn").show();
            }
            else if (selected == 3) {
                $("#memberInfo").html("Are you currently a member of LA Fitness (City Sports Club)?");
                $("#memberInfo").show();
                $("#memberInfoRadio").show();
                $("#promoOptIn").show();
            } else {
                $("#memberInfo").hide();
                $("#memberInfoRadio").hide();
                $("#promoOptIn").hide();
            }
        }
      

    </script>
    <div style="font-size: 8pt; font-family: Arial, Helvetica, sans-serif;">
        <table style="width: 100%; font-size: 8pt; font-family: Arial, Helvetica, sans-serif;">
            <%--<tr>
                <td>
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <span style="font-size: medium"><strong>Create an Account</strong></span>
                            </td>
                            <td style="text-align: right;">
                                <asp:Button ID="Button1" runat="server" Text="Next" Style="width: 80px;" OnClick="btnSumit_Click" />
                                &nbsp;&nbsp;
                                <input name="Button1" type="submit" value="Cancel" style="width: 80px;" onclick="cancelInfo();" />
                                <br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>--%>
            <tr>
                <td><h2>Create an Account</h2></td>
            </tr>
            <tr>
                <td>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="font-size: 9pt; font-family: Arial, Helvetica, sans-serif;">
                    Filling out this form does not automatically assign you to classes.
                    <br />
                    <b>We just need to get some information about you, then your swimmers and what days and times you are looking for.</b>
                    <br />
                    <br />
                    Note: Fields with an asterisk(<strong>*</strong>) are required.<input id="Field_ClientID"
                        type="hidden" runat="server" /><br />
                    <asp:Label ID="errorMsg" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" cellspacing="1" style="border-top: 1px double #9AC6FF; font-size: 9pt;
                        font-family: Arial, Helvetica, sans-serif;">
                        <tr>
                            <td align="right" valign="middle" style="background-color: #EFE8FF; height: 25px;">
                                * Parent's First Name:
                            </td>
                            <td class="tdSpacer" />
                            <td align="left" valign="middle" style="width: 250px">
                                <input type="text" name="Firstname" value="" size="30" maxlength="35" id="TextFirstName"
                                    runat="server" />
                                <asp:RequiredFieldValidator ID="InputFormRequiredFieldValidator1"
                                    runat="server" ControlToValidate="TextFirstName" ErrorMessage="Please enter your first name" />
                                <asp:RegularExpressionValidator ID="InputFormRegularExpressionValidator4"
                                    runat="server" ControlToValidate="TextFirstName" ValidationExpression="^[a-zA-Z]*$"
                                    ErrorMessage="Please enter the valid character" />
                            </td>
                            <td align="right" valign="middle" style="background-color: #EFE8FF; width: 180px;
                                height: 25px;">
                                * Parent's Last Name:
                            </td>
                            <td class="tdSpacer" />
                            <td align="left" valign="middle" style="width: 250px">
                                <input type="text" name="Lastname" value="" size="30" maxlength="35" id="TextLastName"
                                    runat="server" />
                                <asp:RequiredFieldValidator ID="InputFormRequiredFieldValidator2"
                                    runat="server" ControlToValidate="TextLastName" ErrorMessage="Please enter your last name" />
                                <asp:RegularExpressionValidator ID="InputFormRegularExpressionValidator5"
                                    runat="server" ControlToValidate="TextLastName" ValidationExpression="^[a-zA-Z]*$"
                                    ErrorMessage="Please enter the valid character" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="middle" style="background-color: #EFE8FF; height: 25px;">
                                * Email:
                            </td>
                            <td class="tdSpacer" />
                            <td align="left" valign="middle">
                                <input type="text" name="Email" value="" size="30" maxlength="50" id="TextEmail"
                                    runat="server" />
                                <asp:RequiredFieldValidator ID="InputFormRequiredFieldValidator3"
                                    runat="server" ControlToValidate="TextEmail" ErrorMessage="Please enter the email address" />
                                <asp:RegularExpressionValidator ID="InputFormRegularExpressionValidator1"
                                    runat="server" ControlToValidate="TextEmail" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zAZ0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                    ErrorMessage="Your email address is not valid" />
                            </td>
                            <td align="right" valign="middle" style="background-color: #EFE8FF; height: 25px;">
                                * Primary Phone Number:
                            </td>
                            <td class="tdSpacer" />
                            <td align="left" valign="middle">
                                <asp:TextBox runat="server" class="PhoneControl" MaxLength="12" ID="textPrimaryPhone"
                                    Width="180px"></asp:TextBox>(xxx-xxx-xxxx)
                                <asp:RequiredFieldValidator ID="InputFormRequiredFieldValidator4"
                                    runat="server" ControlToValidate="textPrimaryPhone" ErrorMessage="Please enter the primary phone number" />
                                <asp:RegularExpressionValidator ID="InputFormRegularExpressionValidator2"
                                    runat="server" ControlToValidate="textPrimaryPhone" ValidationExpression="^\d{3}-\d{3}-\d{4}$"
                                    ErrorMessage="Your primary phone number is not valid" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="middle" style="background-color: #EFE8FF; height: 25px;">
                                Spouse's name:
                            </td>
                            <td class="tdSpacer" />
                            <td align="left" valign="middle">
                                <input type="text" name="Address1" value="" size="30" maxlength="40" id="TextSpouseName"
                                    runat="server" />
                            </td>
                            <td align="right" valign="middle" style="background-color: #EFE8FF; height: 25px;">
                                Secondary Phone Number:
                            </td>
                            <td class="tdSpacer" />
                            <td align="left" valign="middle">
                                <asp:TextBox runat="server" ID="textSecondPhone" class="PhoneControl" MaxLength="12"
                                    Width="180px"></asp:TextBox>(xxx-xxx-xxxx)
                                <asp:RegularExpressionValidator ID="InputFormRegularExpressionValidator6"
                                    runat="server" ControlToValidate="textSecondPhone" ValidationExpression="^\d{3}-\d{3}-\d{4}$"
                                    ErrorMessage="Your second phone number is not valid" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="middle" style="background-color: #EFE8FF; height: 25px;">
                                * Address:
                            </td>
                            <td class="tdSpacer" />
                            <td align="left" valign="middle" colspan="4">
                                <input type="text" name="Address" value="" maxlength="40" id="TextAddress" runat="server"
                                    style="width: 660px" />
                                <asp:RequiredFieldValidator ID="InputFormRequiredFieldValidator5"
                                    runat="server" ControlToValidate="TextAddress" ErrorMessage="Please enter the address" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="middle" style="background-color: #EFE8FF;">
                                * City:
                            </td>
                            <td class="tdSpacer" />
                            <td align="left" valign="middle" colspan="4">
                                <table style="width: 100%; font-size: 9pt; font-family: Arial, Helvetica, sans-serif;">
                                    <tr>
                                        <td>
                                            <input name="City" type="text" class="pubForm" id="TextCity" value="" size="25" maxlength="40"
                                                runat="server" />
                                            <asp:RequiredFieldValidator ID="InputFormRequiredFieldValidator7"
                                                runat="server" ControlToValidate="TextCity" ErrorMessage="Please enter the city name." />
                                        </td>
                                        <td align="right" valign="middle" style="background-color: #EFE8FF;">
                                            * State:
                                        </td>
                                        <td class="tdSpacer" />
                                        <td align="left" valign="middle" colspan="3">
                                            <asp:DropDownList runat="server" ID="ddlState" Width="180px">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="InputFormRequiredFieldValidator8"
                                                runat="server" ControlToValidate="ddlState" ErrorMessage="Please select the State you are living in." />
                                        </td>
                                        <td align="right" valign="middle" style="background-color: #EFE8FF;">
                                            * Zip:
                                        </td>
                                        <td class="tdSpacer" />
                                        <td align="left" valign="middle" colspan="3">
                                            <input value="" type="text" name="Zip" size="20" maxlength="40" id="TextZip" runat="server" />
                                            <asp:RequiredFieldValidator ID="InputFormRequiredFieldValidator6"
                                                runat="server" ControlToValidate="TextZip" ErrorMessage="Please enter the Zip code." />
                                            <asp:RegularExpressionValidator ID="InputFormRegularExpressionValidator3"
                                                runat="server" ControlToValidate="TextZip" ValidationExpression="(^\d{5}$)|(^\d{5}-\d{4}$)"
                                                ErrorMessage="Your zip code is not valid" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="middle" style="background-color: #EFE8FF; height: 25px;">
                                Emergency Contact:
                            </td>
                            <td class="tdSpacer" />
                            <td align="left" valign="middle">
                                <asp:TextBox runat="server" ID="textEmergency" Width="180px">
                                </asp:TextBox>
                            </td>
                            <td align="right" valign="middle" style="background-color: #EFE8FF; height: 25px;">
                                Contact Phone:
                            </td>
                            <td class="tdSpacer" />
                            <td align="left" valign="middle">
                                <asp:TextBox runat="server" ID="txbEmergencyPhone" class="PhoneControl" MaxLength="12"
                                    Width="180px">
                                </asp:TextBox>(xxx-xxx-xxxx)
                                <asp:RegularExpressionValidator ID="InputFormRegularExpressionValidator7"
                                    runat="server" ControlToValidate="txbEmergencyPhone" ValidationExpression="^\d{3}-\d{3}-\d{4}$"
                                    ErrorMessage="Your primary phone number is not valid" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="middle" style="background-color: #EFE8FF; height: 25px;">
                                * How did you hear about us?
                            </td>
                            <td class="tdSpacer" />
                            <td align="left" valign="middle">
                                <asp:DropDownList runat="server" ID="ddlAboutUs" Width="180px" onchange='OnChange(this);'>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="InputFormRequiredFieldValidator10"
                                    runat="server" ControlToValidate="ddlAboutUs" ErrorMessage="Please select an option" />
                            </td>
                            <td align="right" valign="middle" style="background-color: #EFE8FF; height: 25px;">
                                <asp:Label ID="referTitle" runat="server" Text="* Who referred you?"></asp:Label>
                            </td>
                            <td class="tdSpacer" />
                            <td align="left" valign="middle" colspan="3">
                                <asp:TextBox ID="TextWhoReferred" runat="server" Width="210px"></asp:TextBox>
                                <asp:CustomValidator ID="TextWhoReferredCustomValidator" runat="server"
                                    ControlToValidate="TextWhoReferred" ClientValidationFunction="IsReferalEntered"
                                    ValidateEmptyText="true" ErrorMessage="Please enter who referred you." />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" colspan="1" style="background-color: #EFE8FF;">
                                * Which of our locations are you interested in?
                            </td>
                            <td class="tdSpacer" />
                            <td align="left" valign="middle" colspan="4">
                                <asp:ListBox runat="server" ID="listBoxLocation" Width="420px" Height="80px" onchange="locationChange(this.value)" Font-Size="9pt"
                                    Font-Names="Arial, Helvetica, sans-serif"></asp:ListBox>
                            </td>
                        </tr>
                          <tr>
                            <td align="right" valign="middle" style="background-color: #EFE8FF; height: 25px;">
                                * Are you interested in a gym membership?
                            </td>
                            <td class="tdSpacer" />
                            <td align="left" valign="middle" colspan="4">
                                <asp:RadioButton runat="server" ID="radio24HourFitness1" EnableViewState="true" Text="Yes" Value="1" GroupName="24HourFitness"
                                    onClick="SetTwentyFourHourFitnessValue(this.value)" />
                                &nbsp;&nbsp;
                                <asp:RadioButton runat="server" ID="radio24HourFitness2" EnableViewState="true" Text="No" Value="0" GroupName="24HourFitness"
                                    onClick="SetTwentyFourHourFitnessValue(this.value)" />
                                <asp:TextBox ID="TextTwentyFourHourFitness" runat="server" Style="display: none;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="InputFormRequiredFieldValidator9"
                                    runat="server" ControlToValidate="TextTwentyFourHourFitness" ErrorMessage="Please select an option" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="memberInfo" style="background-color: #EFE8FF; height: 25px; display:none">
                                    
                                </div>
                            </td>
                            <td class="tdSpacer" />
                            <td colspan="4">
                                <div id="memberInfoRadio" style="display: none;">
                                    <asp:RadioButton runat="server" ID="fitnessMember1" Text="Yes" value="1" GroupName="fitnessMember"/>
                                    <asp:RadioButton runat="server" ID="fitnessMember2" Text="No" value="0" GroupName="fitnessMember"/>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <div id="promoOptIn" style="height: 25px; display: none;">
                                    <br/><br/>
                                    <asp:Checkbox runat="server" id="promos" Checked="True" /> &nbsp;&nbsp;Please have someone contact me with special gym membership offers and promotions for SafeSplash families
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="border-top: 1px double #80DBFC;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    <input name="Button1" type="submit" value="Cancel" class="button gray" onclick="cancelInfo();" />
                    <asp:Button ID="btnNextBelow" runat="server" Text="Next" CssClass="button blue" style="margin-left: 20px;" OnClick="btnSumit_Click" />                    
                </td>
            </tr>
            <tr style="height: 15px;">
                <td/>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    User Registration
</asp:Content>
