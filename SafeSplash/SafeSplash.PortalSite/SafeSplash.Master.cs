﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Claims;
using System.Security.Principal;
using System.Web.Security;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using SafeSplash.PortalSite.Models;


namespace SafeSplash.PortalSite
{
    public partial class SafeSplash : System.Web.UI.MasterPage
    {

        private string _userRole = "N/A";
        protected string UserRole
        {
            get
            {
                // Validate the user password
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                if (manager != null)
                {
                    ///RAB: This also works
                    Context.User.IsInRole("Client");


                    try
                    {
                        string userID = Context.User.Identity.GetUserId();

                        List<string> roles = manager.GetRoles(userID) as List<string>;

                        if ((roles != null) && (roles.Count >= 1))
                        {
                            _userRole = roles[0];
                        }
                    }
                    catch { }




                    //_userRole = manager.GetRoles(Context.User.Identity.GetUserId()).ToString();
                    //_userRole = manager.GetRoles(Context.User.Identity.GetUserId()).ToList<string>().ToString();
                }

                // This doen't count login failures towards account lockout
                // To enable password failures to trigger lockout, change to shouldLockout: true
                //var result = signinManager.PasswordSignIn(Email.Text, Password.Text, RememberMe.Checked, shouldLockout: false);

                //if (result == SignInStatus.Success)
                //{
                //    // Validate Roles
                //    AspNetUser newUser = manager.FindByName(Email.Text);
                //    if (newUser != null)
                //    {
                //        var roles = manager.GetRoles(newUser.Id);

                //        // // // OR // // // 

                //        bool inRole = manager.IsInRole(newUser.Id, "Client");
                //    }
                //}
                return _userRole;
            }
            private set { }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut();
        }
    }
}