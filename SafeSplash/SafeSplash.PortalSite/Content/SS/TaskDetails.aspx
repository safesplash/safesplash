﻿<%--<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaskDetails.aspx.cs" Inherits="SafeSplash.PortalSite.Layouts.SS.TaskDetails" MasterPageFile="~/SafeSplash.Master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
<script type="text/javascript">
    function editTask() {
        var ddlID = document.getElementById("<%=this.Field_TaskID.ClientID %>").value;
        var url = "EditTask.aspx?ID=" + ddlID;
        ShowEditTaskDialog(url);
    }

    function ShowEditTaskDialog(pageToLoad) {
        var options = {
            url: pageToLoad,
            title: 'Edit Task',
            showClose: true,
            width: 560,
            height: 820,
            allowMaximize: false,
            dialogReturnValueCallback: EditTaskCallBack
        };

        window.location = pageToLoad;

        // window.showModalDialog( pageToLoad );
        //imgwindow1 = window.open("taskDetails.aspx", "imgwindow1", "");
        //imgwindow1.focus();
        // SP.UI.ModalDialog.showModalDialog(options);
    }

    function EditTaskCallBack(result, value) {
        if (result == SP.UI.DialogResult.OK) {
            __doPostBack('__Page', 'ReloadControl');
        }
    }

   
</script>

<style type="text/css">
.label
{
    width: 180px; 
    height: 25px; 
    background-color: #EFE8FF; 
    text-align: right;
    font-weight: bold;
    margin-right: 5px;
}
.tdSpacer
{
    width: 5px;
}
.trSpacer
{
    height: 10px;
}
</style>

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
<div>
	<table style="width: 100%">
		<tr>
			<td>
                <table style="width: 100%">
				    <tr>
					    <td style="font-size: large">
                        <asp:Label ID="lblPageTitle" Text="Task: " runat="server"></asp:Label> 
                        <asp:Label ID="lblTaskTitle" runat="server"></asp:Label>
                        </td>
					    <td>
                        &nbsp;</td>
					    <td align="right"> 
                        <input id="btnEditTask" name="btnEditTask" type="button" value="Edit Task" onclick="editTask();" style="border-style: outset; border-width: 1px; background-color: #003399; color: #FFFFFF;" />
                        </td>
				    </tr>
			    </table>
			</td>
        </tr>
        <tr>
            <td>
                <hr />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID = "errorMsg" ForeColor="Red" runat="server" Visible="false"></asp:Label>
                <input id="Field_TaskID" type="hidden" runat="server" />
            </td>
        </tr>
		<tr>
			<td align="center">
			<table style="border: 1px double #9AC6FF; width: 100%" cellpadding="0" cellspacing="0">
            <tr>
					<td class="label">
					Location:</td>
                    <td class="tdSpacer" />
					<td style="text-align: left;">
                        <asp:Label ID="lblLocation" runat="server"></asp:Label>
					</td>
				</tr>
                <tr>
					<td class="label" style="vertical-align: top;">
					Assigned To:</td>
                    <td class="tdSpacer" />
					<td style="text-align: left; vertical-align: top;">
                        <asp:Label ID="lblAssignedTo" runat="server"></asp:Label>
					</td>
				</tr>
                <tr class="trSpacer">
					<td class="label" />
                    <td class="tdSpacer" />
					<td />
				</tr>
                <tr>
					<td class="label">
					Category:</td>
                    <td class="tdSpacer" />
					<td style="text-align: left;">
                        <asp:Label ID="lblCategory" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td class="label">
					Start Date:</td>
                    <td class="tdSpacer" />
					<td style="text-align: left;">
                        <asp:Label ID="lblStartDate" runat="server"></asp:Label>
					</td>
				</tr>
                <tr>
					<td class="label">
					Due Date:</td>
                    <td class="tdSpacer" />
					<td style="text-align: left;">
                        <asp:Label ID="lblDueDate" runat="server"></asp:Label>
					</td>
				</tr>
                <tr>
					<td class="label">
					Status:</td>
                    <td class="tdSpacer" />
					<td style="text-align: left;">
                        <asp:Label ID="lblStatus" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td class="label" style="vertical-align: top;">
					Additional Recipients:</td>
                    <td class="tdSpacer" />
					<td style="text-align: left; vertical-align: top;">
                        <asp:Label ID="lblAdditionalRecipients" runat="server"></asp:Label>
					</td>
				</tr>
                <tr class="trSpacer">
					<td class="label" />
                    <td class="tdSpacer" />
					<td />
				</tr>
                <tr>
					<td class="label" style="vertical-align: top;">
					Content:</td>
                    <td class="tdSpacer" />
					<td style="text-align: left; vertical-align: top;">
                        <asp:Label ID="lblContent" runat="server"></asp:Label>
					</td>
				</tr>
			</table>
			</td>
		</tr>
        <tr>
			<td align="left" colspan="2">
                <asp:Label ID="lblCreatedBy" runat="server"></asp:Label><br />
                <asp:Label ID="lblModifiedBy" runat="server"></asp:Label>
			</td>
		</tr>
	</table>
</div>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
Task Details
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
Task Details
</asp:Content>
