﻿using System;
//using Microsoft.SharePoint;
//using Microsoft.SharePoint.WebControls;
using AKMII.SafeSplash.BLL;
using SafeSplash.ObjectModel.Class;
using AKMII.SafeSplash.Entity;
using SafeSplash.Service.Employee;
using System.Web.UI;

using Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace SafeSplash.PortalSite.Layouts.SS
{
    public partial class StaffDelete : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //if (CurrentUserType != ObjectModel.UserType.CorpAdmin && CurrentUserType != ObjectModel.UserType.FacilityManager)
            if (!Context.User.IsInRole(ObjectModel.Constants.RoleCorpAdmin) && !Context.User.IsInRole(ObjectModel.Constants.RoleFacilityManager)) 
            {
                ObjectModel.Class.GenericHelper.pageRedirect(this.Page, "/_layouts/ss/accessdenied.aspx");
            }
            lblErrorMsg.Text = "";
        }

        protected override void OnPreInit(EventArgs e)
        {
            // this.isPopWindow = true;
            base.OnPreInit(e);
        }

        protected void btSubmit_Click(object sender, EventArgs e)
        {
            string accountIDs = Request["ID"];
            if (accountIDs != null)
            {
                string[] accountID = accountIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                EmployeeBll employeeBll = new EmployeeBll();
                UserBll userBll = new UserBll();
                for (int i = 0; i < accountID.Length; i++)
                {
                    bool allowDelete = true;
                    string loginName = string.Empty;
                    string roleName = string.Empty;
                    tb_Employee employee = employeeBll.GetByID(new Guid(accountID[i]));
                    if (employee != null && (employee.user_ID != null))
                    {
                        vw_GetUserInfoByUserId userInfo = userBll.GetUserInfoByUserID((Guid)employee.user_ID);
                        if (userInfo != null)
                        {
                            roleName = userInfo.RoleName;
                            loginName = userInfo.UserName;
                            if (loginName == "FBAADMIN")
                            {
                                this.lblErrorMsg.Text += "User 'FBAADMIN' is a System Admin account and cannot be deleted.<br/>";
                                allowDelete = false;
                            }
                        }
                    }
                    if (allowDelete)
                    {
                        //string errorMsg = string.Empty;
                        //if (!employeeBll.DeleteEmployee(new Guid(accountID[i]), out errorMsg))
                        //{
                        //    this.lblErrorMsg.Text += errorMsg + "<br/>";
                        //}
                        //else
                        //{
                        //    if (!String.IsNullOrEmpty(loginName))
                        //    {
                        //        ObjectModel.MembershipManage membership = new ObjectModel.MembershipManage();
                        //        membership.DeleteUser(loginName, true);
                        //        GenericHelper.RemoveUserFromAGroup(this.Page, loginName, roleName);
                        //    }
                        //}
                        //UserBll userBll = new UserBll();
                        //EmployeeBll employeeBll = new EmployeeBll();

                        Guid userID = new Guid( Context.User.Identity.GetUserId() ); //  userBll.GetUserIDByUserName(SPContext.Current.Web.CurrentUser.Name);
                        
                        tb_Employee employeePerformingDelete = employeeBll.GetByUserID(userID);
                        if (employeePerformingDelete != null)
                        {
                            IEmployeeService service = new EmployeeService();
                            IEmployeeAction action = new EmployeeAction()
                            {
                                EmployeeId = employeePerformingDelete.empl_ID,
                                Action = EmployeeActionType.DeleteEmployee.Description,
                                Date = DateTime.Now,
                                Description = string.Format("Attempted to delete emloyee {0}", loginName)
                            };
                            service.Save(action);
                        }
                    }
                }
            }
            if (String.IsNullOrEmpty(this.lblErrorMsg.Text))
            {
                GenericHelper.CloseDialog(this.Page, 1, "");
            }
        }
    }
}
