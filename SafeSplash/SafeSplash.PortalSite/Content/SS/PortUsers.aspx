﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SafeSplash.Master" AutoEventWireup="true" CodeBehind="PortUsers.aspx.cs" Inherits="SafeSplash.PortalSite.Content.SS.PortUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderBodyAreaClass" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderTitleAreaClass" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="PlaceHolderLeftNav" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <asp:Button runat="server" OnClick="PortUsers_Clicked" Text="Port Users" CssClass="btn btn-default" />
                </div>
                <div class="col-md-offset-2 col-md-10">
                    <asp:Button runat="server" OnClick="ResetPasswords_Clicked" Text="Reset Passwords" CssClass="btn btn-default" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
