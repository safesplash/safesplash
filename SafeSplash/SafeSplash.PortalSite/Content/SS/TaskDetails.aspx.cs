﻿using System;
using System.Web.UI;
//using Microsoft.SharePoint;
//using Microsoft.SharePoint.WebControls;
using AKMII.SafeSplash.BLL;
using AKMII.SafeSplash.Entity;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using SafeSplash.DataAccess.Tasks;

namespace SafeSplash.PortalSite.Layouts.SS
{
    public partial class TaskDetails : Page, IPostBackEventHandler
    {
        ITaskDataAccess TaskRepository = new DataAccess.NHibernate.TaskDataAccess();

        private Guid TaskId
        {
            get
            {
                if (ViewState["ID"] == null)
                    return Guid.Empty;
                else
                    return (Guid)ViewState["ID"];
            }
            set
            {
                ViewState["ID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request["ID"]))
            {
                this.Field_TaskID.Value = Request["ID"];
                Guid taskId = new Guid(Request["ID"]);
                TaskId = taskId;
                LoadTaskInfo(TaskId);
            }
        }

        void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
        {
            if (!string.IsNullOrEmpty(eventArgument))
            {
                if (eventArgument.Trim().Contains("ReloadControl"))
                {
                    LoadTaskInfo(TaskId);
                }
            }
        }

        private void LoadTaskInfo(Guid taskId)
        {
            TaskBll taskBll = new TaskBll();
            tb_Task task = taskBll.GetByID(taskId);
            if (task != null)
            {
                if (task.tb_Location != null)
                {
                    this.lblLocation.Text = task.tb_Location.loca_Name;
                }
                if (task.tb_TaskCategory != null)
                {
                    this.lblCategory.Text = task.tb_TaskCategory.taskCategory_Name;
                }
                this.lblTaskTitle.Text = task.task_Subject;
                this.lblContent.Text = ObjectModel.Class.GenericHelper.WrappableText(task.task_Content);
                this.lblStartDate.Text = ((DateTime)task.task_StartDate).ToShortDateString();
                this.lblDueDate.Text = ((DateTime)task.task_EndDate).ToShortDateString();
                //this.lblStatus.Text = ObjectModel.Class.GenericHelper.GetTaskStatusTextByValue(task.task_Status);
                this.lblStatus.Text = "OPEN".Equals(task.task_Status.Trim().ToUpper()) ? "Open" : "CLOSED".Equals(task.task_Status.Trim().ToUpper()) ? "Closed" : string.Empty;
                EmployeeBll employeeBll = new EmployeeBll();
                StringBuilder assignedTo = new StringBuilder();
                List<tb_Employee> assignedToEmployees = new List<tb_Employee>();
                if (!string.IsNullOrEmpty(task.task_AssignedTo))
                {
                    string[] assignedToList = task.task_AssignedTo.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var id in assignedToList)
                    {
                        tb_Employee employee = employeeBll.GetByID(new Guid(id));
                        if (employee != null)
                        {
                            assignedToEmployees.Add(employee);
                        }
                    }
                    foreach (var employee in assignedToEmployees.OrderBy(emp => emp.empl_FirstName).ThenBy(emp => emp.empl_LastName))
                    {
                        assignedTo.AppendFormat("{0} {1}; ", employee.empl_FirstName, employee.empl_LastName);
                    }
                }

                //TaskDataObject taskDataObject = TaskRepository.GetTask(this.TaskId);
                //foreach (var eg in taskDataObject.AssignedToEmployeeGroups)
                //{
                //    assignedTo.AppendFormat("{0}; ", eg.Name);
                //}


                this.lblAssignedTo.Text = assignedTo.ToString();
                StringBuilder additionalRecipients = new StringBuilder();
                List<tb_Employee> additionalRecipientsEmployees = new List<tb_Employee>();
                if (!string.IsNullOrEmpty(task.task_Recipient))
                {
                    string[] additionalRecipientsList = task.task_Recipient.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var id in additionalRecipientsList)
                    {
                        tb_Employee employee = employeeBll.GetByID(new Guid(id));
                        if (employee != null)
                        {
                            additionalRecipientsEmployees.Add(employee);
                        }
                    }
                    foreach (var employee in additionalRecipientsEmployees.OrderBy(emp => emp.empl_FirstName).ThenBy(emp => emp.empl_LastName))
                    {
                        additionalRecipients.AppendFormat("{0} {1}; ", employee.empl_FirstName, employee.empl_LastName);
                    }
                }
                this.lblAdditionalRecipients.Text = additionalRecipients.ToString();


                if (task.task_Creator != null)
                {
                    tb_Employee creator = employeeBll.GetByID((Guid)task.task_Creator);
                    this.lblCreatedBy.Text = "Created by " + creator.empl_FirstName + " " + creator.empl_LastName + " on " + ((DateTime)task.task_CreatedTime).ToString();
                }
                else
                {
                    this.lblCreatedBy.Text = "Created by " + "System" + " on " + ((DateTime)task.task_CreatedTime).ToString();
                }
                if (task.task_ModifiedBy != null)
                {
                    tb_Employee modified = employeeBll.GetByID((Guid)task.task_ModifiedBy);
                    if (modified != null)
                    {
                        this.lblModifiedBy.Text = "Last Modified by " + modified.empl_FirstName + " " + modified.empl_LastName + " on " + ((DateTime)task.task_ModifiedTime).ToString();
                    }
                    else
                    {
                        string modifiedByEmployeeGuid = task.task_ModifiedBy.Value.ToString();

                    }
                }
            }
        }
    }
}
