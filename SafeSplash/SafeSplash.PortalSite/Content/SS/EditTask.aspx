﻿<%--<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> --%>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%--<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditTask.aspx.cs" Inherits="SafeSplash.PortalSite.Layouts.SS.EditTask" MasterPageFile="~/SafeSplash.Master" %>

<%@ Register TagPrefix="SafeSplashControls" Namespace="SafeSplash.ObjectModel.Controls.PeoplePicker" Assembly="SafeSplash.ObjectModel, Version=1.0.0.0, Culture=neutral, PublicKeyToken=cdcfd765862d9189" %>
<%@ Register Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <%--<asp:ScriptManager runat="server">
        <scripts>
            
        </scripts>
    </asp:ScriptManager>--%>
    <style type="text/css">
        .label {
            width: 180px;
            height: 25px;
            background-color: #EFE8FF;
            text-align: right;
            font-weight: bold;
        }

        .tdSpacer {
            width: 5px;
        }
    </style>
</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <asp:HiddenField ID="AssignedToHiddenField" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="AdditionalRecipientsHiddenField" runat="server" EnableViewState="true" />
    <div>
        <table style="width: 100%">
            <tr>
                <td>&nbsp;
            <asp:Label ID="errorMsg" ForeColor="Red" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table style="border: 1px double #9AC6FF; width: 500px" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="label">Location:</td>
                            <td class="tdSpacer" />
                            <td style="text-align: left;">
                                <asp:DropDownList ID="LocationsDropDownList" runat="server" OnSelectedIndexChanged="LocationsDropDownList_SelectedIndexChanged" AutoPostBack="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="vertical-align: top;">Assign To Employees:</td>
                            <td class="tdSpacer" />
                            <td style="text-align: left;">
                                <asp:ListBox ID="AssignToListBox" runat="server" Style="min-width: 150px;" SelectionMode="Multiple" Rows="8" />
                            </td>
                        </tr>
                        <%--<tr>
                    <td class="label" style="vertical-align: top;">
					Assign To Employee Groups:</td>
                    <td class="tdSpacer" />
                    <td style="text-align: left;">
                        <asp:ListBox ID="AssignToGroupListBox" runat="server" style="min-width: 150px;" SelectionMode="Multiple" Rows="8" />
                    </td>                
                </tr>--%>
                        <tr>
                            <td class="label" style="vertical-align: top;">Category:</td>
                            <td class="tdSpacer" />
                            <td style="text-align: left;">
                                <asp:DropDownList ID="TaskCategoriesDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">Title:</td>
                            <td class="tdSpacer" />
                            <td style="text-align: left;">
                                <asp:TextBox ID="txbName" Width="200px" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="InputFormRequiredFieldValidator1" runat="server"
                                    ControlToValidate="txbName" ErrorMessage="Please enter the title." />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">Family Name:</td>
                            <td class="tdSpacer" />
                            <td style="text-align: left;">
                                <asp:TextBox ID="FamilyNameTextBox" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">Start Date:</td>
                            <td class="tdSpacer" />
                            <td style="text-align: left;">
                                <%--<sharepoint:datetimecontrol id="dtStartDate_old" isrequiredfield="true" runat="server" dateonly="True" />--%>

                                <asp:TextBox ClientIDMode="Static" ID="dtStartDate" runat="server" CssClass="calendarImg" Width="100px"></asp:TextBox>
                                <asp:ImageButton runat="Server" ID="imgStartDate" ImageUrl="~/Layouts/Shares/Images/calendar.gif" BorderStyle="None"
                                    AlternateText="Click to show calendar" TabIndex="300" Style="cursor: pointer;" Width="16px" />
                                <ajaxToolkit:CalendarExtender ID="calExtStartDate" runat="server" TargetControlID="dtStartDate" PopupButtonID="imgStartDate" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">Due Date:</td>
                            <td class="tdSpacer" />
                            <td style="text-align: left;">
                                <%--<sharepoint:datetimecontrol id="dtDueDate" isrequiredfield="true" runat="server" dateonly="True" />--%>

                                <asp:TextBox ClientIDMode="Static" ID="dtDueDate" runat="server" CssClass="calendarImg" Width="100px"></asp:TextBox>
                                <asp:ImageButton runat="Server" ID="ImgDueDate" ImageUrl="~/Layouts/Shares/Images/calendar.gif" BorderStyle="None"
                                    AlternateText="Click to show calendar" TabIndex="300" Style="cursor: pointer;" Width="16px" />
                                <ajaxToolkit:CalendarExtender ID="calExtDueDate" runat="server" TargetControlID="dtDueDate" PopupButtonID="imgDueDate" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">Status:</td>
                            <td class="tdSpacer" />
                            <td style="text-align: left;">
                                <asp:DropDownList runat="server" ID="ddlStatus" Width="98%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="vertical-align: top;">Additional Recipient Employees:</td>
                            <td class="tdSpacer" />
                            <td style="text-align: left;">
                                <asp:ListBox ID="AdditionalRecipientsListBox" runat="server" Style="min-width: 150px;" SelectionMode="Multiple" Rows="8" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="vertical-align: top;">Additional Recipient Groups:</td>
                            <td class="tdSpacer" />
                            <td style="text-align: left;">
                                <asp:ListBox ID="AdditionalRecipientGroupsListBox" runat="server" Style="min-width: 150px;" SelectionMode="Multiple" Rows="8" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="vertical-align: top;">Content:</td>
                            <td class="tdSpacer" />
                            <td style="text-align: left;">
                                <asp:TextBox Width="98%" ID="txbContent" runat="server" TextMode="MultiLine" Rows="8"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnSubmit" runat="server" Text="Save" OnClick="btnSubmit_Click" Width="80px" />
                    &nbsp;<input id="Cancel" type="button" value="Cancel" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel)" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Edit Task
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Edit Task
</asp:Content>
