﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StaffList.aspx.cs" Inherits="SafeSplash.PortalSite.Layouts.SS.StaffList" MasterPageFile="~/SafeSplash.Master" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="SafeSplashControls" Namespace="SafeSplash.ObjectModel.Controls.GridViewControl" Assembly="SafeSplash.ObjectModel" %> <%--Assembly="SafeSplash.ObjectModel, Version=1.0.0.0, Culture=neutral, PublicKeyToken=cdcfd765862d9189" %>--%>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Employee Management
</asp:Content>
<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Employee Management
</asp:Content>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    
    <link type="text/css" href="../../Layouts/Shares/css/igoogle-classic.css" rel="stylesheet" />
    <script type="text/javascript">
        var _currentRowKey = null;
        var _selectedRows = null;
        var _selectedRowsCount = 0;
        function createAccount()
        {
            var address = 'CreateAccount.aspx';
            window.location = address;

            //var url =  "CreateAccount.aspx";
            //ShowAccountDialog (url);
        }

        function switchSearchPane(e) {
            if (e.value == "Search") {
                document.getElementById("SearchPane").style.display = 'block';
                e.value = "Hide Search Pane";
            }
            else {
                document.getElementById("SearchPane").style.display = 'none';
                e.value = "Search";
            }
        }

        function ShowAccountDialog(pageToLoad) {
            //var options = {
            //    url: pageToLoad,
            //    title: 'Add/Edit Account(s)',
            //    showClose: true,
            //    width: 560,
            //    height: 400,
            //    allowMaximize: false,
            //    dialogReturnValueCallback: NotifyCallBack
            //};

            var newOptions = {
                title: 'Add/Edit Account(s)',
                showClose: true,
                width: 560,
                height: 400,
                allowMaximize: false,
                dialogReturnValueCallback: NotifyCallBack
            };
            // SP.UI.ModalDialog.showModalDialog(options);
            
            //window.showModalDialog('CreateAccount.aspx', '', newOptions);
            window.showModalDialog('CreateAccount.aspx', '', '');
        }

        function NotifyCallBack(result, value) {
            if (result == SP.UI.DialogResult.OK) {
                SP.UI.Notify.addNotification('New Employee Account is Created Successfully');
                __doPostBack('__Page', '{\"id\":\"ReloadGrid\"}');
            }
            else if (result == SP.UI.DialogResult.cancel) {
                SP.UI.Notify.addNotification('Operation is Cancelled');
            }
        }

        function staffEdit() {
            if (_currentRowKey != null) {
                var address = 'StaffEdit.aspx?ID=' + _currentRowKey;
                window.location = address;
            }
        }
        function StaffDelete() {
            var url = "StaffDelete.aspx?ID=" + _selectedRows;
            window.location = url;
            // ShowDeleteDialog(url);
        }
        function ShowDeleteDialog(pageToLoad) {
            var options = {
                url: pageToLoad,
                title: 'Delete Items',
                showClose: true,
                width: 400,
                height: 150,
                allowMaximize: false,
                dialogReturnValueCallback: DeleteNotifyCallBack
            };

            SP.UI.ModalDialog.showModalDialog(options);
        }
        function DeleteNotifyCallBack(result, value) {
            if (result == SP.UI.DialogResult.OK) {
                SP.UI.Notify.addNotification('Deleting Operation is done successfully');
                __doPostBack('__Page', '{\"id\":\"ReloadGrid\"}');
            }
            else if (result == SP.UI.DialogResult.cancel) {
                SP.UI.Notify.addNotification('Operation is Cancelled');
            }
        }
        function EnableDelete() {
            return _selectedRowsCount > 0;
        }
        function EnableDisabled() {
            if (_currentRowKey == null) {
                return true;
            }
            else {
                return false;
            }
        }
        function onGridViewRowSelected(rowKey) {
            _currentRowKey = rowKey;
            if (rowKey == null) {
                document.getElementById("btnEditAccount").disabled = true;
            }
            else {
                document.getElementById("btnEditAccount").disabled = false;
            }
        }

        function onCheckedChanged(rowKey, checked) {
            if (_selectedRows == null) {
                _selectedRows = new Array();
            }
            if (checked) {
                if (!txbcontains(_selectedRows, rowKey)) {
                    _selectedRows.push(rowKey);
                    ++_selectedRowsCount;
                }
            }
            else {
                if (txbcontains(_selectedRows, rowKey)) {
                    for (var i = 0; i < _selectedRows.length; i++) {
                        if (_selectedRows[i] == rowKey) {
                            _selectedRows.splice(i, 1);
                            break;
                        }
                    }
                    --_selectedRowsCount;
                }
            }
            if (_selectedRowsCount > 0) {
                document.getElementById("btnDeleteAccount").disabled = false;
            }
            else {
                document.getElementById("btnDeleteAccount").disabled = true;
            }
        }

        function txbcontains(a, obj) {
            var i = _selectedRows.length;
            while (i--) {
                if (_selectedRows[i] === obj) {
                    return true;
                }
            }
            return false;
        }
    </script>
</asp:Content>


<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <link type="text/css" href="../../Layouts/Shares/css/igoogle-classic.css" rel="stylesheet" />

    <div>
        <table style="width: 100%">
            <tr>
                <td>
                    <table style="width: 100%">
                        <tr>
                            <td style="font-size: large">Employee Management</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right" valign="bottom">
                                <input id="btnCreateAccount" name="btnCreateAccount" type="button" value="Create Account" onclick="createAccount();" style="border-style: outset; border-width: 1px; background-color: #003399; color: #FFFFFF;" />
                                <input id="btnEditAccount" name="btnEditAccount" type="button" value="Edit Selected Staff" onclick="staffEdit();" disabled="disabled" style="border-style: outset; border-width: 1px; background-color: #003399; color: #FFFFFF;" />
                                <input id="btnDeleteAccount" name="btnDeleteAccount" type="button" value="Delete Selected Staff" onclick="StaffDelete();" disabled="disabled" style="border-style: outset; border-width: 1px; background-color: #003399; color: #FFFFFF;" />
                                <input id="btnShowSearch" type="button" value="Search" onclick="switchSearchPane(this);" style="border-style: outset; border-width: 1px; background-color: #003399; color: #FFFFFF;" />
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <div id="SearchPane" style="background-color: #EFE8FF; text-align: left; display: none;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 80px; height: 15px;" align="right">First Name:</td>
                                <td style="height: 15px">
                                    <asp:TextBox runat="server" ID="txbFirstName" Width="100%">
                                    </asp:TextBox>
                                </td>
                                <td style="width: 80px; height: 15px;" align="right">Last Name:</td>
                                <td style="height: 15px">
                                    <asp:TextBox runat="server" ID="txbLastName" Width="100%">
                                    </asp:TextBox>
                                </td>
                                <td style="width: 80px; height: 15px;" align="right">Email:</td>
                                <td style="height: 15px">
                                    <asp:TextBox runat="server" ID="txbEmail" Width="100%"></asp:TextBox></td>
                                <td style="width: 100px; height: 15px;" align="right">Phone Number:</td>
                                <td style="height: 15px">
                                    <asp:TextBox runat="server" ID="txbPhoneNumber" Width="100%"></asp:TextBox></td>
                                <td style="width: 80px; height: 15px;" align="center">
                                    <asp:Button runat="server" Text="Search" OnClick="btnSearch_Click" ID="btnSearch"></asp:Button></td>
                            </tr>
                        </table>
                    </div>
                    <asp:GridView ID="staffGrid" runat="server" GridLines="Horizontal" CssClass="igoogle igoogle-classic" Width="100%" DataSourceID="StaffDataSource" AutoGenerateColumns="false"
                        OnRowDataBound="Grid_RowDataBound" DataKeyNames="empl_ID" AllowPaging="true" PageSize="30"
                        OnPreRender="Grid_PreRender">
                        <RowStyle CssClass="data-row" />
                        <AlternatingRowStyle CssClass="alt-data-row" />
                        <HeaderStyle CssClass="header-row" HorizontalAlign="Left" Height="25px" />
                        <RowStyle Height="25px" />
                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last" Position="Bottom" PageButtonCount="10" />
                        <PagerStyle HorizontalAlign="right" />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <input name="cxbControl" onclick="onCheckedChanged('<%# DataBinder.Eval(Container.DataItem, "empl_ID") %>    ', this.checked)" type="checkbox" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name" SortExpression="empl_FirstName">
                                <ItemTemplate>
                                    <a href="StaffDetails.aspx?ID=<%# DataBinder.Eval(Container.DataItem, "empl_ID") %>">
                                        <%# DataBinder.Eval(Container.DataItem, "empl_FirstName")%>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Last Name" SortExpression="empl_LastName" DataField="empl_LastName" />
                            <asp:BoundField HeaderText="E-mail" SortExpression="empl_Email" DataField="empl_Email" />
                            <asp:TemplateField HeaderText="Account Type">
                                <ItemTemplate>
                                    <asp:Label ID="lblAccountType" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Staff Type" DataField="empl_Type" />
                        </Columns>
                    </asp:GridView>

                <%--    <SafeSplashControls:GridViewControlExtender ID="gridviewEnxtender"
                        runat="server" TargetControlID="staffGrid"
                        RowHoverCssClass="row-over" RowSelectCssClass="row-select" />--%>

                    <%--<asp:ObjectDataSource runat="server" ID="StaffDataSource" TypeName="SafeSplash.PortalSite.Layouts.SS.StaffList, $SharePoint.Project.AssemblyFullName$"
                        SelectMethod="GetAllStaff"></asp:ObjectDataSource>--%>
                    <asp:ObjectDataSource runat="server" ID="StaffDataSource" TypeName="SafeSplash.PortalSite.Layouts.SS.StaffList"
                        SelectMethod="GetAllStaff"></asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

</asp:Content>
