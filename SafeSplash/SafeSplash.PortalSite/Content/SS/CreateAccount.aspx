﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateAccount.aspx.cs" Inherits="SafeSplash.PortalSite.Layouts.SS.CreateAccount" MasterPageFile="~/SafeSplash.Master" %>


<%--<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>--%>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%--<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>--%>


<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <div>
		<table style="width: 100%">
			<tr>
				<td style="text-align: center;">
				<table style="border: 1px double #9AC6FF; width: 500px" cellpadding="0" cellspacing="0">
                     
                    <tr>
						<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="style1">
						Login ID:&nbsp;</td>
						<td style="text-align: left;">
						<asp:TextBox runat="server" id="txbLoginID">
						</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txbLoginID"
                        runat="server" ErrorMessage="RequiredFieldValidator">Please input the LoginID.</asp:RequiredFieldValidator>
						</td>
					</tr>
					<tr>
						<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="style1">
						Account Type:&nbsp;</td>
						<td style="text-align: left;">
						<asp:DropDownList runat="server" id="ddlAccountType" Width="200px">
                           
						</asp:DropDownList>
						&nbsp;</td>
					</tr>
					<tr>
						<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="style1">
						First Name:&nbsp;</td>
						<td style="text-align: left;">
						<asp:TextBox runat="server" id="txbFirstName">
						</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txbFirstName"
                        runat="server" ErrorMessage="RequiredFieldValidator">Please input the frist name.</asp:RequiredFieldValidator>
						</td>
					</tr>
					<tr>
						<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="style1">
						Last Name:&nbsp;</td>
						<td style="text-align: left;">
						<asp:TextBox runat="server" id="txbLastName">
						</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txbLastName"
                        runat="server" ErrorMessage="RequiredFieldValidator">Please input the last name.</asp:RequiredFieldValidator>
						</td>
					</tr>
					<tr>
						<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="style1">
						Email Address:&nbsp;</td>
						<td style="text-align: left;">
						<asp:TextBox runat="server" id="txbEmail" Width="250px">
						</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txbEmail"
                        runat="server" ErrorMessage="RequiredFieldValidator">Please input the e-mail address.</asp:RequiredFieldValidator>
						</td>
					</tr>
                    <tr>
						<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="style1">
						Location:&nbsp;</td>
						<td style="text-align: left;">
                        <asp:ListBox runat="server" id="ddlLocations" Width="200px" SelectionMode="Multiple" Rows="4">
						</asp:ListBox>
						</td>
					</tr>
                    <tr id="PrimaryInstructorSegmenRow" runat="server">
						<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="style1">
						Primary Instructor Segment:&nbsp;</td>
						<td style="text-align: left;">
                        <asp:DropDownList runat="server" id="ddlPrimaryInstructorSegment" Width="200px">
						</asp:DropDownList>
						</td>
					</tr>
                    <tr id="SecondaryInstructorSegmentRow" runat="server">
						<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="style1">
						Secondary Instructor Segment:&nbsp;</td>
						<td style="text-align: left;">
                        <asp:DropDownList runat="server" id="ddlSecondaryInstructorSegment" Width="200px">
						</asp:DropDownList>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;<em>* Temporary password: <asp:Label runat="server" ID="lblTempPwd"></asp:Label></em></td>
			</tr>
			<tr style="display: none;">
				<td>
				<asp:CheckBox runat="server" id="cxbEmailInstru" Text="Email instructions" Font-Bold="True" Checked="True" /></td>
			</tr>
			<tr>
				<td>
                &nbsp;<asp:Label ForeColor="Red" runat="server" ID="errorMsg" Visible="false"></asp:Label>
                </td>
			</tr>
			<tr>
				<td>
                    <asp:Button runat="server" Text="Create" id="Button1" Width="80px" OnClick="btnSubmit_Click"/>
				    &nbsp;<input id="Cancel" type="button" value="Cancel" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel)"/>
				</td>
			</tr>
		</table>
	</div>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
Create New Account
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
Create New Account
</asp:Content>
