﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
//using Microsoft.SharePoint;
using System.Web.UI;
//using Microsoft.SharePoint.WebControls;
using AKMII.SafeSplash.BLL;
using AKMII.SafeSplash.Entity;
using SafeSplash.ObjectModel.Class;
using SafeSplash.PortalSite.ControlTemplates.SS;
using SafeSplash.DataAccess.InstructorSegment;
using System.Web.UI.WebControls;
using System.Linq;
using SafeSplash.DataAccess.NHibernate;

namespace SafeSplash.PortalSite.Layouts.SS
{
    public partial class StaffEdit : Page, IPostBackEventHandler //  SSBasePage
    {
        private string employeeID
        {
            get
            {
                if (ViewState["ID"] == null)
                {
                    return String.Empty;
                }
                else
                {
                    return ViewState["ID"].ToString();
                }
            }
            set
            {
                ViewState["ID"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Context.User.IsInRole(ObjectModel.Constants.RoleCorpAdmin) && !Context.User.IsInRole(ObjectModel.Constants.RoleFacilityManager)) 
            {
                ObjectModel.Class.GenericHelper.pageRedirect(this.Page, "/_layouts/ss/accessdenied.aspx");
            }
                        
            if (!IsPostBack)
            {
                employeeID = Request["ID"];
                InitControls();
                LoadInstructorSegments();
                LoadReports();
                LoadEmployeeInfo();
                LoadAccountInfo();
                RegisterScripts();
            }
        }

        private void RegisterScripts()
        {            
            //string serverRelativeUrl = Context.Current.Web.ServerRelativeUrl;
            StringBuilder sbScripts = new StringBuilder();
            sbScripts.AppendLine("function addCertification() {");
            sbScripts.AppendLine("   var url = \"AddCertification.aspx?userID=" + employeeID + "\";");
            sbScripts.AppendLine("   ShowCertificationDialog(url);}");
            sbScripts.AppendLine();
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClientCommand", sbScripts.ToString(), true);
        }

        private void InitControls()
        {
            StateBll stateBll = new StateBll();
            this.ddlState.DataValueField = "state_Code";
            this.ddlState.DataTextField = "state_Name";
            this.ddlState.DataSource = stateBll.GetAll();
            this.ddlState.DataBind();
            this.ddlState.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", ""));

            LocationBll locationBll = new LocationBll();
            List<AKMII.SafeSplash.EntityNew.tb_Location> allLocations = locationBll.GetAll();
            this.ddlLocation.DataValueField = "loca_ID";
            this.ddlLocation.DataTextField = "loca_Name";
            this.ddlLocation.DataSource = allLocations;
            this.ddlLocation.DataBind();

            this.ddlReportLocations.DataValueField = "loca_ID";
            this.ddlReportLocations.DataTextField = "loca_Name";
            this.ddlReportLocations.DataSource = allLocations;
            this.ddlReportLocations.DataBind();

            CertificationControl certControl = certList as CertificationControl;
            certControl.UserID = employeeID;

            ControlInitialization.DDL_Month(this.ddlDOBMonth, true);
            ControlInitialization.DDL_Day(this.ddlDOBDay, true);
            ControlInitialization.DDL_EmployeeYear(this.ddlDOBYear, true);

            ControlInitialization.DDL_Gender(this.ddlSex, true);
        }

        private void LoadEmployeeInfo()
        {
            if (employeeID != null)
            {
                tb_Employee employee;
                EmployeeBll employeeBll = new EmployeeBll();
                try
                {
                    employee = employeeBll.GetByID(new Guid(employeeID));
                }
                catch
                {
                    return;
                }
                if (employee != null)
                {
                    if (!String.IsNullOrEmpty(employee.empl_Picture))
                    {
                        string imageUrl = GenericHelper.GetAppSettings(Constant.ImageLibrary_EmployeeProfile);
                       // imageUrl = SPContext.Current.Web.Url + "/" + imageUrl + "/";
                        this.employeeImage.ImageUrl = imageUrl += employee.empl_Picture;
                    }
                    else
                    {
//                         this.employeeImage.ImageUrl = SPContext.Current.Web.Url + Constant.SP_EmployeeProfile_DefaultPic;
                        this.employeeImage.ImageUrl = Constant.SP_EmployeeProfile_DefaultPic;
                    }
                    lblEmployeeName.Text = employee.empl_FirstName + " " + employee.empl_LastName;

                    txbFirstName.Text = employee.empl_FirstName;
                    txbLastName.Text = employee.empl_LastName;
                    EmployeeLocationBll employeeLocationBll = new EmployeeLocationBll();
                    List<AKMII.SafeSplash.EntityNew.tb_Location> locationList = employeeLocationBll.GetLocationsByEmplID(employee.empl_ID);
                    if (locationList != null)
                    {
                        foreach (AKMII.SafeSplash.EntityNew.tb_Location location in locationList)
                        {
                            for (int i = 0; i < ddlLocation.Items.Count; i++)
                            {
                                if (ddlLocation.Items[i].Value == location.loca_ID.ToString())
                                {
                                    ddlLocation.Items[i].Selected = true;
                                    break;
                                }
                            }
                        }
                    }
                    IReportsDataAccess reportsRepository = new ReportsDataAccess();
                    IEnumerable<ReportUser> userReports = reportsRepository.GetReportsForEmployee(employee.empl_ID);
                    IEnumerable<ReportUserLocation> userReportLocations = reportsRepository.GetReportUserLocations(employee.empl_ID);
                    int itemIndex = 0;
                    foreach (var userReport in ddlReports.Items)
                    {
                        int reportId = int.Parse(((ListItem)userReport).Value);
                        ddlReports.Items[itemIndex].Selected = userReports.Where(ur => ur.ReportType.Id == reportId).Count() > 0;
                        itemIndex++;
                    }
                    itemIndex = 0;
                    foreach (var userReportLocation in ddlReportLocations.Items)
                    {
                        Guid reportLocationId = new Guid(((ListItem)userReportLocation).Value);
                        ddlReportLocations.Items[itemIndex].Selected = userReportLocations.Where(ur => ur.LocationId == reportLocationId).Count() > 0;
                        itemIndex++;
                    }

                    this.txbNotes.Text = employee.empl_Notes;
                    GenericHelper.DDLSelectedValue(ddlStaffCode, employee.empl_Type);
                    if(employee.empl_PrimaryInstructorSegment_ID.HasValue)
                    {
                        ddlPrimaryInstructorSegment.SelectedValue = employee.empl_PrimaryInstructorSegment_ID.Value.ToString();
                    }
                    if(employee.empl_SecondaryInstructorSegment_ID.HasValue)
                    {
                        ddlSecondaryInstructorSegment.SelectedValue = employee.empl_SecondaryInstructorSegment_ID.Value.ToString();
                    }
                    this.txbTitle.Text = employee.empl_Title;
                    if (employee.empl_Gender != null)
                    {
                        this.ddlSex.SelectedValue = (bool)employee.empl_Gender ? "Male" : "Female";
                    }
                    if (employee.empl_DOB != null)
                    {
                        DateTime dtEmplDob = (DateTime)employee.empl_DOB;
                        this.ddlDOBDay.SelectedValue = dtEmplDob.Day.ToString();
                        this.ddlDOBMonth.SelectedValue = dtEmplDob.Month.ToString();
                        this.ddlDOBYear.SelectedValue = dtEmplDob.Year.ToString();
                        int iAge = DateTime.Today.Year - ((DateTime)employee.empl_DOB).Year;
                        if (new DateTime(DateTime.Today.Year, ((DateTime)employee.empl_DOB).Month, ((DateTime)employee.empl_DOB).Day).CompareTo(DateTime.Today) > 0)
                        {
                            iAge = iAge - 1;
                        }
                        this.txbAge.Text = iAge.ToString();
                    }
                    this.txbAddress1.Text = employee.empl_Address;
                    this.txbAddress2.Text = employee.empl_Address2;
                    this.txbCity.Text = employee.empl_City;
                    GenericHelper.DDLSelectedValue(this.ddlState, employee.empl_Sate);
                    this.txbZip.Text = employee.empl_Zip;
                    this.txbHomePhone.Text = employee.empl_HomePhone;
                    this.txbWorkPhone.Text = employee.empl_WorkPhone;
                    this.txbEmail.Text = employee.empl_Email;

                }
            }
        }

        private void LoadAccountInfo()
        {
            EmployeeBll employeeBll = new EmployeeBll();
            tb_Employee employee = employeeBll.GetByID(new Guid(this.employeeID));
            if (employee.user_ID != null)
            {
                Guid userID = (Guid)employee.user_ID;
                UserBll userBll = new UserBll();
                vw_GetUserInfoByUserId UserInfo = userBll.GetUserInfoByUserID(userID);
                if (UserInfo != null)
                {
                    this.lblAccountID.Text = UserInfo.UserName;
                    this.lblAccountType.Text = UserInfo.RoleName;
                    this.lblStatus.Text = UserInfo.IsApproved ? "Active" : "Inactive";
                    if (UserInfo.UserName.ToUpper() == "FBAADMIN")
                    {
                        btnChangeAccount.Disabled = true;
                    }
                }
            }
        }

        private void LoadInstructorSegments()
        {
            IInstructorSegmentDataAccess repository = new InstructorSegmentDataAccess();
            //IInstructorSegmentDataAccess repository = new SafeSplash.DataAccess.NHibernate.InstructorSegmentDataAccess();
            IEnumerable<InstructorSegmentDataObject> segments = repository.GetAllInstructorSegments().OrderBy(s => s.Number);
            this.ddlPrimaryInstructorSegment.Items.Add(new ListItem(string.Empty, string.Empty));
            this.ddlSecondaryInstructorSegment.Items.Add(new ListItem(string.Empty, string.Empty));
            foreach (var segment in segments)
            {
                this.ddlPrimaryInstructorSegment.Items.Add(new ListItem(segment.Name, segment.Id.ToString()));
                this.ddlSecondaryInstructorSegment.Items.Add(new ListItem(segment.Name, segment.Id.ToString()));
            }
        }

        private void LoadReports()
        {
            IReportsDataAccess reportsRepository = new ReportsDataAccess();
            List<int> reportIds = new List<int>() { 15, 16 };
            List<ReportType> reports = reportsRepository.GetAllReportTypes().Where(r => reportIds.Contains(r.Id)).OrderBy(r => r.SortOrder).ToList<ReportType>();
            ddlReports.DataTextField = "Name";
            ddlReports.DataValueField = "Id";
            ddlReports.DataSource = reports;
            ddlReports.DataBind();
        }

        public void btnSubmit_Click(object sender, EventArgs e)
        {
            
            if (!String.IsNullOrEmpty(employeeID))
            {
                tb_Employee employee;
                EmployeeBll employeeBll = new EmployeeBll();
                try
                {
                    employee = employeeBll.GetByID(new Guid(employeeID));
                }
                catch
                {
                    return;
                }
                if (imageUpload.PostedFile != null && imageUpload.HasFile)
                {
                    /// RAB
                    //SPWeb web = SPContext.Current.Web;
                    //// upload pictures
                    //SPList imgLibrary = web.Lists[GenericHelper.GetAppSettings(Constant.ImageLibrary_EmployeeProfile)];
                    //// check if the folder for product series exists;
                    //SPFolder folder = imgLibrary.RootFolder;
                    //if (folder.Exists)
                    //{
                    //    Stream picStream = imageUpload.PostedFile.InputStream;
                    //    byte[] picContent = new byte[picStream.Length];
                    //    picStream.Read(picContent, 0, picContent.Length);
                    //    picStream.Close();
                    //    string extensionName = imageUpload.FileName.Substring(imageUpload.FileName.LastIndexOf('.'));
                    //    string fileName = this.lblAccountID.Text + extensionName;
                    //    string destFileUrl = folder.Url + "/" + fileName;
                    //    web.AllowUnsafeUpdates = true;
                    //    SPFile file = web.Files.Add(destFileUrl, picContent, true);
                    //    web.AllowUnsafeUpdates = false;
                    //    employee.empl_Picture = fileName;
                    //}
                }
                employee.empl_FirstName = txbFirstName.Text;
                employee.empl_LastName = txbLastName.Text;
                EmployeeLocationBll employeeLocationBll = new EmployeeLocationBll();
                List<tb_EmployeeLocation> locationList = employeeLocationBll.GetByEmplID(employee.empl_ID);
                for(int i = 0; i < ddlLocation.Items.Count; i ++)
                {
                    if (!ddlLocation.Items[i].Selected)
                    {
                        foreach (tb_EmployeeLocation emplLoca in locationList)
                        {
                            if (ddlLocation.Items[i].Value == emplLoca.loca_ID.ToString())
                            {
                                employeeLocationBll.Delete (emplLoca);
                                break;
                            }
                        }
                    }
                    else
                    {
                        bool existed = false;
                        foreach (tb_EmployeeLocation emplLoca in locationList)
                        {
                            if (ddlLocation.Items[i].Value == emplLoca.loca_ID.ToString())
                            {
                                existed = true;
                                break;
                            }
                        }
                        if (! existed)
                        {
                            tb_EmployeeLocation newEmplLoca = new tb_EmployeeLocation();
                            newEmplLoca.emploca_ID = Guid.NewGuid();
                            newEmplLoca.empl_ID = employee.empl_ID;
                            newEmplLoca.loca_ID = new Guid(ddlLocation.Items[i].Value);
                            //RABemployeeLocationBll.Add(newEmplLoca);
                        }
                    }
                }
                employee.empl_Type = this.ddlStaffCode.SelectedValue;
                if (!string.IsNullOrEmpty(this.ddlPrimaryInstructorSegment.SelectedValue))
                {
                    employee.empl_PrimaryInstructorSegment_ID = int.Parse(this.ddlPrimaryInstructorSegment.SelectedValue);
                }
                if (!string.IsNullOrEmpty(this.ddlSecondaryInstructorSegment.SelectedValue))
                {
                    employee.empl_SecondaryInstructorSegment_ID = int.Parse(this.ddlSecondaryInstructorSegment.SelectedValue);
                }

                #region Update Report and Report Location Permissions

                List<int> selectedReportIds = new List<int>();
                foreach (var userReportItem in ddlReports.Items)
                {
                    if (((ListItem)userReportItem).Selected) { selectedReportIds.Add(int.Parse(((ListItem)userReportItem).Value)); }
                }
                List<Guid> selectedReportLocationIds = new List<Guid>();
                foreach (var userReportLocationItem in ddlReportLocations.Items)
                {
                    if (((ListItem)userReportLocationItem).Selected) { selectedReportLocationIds.Add(new Guid(((ListItem)userReportLocationItem).Value)); }
                }
                IReportsDataAccess reportsRepository = new ReportsDataAccess();
                IEnumerable<ReportUser> userReportPermissions = reportsRepository.GetReportsForEmployee(employee.empl_ID);
                List<int> existingReportPermissionsIds = userReportPermissions.Select(urp => urp.ReportType.Id).ToList<int>();
                IEnumerable<ReportUserLocation> userReportLocations = reportsRepository.GetReportUserLocations(employee.empl_ID);
                List<Guid> existingUserReportLocationIds = userReportLocations.Select(url => url.LocationId).ToList<Guid>();
                foreach (var reportId in selectedReportIds)
                {
                    if (!existingReportPermissionsIds.Contains(reportId))
                    {
                        ReportUser reportUser = new ReportUser() { ReportType = new ReportType() { Id = reportId }, EmployeeId = employee.empl_ID };
                        reportsRepository.SaveReportUser(reportUser);                             
                    }
                }
                foreach (var reportId in existingReportPermissionsIds)
                {
                    if(!selectedReportIds.Contains(reportId))
                    {
                        ReportUser reportUser = userReportPermissions.Where(urp => urp.ReportType.Id == reportId).FirstOrDefault();
                        if(reportUser != null)
                        {
                            //ReportUser reportUser = new ReportUser() { ReportType = reportType, EmployeeId = employee.empl_ID };
                            reportsRepository.DeleteReportUser(reportUser);
                        }
                    }
                }

                foreach (var userReportLocationId in selectedReportLocationIds)
                {
                    if (!existingUserReportLocationIds.Contains(userReportLocationId))
                    {
                        ReportUserLocation userReportLocation = new ReportUserLocation() { EmployeeId = employee.empl_ID, LocationId = userReportLocationId };
                        reportsRepository.SaveReportUserLocation(userReportLocation);
                    }
                }
                foreach (var userReportLocationId in existingUserReportLocationIds)
                {
                    if (!selectedReportLocationIds.Contains(userReportLocationId))
                    {
                        ReportUserLocation userLocation = userReportLocations.Where(url => url.LocationId == userReportLocationId).FirstOrDefault();
                        if (userLocation != null)
                        {
                            reportsRepository.DeleteReportUserLocation(userLocation);
                        }
                    }
                }

                #endregion

                employee.empl_Notes = this.txbNotes.Text; 
                employee.empl_Title = this.txbTitle.Text;
                employee.empl_Gender = this.ddlSex.SelectedValue.StartsWith("Male");
                if (!String.IsNullOrEmpty(this.ddlDOBDay.SelectedValue) && !String.IsNullOrEmpty (this.ddlDOBMonth.SelectedValue) && !String.IsNullOrEmpty (this.ddlDOBYear.SelectedValue))
                    employee.empl_DOB = new DateTime(Int32.Parse(this.ddlDOBYear.SelectedValue), Int32.Parse(this.ddlDOBMonth.SelectedValue), Int32.Parse(this.ddlDOBDay.SelectedValue));
                employee.empl_Address = this.txbAddress1.Text;
                employee.empl_Address2 = this.txbAddress2.Text;
                employee.empl_City = this.txbCity.Text;
                employee.empl_Sate = this.ddlState.SelectedValue;
                employee.empl_Zip = this.txbZip.Text;
                employee.empl_HomePhone = this.txbHomePhone.Text;
                employee.empl_WorkPhone = this.txbWorkPhone.Text;
                employee.empl_CellPhone = this.txbCellPhone.Text;
                employee.empl_Email = this.txbEmail.Text;
                employeeBll.Update(employee);
            }
            GenericHelper.pageRedirect(this.Page, "StaffList.aspx");
        }

        void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
        {
            RegisterScripts();
            if (eventArgument == "ReloadCertControl")
            {
                CertificationControl certControl = certList as CertificationControl;
                certControl.UserID = employeeID;
                certControl.BindData();
            }
            if (eventArgument == "ReloadAccountControl")
            {
                LoadAccountInfo();
            }
        }
    }
}
