﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StaffEdit.aspx.cs" Inherits="SafeSplash.PortalSite.Layouts.SS.StaffEdit" MasterPageFile="~/SafeSplash.Master" %>

<%--<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>--%>

<%@ Register TagPrefix="wssuc" TagName="Certification" Src="~/Content/Controls/CertificationControl.ascx"%>


<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <link type="text/css" href="/_layouts/Shares/css/igoogle-classic.css" rel="stylesheet" />
    <script type="text/javascript">
        $(function () {
            $('#tabs').tabs();
        });

        function backToList() {
            window.location = "StaffList.aspx";
        }

        function EditCert(recordID) {
            var url = "AddCertification.aspx?ID=" + recordID;
            ShowCertificationDialog(url);
        }

        function ShowCertificationDialog(pageToLoad) {
            var options = {
                url: pageToLoad,
                title: 'Add Certification',
                showClose: true,
                width: 560,
                height: 300,
                allowMaximize: false,
                dialogReturnValueCallback: NotifyCallBack
            };

            SP.UI.ModalDialog.showModalDialog(options);
        }

        function NotifyCallBack(result, value) {
            if (result == SP.UI.DialogResult.OK) {
                if (value == "AccountEdit") {
                    __doPostBack('__Page', 'ReloadAccountControl');
                }
                else if (value == "Certification") {
                    __doPostBack('__Page', 'ReloadCertControl');
                }
            }
            else if (result == SP.UI.DialogResult.cancel) {
                SP.UI.Notify.addNotification('Operation is Cancelled');
            }
        }
        function staffEdit() {
            window.location = "StaffEdit.aspx";
        }
        function editAccount() {
            var userAccount = document.getElementById("ctl00_PlaceHolderMain_lblAccountID").innerHTML;
            var url = "AccountEdit.aspx?ID=" + userAccount ;
            ShowAccountDialog(url);
        }
        function ShowAccountDialog(pageToLoad) {
            var options = {
                url: pageToLoad,
                title: 'Edit Account',
                showClose: true,
                width: 560,
                height: 300,
                allowMaximize: false,
                dialogReturnValueCallback: NotifyCallBack
            };
            SP.UI.ModalDialog.showModalDialog(options);
        }

        function resetPwd() {
            var userAccount = document.getElementById("ctl00_PlaceHolderMain_lblAccountID").innerHTML;
            var url = "ResetPassword.aspx?ID=" + userAccount;
            ShowPasswordDialog(url);
        }
        function ShowPasswordDialog(pageToLoad) {
            var options = {
                url: pageToLoad,
                title: 'Change Password',
                showClose: true,
                width: 560,
                height: 300,
                allowMaximize: false,
                dialogReturnValueCallback: NotifyCallBack
            };
            SP.UI.ModalDialog.showModalDialog(options);
        }
    </script>
</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
<div>
	<table style="width: 100%">
		<tr>
			<td  style="border: 1px double #80DBFC; background-color: #EFE8FF;"> 
			<span style="font-size: medium">
			Employee Information:</span>
            <asp:Label ID="lblEmployeeName" runat="server"></asp:Label>
			</td>
</tr>
<tr>
	<td>
    <table style="width: 100%;">
    <tr>
        <td style="vertical-align: top;">
        <table style="width: 100%">
			<tr>
				<td style="text-align: center;">
                    <asp:Image runat="server" id="employeeImage" Width="100px" ImageUrl="images/profile-default.png" />
				</td>
			</tr>
			<tr>
				<td style="text-align: right;">
				    <asp:FileUpload runat="server" id="imageUpload"/>
                </td>
			</tr>
												
		</table>
        </td>
        <td style="vertical-align: top;">
            <table style="width: 100%; border : 1px double #9AC6FF; border-top: 1px double #9AC6FF;">
	        <tr>
		        <td style="text-align: right; vertical-align: middle; background-color: #EFE8FF; height: 25px;">
			        First Name:</td>
		        <td style="text-align: left; vertical-align: middle; width: 235px">
			        <asp:TextBox runat="server" id="txbFirstName" Width="150px"></asp:TextBox></td>
		        <td style="text-align: right; vertical-align: middle; background-color: #EFE8FF; height: 25px;">
			        Last Name :</td>
		        <td style="text-align: left; vertical-align: middle; width : 235px" >
			        <asp:TextBox runat="server" id="txbLastName" Width="150px"></asp:TextBox>
		        </td>		        
	        </tr>	
            <tr>
                <td style="text-align: right; vertical-align: middle; background-color: #EFE8FF; height: 25px; width: 160px;">
			        Location:</td>
		        <td style="text-align: left;">
                    <asp:ListBox runat="server" id="ddlLocation" SelectionMode="Multiple" Rows="4" Width="200px"></asp:ListBox>
                </td>
                <td style="text-align: right; vertical-align: middle; background-color: #EFE8FF; height: 25px; width: 160px;">
			        Staff Type:</td>
		        <td style="text-align: left; vertical-align: middle; width: 235px; height: 17px;">
			        <asp:DropDownList runat="server" id="ddlStaffCode" Width="150px" Rows="4">
				        <asp:ListItem>Instructor</asp:ListItem>
				        <asp:ListItem>Front</asp:ListItem>
				        <asp:ListItem>Office</asp:ListItem>
			        </asp:DropDownList>
		        </td>
            </tr>
            <tr id="InstructorSegmentRow" runat="server">
                <td style="text-align: right; vertical-align: middle; background-color: #EFE8FF; height: 25px; width: 160px;">
			        Primary Instructor Segment:</td>
		        <td style="text-align: left;">
                    <asp:DropDownList ID="ddlPrimaryInstructorSegment" runat="server" EnableViewState="true"></asp:DropDownList>
                </td>
                <td style="text-align: right; vertical-align: middle; background-color: #EFE8FF; height: 25px; width: 160px;">
			        Secondary Instructor Segment:</td>
		        <td style="text-align: left;">
			        <asp:DropDownList ID="ddlSecondaryInstructorSegment" runat="server" EnableViewState="true"></asp:DropDownList>
		        </td>
            </tr>
             <tr id="ReportPermissionsRow" runat="server">
                <td style="text-align: right; vertical-align: top; background-color: #EFE8FF; height: 25px; width: 160px;">
			        Dashboard Reports Access:</td>	
                <td>
                <asp:ListBox runat="server" id="ddlReports" SelectionMode="Multiple" Rows="4" Width="250px"></asp:ListBox>
                </td>
                <td style="text-align: right; vertical-align: top; background-color: #EFE8FF; height: 25px; width: 160px;">
			        Dashboard Reports Locations:</td>	
                <td>
                <asp:ListBox runat="server" id="ddlReportLocations" SelectionMode="Multiple" Rows="4" Width="250px"></asp:ListBox>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; vertical-align: middle; background-color: #EFE8FF; height: 25px; width: 160px;">
			        Notes:</td>	
                <td colspan="3">
                <asp:TextBox ID="txbNotes" runat="server" TextMode="MultiLine" Rows="5" Width="90%"></asp:TextBox>
                </td>
            </tr>
        </table>
        </td>
    </tr>
    </table>
	
</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
									
			<div id="tabs">
            <ul>
                <li><a href="#tabs-Personal">
                    <asp:Label ID="Literal23" runat="server" Text="Personal Info" /></a></li>
                <li><a href="#tabs-Contact">
                    <asp:Label ID="Literal17" runat="server" Text="Contact" /></a></li>
                <li><a href="#tabs-Security">
                    <asp:Label ID="Literal21" runat="server" Text="Security" /></a></li>
               <li><a href="#tabs-Skills">
                    <asp:Label ID="Literal22" runat="server" Text="Skills/Qualifications" /></a></li>
            </ul>
            <div id="tabs-Personal">
                <table style="width: 100%;">
                    <tr>
                        <td style="height: 150px">
                            <table style="border: 1px double #9AC6FF; width: 500px" cellpadding="0" cellspacing="0">
							    <tr>
								    <td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
								    Title:&nbsp;</td>
								    <td align="left">
                                    <asp:TextBox runat="server" id="txbTitle" Width="240px">
								    </asp:TextBox>
								    &nbsp;</td>
							    </tr>
							    <tr>
								    <td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
								    Gender:&nbsp;</td>
								    <td style="text-align: left;">
								    <asp:DropDownList runat="server" id="ddlSex">
								    </asp:DropDownList>
								    </td>
							    </tr>
							    <tr>
								    <td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
								    Date of Birth:&nbsp;</td>
                                    <td style="text-align: left;">
                                    <table style="width:100%">
                                        <tr>
                                            <td>
                                            <asp:DropDownList runat="server" ID="ddlDOBMonth">
                                            </asp:DropDownList> / 
                                            </td>
                                            <td>
                                            <asp:DropDownList runat="server" ID="ddlDOBDay">
                                            </asp:DropDownList> / 
                                            </td>
                                            <td>
                                            <asp:DropDownList runat="server" ID="ddlDOBYear">
                                            </asp:DropDownList> 
                                            </td>
                                        </tr>
                                    </table>
								    </td>
							    </tr>
							    <tr>
								    <td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
								    Age:&nbsp;</td>
								    <td align="left">
								    <asp:TextBox runat="server" id="txbAge" Enabled = "false" Width="80px">
								    </asp:TextBox>
                                    </td>
							    </tr>
						    </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="tabs-Contact">
				<table>
                    <tr>
                        <td style="height: 220px">
                            <table style="border: 1px double #9AC6FF; width: 500px">
								<tr>
									<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
									Address 1:&nbsp;</td>
									<td style="text-align: left;" colspan="3">
									<asp:TextBox runat="server" id="txbAddress1" Width="280px" TextMode="MultiLine">
									</asp:TextBox>
									&nbsp;</td>
								</tr>
								<tr>
									<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
									Address 2:&nbsp;</td>
									<td align="left" colspan="3">
									<asp:TextBox runat="server" id="txbAddress2" Width="280px" TextMode="MultiLine">
									</asp:TextBox>
									&nbsp;</td>
								</tr>
								<tr>
									<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
									City:&nbsp;</td>
									<td align="left" colspan="3">
									<asp:TextBox runat="server" id="txbCity" Width="187px">
									</asp:TextBox>
									</td>
								</tr>
								<tr>
									<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
									State:&nbsp;</td>
									<td align="left">
									<asp:DropDownList runat="server" id="ddlState" Width="140px">
									</asp:DropDownList>
									</td>
									<td align="right" style="width: 40px; background-color: #EFE8FF;">
									Zip:</td>
									<td align="left">
									<asp:TextBox runat="server" id="txbZip" Width="106px">
									</asp:TextBox>
									</td>
								</tr>
								<tr>
									<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
									Home Phone:&nbsp;</td>
									<td align="left" colspan="3">
									<asp:TextBox runat="server" id="txbHomePhone" Width="187px">
									</asp:TextBox>
                                    <asp:regularexpressionvalidator ID="InputFormRegularExpressionValidator6" runat="server" ControlToValidate="txbHomePhone"
                            ValidationExpression="^\d{3}-\d{3}-\d{4}$" ErrorMessage="Your home phone number is not valid"/>
                            (xxx-xxx-xxxx)
									</td>
								</tr>
								<tr>
									<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
									Work Phone:</td>
									<td align="left" colspan="3">
									<asp:TextBox runat="server" id="txbWorkPhone" Width="187px">
									</asp:TextBox>
                                    <asp:regularexpressionvalidator ID="InputFormRegularExpressionValidator1" runat="server" ControlToValidate="txbWorkPhone"
                            ValidationExpression="^\d{3}-\d{3}-\d{4}$" ErrorMessage="the work phone number is not valid"/>
									</td>
								</tr>
								<tr>
									<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
									Cell Phone:</td>
									<td align="left" colspan="3">
									<asp:TextBox runat="server" id="txbCellPhone" Width="187px">
									</asp:TextBox>
                                    <asp:regularexpressionvalidator ID="InputFormRegularExpressionValidator2" runat="server" ControlToValidate="txbCellPhone"
                            ValidationExpression="^\d{3}-\d{3}-\d{4}$" ErrorMessage="the cell phone number is not valid"/>
									</td>
								</tr>
								<tr>
									<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
									Email Address:</td>
									<td align="left" colspan="3">
									<asp:TextBox runat="server" id="txbEmail" Width="187px">
									</asp:TextBox>
									</td>
								</tr>
							</table>
                        </td>
                    </tr>
                </table>            
            </div>
            <div id="tabs-Security">
            	<table>
                    <tr>
                        <td align="right">
                            <input id="btnChangeAccount" name="btnChangeAccount" type="button" value="Update Account" onclick="editAccount();" runat="server" style="border-style: outset; border-width: 1px; background-color: #003399; color: #FFFFFF;" />
                            <input id="Button1" name="btnChangePwd" type="button" value="Change Password" onclick="resetPwd();" style="border-style: outset; border-width: 1px; background-color: #003399; color: #FFFFFF;" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="border: 1px double #9AC6FF; width: 500px">
                                <tr>
									<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
									Login ID:&nbsp;</td>
									<td align="left">
									    <asp:Label runat="server" ID="lblAccountID"></asp:Label>
                                    </td>
								</tr>
                                <tr>
									<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
									Account Type:&nbsp;</td>
									<td align="left">
									    <asp:Label runat="server" ID="lblAccountType"></asp:Label>
                                    </td>
								</tr>
                                <tr>
									<td style="width: 180px; height: 25px; background-color: #EFE8FF;" class="ms-wpselectlink">
									Status:&nbsp;</td>
									<td align="left">
									    <asp:Label runat="server" ID="lblStatus"></asp:Label>
                                    </td>
								</tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="tabs-Skills">
            	<table style="border: 1px double #9AC6FF; width: 500px" cellpadding="0" cellspacing="0">
            	    <tr>
					    <td align="left" style="height: 32px">
									    
                            <table style="width: 100%">
					            <tr>
                                <td style="width: 80px">
                                    <input name="Button1" type="button" value="Add" onclick="addCertification();" />
                                </td>
                                <td align="right">
                                    <div style="background-color: #FFAA95; width: 40px; height: 15px"></div>
                                </td>
    				            <td align="right" style="width: 220px">* will expired within 3 months.</td>
					            </tr>
				            </table>
					    </td>

				    </tr>
                    <tr>
                        <td>
                            <wssuc:Certification ID="certList" runat="server"></wssuc:Certification>
                        </td>
                    </tr>
                </table>
            </div>
            </div>
            </td>
		</tr>
								
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><hr /></td>
		</tr>
		<tr>
			<td align="center">
            <asp:Button ID="btnCreate" runat="server" Text="Submit"  OnClick="btnSubmit_Click" Width="80px" />
			&nbsp;<input id="Cancel" type="button" value="Cancel" onclick="backToList();"/>
            </td>
		</tr>
	</table>
</div>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
Edit Employee Information
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
Edit Employee Information
</asp:Content>
