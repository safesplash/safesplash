﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

using AKMII.SafeSplash.BLL;
using AKMII.SafeSplash.EntityNew;



namespace SafeSplash.PortalSite.Content.SS
{
    public partial class PortUsers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Context.User.IsInRole(ObjectModel.Constants.RoleCorpAdmin) && !Context.User.IsInRole(ObjectModel.Constants.RoleFacilityManager))
            {
                ObjectModel.Class.GenericHelper.pageRedirect(this.Page, "/_layouts/ss/accessdenied.aspx");
            }
        }


        protected void PortUsers_Clicked(object sender, EventArgs e)
        {
            PortUsersBLL portBLL = new PortUsersBLL();

            // Get users from old aspnet_users table
            List<aspnet_User> oldSystemUsers = portBLL.GetAllOldSystemUsers();
            aspnet_UsersInRole oldSystemRole = null;
            List<tb_Employee> employees = portBLL.GetEmployees();
            List<tb_Client> clients = portBLL.GetClients();

            // Get existing users from aspnetusers table
            List<AspNetUser> newSystemUsers = portBLL.GetAllNewSystemUsers();

            int errorCount = 0;
            int portedCount = 0;
            int existsCount = 0;

            foreach (aspnet_User oldUser in oldSystemUsers)
            {
                AspNetUser newSystemUser = newSystemUsers.FirstOrDefault(u => u.UserName == oldUser.UserName);

                // verify old user is not already ported
                if (newSystemUser == null)
                {
                    // Create the user in their role within the new system constructs
                    var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var user = new PortalSite.Models.AspNetUser();

                    user.AccessFailedCount = 0;
                    user.Email = "";                                    // Needed for future get from employee or client
                    user.EmailConfirmed = true;
                    user.Id = oldUser.UserId.ToString();
                    user.LockoutEnabled = false;
                    user.PhoneNumber = "";                              // RAB needed for future  get from employee or client
                    user.PhoneNumberConfirmed = false;
                    user.UserName = oldUser.UserName;

                    // Old System Role
                    oldSystemRole = portBLL.GetOldUserRole(oldUser.UserId);

                    string newRoleName = string.Empty;
                    switch (oldSystemRole.aspnet_Role.RoleName)
                    {
                        case ObjectModel.Constants.RoleCorpAdmin:
                            newRoleName = ObjectModel.Constants.RoleCorpAdmin;
                            break;

                        case ObjectModel.Constants.RoleFacilityManager:
                            newRoleName = ObjectModel.Constants.RoleFacilityManager;
                            break;

                        case ObjectModel.Constants.RoleFacilityUser:
                            newRoleName = ObjectModel.Constants.RoleFacilityUser;
                            break;

                        case ObjectModel.Constants.RoleInstructor:
                            newRoleName = ObjectModel.Constants.RoleInstructor;
                            break;

                        case ObjectModel.Constants.RoleClient:
                        default:
                            newRoleName = ObjectModel.Constants.RoleClient;
                            break;
                    }

                    //user.Roles.Add(new Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole() { RoleId = newRoleName, UserId = user.Id });


                    // determine email
                    if (newRoleName == ObjectModel.Constants.RoleClient)
                    {
                        tb_Client clientData = portBLL.GetClientByUserID(oldUser.UserId);
                        if (clientData != null)
                        {
                            user.Email = clientData.client_Email;
                        }
                    }
                    else
                    {
                        tb_Employee employeeData = portBLL.GetEmployeeByUserID(oldUser.UserId);
                        if (employeeData != null)
                        {
                            user.Email = employeeData.empl_Email;
                        }
                    }
                    
                    // Create the User                    
                    if (!string.IsNullOrEmpty(user.Email))
                    {
                        IdentityResult result = manager.Create(user, "10xPeople!");
                        if (result.Succeeded)
                        {
                            // Add the new user to the proper role
                            result = manager.AddToRole(user.Id, newRoleName);

                            portedCount++;
                        }
                        else
                        {
                            string foo = "Houston we have a problem";
                            errorCount++;
                        }
                    }
                    else
                    {
                        string foo = "No Emails";
                        errorCount++;
                    }

                }
                else
                {
                    existsCount++;
                }

                // add user to new aspnetuser table

                // verify user is assigned a role
                // assign them to the proper role

            }






            //AKMII.SafeSplash.BLL.EmployeeBll employeeBll = new AKMII.SafeSplash.BLL.EmployeeBll();
            //List<AKMII.SafeSplash.EntityNew.AspNetUser> list = employeeBll.GetAllUsers();

            //var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

            //foreach (AKMII.SafeSplash.EntityNew.AspNetUser currentUser in list)
            //{
            //    if (currentUser.UserName.Equals(currentUser.UserName))
            //    {
            //        var userInfo = manager.FindByName(currentUser.UserName);
            //        if (userInfo != null)
            //        {
            //            string code = manager.GeneratePasswordResetToken(userInfo.Id);
            //            var result = manager.ResetPassword(userInfo.Id, code, "10xPeople!");
            //        }
            //    }
            //}


        }

        protected void ResetPasswords_Clicked(object sender, EventArgs e)
        {
            // foobar = ConstructorNeedsTagAttribute ToolboxDataAttribute do as conversion from old IQueryableUserStore list ToolboxDataAttribute new IQueryableUserStore list.

            AKMII.SafeSplash.BLL.EmployeeBll employeeBll = new AKMII.SafeSplash.BLL.EmployeeBll();
            List<AKMII.SafeSplash.EntityNew.AspNetUser> list = employeeBll.GetAllUsers();

            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

            foreach (AKMII.SafeSplash.EntityNew.AspNetUser currentUser in list)
            {
                if (currentUser.UserName.Equals(currentUser.UserName))
                {
                    var userInfo = manager.FindByName(currentUser.UserName);
                    if (userInfo != null)
                    {
                        string code = manager.GeneratePasswordResetToken(userInfo.Id);
                        var result = manager.ResetPassword(userInfo.Id, code, "10xPeople!");
                    }
                }
            }
        }

    }
}