﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using Microsoft.SharePoint;
using AKMII.SafeSplash.BLL;
using AKMII.SafeSplash.Entity;
using SafeSplash.ObjectModel;
using SafeSplash.Service.Task;
using System.Linq;

namespace SafeSplash.PortalSite.Layouts.SS
{
    public partial class MyTasks : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ControlTemplates.SS.MyTaskCtrl taskCtrl = this.MyTasksControl as ControlTemplates.SS.MyTaskCtrl;
            taskCtrl.TaskStartDateBeginRange = DateTime.MaxValue;
        }        
    }
}
