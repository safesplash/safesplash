﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using AKMII.SafeSplash.Entity;
using AKMII.SafeSplash.BLL;
using SafeSplash.ObjectModel.Class;
using System.Data;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using SafeSplash.PortalSite.Models;

namespace SafeSplash.PortalSite.Layouts.SS
{
    public partial class StaffList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (CurrentUserType != ObjectModel.UserType.CorpAdmin && CurrentUserType != ObjectModel.UserType.FacilityManager)
            {
                // ObjectModel.Class.GenericHelper.pageRedirect(this.Page, "/_layouts/ss/accessdenied.aspx");
            }
        }

        public DataTable GetAllStaff()
        {
            EmployeeBll employeeList = new EmployeeBll();
            //  SPGroup groupCorpAdmin = SPContext.Current.Web.Groups[ObjectModel.UserType.CorpAdmin.ToString()];
            //  SPGroup groupFacilityManager = SPContext.Current.Web.Groups[ObjectModel.UserType.FacilityManager.ToString()];
            
            if ( Context.User.IsInRole( "CorpAdmin" ) )
            {
                return GenericHelper.ToDataTable(employeeList.GetAll());
            }
            else if ( Context.User.IsInRole( "FacilityManager" ) )
            {
                UserBll userBll = new UserBll();
                Guid userID = userBll.GetUserIDByUserName( Context.User.Identity.GetUserName() );
                EmployeeBll employeeBll = new EmployeeBll();
                tb_Employee employee = employeeBll.GetByUserID(userID);
                if (employee != null)
                {
                    EmployeeLocationBll employeeLocationBll = new EmployeeLocationBll();
                    Guid[] locaIDs = employeeLocationBll.GetEmplocaID(employee.empl_ID);
                    if (locaIDs != null && locaIDs.Length > 0)
                    {
                        return GenericHelper.ToDataTable(employeeList.GetLocationEmployees(locaIDs));
                    }
                }
            }
            return null;
        }

        public void btnSearch_Click(object sender, EventArgs e)
        { }

        protected virtual void Grid_PreRender(object sender, EventArgs e)
        {
            StringBuilder strSearch = new StringBuilder();

            string searchFirstName = this.txbFirstName.Text.Trim();
            if (searchFirstName.Length > 0)
            {
                strSearch.Append("empl_FirstName LIKE '%" + searchFirstName + "%'");
                strSearch.Append(" AND ");
            }

            string searchLastName = this.txbLastName.Text.Trim();
            if (searchLastName.Length > 0)
            {
                strSearch.Append("empl_LastName LIKE '%" + searchLastName + "%'");
                strSearch.Append(" AND ");
            }

            string searchEmail = this.txbEmail.Text.Trim();
            if (searchEmail.Length > 0)
            {
                strSearch.Append("empl_Email LIKE '%" + searchEmail + "%'");
                strSearch.Append(" AND ");
            }

            string searchPhone = this.txbPhoneNumber.Text.Trim();
            if (searchPhone.Length > 0)
            {
                strSearch.Append("(empl_WorkPhone LIKE '%" + searchPhone + "%' OR empl_CellPhone LIKE '%" + searchPhone + "%' OR empl_HomePhone LIKE '%" + searchPhone + "%')");
                strSearch.Append(" AND ");
            }
            if (strSearch.Length > 0)
            {
                strSearch.Remove(strSearch.Length - 5, 5);
                StaffDataSource.FilterExpression = strSearch.ToString();
            }
            else
            {
                StaffDataSource.FilterParameters.Clear();
            }
        }

        protected virtual void Grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LocationBll locationBll = new LocationBll();
                UserBll userBll = new UserBll();
                DataRowView drData = (DataRowView)e.Row.DataItem;
                if (drData["user_ID"] != null && (!String.IsNullOrEmpty(drData["user_ID"].ToString())))
                {
                    Label lblAccountType = e.Row.FindControl("lblAccountType") as Label;
                    vw_GetUserInfoByUserId userInfo = userBll.GetUserInfoByUserID((Guid)drData["user_ID"]);
                    if (userInfo != null)
                    {
                        if (!String.IsNullOrEmpty(userInfo.RoleName))
                        {
                            switch (userInfo.RoleName)
                            {
                                case "Instructor":
                                    lblAccountType.Text = "Instructor";
                                    break;
                                case "FacilityUser":
                                    lblAccountType.Text = "Facility User";
                                    break;
                                case "FacilityManager":
                                    lblAccountType.Text = "Facility Manager";
                                    break;
                                case "CorpAdmin":
                                    lblAccountType.Text = "Corp Admin";
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                string employeeID = ((DataRowView)e.Row.DataItem)["empl_ID"].ToString();
                e.Row.Attributes.Add("onclick", String.Format("onGridViewRowSelected('{0}')", employeeID));
            }
        }

        //void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
        //{
        //    //this.StaffDataSource.Select();
        //    //this.staffGrid.DataSourceID = this.StaffDataSource;
        //    this.staffGrid.DataBind();
        //}
    }
}