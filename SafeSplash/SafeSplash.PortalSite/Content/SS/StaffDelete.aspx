﻿<%--<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>--%>
<%--<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>--%>
<%--<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StaffDelete.aspx.cs" Inherits="SafeSplash.PortalSite.Layouts.SS.StaffDelete" MasterPageFile="~/SafeSplash.Master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
<table style="width: 90%;">
    <tr style="font-family: Calibri; font-size: medium; padding: 6pt">
      <td>
          <asp:Label ID="lblErrorMsg" ForeColor="Red" runat="server"></asp:Label>
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr style="font-family: Calibri; font-size: medium; padding: 6pt">
      <td>
          <asp:Label ID="lblText" runat="server" Text="This will delete the selected employee accounts."></asp:Label>
      </td>
    </tr>
    <tr>
      <td>
          <asp:Label ID="lblConfirm" runat="server" Text="Are you sure?"></asp:Label></td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr>
      <td align ="right">
    <asp:Button runat="server" Text="Delete" id="btnSave"  onclick="btSubmit_Click" />&nbsp;&nbsp;
    <input id="Cancel" type="button" value="Cancel" onclick="SP.UI.ModalDialog.commonModalDialogClose(SP.UI.DialogResult.cancel)"/>&nbsp;
      </td>
    </tr>
  </table>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
Delete Employee
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
Delete Employee
</asp:Content>
