﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Collections.Generic;
//using Microsoft.SharePoint;
//using Microsoft.SharePoint.WebControls;
using SafeSplash.ObjectModel.Class;
using AKMII.SafeSplash.BLL;
using AKMII.SafeSplash.EntityNew;
//using SafeSplash.Service.Email;
using SafeSplash.DataAccess.InstructorSegment;
using SafeSplash.DataAccess.NHibernate;
using SafeSplash.ObjectModel;

using SafeSplash.PortalSite.Models;
using Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace SafeSplash.PortalSite.Layouts.SS
{
    public partial class CreateAccount : Page //  SSBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Context.User.IsInRole(ObjectModel.Constants.RoleCorpAdmin) && !Context.User.IsInRole(ObjectModel.Constants.RoleFacilityManager)) 
            // if (CurrentUserType != ObjectModel.UserType.CorpAdmin && CurrentUserType != ObjectModel.UserType.FacilityManager)
            {
                ObjectModel.Class.GenericHelper.pageRedirect(this.Page, "/_layouts/ss/accessdenied.aspx");
            }
            // string userName = SPContext.Current.Web.CurrentUser.Name.ToLower();
            //PrimaryInstructorSegmenRow.Visible = SecondaryInstructorSegmentRow.Visible = EmployeeBll.EmployeeAccountsWhoCanManageInstructorSegments().Contains(userName);
            errorMsg.Visible = false;
            if (!IsPostBack)
            {
                LoadLocations();
                LoadAccountTypes();
                LoadInstructorSegments();
                
                //ObjectModel.MembershipManage memberShip = new ObjectModel.MembershipManage();
                string tempPwd = "10xPeople!"; //memberShip.GeneratePassword();
                // RAB need to generate a default password.
                
                this.lblTempPwd.Text = tempPwd;
            }
        }

        private void LoadLocations()
        {
            //SPGroup groupCorpAdmin = SPContext.Current.Web.Groups[ObjectModel.UserType.CorpAdmin.ToString()];
            //SPGroup groupFacilityManager = SPContext.Current.Web.Groups[ObjectModel.UserType.FacilityManager.ToString()];

            if (Context.User.IsInRole(ObjectModel.Constants.RoleCorpAdmin) ) 
            // if (groupCorpAdmin.ContainsCurrentUser)
            {
                LocationBll locationBll = new LocationBll();
                List<tb_Location> locationList = locationBll.GetAll();
                this.ddlLocations.Items.Clear();
                foreach (tb_Location location in locationList)
                {
                    this.ddlLocations.Items.Add(new ListItem(location.loca_Name, location.loca_ID.ToString()));
                }
            }
            else if ( Context.User.IsInRole(ObjectModel.Constants.RoleFacilityManager))
            // else if (groupFacilityManager.ContainsCurrentUser)
            {
                EmployeeBll employeeBll = new EmployeeBll();
                //UserBll userBll = new UserBll();
                //Guid userID = userBll.GetUserIDByUserName(User.Identity.Name);  //SPContext.Current.Web.CurrentUser.Name);

                EmployeeLocationBll empLocations = new EmployeeLocationBll();
                AKMII.SafeSplash.Entity.tb_Employee employee = employeeBll.GetByUserID( new Guid( Context.User.Identity.GetUserId() ) );  //RAB
                List<AKMII.SafeSplash.EntityNew.tb_Location> locationList = empLocations.GetLocationsByEmplID(employee.empl_ID);
                
                this.ddlLocations.Items.Clear();
                foreach (AKMII.SafeSplash.EntityNew.tb_Location location in locationList)
                {
                    this.ddlLocations.Items.Add(new ListItem(location.loca_Name, location.loca_ID.ToString()));
                }
            }
            
        }

        private void LoadAccountTypes()
        {
            this.ddlAccountType.Items.Clear();
            //SPGroup groupCorpAdmin = SPContext.Current.Web.Groups[ObjectModel.UserType.CorpAdmin.ToString()];
            //SPGroup groupFacilityManager = SPContext.Current.Web.Groups[ObjectModel.UserType.FacilityManager.ToString()];

            if (Context.User.IsInRole(ObjectModel.Constants.RoleCorpAdmin)) 
            // if (groupCorpAdmin.ContainsCurrentUser)
            {
                ddlAccountType.Items.Add(new ListItem("Instructor", ObjectModel.Constants.RoleInstructor ));                
                ddlAccountType.Items.Add(new ListItem("Facility User", ObjectModel.Constants.RoleFacilityUser));
                ddlAccountType.Items.Add(new ListItem("Facility Manager", ObjectModel.Constants.RoleFacilityManager));
                ddlAccountType.Items.Add(new ListItem("Corp Admin", ObjectModel.Constants.RoleCorpAdmin));
            }
            else if (Context.User.IsInRole(ObjectModel.Constants.RoleFacilityManager))
            // else if (groupFacilityManager.ContainsCurrentUser)
            {
                ddlAccountType.Items.Add(new ListItem("Instructor", ObjectModel.Constants.RoleInstructor));
                ddlAccountType.Items.Add(new ListItem("Facility User", ObjectModel.Constants.RoleFacilityUser));
            }
        }

        private void LoadInstructorSegments()
        {
            IInstructorSegmentDataAccess repository = new InstructorSegmentDataAccess();
            //IInstructorSegmentDataAccess repository = new SafeSplash.DataAccess.NHibernate.InstructorSegmentDataAccess();
            IEnumerable<InstructorSegmentDataObject> segments = repository.GetAllInstructorSegments().OrderBy(s => s.Number);
            this.ddlPrimaryInstructorSegment.Items.Add(new ListItem(string.Empty, string.Empty));
            this.ddlSecondaryInstructorSegment.Items.Add(new ListItem(string.Empty, string.Empty));
            foreach (var segment in segments)
            {
                this.ddlPrimaryInstructorSegment.Items.Add(new ListItem(segment.Name, segment.Id.ToString()));
                this.ddlSecondaryInstructorSegment.Items.Add(new ListItem(segment.Name, segment.Id.ToString()));
            }
        }

        protected override void OnPreInit(EventArgs e)
        {
            /// RAB
            //this.isPopWindow = true;
            base.OnPreInit(e);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string locationIDs = "";
            for (int i = 0; i < ddlLocations.Items.Count; i++)
            {
                if (ddlLocations.Items[i].Selected)
                {
                    locationIDs += ddlLocations.Items[i].Value + ",";
                }
            }
            if (string.IsNullOrEmpty(locationIDs))
            {
                errorMsg.Text = "Please select a location.";
                errorMsg.Visible = true;
                return;
            }
            if (string.IsNullOrEmpty(ddlPrimaryInstructorSegment.SelectedValue))
            {
                errorMsg.Text = "Please select a Primary Instructor segment.";
                errorMsg.Visible = true;
                return;
            }
            string AccountID = this.txbLoginID.Text.Trim();
            string Email = this.txbEmail.Text.Trim();

            if (AccountID.Length > 0)
            {                
                UserBll userBll = new UserBll();
                if (userBll.UserExisted(AccountID))
                {
                    errorMsg.Text = "The Login ID already exists in the system.";
                    errorMsg.Visible = true;
                    return;
                }
                else
                {
                    var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var user = new PortalSite.Models.AspNetUser() { UserName = AccountID, Email = Email };

                    IdentityResult result = manager.Create(user, lblTempPwd.Text);
                    if (result.Succeeded)
                    {
                        // Add the new user to the proper role
                        result = manager.AddToRole(user.Id, ddlAccountType.SelectedValue);

                        if (result.Succeeded)
                        {
                            // Probably dont need this stuff
                            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                            //string code = manager.GenerateEmailConfirmationToken(user.Id);
                            //string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                            //manager.SendEmail(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");
                            // end dont need this stuff



                                EmployeeBll employeeBll = new EmployeeBll();
                                tb_Employee newEmployee = new tb_Employee();
                                newEmployee.empl_ID = Guid.NewGuid();
                                newEmployee.user_ID = new Guid(user.Id);
                                newEmployee.empl_FirstName = this.txbFirstName.Text;
                                newEmployee.empl_LastName = this.txbLastName.Text;
                                newEmployee.empl_Email = this.txbEmail.Text;
                                newEmployee.empl_Type = this.ddlAccountType.SelectedValue;
                                if (!string.IsNullOrEmpty(ddlPrimaryInstructorSegment.SelectedValue))
                                {
                                    newEmployee.empl_PrimaryInstructorSegment_ID = int.Parse(ddlPrimaryInstructorSegment.SelectedValue);
                                }
                                if (!string.IsNullOrEmpty(ddlSecondaryInstructorSegment.SelectedValue))
                                {
                                    newEmployee.empl_SecondaryInstructorSegment_ID = int.Parse(ddlSecondaryInstructorSegment.SelectedValue);
                                }
                                employeeBll.Add(newEmployee);
                            
                                string[] locationIDList = locationIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                List<tb_EmployeeLocation> employeeLocationList = new List<tb_EmployeeLocation>();
                                for (int i = 0; i < locationIDList.Length; i++)
                                {
                                    tb_EmployeeLocation employeeLoca = new tb_EmployeeLocation();
                                    employeeLoca.emploca_ID = Guid.NewGuid();
                                    employeeLoca.empl_ID = newEmployee.empl_ID;
                                    employeeLoca.loca_ID = new Guid(locationIDList[i]);
                                    employeeLocationList.Add(employeeLoca);
                                }
                                EmployeeLocationBll employeeLocaBll = new EmployeeLocationBll();
                                employeeLocaBll.Add(employeeLocationList);
                            
                            // RAB todo    SendMailNotification();
                        }
                        else
                        {
                            errorMsg.Text = result.Errors.FirstOrDefault();
                        }
                    }
                    else
                    {
                        errorMsg.Text = result.Errors.FirstOrDefault();
                    }
                }
            }
            GenericHelper.CloseDialog(this.Page, 1, "");
            // RAB - Need to redirect here
        }

        private bool DataValidation()
        {
            
            //Check if the LoginID is available.
            return true;
        }

        private void SendMailNotification()
        {
            /// RAB
            //IEmailService emailService = new EmailService();
            //IAutomatedEmail email = emailService.GetAutomatedEmail(AutomatedEmailType.NewEmployeeAccountNotification);
            //string mailBody = email.Content;
            //if (mailBody != null)
            //{
            //    mailBody = mailBody.Replace("[User_Name]", this.txbLoginID.Text).Replace("[Password]", lblTemPwd.Text);
            //    Mail mail = new Mail();
            //    mail.Subject = email.Subject;
            //    mail.Content = mailBody;
            //    mail.Recipients = new List<string>() { this.txbEmail.Text };
            //    if (!string.IsNullOrEmpty(email.CcRecipients))
            //    {
            //        mail.CcRecipients = email.CcRecipients.Split(';').ToList<string>();
            //    }
            //    if (!string.IsNullOrEmpty(email.BccRecipients))
            //    {
            //        mail.BccRecipients = email.BccRecipients.Split(';').ToList<string>();
            //    }
            //    mail.FromAddress = GenericHelper.GetAppSettings(Constant.AppSetting_MailAccount);
                
            //    GenericHelper.SendMail(mail);
            //}

        }
    }
}
