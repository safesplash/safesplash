﻿using System;
using System.Collections.Generic;
//using Microsoft.SharePoint;
//using Microsoft.SharePoint.WebControls;
using AKMII.SafeSplash.Entity;
using AKMII.SafeSplash.EntityNew;
using AKMII.SafeSplash.BLL;
using SafeSplash.ObjectModel.Class;
using System.Linq;
using System.Web.UI.WebControls;
using System.Text;
using SafeSplash.DataAccess.Groups;
using SafeSplash.DataAccess.Tasks;
using System.Web;
using System.Web.UI;

using Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;


namespace SafeSplash.PortalSite.Layouts.SS
{
    public partial class EditTask : Page
    {
        IEmployeeGroupsDataAccess GroupsRepository = new DataAccess.NHibernate.EmployeeGroupsRepository();
        ITaskDataAccess TaskRepository = new DataAccess.NHibernate.TaskDataAccess();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //ControlInitialization.DDL_TaskStatus(this.ddlStatus, false);
                //this.dtStartDate.SelectedDate = DateTime.Today;
                // this.dtStartDate.Text = DateTime.Today.ToString();
                calExtStartDate.SelectedDate = DateTime.Today;
                //this.dtDueDate.SelectedDate = DateTime.Today;
                // this.dtDueDate.Text = DateTime.Today.ToString();
                calExtDueDate.SelectedDate = DateTime.Today;

                LoadLocations();
                LoadCategories();
                LoadStatuses();
                LoadGroups();
                if (!String.IsNullOrEmpty(Request["ID"]))
                {
                    ViewState["ID"] = new Guid(Request["ID"]);
                    TaskBll taskBll = new TaskBll();
                   AKMII.SafeSplash.Entity.tb_Task task = taskBll.GetByID((Guid)ViewState["ID"]);
                    if (task != null)
                    {
                        LoadTaskInfo(task);
                    }
                }
            }
        }

        protected override void OnPreInit(EventArgs e)
        {
            // RAB this.isPopWindow = true;
            base.OnPreInit(e);
        }

        private void LoadLocations()
        {
            LocationBll locationBll = new LocationBll();
            List<AKMII.SafeSplash.EntityNew.tb_Location> allLocations = locationBll.GetAll();
            List<AKMII.SafeSplash.EntityNew.tb_Location> locations = new List<AKMII.SafeSplash.EntityNew.tb_Location>();
            locations.Add(new AKMII.SafeSplash.EntityNew.tb_Location() { loca_ID = Guid.Empty, loca_Name = string.Empty });
            locations.AddRange(allLocations.OrderBy(l => l.loca_Name));
            LocationsDropDownList.DataSource = locations;
            LocationsDropDownList.DataTextField = "loca_Name";
            LocationsDropDownList.DataValueField = "loca_ID";
            LocationsDropDownList.DataBind();
            LocationsDropDownList.SelectedIndex = 0;
        }

        private void LoadStatuses()
        {
            ddlStatus.Items.Add(new ListItem(string.Empty, string.Empty));
            IEnumerable<Service.Task.TaskStatus> statuses = Service.Task.TaskStatus.AllTaskStatuses;
            foreach (var status in statuses)
            {
                ddlStatus.Items.Add(new ListItem(status.Description, status.Value));
            }
            ddlStatus.SelectedIndex = 0;
        }

        private void LoadEmployeesForLocationId(Guid locationId)
        {
            EmployeeBll employeeBll = new EmployeeBll();
            List<EmployeeDisplay> employees = new List<EmployeeDisplay>();
            if (!Guid.Empty.Equals(locationId))
            {
                List<AKMII.SafeSplash.Entity.tb_Employee> employeesForLocation = employeeBll.GetByLocationID(locationId);
                List<AKMII.SafeSplash.Entity.tb_Employee> locationManagers = employeeBll.GetCurrentLocationManagers(new Guid[] { locationId });
                foreach (var emp in locationManagers)
                {
                    employees.Add(new EmployeeDisplay() { Id = emp.empl_ID, FirstName = emp.empl_FirstName, LastName = emp.empl_LastName });
                }
                List<AKMII.SafeSplash.Entity.tb_Employee> locationUsers = employeeBll.GetCurrentLocationInstructors(new Guid[] { locationId });
                foreach (var emp in locationUsers)
                {
                    if(!"Instructor".Equals(emp.empl_Type.Trim()))
                    {
                    employees.Add(new EmployeeDisplay() { Id = emp.empl_ID, FirstName = emp.empl_FirstName, LastName = emp.empl_LastName });
                    }
                }
            }
            // add corporate admins to all locations, if they are not already in the list for this location
            List<AKMII.SafeSplash.Entity.tb_Employee> admins = employeeBll.GetCorpAdmins();
            foreach (var employee in admins)
            {
                if (employees.Where(e => e.Id == employee.empl_ID).Count() < 1)
                {
                    employees.Add(new EmployeeDisplay() { Id = employee.empl_ID, FirstName = employee.empl_FirstName, LastName = employee.empl_LastName });
                }
            }
            BindEmployeeLists(employees);
        }

        private void LoadGroups()
        {
            IEnumerable<EmployeeGroupDataObject> allGroups = GroupsRepository.GetAllEmployeeGroups().OrderBy(g => g.Name);
            //AssignToGroupListBox.DataSource = allGroups;
            //AssignToGroupListBox.DataValueField = "Id";
            //AssignToGroupListBox.DataTextField = "Name";
            //AssignToGroupListBox.DataBind();

            AdditionalRecipientGroupsListBox.DataSource = allGroups;
            AdditionalRecipientGroupsListBox.DataValueField = "Id";
            AdditionalRecipientGroupsListBox.DataTextField = "Name";
            AdditionalRecipientGroupsListBox.DataBind();
        }

        private void BindEmployeeLists(List<EmployeeDisplay> employees)
        {
            AssignToListBox.DataSource = employees.OrderBy(e => e.Name);
            AssignToListBox.DataValueField = "Id";
            AssignToListBox.DataTextField = "Name";
            AssignToListBox.DataBind();

            AdditionalRecipientsListBox.DataSource = employees.OrderBy(e => e.Name);
            AdditionalRecipientsListBox.DataValueField = "Id";
            AdditionalRecipientsListBox.DataTextField = "Name";
            AdditionalRecipientsListBox.DataBind();
        }

        private void SetAssignedTo(string assignedTo)
        {
            if (!string.IsNullOrEmpty(assignedTo))
            {
                string[] assigneeList = assignedTo.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (ListItem item in AssignToListBox.Items)
                {
                    if (assigneeList.Contains(item.Value))
                    {
                        item.Selected = true;
                    }
                }
            }
        }

        private void SetAdditionalRecipients(string additionalRecipients)
        {
            if (!string.IsNullOrEmpty(additionalRecipients))
            {
                string[] additionalRecipientsList = additionalRecipients.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (ListItem item in AdditionalRecipientsListBox.Items)
                {
                    if (additionalRecipientsList.Contains(item.Value))
                    {
                        item.Selected = true;
                    }
                }
            }
        }

        private void LoadCategories()
        {
            TaskCategoryBll taskCategoryBll = new TaskCategoryBll();
            List<AKMII.SafeSplash.Entity.tb_TaskCategory> allCategories = taskCategoryBll.GetAll();
            List<AKMII.SafeSplash.Entity.tb_TaskCategory> categories = new List<AKMII.SafeSplash.Entity.tb_TaskCategory>();
            categories.Add(new AKMII.SafeSplash.Entity.tb_TaskCategory() { taskCategory_ID = 0, taskCategory_Name = string.Empty });
            categories.AddRange(allCategories.OrderBy(c => c.taskCategory_SortOrder));
            TaskCategoriesDropDownList.DataSource = categories;
            TaskCategoriesDropDownList.DataValueField = "taskCategory_ID";
            TaskCategoriesDropDownList.DataTextField = "taskCategory_Name";
            TaskCategoriesDropDownList.DataBind();
        }

        private void LoadTaskInfo(AKMII.SafeSplash.Entity.tb_Task task)
        {
            this.AssignedToHiddenField.Value = task.task_AssignedTo;
            this.AdditionalRecipientsHiddenField.Value = task.task_Recipient;
            if (task.tb_Location != null)
            {
                this.LocationsDropDownList.SelectedValue = task.tb_Location.loca_ID.ToString();
                LocationChanged(task.tb_Location.loca_ID);
            }
            else
            {
                LocationChanged(Guid.Empty);
            }
            if (task.taskCategory_ID != null)
            {
                this.TaskCategoriesDropDownList.SelectedValue = task.taskCategory_ID.ToString();
            }
            if (task.tb_TaskCategory != null)
            {
                TaskCategoriesDropDownList.SelectedValue = task.tb_TaskCategory.taskCategory_ID.ToString();
            }
            this.txbName.Text = task.task_Subject;
            this.FamilyNameTextBox.Text = task.task_FamilyName;
            //this.dtStartDate.SelectedDate = (DateTime)task.task_StartDate;
            //this.dtDueDate.SelectedDate = (DateTime)task.task_EndDate;
            calExtStartDate.SelectedDate = (DateTime)task.task_StartDate;
            calExtDueDate.SelectedDate = (DateTime)task.task_EndDate;
            this.ddlStatus.SelectedValue = !string.IsNullOrEmpty(task.task_Status) ? task.task_Status.Trim() : string.Empty;
            this.txbContent.Text = task.task_Content;
        }        

        public void btnSubmit_Click(object sender, EventArgs e)
        {
            if (GetSelectedAssignedToIds().Count() < 1 && GetSelectedAdditionalRecipientsIds().Count() < 1)
            {
                this.errorMsg.Text = "At least one Assigned To or Additional Recipient employee is required.";
                this.errorMsg.Visible = true;
                return;
            }
            if (string.IsNullOrEmpty(ddlStatus.SelectedValue.ToString()))
            {
                this.errorMsg.Text = "Status cannot be blank.";
                this.errorMsg.Visible = true;
                return;
            }
            DateTime dtStartDate = (DateTime)calExtStartDate.SelectedDate; // this.dtStartDate.SelectedDate;
            DateTime dtEndDate = (DateTime)calExtDueDate.SelectedDate; //  this.dtDueDate.SelectedDate;
            if (dtStartDate.CompareTo(dtEndDate) > 0)
            {
                this.errorMsg.Text = "The due date is earlier than the start date.";
                this.errorMsg.Visible = true;
                return;
            }
            TaskBll taskBll = new TaskBll();
            AKMII.SafeSplash.Entity.tb_Task task = taskBll.GetByID((Guid)ViewState["ID"]);
            if (!string.IsNullOrEmpty(LocationsDropDownList.SelectedValue))
            {
                Guid locationId = new Guid(LocationsDropDownList.SelectedValue);
                //task.loca_ID = !Guid.Empty.Equals(locationId) ? locationId : null;
                if (!Guid.Empty.Equals(locationId))
                {
                    task.loca_ID = locationId;
                }
                else
                {
                    task.loca_ID = null;
                }
            }
            else
            {
                task.loca_ID = null;
            }
            int selectedCategoryId = 0;
            int.TryParse(TaskCategoriesDropDownList.SelectedValue, out selectedCategoryId);
            if (selectedCategoryId > 0)
            {
                task.taskCategory_ID = selectedCategoryId;
            }
            else
            {
                task.taskCategory_ID = null;
            }
            task.task_Subject = this.txbName.Text;
            task.task_Content = this.txbContent.Text;
            task.task_StartDate = dtStartDate;
            task.task_EndDate = dtEndDate;
            task.task_Status = this.ddlStatus.SelectedValue.ToString();
            task.task_FamilyName = this.FamilyNameTextBox.Text;

            EmployeeBll employeeBll = new EmployeeBll();
            List<string> emailRecipients = new List<string>();
            StringBuilder recipientIds = new StringBuilder();
            foreach (var id in GetSelectedAssignedToIds())
            {
                recipientIds.AppendFormat("{0},", id.ToString());
                AKMII.SafeSplash.Entity.tb_Employee employee = employeeBll.GetByID((id));
                if (employee != null && !string.IsNullOrEmpty(employee.empl_Email))
                {
                    emailRecipients.Add(employee.empl_Email);
                }
            }
            //foreach (var id in GetEmployeeIdsFromSelectedAssignToGroups())
            //{
            //    if (!recipientIds.ToString().Contains(id.ToString()))
            //    {
            //        recipientIds.AppendFormat("{0},", id.ToString());
            //        tb_Employee employee = employeeBll.GetByID((id));
            //        if (employee != null && !string.IsNullOrEmpty(employee.empl_Email))
            //        {
            //            emailRecipients.Add(employee.empl_Email);
            //        }
            //    }
            //}
            task.task_AssignedTo = recipientIds.ToString();

            StringBuilder additionalRecipientIds = new StringBuilder();
            foreach (var id in GetSelectedAdditionalRecipientsIds())
            {
                additionalRecipientIds.AppendFormat("{0},", id.ToString());
                AKMII.SafeSplash.Entity.tb_Employee employee = employeeBll.GetByID((id));
                if (employee != null && !string.IsNullOrEmpty(employee.empl_Email))
                {
                    emailRecipients.Add(employee.empl_Email);
                }
            }
            foreach (var id in GetEmployeeIdsFromSelectedAdditionalRecipientGroups())
            {
                if (!additionalRecipientIds.ToString().Contains(id.ToString()))
                {
                    additionalRecipientIds.AppendFormat("{0},", id.ToString());
                    AKMII.SafeSplash.Entity.tb_Employee employee = employeeBll.GetByID((id));
                    if (employee != null && !string.IsNullOrEmpty(employee.empl_Email))
                    {
                        emailRecipients.Add(employee.empl_Email);
                    }
                }
            }
            task.task_Recipient = additionalRecipientIds.ToString();

            UserBll userBll = new UserBll();
            if (task.task_Creator != null && !String.IsNullOrEmpty(task.task_Creator.ToString()))
            {
                AKMII.SafeSplash.Entity.tb_Employee creator = employeeBll.GetByID((Guid)task.task_Creator);
                if (creator != null)
                {
                    if (!String.IsNullOrEmpty(creator.empl_Email))
                        emailRecipients.Add(creator.empl_Email);
                }
            }
            string strModifiedBy = "";
            Guid userID = new Guid(Context.User.Identity.GetUserId()); // userBll.GetUserIDByUserName(SPContext.Current.Web.CurrentUser.Name);
            AKMII.SafeSplash.Entity.tb_Employee modifiedBy = employeeBll.GetByUserID(userID);
            if (modifiedBy != null)
            {
                task.task_ModifiedBy = modifiedBy.empl_ID;
                strModifiedBy = modifiedBy.empl_FirstName + " " + modifiedBy.empl_LastName;
            }
            task.task_ModifiedTime = DateTime.Now;

            taskBll.Update(task);

            SendMailNotification(strModifiedBy, emailRecipients, this.txbName.Text, this.txbContent.Text, (DateTime)task.task_ModifiedTime);
            GenericHelper.CloseDialog(this.Page, 1, "");
        }

        //private IEnumerable<Guid> GetEmployeeIdsFromSelectedAssignToGroups()
        //{
        //    List<Guid> selectedAssignedToGroupIds = GetSelectedAssignedToGroupIds();

        //    List<Guid> employeeIds = new List<Guid>();
        //    foreach (var groupId in selectedAssignedToGroupIds)
        //    {
        //        EmployeeGroupDataObject group = GroupsRepository.GetEmployeeGroup(groupId);
        //        if (group != null)
        //        {
        //            employeeIds.AddRange(group.Employees.Select(eg => eg.Employee.Id).ToList<Guid>());
        //        }
        //    }
        //    return employeeIds.Distinct<Guid>();
        //}

        private IEnumerable<Guid> GetEmployeeIdsFromSelectedAdditionalRecipientGroups()
        {
            List<Guid> selectedAdditionalRecipientGroupIds = GetSelectedAdditionalRecipientGroupIds();

            List<Guid> employeeIds = new List<Guid>();
            foreach (var groupId in selectedAdditionalRecipientGroupIds)
            {
                EmployeeGroupDataObject group = GroupsRepository.GetEmployeeGroup(groupId);
                if (group != null)
                {
                    employeeIds.AddRange(group.Employees.Select(eg => eg.Employee.Id).ToList<Guid>());
                }
            }
            return employeeIds.Distinct<Guid>();
        }

        private void SendMailNotification(string creator, List<string> recipientsMail, string taskSubject, string taskContent, DateTime dtModified)
        {
            if (recipientsMail.Count > 0)
            {
                string mailSubject = GenericHelper.GetMailContent(Constant.Task_Edit_Notify_Title);
                string mailBody = GenericHelper.GetMailContent(Constant.Task_Edit_Notify_Body);
                mailSubject = String.Format(mailSubject, creator);
                mailBody = String.Format(mailBody, taskSubject, taskContent, creator, dtModified);
                Mail mail = new Mail();
                mail.Subject = mailSubject;
                mail.Content = GenericHelper.WrappableText(mailBody);
                mail.Recipients = recipientsMail;
                mail.FromAddress = GenericHelper.GetAppSettings(Constant.AppSetting_MailAccount);

                GenericHelper.SendMail(mail);
                //GenericHelper.CloseDialog(this.Page, 1, "");
            }
        }

        protected void LocationsDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedGuidString = this.LocationsDropDownList.SelectedValue;
            Guid selectedLocationId = !string.IsNullOrEmpty(selectedGuidString) ? new Guid(selectedGuidString) : Guid.Empty;
            LocationChanged(selectedLocationId);
        }

        private void LocationChanged(Guid selectedLocationId)
        {
            LoadEmployeesForLocationId(selectedLocationId);
            SetAssignedTo(this.AssignedToHiddenField.Value);
            SetAdditionalRecipients(this.AdditionalRecipientsHiddenField.Value);
        }

        private List<Guid> GetSelectedAssignedToIds()
        {
            List<Guid> selectedIds = new List<Guid>();
            foreach(ListItem item in this.AssignToListBox.Items)
            {
                if (item.Selected)
                {
                    selectedIds.Add(new Guid(item.Value));
                }
            }
            return selectedIds;
        }

        //private List<Guid> GetSelectedAssignedToGroupIds()
        //{
        //    List<Guid> selectedIds = new List<Guid>();
        //    foreach (ListItem item in this.AssignToGroupListBox.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            selectedIds.Add(new Guid(item.Value));
        //        }
        //    }
        //    return selectedIds;
        //}

        private List<Guid> GetSelectedAdditionalRecipientsIds()
        {
            List<Guid> selectedIds = new List<Guid>();
            foreach (ListItem item in this.AdditionalRecipientsListBox.Items)
            {
                if (item.Selected)
                {
                    selectedIds.Add(new Guid(item.Value));
                }
            }
            return selectedIds;
        }

        private List<Guid> GetSelectedAdditionalRecipientGroupIds()
        {
            List<Guid> selectedIds = new List<Guid>();
            foreach (ListItem item in this.AdditionalRecipientGroupsListBox.Items)
            {
                if (item.Selected)
                {
                    selectedIds.Add(new Guid(item.Value));
                }
            }
            return selectedIds;
        }
    }

    public class EmployeeDisplay
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get { return string.Format("{0} {1}", FirstName, LastName); } }
    }    

    public class LocationChangedEventArgs : EventArgs
    {
        public Guid LocationId { get; set; }

        public LocationChangedEventArgs(Guid locationId)
        {
            LocationId = locationId;
        }
    }
}
