﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using AKMII.SafeSplash.BLL;
using AKMII.SafeSplash.Entity;
using SafeSplash.ObjectModel;
using SafeSplash.Service.Task;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Text;

using Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace SafeSplash.PortalSite.ControlTemplates.SS
{
    public partial class MyTaskCtrl : UserControl
    {
        private Dictionary<Guid, tb_Employee> EmployeesById = new Dictionary<Guid, tb_Employee>();

        #region ViewState Properties

        public string SortBy
        {
            get
            {
                return ViewState["SortBy"].ToString();
            }
            set
            {
                ViewState["SortBy"] = value;
            }
        }

        public int CategoryFilter
        {
            get
            {
                return (int)ViewState["FilterByCategory"];
            }
            set
            {
                ViewState["FilterByCategory"] = value;
            }
        }

        public Guid LocationFilter
        {
            get
            {
                return (Guid)ViewState["FilterByLocation"];
            }
            set
            {
                ViewState["FilterByLocation"] = value;
            }
        }

        public string StatusFilter
        {
            get
            {
                return ViewState["Status"].ToString();
            }
            set
            {
                ViewState["Status"] = value;
            }
        }

        public string RecipientFilter
        {
            get
            {
                return ViewState["Filter"].ToString();
            }
            set
            {
                ViewState["Filter"] = value;
            }
        }

        public DateTime TaskStartDateBeginRange
        {
            get
            {
                DateTime startDate = DateTime.MaxValue;
                if (ViewState["TaskStartDateBeginRange"] != null)
                {
                    startDate = DateTime.Parse(ViewState["TaskStartDateBeginRange"].ToString());
                }
                return startDate;
            }
            set
            {
                ViewState["TaskStartDateBeginRange"] = value;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CreateLegend();
                PopulateSortByValues();
                PopulateFilterByCategories();
                PopulateFilterByLocations();
                PopulateStatuses();
                PopulateTaskTypes();
                ddlSortBy.SelectedValue = "Last Modified Date";
                RefreshData();
            }
        }

        private void CreateLegend()
        {
            TaskCategoryBll categoryBll = new TaskCategoryBll();
            List<tb_TaskCategory> categories = categoryBll.GetAll().OrderBy(c => c.taskCategory_SortOrder).ToList<tb_TaskCategory>();
            StringBuilder sb = new StringBuilder();
            int columnCount = 1;
            sb.Append("<table>");
            sb.Append("<tr>");
            //sb.Append("<td><span style='font-weight: bold;'>Legend:</span></td>");
            foreach (var cat in categories)
            {
                if (columnCount > 6)
                {
                    sb.Append("</tr><tr>");
                    columnCount = 1;
                }
                sb.AppendFormat("<td><div class='CategoryIndicator' style='margin-left: 15px; background-color: {0};'></div></td>", cat.taskCategory_Color);
                sb.AppendFormat("<td><span style='margin-left: 2px;'>{0}</span></td>", cat.taskCategory_Name);
                columnCount++;
            }
            sb.Append("</tr>");
            sb.Append("</table>");
            //LegendDiv.InnerHtml = sb.ToString();
            LegendContentDiv.InnerHtml = sb.ToString();
        }

        private void PopulateSortByValues()
        {
            ddlSortBy.Items.Add(new ListItem("Assigned To"));
            ddlSortBy.Items.Add(new ListItem("Category"));
            ddlSortBy.Items.Add(new ListItem("Created Date"));
            ddlSortBy.Items.Add(new ListItem("Family Name"));
            ddlSortBy.Items.Add(new ListItem("Last Modified Date"));
            ddlSortBy.Items.Add(new ListItem("Location"));
            ddlSortBy.Items.Add(new ListItem("Subject"));
        }

        private void PopulateFilterByCategories()
        {
            TaskCategoryBll categoryBll = new TaskCategoryBll();
            List<tb_TaskCategory> allCategories = categoryBll.GetAll().OrderBy(tc => tc.taskCategory_Name).ToList<tb_TaskCategory>();
            List<tb_TaskCategory> categories = new List<tb_TaskCategory>();
            categories.Add(new tb_TaskCategory() { taskCategory_ID = 0, taskCategory_Name = string.Empty });
            categories.AddRange(allCategories);
            ddlFilterByCategory.DataSource = categories;
            ddlFilterByCategory.DataTextField = "taskCategory_Name";
            ddlFilterByCategory.DataValueField = "taskCategory_ID";
            ddlFilterByCategory.DataBind();
        }

        private void PopulateFilterByLocations()
        {
            
                
            // SPGroup groupCorpAdmin = SPContext.Current.Web.Groups[ObjectModel.UserType.CorpAdmin.ToString()];
            UserBll userBll = new UserBll();
            Guid userId = new Guid(Context.User.Identity.GetUserId()); // userBll.GetUserIDByUserName(SPContext.Current.Web.CurrentUser.Name);
            EmployeeBll employeeBll = new EmployeeBll();
            tb_Employee employee = employeeBll.GetByUserID(userId);
            EmployeeLocationBll empLocBll = new EmployeeLocationBll();
            List<AKMII.SafeSplash.EntityNew.tb_Location> allEmployeeLocations = new List<AKMII.SafeSplash.EntityNew.tb_Location>();
            // if (groupCorpAdmin.ContainsCurrentUser)
            if (Context.User.IsInRole(ObjectModel.Constants.RoleCorpAdmin) ) 
            {
                LocationBll locationBll = new LocationBll();
                allEmployeeLocations = locationBll.GetAll().OrderBy(l => l.loca_Name).ToList<AKMII.SafeSplash.EntityNew.tb_Location>();
            }
            else
            {
                allEmployeeLocations = empLocBll.GetLocationsByEmplID(employee.empl_ID).OrderBy(l => l.loca_Name).ToList<AKMII.SafeSplash.EntityNew.tb_Location>();
            }
            List<AKMII.SafeSplash.EntityNew.tb_Location> employeeLocations = new List<AKMII.SafeSplash.EntityNew.tb_Location>();
            employeeLocations.Add(new AKMII.SafeSplash.EntityNew.tb_Location() { loca_ID = Guid.Empty, loca_Name = string.Empty });
            employeeLocations.AddRange(allEmployeeLocations);
            ddlFilterByLocation.DataSource = employeeLocations.OrderBy(l => l.loca_Name);
            ddlFilterByLocation.DataTextField = "loca_Name";
            ddlFilterByLocation.DataValueField = "loca_ID";
            ddlFilterByLocation.DataBind();
        }

        private void PopulateStatuses()
        {
           // ddlFilterByStatus.Items.Add(new ListItem("", ""));
            //IEnumerable<SafeSplash.Service.Task.TaskStatus> statuses = SafeSplash.Service.Task.TaskStatus.AllTaskStatuses.Where(ts => ts.Value != SafeSplash.Service.Task.TaskStatus.Closed.Value);
            IEnumerable<Service.Task.TaskStatus> statuses = Service.Task.TaskStatus.AllTaskStatuses;
            foreach (var status in statuses)
            {
                ddlFilterByStatus.Items.Add(new ListItem(status.Description, status.Value));
            }
            ddlFilterByStatus.SelectedIndex = 0;
        }

        private void PopulateTaskTypes()
        {
            ddlFilterByRecipient.Items.Add(new ListItem(string.Empty));
            ddlFilterByRecipient.Items.Add(new ListItem("My Tasks"));
            ddlFilterByRecipient.Items.Add(new ListItem("I am Additional Recipient"));
            ddlFilterByRecipient.Items.Add(new ListItem("Tasks I Opened"));
            ddlFilterByRecipient.SelectedIndex = 0;
            ddlFilterByRecipient.SelectedIndex = 0;
        }

        public void TaskViewRebind()
        {
            TaskBll taskBll = new TaskBll();
            List<tb_Task> taskList = new List<tb_Task>();
            DateTime taskStartDate = TaskStartDateBeginRange;
            UserBll userBll = new UserBll();
            Guid userID = new Guid(Context.User.Identity.GetUserId()); // userBll.GetUserIDByUserName(SPContext.Current.Web.CurrentUser.Name);
            EmployeeBll employeeBll = new EmployeeBll();
            tb_Employee employee = employeeBll.GetByUserID(userID);

            if (employee != null)
            {
                // determine which tasks to retrieve from DB based on status
                //if (SafeSplash.Service.Task.TaskStatus.Closed.Value.Equals(ddlFilterByStatus.SelectedValue))
                //{
                //    taskList = taskBll.GetClosedTasksForEmployee(employee.empl_ID.ToString());
                //}
                //else
                //{
                //    taskList = taskBll.GetOpenTasksForEmployee(employee.empl_ID.ToString());
                //    taskList = taskList.Where(t => t.task_StartDate.Value.CompareTo(taskStartDate) <= 0).ToList<tb_Task>();
                //}

                taskList = taskBll.GetTasksForEmployee(employee.empl_ID, ddlFilterByStatus.SelectedValue).Where(t => t.task_StartDate.Value.CompareTo(taskStartDate) <= 0).ToList<tb_Task>();

                // filter out based on recipient filter
                if ("My Tasks".Equals(ddlFilterByRecipient.SelectedValue))
                {
                    //taskList = taskList.Where(t => t.task_AssignedTo.Contains(employee.empl_ID.ToString())).ToList<tb_Task>();                    
                    taskList = taskList.Where(t => !string.IsNullOrEmpty(t.task_AssignedTo) && t.task_AssignedTo.Contains(employee.empl_ID.ToString())).ToList<tb_Task>();
                }
                if ("I am Additional Recipient".Equals(ddlFilterByRecipient.SelectedValue))
                {
                    List<tb_Task> notNullAdditionalRecipientTasks = taskList.Where(t => !string.IsNullOrEmpty(t.task_Recipient)).ToList<tb_Task>();
                    taskList = notNullAdditionalRecipientTasks.Where(t => t.task_Recipient.Contains(employee.empl_ID.ToString())).ToList<tb_Task>();
                }
                if ("Tasks I Opened".Equals(ddlFilterByRecipient.SelectedValue))
                {
                    taskList = taskBll.GetTasksCreatedByEmployee(employee.empl_ID);
                }

                // filter out based on status
                if (!string.IsNullOrEmpty(ddlFilterByStatus.SelectedValue))
                {
                    taskList = taskList.Where(t => t.task_Status.Contains(ddlFilterByStatus.SelectedValue)).ToList<tb_Task>();
                }
            }

            List<TaskDisplay> displayTasks = new List<TaskDisplay>();
            foreach (var task in taskList)
            {
                TaskDisplay td = new TaskDisplay();
                td.Id = task.task_ID;
                td.Category = task.tb_TaskCategory;
                td.AssignedTo = task.task_AssignedTo;
                td.AssignedToNames = BuildAssignedToText(task);
                td.FamilyName = task.task_FamilyName;
                td.Location = task.tb_Location;
                td.LocationName = (task.tb_Location != null ? task.tb_Location.loca_Name : string.Empty);
                td.Subject = task.task_Subject;
                td.CreatedDate = task.task_CreatedTime.Value;
                td.LastModifiedDate = task.task_ModifiedTime.HasValue ? task.task_ModifiedTime.Value : task.task_CreatedTime.Value;
                displayTasks.Add(td);
            }

            Session["Tasks"] = displayTasks;

            displayTasks = SortAndFilterTasks(displayTasks);

            BindData(displayTasks);
        }

        private List<TaskDisplay> SortAndFilterTasks(List<TaskDisplay> tasks)
        {
            List<TaskDisplay> sortedTasks = new List<TaskDisplay>();
            if (tasks != null)
            {
                List<TaskDisplay> taskList = tasks;


                if (CategoryFilter > 0)
                {
                    taskList = taskList.Where(t => t.Category != null).ToList<TaskDisplay>();
                    taskList = taskList.Where(t => t.Category.taskCategory_ID == CategoryFilter).ToList<TaskDisplay>();
                }

                if (!Guid.Empty.Equals(LocationFilter))
                {
                    taskList = taskList.Where(t => t.Location != null).ToList<TaskDisplay>();
                    taskList = taskList.Where(t => t.Location.loca_ID == LocationFilter).ToList<TaskDisplay>();
                }


                sortedTasks = SortTasks(taskList, ddlSortBy.SelectedValue);
            }

            return sortedTasks;
        }

        private void BindData(List<TaskDisplay> tasks)
        {
            myTaskGrid.DataSource = tasks;
            myTaskGrid.DataBind();
            TaskCountLabel.Text = string.Format("Tasks ({0})", tasks.Count().ToString());
        }


        protected void ddlSortBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<TaskDisplay> tasks = (List<TaskDisplay>)Session["Tasks"];
            //List<TaskDisplay> sortedTasks = SortTasks(tasks, ddlSortBy.SelectedValue);
            List<TaskDisplay> sortedTasks = SortAndFilterTasks(tasks);
            BindData(sortedTasks);
        }

        protected void ddlFilterByCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            CategoryFilter = int.Parse(ddlFilterByCategory.SelectedValue);
            List<TaskDisplay> tasks = (List<TaskDisplay>)Session["Tasks"];
            List<TaskDisplay> sortedTasks = SortAndFilterTasks(tasks);
            BindData(sortedTasks);
        }

        protected void ddlFilterByLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            LocationFilter = new Guid(ddlFilterByLocation.SelectedValue);
            List<TaskDisplay> tasks = (List<TaskDisplay>)Session["Tasks"];
            List<TaskDisplay> sortedTasks = SortAndFilterTasks(tasks);
            BindData(sortedTasks);
        }

        protected void ddlFilterByStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        protected void ddlFilterByRecipient_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void RefreshData()
        {
            //this.TaskStartDateBeginRange = DateTime.Today;
            this.SortBy = ddlSortBy.SelectedValue;
            this.CategoryFilter = int.Parse(ddlFilterByCategory.SelectedValue);
            this.LocationFilter = new Guid(ddlFilterByLocation.SelectedValue);
            this.StatusFilter = ddlFilterByStatus.SelectedValue;
            this.RecipientFilter = ddlFilterByRecipient.SelectedValue;
            TaskViewRebind();
        }


        protected virtual void TasksGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TaskDisplay task = (TaskDisplay)e.Row.DataItem;

                if (task.Category != null)
                {
                    HtmlGenericControl div = e.Row.FindControl("TaskCategoryIndicatorDiv") as HtmlGenericControl;
                    div.Style.Add("background-color", task.Category.taskCategory_Color);
                    Label categoryLabel = e.Row.FindControl("lblTaskCategoryDescription") as Label;
                    categoryLabel.Text = task.Category.taskCategory_Name;
                }

            }
        }

        protected void TaskList_Sorting(object sender, GridViewSortEventArgs e)
        {
            ////int i = 0;
            ////TravelAuthorizationRequestList requests = (TravelAuthorizationRequestList)Session[Wsl.BusinessObjects.TravelAuthorization.ApplicationContext.SessionVariables.TravelRequests];
            ////Filter filter = (Filter)Session[Wsl.BusinessObjects.TravelAuthorization.ApplicationContext.SessionVariables.Filter];
            ////SortDirection newSortDirection = CurrentSortDirection.Equals(SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending;
            ////SortRequests(requests.FilterRequests(filter), e.SortExpression, newSortDirection);
            //SortDirection newSortDirection = CurrentSortDirection.Equals(SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending;
            ////SortTasks(requests.FilterRequests(filter), e.SortExpression, newSortDirection);
            //IEnumerable<tb_Task> tasks = (IEnumerable<tb_Task>)Session["Tasks"];
            //SortTasks(tasks, e.SortExpression, newSortDirection);
        }

        public List<TaskDisplay> SortTasks(List<TaskDisplay> tasks, string sortExpression)
        {
            //IEnumerable<tb_Task> tasks = (IEnumerable<tb_Task>)Session["Tasks"];
            //IEnumerable<tb_Task> sortedTasks = tasks;
            List<TaskDisplay> sortedTasks = tasks;
            switch (sortExpression)
            {
                case "Assigned To":
                    sortedTasks = sortedTasks.OrderBy(t => t.AssignedToNames).ToList<TaskDisplay>();
                    break;
                case "Category":
                    List<TaskDisplay> nullCategoryTasks = tasks.Where(t => t.Category == null).ToList<TaskDisplay>();
                    List<TaskDisplay> notNullCategoryTasks = tasks.Where(t => t.Category != null).ToList<TaskDisplay>();
                    List<TaskDisplay> combinedSortedTasks = new List<TaskDisplay>(nullCategoryTasks);
                    combinedSortedTasks.AddRange(notNullCategoryTasks.OrderBy(t => t.Category.taskCategory_Name));
                    sortedTasks = combinedSortedTasks;
                    break;
                case "Created Date":
                    sortedTasks = tasks.OrderBy(t => t.CreatedDate).ToList<TaskDisplay>();
                    break;
                case "Family Name":
                    sortedTasks = tasks.OrderBy(t => t.FamilyName).ToList<TaskDisplay>();
                    break;
                case "Last Modified Date":
                    sortedTasks = tasks.OrderByDescending(t => t.LastModifiedDate).ToList<TaskDisplay>();
                    break;
                case "Location":
                    List<TaskDisplay> nullLocationTasks = tasks.Where(t => t.Location == null).ToList<TaskDisplay>();
                    List<TaskDisplay> notNullLocationTasks = tasks.Where(t => t.Location != null).ToList<TaskDisplay>();
                    List<TaskDisplay> combinedSortedByLocationTasks = new List<TaskDisplay>(nullLocationTasks);
                    combinedSortedByLocationTasks.AddRange(notNullLocationTasks.OrderBy(t => t.Location.loca_Name));
                    sortedTasks = combinedSortedByLocationTasks;
                    break;
                case "Subject":
                    sortedTasks = tasks.OrderBy(t => t.Subject).ToList<TaskDisplay>();
                    break;
                default:
                    break;
            }

            return sortedTasks;

            // reverse order if descending
            //if (SortDirection.Descending.Equals(sortDirection))
            //{
            //    sortedTasks = sortedTasks.Reverse().ToList<tb_Task>();
            //}

            //BindData(sortedTasks);
            //SetSortedColumnHeader(sortExpression, sortDirection);
            //SortExpression = sortExpression;
            //CurrentSortDirection = sortDirection;
        }

        public void Sort(string sortExpression)
        {
            //IEnumerable<tb_Task> tasks = (IEnumerable<tb_Task>)Session["Tasks"];
            //IEnumerable<tb_Task> sortedTasks = tasks;
            //switch (sortExpression)
            //{
            //    case "Assigned To":
            //        break;
            //    case "Category":
            //        List<tb_Task> nullCategoryTasks = tasks.Where(t => t.tb_TaskCategory == null).ToList<tb_Task>();
            //        List<tb_Task> notNullCategoryTasks = tasks.Where(t => t.tb_TaskCategory != null).ToList<tb_Task>();
            //        List<tb_Task> combinedSortedTasks = new List<tb_Task>(nullCategoryTasks);
            //        combinedSortedTasks.AddRange(notNullCategoryTasks.OrderBy(t => t.tb_TaskCategory.taskCategory_Name));
            //        sortedTasks = combinedSortedTasks;
            //        break;
            //    //case "AssignedTo":
            //    //    sortedTasks = tasks.OrderBy(t => t.task_AssignedTo).ToList<tb_Task>();
            //    //    break;
            //    case "Created By":
            //        break;
            //    case "Created Date":
            //        sortedTasks = tasks.OrderBy(t => t.task_CreatedTime).ToList<tb_Task>();
            //        break;
            //    case "Last Modified Date":
            //        sortedTasks = tasks.OrderBy(t => t.task_ModifiedTime).ToList<tb_Task>();
            //        break;
            //    case "Location":
            //        List<tb_Task> nullLocationTasks = tasks.Where(t => t.tb_Location == null).ToList<tb_Task>();
            //        List<tb_Task> notNullLocationTasks = tasks.Where(t => t.tb_Location != null).ToList<tb_Task>();
            //        List<tb_Task> combinedSortedByLocationTasks = new List<tb_Task>(nullLocationTasks);
            //        combinedSortedByLocationTasks.AddRange(notNullLocationTasks.OrderBy(t => t.tb_Location.loca_Name));
            //        sortedTasks = combinedSortedByLocationTasks;
            //        break;
            //    case "Subject":
            //        sortedTasks = tasks.OrderBy(t => t.task_Subject).ToList<tb_Task>();
            //        break;                
            //    default:
            //        break;
            //}

            // reverse order if descending
            //if (SortDirection.Descending.Equals(sortDirection))
            //{
            //    sortedTasks = sortedTasks.Reverse().ToList<tb_Task>();
            //}

            //BindData(sortedTasks);
            //SetSortedColumnHeader(sortExpression, sortDirection);
            //SortExpression = sortExpression;
            //CurrentSortDirection = sortDirection;
        }

        private void SortTasks(IEnumerable<tb_Task> tasks, string sortExpression, SortDirection sortDirection)
        {
            //IEnumerable<tb_Task> sortedTasks = tasks;

            //switch (sortExpression)
            //{
            //    case "Category":
            //        List<tb_Task> nullCategoryTasks = tasks.Where(t => t.tb_TaskCategory == null).ToList<tb_Task>();
            //        List<tb_Task> notNullCategoryTasks = tasks.Where(t => t.tb_TaskCategory != null).ToList<tb_Task>();
            //        List<tb_Task> combinedSortedTasks = new List<tb_Task>(nullCategoryTasks);
            //        combinedSortedTasks.AddRange(notNullCategoryTasks.OrderBy(t => t.tb_TaskCategory.taskCategory_Name));
            //        sortedTasks = combinedSortedTasks;
            //        break;
            //    //case "AssignedTo":
            //    //    sortedTasks = tasks.OrderBy(t => t.task_AssignedTo).ToList<tb_Task>();
            //    //    break;
            //    case "Location":
            //        List<tb_Task> nullLocationTasks = tasks.Where(t => t.tb_Location == null).ToList<tb_Task>();
            //        List<tb_Task> notNullLocationTasks = tasks.Where(t => t.tb_Location != null).ToList<tb_Task>();
            //        List<tb_Task> combinedSortedByLocationTasks = new List<tb_Task>(nullLocationTasks);
            //        combinedSortedByLocationTasks.AddRange(notNullLocationTasks.OrderBy(t => t.tb_Location.loca_Name));
            //        sortedTasks = combinedSortedByLocationTasks;
            //        break;
            //    case "Subject":
            //        sortedTasks = tasks.OrderBy(t => t.task_Subject).ToList<tb_Task>();
            //        break;
            //    case "CreatedDate":
            //        sortedTasks = tasks.OrderBy(t => t.task_CreatedTime).ToList<tb_Task>();
            //        break;
            //    case "LastModifiedDate":
            //        sortedTasks = tasks.OrderBy(t => t.task_ModifiedTime).ToList<tb_Task>();
            //        break;
            //    default:
            //        break;
            //}

            //// reverse order if descending
            //if (SortDirection.Descending.Equals(sortDirection))
            //{
            //    sortedTasks = sortedTasks.Reverse().ToList<tb_Task>();
            //}

            //BindData(sortedTasks);
            ////SetSortedColumnHeader(sortExpression, sortDirection);
            //SortExpression = sortExpression;
            //CurrentSortDirection = sortDirection;
        }

        private string BuildAssignedToText(tb_Task task)
        {
            StringBuilder sb = new StringBuilder();
            EmployeeBll employeeBll = new EmployeeBll();
            if (!string.IsNullOrEmpty(task.task_AssignedTo))
            {
                string[] empIds = task.task_AssignedTo.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                int index = 0;
                foreach (var id in empIds)
                {
                    Guid empId = new Guid(id);
                    tb_Employee employee = null;
                    if (EmployeesById.ContainsKey(empId))
                    {
                        employee = EmployeesById[empId];
                    }
                    else
                    {
                        employee = employeeBll.GetByID(new Guid(id));
                        EmployeesById.Add(empId, employee);
                    }
                    //tb_Employee employee = employeeBll.GetByID(new Guid(id));
                    if (employee != null)
                    {
                        //sb.AppendFormat("{0} {1}, ", employee.empl_FirstName, employee.empl_LastName);
                        sb.AppendFormat("{0} {1}{2} ", employee.empl_FirstName, employee.empl_LastName, (index < empIds.Count() - 1 ? "," : string.Empty));
                    }
                    index++;
                }
            }
            return sb.ToString();
        }

        //private string BuildTaskLinkText(tb_Task task)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    EmployeeBll employeeBll = new EmployeeBll();
        //    if (task.loca_ID != null || task.taskCategory_ID != null)
        //    {
        //        StringBuilder employeesAssignedTo = new StringBuilder();
        //        if (!string.IsNullOrEmpty(task.task_AssignedTo))
        //        {
        //            string[] empIds = task.task_AssignedTo.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        //            foreach (var id in empIds)
        //            {
        //                Guid empId = new Guid(id);
        //                tb_Employee employee = null;
        //                if (EmployeesById.ContainsKey(empId))
        //                {
        //                    employee = EmployeesById[empId];
        //                }
        //                else
        //                {
        //                    employee = employeeBll.GetByID(new Guid(id));
        //                    EmployeesById.Add(empId, employee);
        //                }
        //                if (employee != null)
        //                {
        //                    employeesAssignedTo.AppendFormat("{0} {1}; ", employee.empl_FirstName, employee.empl_LastName);
        //                }
        //            }
        //        }
        //        string locationName = (task.tb_Location != null) ? task.tb_Location.loca_Name : string.Empty;
        //        string category = (task.tb_TaskCategory != null) ? task.tb_TaskCategory.taskCategory_Name : string.Empty;
        //        string familyName = (!string.IsNullOrEmpty(task.task_FamilyName)) ? task.task_FamilyName : string.Empty;
        //        //sb.AppendFormat("<span style='font-weight: bold;'>Assigned To: </span>{0} <span style='font-weight: bold;'>Location: </span>{1}; <span style='font-weight: bold;'>Category: </span>{2}; <span style='font-weight: bold;'>Family Name: </span>{3};", employeesAssignedTo.ToString(), locationName, category, familyName);                
        //        if(!string.IsNullOrEmpty(employeesAssignedTo.ToString()))
        //        {
        //            sb.AppendFormat("<span style='font-weight: bold;'>Assigned To: </span>{0} ", employeesAssignedTo.ToString());
        //        }
        //        if (!string.IsNullOrEmpty(locationName))
        //        {
        //            sb.AppendFormat("<span style='font-weight: bold;'>Location: </span>{0}; ", locationName);
        //        }
        //        if (!string.IsNullOrEmpty(category))
        //        {
        //            sb.AppendFormat("<span style='font-weight: bold;'>Category: </span>{0}; ", category);
        //        }
        //        if (!string.IsNullOrEmpty(familyName))
        //        {
        //            sb.AppendFormat("<span style='font-weight: bold;'>Family Name: </span>{0}", familyName);
        //        }
        //    }
        //    else
        //    {
        //        sb.Append(task.task_Subject);
        //    }
        //    return sb.ToString();
        //}
    }

    public class TaskDisplay
    {
        public Guid Id { get; set; }
        public tb_TaskCategory Category { get; set; }
        public string AssignedTo { get; set; }
        public string AssignedToNames { get; set; }
        public string FamilyName { get; set; }
        public tb_Location Location { get; set; }
        public string LocationName { get; set; }
        public string Subject { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}
