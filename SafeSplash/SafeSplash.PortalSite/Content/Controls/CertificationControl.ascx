﻿<%--<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>--%>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%--<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CertificationControl.ascx.cs" Inherits="SafeSplash.PortalSite.ControlTemplates.SS.CertificationControl" %>
<%@ Register Tagprefix="SafeSplashControls" Namespace="SafeSplash.ObjectModel.Controls.GridViewControl" Assembly="SafeSplash.ObjectModel, Version=1.0.0.0, Culture=neutral, PublicKeyToken=cdcfd765862d9189" %>

<SafeSplashControls:EmptyGridView ID="CertificationGrid" runat="server" AutoGenerateColumns="False" GridLines="Horizontal"
    EnableModelValidation="True" OnRowDeleting = "cert_RowDeleting" CssClass="igoogle igoogle-classic" ShowHeader="true" ShowHeaderWhenEmpty="true" ShowFooterWhenEmpty="false"
    DataKeyNames="Instcf_ID" OnRowDataBound = "Grid_RowDataBound" Width="100%">
    <RowStyle CssClass="data-row" Height="25px" />
    <AlternatingRowStyle CssClass="alt-data-row" />
    <HeaderStyle CssClass="header-row" HorizontalAlign="Left" Height="25px"/>
    <Columns>
        <asp:TemplateField Visible ="False">
            <ItemTemplate>
                <asp:Label ID="Instcf_ID" Text ='<%# Bind("Instcf_ID") %>' runat ="server" ></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
        <HeaderStyle BackColor="#EFE8FF" />
        <ItemStyle Width="30px" Height="20px" HorizontalAlign="Center" />
        <ItemTemplate>
            <asp:ImageButton ID = "ibtEdit" Width="15px" Height="16px" runat="server" ImageUrl="~/_layouts/SS/images/grid_edit.png" />
        </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="cert_Name" HeaderText="Certification" HeaderStyle-BackColor="#EFE8FF" HeaderStyle-Height="20px" HeaderStyle-Width="100px" ItemStyle-BorderStyle="None" ItemStyle-Width="50px" ItemStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
        <asp:BoundField DataField="Instcf_ObtainedDate" DataFormatString = "{0:d}" HeaderText="Date Obtained" HeaderStyle-BackColor="#EFE8FF" HeaderStyle-Height="20px" HeaderStyle-Width="100px" ItemStyle-BorderStyle="None" ItemStyle-Width="50px" ItemStyle-Height="20px" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
        <asp:BoundField DataField="Instcf_ExpirationDate" DataFormatString = "{0:d}" HeaderText="Expiration Date" HeaderStyle-BackColor="#EFE8FF" HeaderStyle-Width="100px" ItemStyle-BorderStyle="None" ItemStyle-Width="100px" ItemStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
        <asp:TemplateField>
        <HeaderStyle BackColor="#EFE8FF" />
        <ItemStyle Width="30px" Height="20px" HorizontalAlign="Center" />
        <ItemTemplate>
            <asp:ImageButton ID ="ibtDelete" Width="15px" Height="16px" runat="server" ImageUrl="~/_layouts/SS/images/grid_delete.png" OnClientClick='return confirm("Are you sure you want to delete this entry?");'  CommandName = "Delete" CausesValidation = "true" />
        </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</SafeSplashControls:EmptyGridView>