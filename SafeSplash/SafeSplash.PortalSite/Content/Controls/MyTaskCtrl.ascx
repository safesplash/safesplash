﻿<%--<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyTaskCtrl.ascx.cs" Inherits="SafeSplash.PortalSite.ControlTemplates.SS.MyTaskCtrl" %>
<style>
    .CategoryIndicator
    {
        border: 1px solid black;
        border-radius: 3px;
        width: 10px;
        height: 10px;
    }
    .contentCell
    {
        text-align: left;
        vertical-align: top; /* text-decoration: underline; */
    }
    
    .gridview
    {
        color: Black;
    }
    .gridview tr.alternating td
    {
        /* background-color: #F8F8F8; */ /* background-color: #F0F0F0; */
        background-color: #E8E8E8;
    }
    .viewDetailsButton
    {
        border-style: outset;
        border-width: 1px; /* background-color: #003399; */ /* color: #FFFFFF; */
        background-color: Gray;
        color: White;
        width: 60px;
    }
    
    .viewDetailsButton a
    {
        color: Yellow;
    }
    
    .linkButton
    {
        text-decoration: none;
        font: menu;
        display: inline-block;
        padding: 2px 8px;
        background: ButtonFace;
        color: ButtonText;
        border-style: solid;
        border-width: 2px;
        border-color: ButtonHighlight ButtonShadow ButtonShadow ButtonHighlight;
    }
    .linkButton:active
    {
        border-color: ButtonShadow ButtonHighlight ButtonHighlight ButtonShadow;
    }
</style>
<%--<table style="width: 100%;">
    <tr>
        <td>--%>
        <div>
        <fieldset class="fieldSetWithBorder">
<legend>Criteria</legend>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 18%; text-align: left;">
                        <span style="font-weight: bold;">Sort By:</span>
                    </td>
                    <td style="width: 17%; text-align: left;">
                        <span style="font-weight: bold;">Filter by Category:</span>
                    </td>
                    <td style="width: 35%; text-align: left;">
                        <span style="font-weight: bold;">Filter by Location:</span>
                    </td>
                    <td style="width: 15%; text-align: left;">
                        <span style="font-weight: bold;">Filter by Status:</span>
                    </td>
                    <td style="width: 15%; text-align: left;">
                        <span style="font-weight: bold;">Filter by Recipient:</span>
                    </td>
                </tr>
                <tr>
                    <td style="width: 18%; text-align: left;">
                        <asp:DropDownList ID="ddlSortBy" runat="server" EnableViewState="true" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlSortBy_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 17%; text-align: left;">
                        <asp:DropDownList ID="ddlFilterByCategory" runat="server" EnableViewState="true"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlFilterByCategory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 35%; text-align: left;">
                        <asp:DropDownList ID="ddlFilterByLocation" runat="server" EnableViewState="true"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlFilterByLocation_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 15%; text-align: left;">
                        <asp:DropDownList ID="ddlFilterByStatus" runat="server" EnableViewState="true" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlFilterByStatus_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 15%; text-align: left;">
                        <asp:DropDownList ID="ddlFilterByRecipient" runat="server" EnableViewState="true"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlFilterByRecipient_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            </fieldset>
            </div>
       <%-- </td>
    </tr>
</table>--%>
<div id="LegendDiv" runat="server" style="width: 100%; margin-top: 5px; margin-bottom: 5px;">
    <fieldset class="fieldSetWithBorder">
<legend>Task Categories Legend</legend>
    <div id="LegendContentDiv" runat="server">
    </div>
    </fieldset>

    <%--<div id="FilterCountDiv" style="float: right; vertical-align: bottom;">
        <span style="font-weight: bold; vertical-align: bottom;">Filtered Task Count: </span>
        <asp:Label ID="TaskCountLabel" runat="server"></asp:Label>
    </div>--%>
</div>
<%--<hr />--%>

<fieldset class="fieldSetWithBorder">
<legend><asp:Label ID="TaskCountLabel" runat="server" style="font-weight: bold;"></asp:Label></legend>
<asp:GridView ID="myTaskGrid" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
    ShowHeader="true" AutoGenerateColumns="False" BorderStyle="None" GridLines="None"
    CssClass="gridview" AlternatingRowStyle-CssClass="alternating" OnRowDataBound="TasksGrid_RowDataBound"
    AllowSorting="True" OnSorting="TaskList_Sorting">
    <Columns>
        <asp:TemplateField HeaderText="Category" SortExpression="Category">
            <ItemStyle HorizontalAlign="Center" Width="10%" />
            <ItemTemplate>
                <div id="TaskCategoryIndicatorDiv" runat="server" class="CategoryIndicator" enableviewstate="true"
                    style="vertical-align: middle;">
                </div>
                <asp:Label ID="lblTaskCategoryDescription" runat="server" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemStyle Width="2%" />
            <ItemTemplate>
                <span></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField HeaderText="Assigned To" DataField="AssignedToNames" HeaderStyle-Width="15%"
            ItemStyle-Width="15%" />
        <asp:TemplateField>
            <ItemStyle Width="2%" />
            <ItemTemplate>
                <span></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField HeaderText="Family Name" DataField="FamilyName" HeaderStyle-Width="10%"
            ItemStyle-Width="10%" />
        <asp:TemplateField>
            <ItemStyle Width="2%" />
            <ItemTemplate>
                <span></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField HeaderText="Location" DataField="LocationName" HeaderStyle-Width="13%"
            ItemStyle-Width="13%" />
        <asp:BoundField HeaderText="Subject" DataField="Subject" HeaderStyle-Width="15%"
            ItemStyle-Width="15%" />
        <asp:TemplateField>
            <ItemStyle Width="2%" />
            <ItemTemplate>
                <span></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField HeaderText="Created Date" DataField="CreatedDate" HeaderStyle-Width="20%"
            ItemStyle-Width="20%" />
        <asp:HyperLinkField Text="View Details" DataNavigateUrlFields="Id" DataNavigateUrlFormatString="/Content/ss/TaskDetails.aspx?ID={0}"
            HeaderStyle-Width="10%" ItemStyle-Width="10%" ControlStyle-CssClass="linkButton"
            ItemStyle-HorizontalAlign="Left" />
        <%--<asp:TemplateField HeaderText="Assigned To">
            <ItemStyle HorizontalAlign="Left" Width="18%" />
            <ItemTemplate>
                <asp:Label ID="lblAssignedTo" runat="server" />
            </ItemTemplate>
        </asp:TemplateField>--%>
        <%-- <asp:TemplateField>
            <ItemStyle Width="2%" />
            <ItemTemplate>
                <span></span>
            </ItemTemplate>
        </asp:TemplateField>--%>
        <%--<asp:TemplateField HeaderText="Location" SortExpression="Location">
            <ItemStyle HorizontalAlign="Left" Width="15%" />
            <ItemTemplate>
                <asp:Label ID="lblLocation" runat="server" />
            </ItemTemplate>
        </asp:TemplateField>--%>
        <%-- <asp:TemplateField HeaderText="Subject" SortExpression="Subject">
            <ItemStyle HorizontalAlign="Left" Width="17%" />
            <ItemTemplate>
                <asp:HyperLink ID="hyperlinkSubject" runat="server" />
            </ItemTemplate>
        </asp:TemplateField>--%>
        <%--<asp:TemplateField HeaderText="Created" SortExpression="CreatedDate">
            <ItemStyle HorizontalAlign="Left" Width="15%" />
            <ItemTemplate>
                <asp:Label ID="litCreated" runat="server" Text='Place_Author' />
            </ItemTemplate>
        </asp:TemplateField>--%>
        <%--<asp:TemplateField HeaderText="By">
            <ItemStyle HorizontalAlign="Left" Width="10%" />
            <ItemTemplate>
                <asp:Label ID="litCreator" runat="server" Text='Place_Author' />
            </ItemTemplate>
        </asp:TemplateField>--%>
        <%--<asp:TemplateField HeaderText="Status">
            <ItemStyle HorizontalAlign="Left" Width="10%" />
            <ItemTemplate>
                <asp:Label ID="lblStatus" runat="server" />
            </ItemTemplate>
        </asp:TemplateField>--%>
        <%--<asp:TemplateField HeaderText="Last Modified" SortExpression="LastModifiedDate">
            <ItemStyle HorizontalAlign="Left" Width="15%" />
            <ItemTemplate>
                <asp:Label ID="lblModified" runat="server" />
            </ItemTemplate>
        </asp:TemplateField>--%>
    </Columns>
</asp:GridView>
</fieldset>
<%--<asp:GridView ID="myTaskGrid" runat="server" Width="100%" ShowHeader="False" AutoGenerateColumns="False" BorderStyle="None" GridLines="None" OnRowDataBound = "TasksGrid_RowDataBound">
    <Columns>
        <asp:TemplateField>
        <ItemStyle HorizontalAlign="Left" />
        <ItemTemplate>
        <table style="width: 100%; font-size:9pt; font-family: Arial, Helvetica, sans-serif;">
        <tr>
            <td align="center" style="width:25px">
                <asp:Image ID="imgStatus" runat="server" />
            </td>
            <td>
                <span id="Subject">
                <asp:HyperLink ID="litSubject" runat="server" Text='<%# Bind("task_Subject") %>'/>
                </span>
                <strong>
                    By 
                </strong>
                <span id="Author">
                    <asp:Label ID="litCreator" runat="server" Text='Place_Author' />
                </span> 
                &nbsp; 
                <span id="EditedDate" style="font-style: italic">
                [<asp:Label ID="litCreated" runat="server" Text='Place_Author' />]
                </span>
                </td>
			</tr>
        </table>
        </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>--%>