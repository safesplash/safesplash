﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using AKMII.SafeSplash.BLL;
using AKMII.SafeSplash.Entity;

namespace SafeSplash.PortalSite.ControlTemplates.SS
{
    public partial class CertificationControl : UserControl
    {
        public string UserID
        {
            get
            {
                return ViewState["UserID"].ToString();
            }
            set
            {
                ViewState["UserID"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(UserID))
                {
                    BindData();
                }
            }
        }

        public void BindData()
        {
            InstructorCertificationBll instCertInfoBll = new InstructorCertificationBll();
            System.Collections.Generic.List<vw_InstructorCertInfo> instCertInfo;
            instCertInfo = instCertInfoBll.GetByEmployeeID(new Guid(UserID));
            this.CertificationGrid.DataSource = instCertInfo;
            this.CertificationGrid.DataBind();
        }

        protected virtual void Grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label Instcf_ID = e.Row.FindControl("Instcf_ID") as Label;
                string strInstcfID = Instcf_ID.Text;
                ImageButton ibtEdit = e.Row.FindControl("ibtEdit") as ImageButton;
                ibtEdit.OnClientClick = "EditCert('" + strInstcfID + "'); return false;";

                String strExpiredDate = e.Row.Cells[4].Text;
                if (!String.IsNullOrEmpty(strExpiredDate))
                {

                    DateTime checkDate = DateTime.Today.AddDays(-30);
                    DateTime dtExpired = DateTime.Parse(strExpiredDate);
                    if (DateTime.Today < dtExpired.AddMonths(3))
                    {
                        e.Row.Cells[4].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFAA95");
                    }
                }
            }
        }

        protected void cert_RowDeleting(Object sender, GridViewDeleteEventArgs e)
        {
            InstructorCertificationBll instCertInfoBll = new InstructorCertificationBll();
            Label Instcf_ID = CertificationGrid.Rows[e.RowIndex].FindControl("Instcf_ID") as Label;
            Guid certID = new Guid(Instcf_ID.Text);
            instCertInfoBll.Delete(certID);
            BindData();
        }
    }
}
