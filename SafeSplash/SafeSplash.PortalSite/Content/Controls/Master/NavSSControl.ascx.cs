﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using AKMII.SafeSplash.BLL;
using System.Web;

namespace SafeSplash.PortalSite.ControlTemplates.SS
{
    public partial class NavSSControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //string userName = SPContext.Current.Web.CurrentUser.Name;
            //string[] allRoles = System.Web.Security.Roles.GetAllRoles();
            //string[] userRoles = System.Web.Security.Roles.GetRolesForUser();

            //string[] agudmundsonUserRoles = System.Web.Security.Roles.GetRolesForUser(string.Format("0#.f|aspnetsqlmembershipprovider|{0}", "agudmundson"));
            //string[] ApayneUserRoles = System.Web.Security.Roles.GetRolesForUser(string.Format("0#.f|aspnetsqlmembershipprovider|{0}", "Apayne"));
            //int i = 0;

            //if (HttpContext.Current.User.IsInRole("CorpAdmin"))
            //{
            //    // my action
            //    int i = 0;
            //}

            //string userRole = string.Empty;

            //if (HttpContext.Current.User.IsInRole("CorpAdmin"))
            //{
            //    userRole = "CorpAdmin";
            //}

            //if (HttpContext.Current.User.IsInRole("Client"))
            //{
            //    userRole = "Client";
            //}

            //if (HttpContext.Current.User.IsInRole("Default"))
            //{
            //    userRole = "Default";
            //}

            //if (HttpContext.Current.User.IsInRole("FacilityManager"))
            //{
            //    userRole = "FacilityManager";
            //}

            //if (HttpContext.Current.User.IsInRole("FacilityUser"))
            //{
            //    userRole = "FacilityUser";
            //}

            //if (HttpContext.Current.User.IsInRole("Instructor"))
            //{
            //    userRole = "Instructor";
            //}

            //string UserName = SPContext.Current.Web.CurrentUser.Name.ToLower();
            string UserName = "fbaadmin"; // SPContext.Current.Web.CurrentUser.Name.ToLower();
            //SPGroup groupCorpAdmin = SPContext.Current.Web.Groups[ObjectModel.UserType.CorpAdmin.ToString()];
            //if (groupCorpAdmin.ContainsCurrentUser)
            //{
                this.ManagementTab.Visible = true;
                this.ClientRegisterItem.Visible = true;
                this.ClientInfoItem.Visible = true;
                this.WithdrawalItem.Visible = true;
                this.ClassManagmentItem.Visible = true;
                this.StaffListItem.Visible = true;
                this.ManageEmployeeGroupsItem.Visible = true;
                this.ManageUserAccountsItem.Visible = true;
                this.SystemSettingsItem.Visible = true;
                this.Reports.Visible = true;
                
                //if (EmployeeBll.EmployeeAccountsWithRestrictedPermissions().Contains(UserName))
                //{
                   this.StaffListItem.Visible = true; // this.ManageUserAccountsItem.Visible = this.SystemSettingsItem.Visible = this.Reports.Visible = false;
                //}
                                
                this.ManageMarketSegments.Visible = EmployeeBll.EmployeeAccountsWhoCanManageMarketSegments().Contains(UserName) ? true : false;
                this.ManageInstructorSegments.Visible = EmployeeBll.EmployeeAccountsWhoCanManageInstructorSegments().Contains(UserName) ? true : false;

                return;
            //}
            //else
            //{
            //    SPGroup groupFacilityManager = SPContext.Current.Web.Groups[ObjectModel.UserType.FacilityManager.ToString()];
            //    if (groupFacilityManager.ContainsCurrentUser)
            //    {
            //        this.ManagementTab.Visible = true;
            //        this.ClientRegisterItem.Visible = true;
            //        this.ClientInfoItem.Visible = true;
            //        this.WithdrawalItem.Visible = true;
            //        this.ClassManagmentItem.Visible = true;
            //        this.StaffListItem.Visible = true;
            //        this.ManageUserAccountsItem.Visible = false;
            //        this.Reports.Visible = true;
            //        this.SystemSettingsItem.Visible = false;
            //        return;
            //    }
            //    else
            //    {
            //        SPGroup groupFacilityUser = SPContext.Current.Web.Groups[ObjectModel.UserType.FacilityUser.ToString()];
            //        if (groupFacilityUser.ContainsCurrentUser)
            //        {
            //            this.ManagementTab.Visible = true;
            //            this.ClassManagmentItem.Visible = true;
            //            //this.ClientRegisterItem.Visible = false;
            //            //this.ClientInfoItem.Visible = false;
            //            this.WithdrawalItem.Visible = false;
            //            //this.ClassManagmentItem.Visible = false;
            //            this.StaffListItem.Visible = false;
            //            this.ManageUserAccountsItem.Visible = false;
            //            this.SystemSettingsItem.Visible = false;
            //            this.Reports.Visible = false;
            //            return;
            //        }
            //        else
            //        {
            //            SPGroup groupInstructor = SPContext.Current.Web.Groups[ObjectModel.UserType.Instructor.ToString()];
            //            if (groupInstructor.ContainsCurrentUser)
            //            {
            //                this.ManagementTab.Visible = false;
            //                this.ClientRegisterItem.Visible = false;
            //                this.ClientInfoItem.Visible = false;
            //                this.WithdrawalItem.Visible = false;
            //                this.ClassManagmentItem.Visible = false;
            //                this.StaffListItem.Visible = false;
            //                this.ManageUserAccountsItem.Visible = false;
            //                this.SystemSettingsItem.Visible = false;
            //                this.Reports.Visible = false;
            //                return;
            //            }
            //            else
            //            {
            //                this.UserDefault.Visible = false;
            //                this.MySchedule.Visible = false;
            //                this.MyTasks.Visible = false;
            //                this.MyCertificate.Visible = false;
            //                this.ClassAttedance.Visible = false;
            //                this.ClassSchedule.Visible = false;
            //                this.ManagementTab.Visible = false;
            //                this.ClientRegisterItem.Visible = false;
            //                this.ClientInfoItem.Visible = false;
            //                this.WithdrawalItem.Visible = false;
            //                this.ClassManagmentItem.Visible = false;
            //                this.StaffListItem.Visible = false;
            //                this.ManageUserAccountsItem.Visible = false;
            //                this.SystemSettingsItem.Visible = false;
            //                this.Reports.Visible = false;
            //                return;
            //            }
            //        }
            //    }
            //}
        }
    }
}
