﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaskPane.ascx.cs" Inherits="SafeSplash.PortalSite.ControlTemplates.SS.TaskPane" %>

<div style="font-size: 8pt; font-family: Arial, Helvetica, sans-serif; width: 100%; font-style: normal; color: #000000; font-weight: normal;" >
    <asp:Label ID="lblTitleFront" runat="server"></asp:Label>
    <strong>
        <asp:Label ID="lblTasksNumber" ForeColor="AliceBlue" runat="server"></asp:Label>
    </strong>
    <asp:Label ID="lblTitleEnd" runat="server"></asp:Label>
    <input id="btnNewTask" name="btnNewTask" type="button" value="Create a Task" onclick="createTask();" style="border-style: outset; border-width: 1px; background-color: #003399; color: #FFFFFF; width: 100px; margin-left: 5px;" />
</div>
