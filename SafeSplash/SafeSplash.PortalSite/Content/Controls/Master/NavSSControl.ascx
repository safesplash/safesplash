﻿<%--<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>--%>
<%--<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>--%>
<%--<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>--%>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%--<%@ Import Namespace="Microsoft.SharePoint" %> --%>
<%--<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>--%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavSSControl.ascx.cs" Inherits="SafeSplash.PortalSite.ControlTemplates.SS.NavSSControl" %>
<link type="text/css" href="../../../Layouts/Shares/css/styles.css" rel="stylesheet" />
<div id="sidebar">
    <dl>
        <dt><asp:image ImageUrl="~/Layouts/Shares/Images/sidenav-life-raft.png" runat="server" alt="liferaft" />My WorkSpace</dt>
        <dd id="MyTasks" runat="server"><a href="/Content/SS/MyTasks.aspx">My Tasks</a></dd>
        <%--<dd id="UserDefault" runat="server"><a href="/_layouts/ss/UserDefault.aspx">Home</a></dd>--%>
        <dd id="MySchedule" runat="server"><a href="/_layouts/ss/MySchedule.aspx">My Schedule</a></dd>
        <dd id="MyCertificate" runat="server"><a href="/_layouts/ss/MyCertificate.aspx">Certificate Status</a></dd>
        <dd id="ClassAttedance" runat="server"><a href="/_layouts/ss/ClassAttendance.aspx">Class Attendance</a></dd>
        <dd id="ClassSchedule" runat="server"><a href="/_layouts/ss/ClassSchedule.aspx">Class Schedule</a></dd>
    </dl>
    <dl id="dlManagement">
        <dt id="ManagementTab" runat="server"><asp:image ImageUrl="~/Layouts/Shares/Images/sidenav-life-raft.png" runat="server" alt="liferaft" />Management</dt>
        <dd id="ClientRegisterItem" runat="server"><a href="/_layouts/ss/ClientRegister.aspx">Client Registrations</a></dd>
        <dd id="ClientInfoItem" runat="server"><a href="/_layouts/ss/ClientsList.aspx">Client Information</a></dd>
        <dd id="WithdrawalItem" runat="server"><a href="/_layouts/ss/WithdrawalRequests.aspx">Withdrawal Requests</a></dd>
        <dd id="ClassManagmentItem" runat="server"><a href="/_layouts/ss/classlist.aspx">Class Management</a></dd>
        <dd id="StaffListItem" runat="server"><a href= "/Content/SS/StaffList.aspx">Employee Management</a></dd>
        <dd id="ManageEmployeeGroupsItem" runat="server" visible="false"><a href="/_layouts/ss/EmployeeGroups.aspx">Employee Groups</a></dd>
        <dd id="ManageUserAccountsItem" runat="server"><a href="/_layouts/SS/ManageUserAccounts.aspx">Manage Accounts</a></dd>
        <dd id="Reports" runat="server"><a href="/_layouts/ss/ReportsList.aspx">Reports</a></dd>
        <dd id="ManageLocationPricingItem" runat="server"><a href="/_layouts/ss/LocationPricing.aspx">Location Pricing</a></dd>
        <dd id="ManageLocationClassPricingItem" runat="server"><a href="/_layouts/ss/LocationClassPricing.aspx">Location Class Pricing</a></dd>
        <dd id="ManageMarketSegments" runat="server" visible="false"><a href="/_layouts/ss/MarketSegments.aspx">Manage Market Segments</a></dd>
        <dd id="ManageInstructorSegments" runat="server" visible="false"><a href="/_layouts/ss/InstructorSegments.aspx">Manage Instructor Segments</a></dd>
        <dd id="SystemSettingsItem" runat="server"><a href="/_layouts/config/settings.aspx">System Settings</a></dd>
    </dl>
</div>