﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using AKMII.SafeSplash.Entity;
using AKMII.SafeSplash.BLL;
using System.Text;


using Microsoft.AspNet.Identity;



namespace SafeSplash.PortalSite.ControlTemplates.SS
{
    public partial class TaskPane : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // string UserName =  SPContext.Current.Web.CurrentUser.Name;
                // UserBll userBll = new UserBll();
                Guid userID = new Guid(Context.User.Identity.GetUserId()); // userBll.GetUserIDByUserName(UserName);
                EmployeeBll employeeBll = new EmployeeBll();                
                tb_Employee employee = employeeBll.GetByUserID(userID);
                if (employee != null)
                {
                    TaskBll taskBll = new TaskBll();
                    List<tb_Task> task = taskBll.GetOpenTasksForEmployee(employee.empl_ID.ToString());
                    for (int i = 0; i < task.Count; i++)
                    {
                        if (task[i].task_StartDate > DateTime.Today)
                        {
                            task.Remove(task[i]);
                            i = 0;
                        }
                    }
                    int iCount = 0;
                    if (task != null)
                    {
                        iCount = task.Count;
                    }
                    
                    this.lblTitleFront.Text = "You have ";
                    this.lblTasksNumber.Text = iCount.ToString();
                    this.lblTasksNumber.ForeColor = System.Drawing.Color.Blue;
                    this.lblTitleEnd.Text = " open tasks.";
                }
            }

            //SetShoppingCartContents();
        }

        [System.Web.Services.WebMethod]
        public static string UpdateShoppingCart(string val)
        {
            SetShoppingCartContents();
            return val;
        }

        private static void SetShoppingCartContents()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table style='width: 100%;'>");
            sb.Append("<thead>");
            sb.Append("<tr><th>Location</th><th>Level</th><th>Type</th><th>Day</th><th>Time</th><th></th></tr>");
            sb.Append("</thead>");
            sb.Append("<tbody>");
            ///RAB List<ShoppingCartClass> classesInCart = HttpContext.Current.Session["ShoppingCartClasses"] != null ? (List<ShoppingCartClass>)HttpContext.Current.Session["ShoppingCartClasses"] : new List<ShoppingCartClass>();

            ///RAB
            //foreach (var cls in classesInCart)
            //{
            //    sb.Append("<tr>");
            //    sb.AppendFormat("<td>{0}</td>", cls.LocationId);
            //    sb.AppendFormat("<td>{0}</td>", cls.ClassLevelId);
            //    sb.AppendFormat("<td>{0}</td>", cls.ClassTypeId);
            //    sb.AppendFormat("<td>{0}</td>", cls.Day);
            //    sb.AppendFormat("<td>{0}</td>", "");
            //    sb.Append("<td><a href='http://www.yahoo.com'>Class Schedule</a></td>");
            //    sb.Append("</tr>");
            //}

            //sb.Append("<tr>");
            //sb.Append("<td>Denver, CO</td>");
            //sb.Append("<td>Adult 1</td>");
            //sb.Append("<td>Group</td>");
            //sb.Append("<td>Monday</td>");
            //sb.Append("<td>8:30AM - 9:00 AM</td>");
            //sb.Append("<td><a href='http://www.yahoo.com'>Class Schedule</a></td>");
            //sb.Append("</tr>");
            //sb.Append("<tr>");
            //sb.Append("<tr>");
            //sb.Append("<td>Lone Tree, CO</td>");
            //sb.Append("<td>Adult 2</td>");
            //sb.Append("<td>Private</td>");
            //sb.Append("<td>Thursday</td>");
            //sb.Append("<td>11:00AM - 11:30 AM</td>");
            //sb.Append("<td><a href='http://www.yahoo.com'>Class Schedule</a></td>");
            //sb.Append("</tr>");
            //sb.Append("<tr>");


            sb.Append("</tbody>");
            sb.Append("</table>");

            //ShoppingCartDiv.InnerHtml = sb.ToString();
        }
    }
}