﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.ClassSchedule
{
    [Serializable]
    public class ClassScheduleClassStudent
    {
        public DateTime? StudentJoinDate { get; set; }
        public DateTime? StudentWithdrawalDate { get; set; }
        public bool? StudentIsWithdrawn { get; set; }
        public string StudentFirstName { get; set; }
        public string StudentLastName { get; set; }
        public DateTime StudentDateOfBirth { get; set; }
    }
}
