﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.ClassSchedule;
using SafeSplash.DataAccess.NHibernate;

namespace SafeSplash.Service.ClassSchedule
{
    [Serializable]
    public class ClassScheduleService : IClassScheduleService
    {
        private IClassScheduleDataAccess Repository = new ClassScheduleDataAccess();

        //public IEnumerable<ClassScheduleClass> GetClasses(Guid locationId, DateTime asOfDate, string dayOfWeek)
        //{
        //    IEnumerable<ClassScheduleDataObject> classDataObjects = Repository.GetClassesForClassSchedule(locationId, asOfDate, dayOfWeek);
        //    return classDataObjects.Select(classDataObject => new ClassScheduleClass
        //    {
        //        ActiveClassId = classDataObject.ActiveClassId,
        //        LocationId = classDataObject.LocationId,
        //        ClassId = classDataObject.ClassId,
        //        ClassName = classDataObject.ClassName,
        //        LevelId = classDataObject.LevelId,
        //        LevelName = classDataObject.LevelName,
        //        ClassStartDate = classDataObject.ClassStartDate,
        //        ClassEndDate = classDataObject.ClassEndDate,
        //        ClassDate = classDataObject.ClassDate,
        //        ClassStartTime = classDataObject.ClassStartTime,
        //        ClassEndTime = classDataObject.ClassEndTime,
        //        ClassPrice = classDataObject.ClassPrice,
        //        InstructorId = classDataObject.InstructorId,
        //        InstructorFirstName = classDataObject.InstructorFirstName,
        //        InstructorLastName = classDataObject.InstructorLastName,
        //        InstructorPrimarySegment = classDataObject.InstructorPrimarySegment,
        //        InstructorSecondarySegment = classDataObject.InstructorSecondarySegment,
        //        StudentCapacity = classDataObject.StudentCapacity,
        //        SpotsRemaining = classDataObject.SpotsRemaining,
        //        AverageStudentAge = classDataObject.AverageStudentAge
        //    }).ToList();
        //}

        public IEnumerable<ClassScheduleClass> GetClasses(Guid locationId, DateTime asOfDate, string dayOfWeek, IEnumerable<Guid> classTypeIds, IEnumerable<Guid> levelIds, Guid instructorId, string instructorSegment)
        {
            IEnumerable<ClassScheduleDataObject> classDataObjects = Repository.GetClassesForClassSchedule(locationId, asOfDate, dayOfWeek);
            if (classTypeIds != null && classTypeIds.Count() > 0)
            {
                classDataObjects = classDataObjects.Where(c => classTypeIds.Contains(c.ClassId));
            }
            if (levelIds != null && levelIds.Count() > 0)
            {
                classDataObjects = classDataObjects.Where(c => levelIds.Contains(c.LevelId));
            }
            if (instructorId != null && !Guid.Empty.Equals(instructorId))
            {
                classDataObjects = classDataObjects.Where(c => c.InstructorId == instructorId);
            }
            if (!string.IsNullOrEmpty(instructorSegment))
            {
                classDataObjects = classDataObjects.Where(c => c.InstructorPrimarySegment == instructorSegment || c.InstructorSecondarySegment == instructorSegment);
            }

            return classDataObjects.Select(classDataObject => new ClassScheduleClass
            {
                ActiveClassId = classDataObject.ActiveClassId,
                LocationId = classDataObject.LocationId,
                ClassId = classDataObject.ClassId,
                ClassName = classDataObject.ClassName,
                LevelId = classDataObject.LevelId,
                LevelName = classDataObject.LevelName,
                ClassStartDate = classDataObject.ClassStartDate,
                ClassEndDate = classDataObject.ClassEndDate,
                ClassDate = classDataObject.ClassDate,
                ClassStartTime = classDataObject.ClassStartTime,
                ClassEndTime = classDataObject.ClassEndTime,
                ClassPrice = classDataObject.ClassPrice,
                InstructorId = classDataObject.InstructorId,
                InstructorFirstName = classDataObject.InstructorFirstName,
                InstructorLastName = classDataObject.InstructorLastName,
                InstructorPrimarySegment = classDataObject.InstructorPrimarySegment,
                InstructorSecondarySegment = classDataObject.InstructorSecondarySegment,
                StudentCapacity = classDataObject.StudentCapacity,
                SpotsRemaining = classDataObject.SpotsRemaining,
                AverageStudentAge = classDataObject.AverageStudentAge
            }).ToList();
        }
    }
}
