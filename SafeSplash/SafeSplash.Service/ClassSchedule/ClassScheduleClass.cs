﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.ClassSchedule
{
    [Serializable]
    public class ClassScheduleClass
    {
        public Guid ActiveClassId { get; set; }
        public Guid ClassId { get; set; }
        public Guid LocationId { get; set; }
        public Guid LevelId { get; set; }
        public string LevelName { get; set; }
        public string ClassName { get; set; }
        public DateTime ClassStartDate { get; set; }
        public DateTime? ClassEndDate { get; set; }
        public DateTime ClassDate { get; set; }
        public TimeSpan ClassStartTime { get; set; }
        public TimeSpan ClassEndTime { get; set; }
        public int StudentCapacity { get; set; }
        public int SpotsRemaining { get; set; }
        public decimal AverageStudentAge { get; set; }
        public Guid InstructorId { get; set; }
        public string InstructorFirstName { get; set; }
        public string InstructorLastName { get; set; }
        public string InstructorPrimarySegment { get; set; }
        public string InstructorSecondarySegment { get; set; }
        public decimal ClassPrice { get; set; }
    }
}
