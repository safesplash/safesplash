﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.ClassSchedule
{
    public interface IClassScheduleService
    {
        //IEnumerable<ClassScheduleClass> GetClasses(DateTime beginDate, DateTime endDate, Guid locationId, string dayOfWeek);
        //IEnumerable<ClassScheduleClass> GetClasses(Guid locationId, DateTime asOfDate, string dayOfWeek);
        IEnumerable<ClassScheduleClass> GetClasses(Guid locationId, DateTime asOfDate, string dayOfWeek, IEnumerable<Guid> classTypeIds, IEnumerable<Guid> levelIds, Guid instructorId, string instructorSegment);
    }
}
