﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace SafeSplash.Service.Reports
{
    public class Month
    {
        public int Number { get; set; }
        public string Name { get; set; }

        public Month(int number)
        {
            Number = number;
            Name = DateTimeFormatInfo.CurrentInfo.GetMonthName(number);
        }

        public static IEnumerable<Month> GetAllMonths()
        {
            List<Month> months = new List<Month>();
            for (int i = 1; i < 13; i++)
            {
                months.Add(new Month(i));
            }

            return months;
        }
    }
}
