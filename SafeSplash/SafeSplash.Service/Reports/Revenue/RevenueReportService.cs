﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.NHibernate;

namespace SafeSplash.Service.Reports.Revenue
{
    public interface IRevenueReportService
    {
        IEnumerable<RevenueClientEnrollment> GetEnrollmentForRevenueReport(Guid locationId, DateTime fromDate, DateTime toDate);

    }

    [Serializable]
    public class RevenueReportService : IRevenueReportService
    {
        public IEnumerable<RevenueClientEnrollment> GetEnrollmentForRevenueReport(Guid locationId, DateTime fromDate, DateTime toDate)
        {
            List<RevenueClientEnrollment> enrollment = new List<RevenueClientEnrollment>();
            IClientEnrollmentDataAccess repository = new ClientEnrollmentDataAccess();
            IEnumerable<RevenueEnrollmentDataObject> dataObjects = repository.GetClientEnrollmentForRevenueReport(locationId, fromDate, toDate);
            foreach (var d in dataObjects)
            {
                enrollment.Add(new RevenueClientEnrollment()
                {
                    ClientId = d.ClientId,
                    ClientLastName = d.ClientLastName,
                    ClientFirstName = d.ClientFirstName,
                    ClientBillingAchId = d.ClientBillingAchId,
                    StudentId = d.StudentId,
                    StudentLastName = d.StudentLastName,
                    StudentFirstName = d.StudentFirstName,
                    StudentJoinDate = d.StudentJoinDate,
                    StudentIsWithdrawn = d.StudentIsWithdrawn,
                    StudentWithdrawalDate = d.StudentWithdrawalDate,
                    StudentEarliestEnrolledDate = d.StudentEarliestEnrolledDate,
                    ClassDay = d.ClassDay,
                    ClassId = d.ClassId,
                    ClassTypeId = d.ClassTypeId,
                    LocationId = d.LocationId
                });
            }

            return enrollment;
        }
    }
}
