﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.Reports.Revenue
{
    [Serializable]
    public class RevenueClientEnrollment
    {
        public Guid ClientId { get; set; }
        public string ClientFirstName { get; set; }
        public string ClientLastName { get; set; }
        public Guid StudentId { get; set; }
        public string StudentFirstName { get; set; }
        public string StudentLastName { get; set; }
        public DateTime StudentJoinDate { get; set; }
        public bool? StudentIsWithdrawn { get; set; }
        public DateTime? StudentWithdrawalDate { get; set; }
        public DateTime StudentEarliestEnrolledDate { get; set; }
        public DateTime ClassStartDate { get; set; }
        public DateTime ClassEndDate { get; set; }
        public Guid ClassId { get; set; }
        public Guid ClassTypeId { get; set; }
        public string ClassDay { get; set; }
        public Guid LocationId { get; set; }
        public int? ClientBillingAchId { get; set; }        
    }
}
