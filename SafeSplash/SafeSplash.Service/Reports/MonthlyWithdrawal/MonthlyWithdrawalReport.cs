﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.Service.Withdrawal;

namespace SafeSplash.Service.Reports.MonthlyWithdrawal
{
    public interface IMonthlyWithdrawalReport
    {
        Guid LocationId { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        int SpotsFilled { get; set; }
        IEnumerable<IClassWithdrawal> Withdrawals { get; set; }
    }

    public class MonthlyWithdrawalReport : IMonthlyWithdrawalReport
    {
        public Guid LocationId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int SpotsFilled { get; set; }
        public IEnumerable<IClassWithdrawal> Withdrawals { get; set; }
    }
}
