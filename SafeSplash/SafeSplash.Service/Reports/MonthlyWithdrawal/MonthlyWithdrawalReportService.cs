﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.Service.Withdrawal;
using SafeSplash.Service.Reports.MonthlyEnrollment;

namespace SafeSplash.Service.Reports.MonthlyWithdrawal
{
    public interface IMonthlyWithdrawalReportService
    {
        IMonthlyWithdrawalReport GetMonthlyWithdrawalReport(Guid locationId, DateTime fromDate, DateTime toDate);
        IEnumerable<IWithdrawalReason> GetReasonsToIncludeInReport();
    }

    public class MonthlyWithdrawalReportService : IMonthlyWithdrawalReportService
    {
        IWithdrawalService withdrawalService = new WithdrawalService();
        IMonthlyEnrollmentReportService monthlyEnrollmentReportService = new MonthlyEnrollmentReportService();

        public IMonthlyWithdrawalReport GetMonthlyWithdrawalReport(Guid locationId, DateTime fromDate, DateTime toDate)
        {
            IMonthlyWithdrawalReport report = new MonthlyWithdrawalReport();
            report.LocationId = locationId;
            report.StartDate = fromDate;
            report.EndDate = toDate;
            //report.Withdrawals = withdrawalService.GetWithdrawals(locationId, fromDate, toDate);
            IEnumerable<IClassWithdrawal> withdrawals = withdrawalService.GetWithdrawals(locationId, fromDate, toDate);
            //var list = from w in withdrawals
            //           where GetReasonsToIncludeInReport().ToList<IWithdrawalReason>().Contains(w.Reason)
            //           select w;
            //GetReasonsToIncludeInReport().ToList<IWithdrawalReason>().Contains(
            List<string> reasonsToInclude = GetReasonsToIncludeInReport().Select(r => r.Name).ToList<string>();
            //report.Withdrawals = withdrawals.Where(reasonsToInclude.Contains(w => w
            //var list = from w in withdrawals
            //           where reasonsToInclude.Contains(w.Reason)
            //           select w;
            //report.Withdrawals = list.ToList<IClassWithdrawal>();
            report.Withdrawals = (from w in withdrawals
                                  where reasonsToInclude.Contains(w.Reason)
                                  select w).ToList<IClassWithdrawal>();
            report.SpotsFilled = (int)Math.Round(monthlyEnrollmentReportService.GetEnrollmentReport(fromDate, toDate, locationId).SpotsFilled);
            return report;

            //var list = from w in withdrawalService.GetWithdrawals(fromDate, toDate, location)
            //           where !SafeSplashReports.BusinessObjects.Withdrawal.ReasonsToExclude().Contains(w.Reason)
            //           select w;

            //report.Withdrawals = list.ToList<Withdrawal.Withdrawal>();
        }


        public IEnumerable<IWithdrawalReason> GetReasonsToIncludeInReport()
        {
            IWithdrawalService service = new WithdrawalService();
            List<string> reasonsToExclude = new List<string>()
                {
                    "Page Layouts",
                    "Completed",
                    "Transfer",
                    "Changed Class",
                    "Change Classes",
                    "Change Class"
                };
            IEnumerable<IWithdrawalReason> allReasons = service.GetAllReasons();
            List<IWithdrawalReason> reasons = (from r in allReasons
                                               where !reasonsToExclude.Contains(r.Name)
                                               select r).ToList<IWithdrawalReason>();
            return reasons;
        }
    }
}
