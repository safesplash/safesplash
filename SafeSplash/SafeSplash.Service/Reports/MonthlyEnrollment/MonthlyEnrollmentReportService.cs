﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using SafeSplash.DataAccess.NHibernate;

namespace SafeSplash.Service.Reports.MonthlyEnrollment
{
    [ServiceContract]
    public interface IMonthlyEnrollmentReportService
    {
        [OperationContract]
        IMonthlyEnrollmentReport GetEnrollmentReport(DateTime fromDate, DateTime toDate, Guid locationId);
    }

    public class MonthlyEnrollmentReportService : IMonthlyEnrollmentReportService
    {
        IMonthlyEnrollmentReportDataAccess da = new MonthlyEnrollmentReportDataAccess();
        private static Dictionary<string, MonthlyEnrollmentReport> MonthlyReports = new Dictionary<string, MonthlyEnrollmentReport>();

        public IMonthlyEnrollmentReport GetEnrollmentReport(DateTime fromDate, DateTime toDate, Guid locationId)
        {
            ////IEnumerable<ReportWeek> weeks = GenerateWeeks(fromDate, toDate, "Thursday");
            ////MonthlyEnrollmentReport report = new MonthlyEnrollmentReport();
            ////List<IMonthlyEnrollmentReportDataObject> monthlyWeeks = new List<IMonthlyEnrollmentReportDataObject>();
            ////foreach (var week in weeks)
            ////{
            ////    IMonthlyEnrollmentReportDataObject weeklyDto = da.GetMonthlyEnrollmentReport(week.StartDate, week.EndDate, locationId);
            ////    monthlyWeeks.Add(weeklyDto);
            ////}
            ////report.FromDate = fromDate;
            ////report.ToDate = toDate;
            ////report.StudentsEnrolled = monthlyWeeks.Sum(r => r.StudentsEnrolled) / weeks.Count();
            ////report.PrivateSpotsFilled = monthlyWeeks.Sum(r => r.PrivateSpotsFilled) / weeks.Count();
            ////report.SemiPrivateSpotsFilled = monthlyWeeks.Sum(r => r.SemiPrivateSpotsFilled) / weeks.Count();
            ////report.SpotsFilled = Enrollment.CalculateSpotsFilled(monthlyWeeks.Sum(r => r.StudentsEnrolled), monthlyWeeks.Sum(r => r.PrivateSpotsFilled), monthlyWeeks.Sum(r => r.SemiPrivateSpotsFilled)) / weeks.Count();
            ////report.Capacity = monthlyWeeks.Sum(r => r.Capacity) / weeks.Count();
            
            ////int totalFamiliesEnrolled = monthlyWeeks.Sum(r => r.FamiliesEnrolled);
            ////decimal familiesEnrolled = (totalFamiliesEnrolled == 0 || weeks.Count() == 0) ? 0.00M : decimal.Round(((Decimal)totalFamiliesEnrolled / (Decimal)weeks.Count()), 0, MidpointRounding.AwayFromZero);
            ////report.FamiliesEnrolled = (int) Math.Round(familiesEnrolled);
            
            ////report.SpotsFilledPerFamily = (report.SpotsFilled == 0 || report.FamiliesEnrolled == 0) ? 0.00M : decimal.Round(((Decimal)report.SpotsFilled / (Decimal)report.FamiliesEnrolled), 2, MidpointRounding.AwayFromZero);
            //////report.SpotsPerChild = (report.SpotsFilled == 0) ? 0.00M : decimal.Round(((Decimal)report.SpotsFilled / (Decimal)report.StudentsEnrolled), 2, MidpointRounding.AwayFromZero);
            ////report.SpotsPerChild = (report.SpotsFilled == 0 || report.StudentsEnrolled == 0) ? 0.00M : decimal.Round(((Decimal)report.SpotsFilled / (Decimal)report.StudentsEnrolled), 2, MidpointRounding.AwayFromZero);

            ////decimal totalCapacityPercentage = 0.00M;
            ////foreach (var weeklyDto in monthlyWeeks)
            ////{
            ////    if (weeklyDto.Capacity > 0)
            ////    {
            ////        decimal weekyCapacityPercentage = (decimal)weeklyDto.StudentsEnrolled / (decimal)weeklyDto.Capacity;
            ////        totalCapacityPercentage = Decimal.Add(totalCapacityPercentage, weekyCapacityPercentage);
            ////    }
            ////}
            ////report.PercentageOfCapacity = totalCapacityPercentage > 0.00M ? decimal.Round(((totalCapacityPercentage / weeks.Count()) * 100), 2, MidpointRounding.AwayFromZero) : 0.00M;
            ////report.RegistrationsInProgress = monthlyWeeks.Max(r => r.RegistrationsInProgress);

            ////// for the following fields, we need to get the number based on the exact begin and end dates for the month
            ////IMonthlyEnrollmentReportDataObject monthlyDto = da.GetMonthlyEnrollmentReport(fromDate, toDate, locationId);
            ////report.NewRegistrations = monthlyDto.NewRegistrations;
            ////report.StudentsWithdrawn = monthlyDto.StudentsWithdrawn;
            ////report.PrivateSpotsWithdrawn = monthlyDto.PrivateSpotsWithdrawn;
            ////report.SemiPrivateSpotsWithdrawn = monthlyDto.SemiPrivateSpotsWithdrawn;
            ////report.SpotsWithdrawn = Enrollment.CalculateSpotsWithdrawn(report.StudentsWithdrawn, report.PrivateSpotsWithdrawn, report.SemiPrivateSpotsWithdrawn);
            ////if (report.SpotsWithdrawn == 0 || report.SpotsFilled == 0)
            ////{
            ////    report.PercentageOfSpotsWithdrawn = 0.00M;
            ////}
            ////else
            ////{
            ////    report.PercentageOfSpotsWithdrawn = decimal.Round((((decimal)report.SpotsWithdrawn / (decimal)report.SpotsFilled) * 100), 2, MidpointRounding.AwayFromZero);
            ////}

            ////// for NewSpotsFilled, we need to subtract current month - previous month totals
            ////DateTime priorMonthFromDate = fromDate.AddMonths(-1);
            ////DateTime priorMonthToDate = new DateTime(priorMonthFromDate.Year, priorMonthFromDate.Month, DateTime.DaysInMonth(priorMonthFromDate.Year, priorMonthFromDate.Month));
            ////string priorMonthKey = string.Format("{0}-{1}-{2}", priorMonthFromDate.ToShortDateString(), priorMonthToDate.ToShortDateString(), locationId.ToString());
            ////if (MonthlyReports.ContainsKey(priorMonthKey))
            ////{
            ////    MonthlyEnrollmentReport priorMonthReport = MonthlyReports[priorMonthKey];
            ////    int priorMonthSpots = priorMonthReport.SpotsFilled - priorMonthReport.SpotsWithdrawn;
            ////    int thisMonthSpots = report.SpotsFilled;
            ////    report.NewSpotsFilled = thisMonthSpots - priorMonthSpots;
            ////}
            ////string key = string.Format("{0}-{1}-{2}", report.FromDate.ToShortDateString(), report.ToDate.ToShortDateString(), locationId.ToString());
            ////MonthlyReports[key] = report;
            ////return report;

            IEnumerable<ReportWeek> weeks = GenerateWeeks(fromDate, toDate, "Thursday");
            MonthlyEnrollmentReport report = new MonthlyEnrollmentReport();
            List<IMonthlyEnrollmentReportDataObject> monthlyWeeks = new List<IMonthlyEnrollmentReportDataObject>();
            foreach (var week in weeks)
            {
                IMonthlyEnrollmentReportDataObject weeklyDto = da.GetMonthlyEnrollmentReport(week.StartDate, week.EndDate, locationId);
                monthlyWeeks.Add(weeklyDto);
            }
            report.FromDate = fromDate;
            report.ToDate = toDate;
            report.StudentsEnrolled = monthlyWeeks.Sum(r => r.StudentsEnrolled) / (decimal)(weeks.Count());
            report.PrivateSpotsFilled = monthlyWeeks.Sum(r => r.PrivateSpotsFilled) / (decimal)weeks.Count();
            report.SemiPrivateSpotsFilled = monthlyWeeks.Sum(r => r.SemiPrivateSpotsFilled) / (decimal)weeks.Count();
            //report.SpotsFilled = (decimal)Enrollment.CalculateSpotsFilled(monthlyWeeks.Sum(r => r.StudentsEnrolled), monthlyWeeks.Sum(r => r.PrivateSpotsFilled), monthlyWeeks.Sum(r => r.SemiPrivateSpotsFilled)) / (decimal)weeks.Count();
            report.SpotsFilled = decimal.Round(((decimal)((decimal)Enrollment.CalculateSpotsFilled(monthlyWeeks.Sum(r => r.StudentsEnrolled), monthlyWeeks.Sum(r => r.PrivateSpotsFilled), monthlyWeeks.Sum(r => r.SemiPrivateSpotsFilled)) / (decimal)weeks.Count())), 0, MidpointRounding.AwayFromZero);
            report.Capacity = (decimal)monthlyWeeks.Sum(r => r.Capacity) / (decimal)weeks.Count();

            int totalFamiliesEnrolled = monthlyWeeks.Sum(r => r.FamiliesEnrolled);
            decimal familiesEnrolled = (totalFamiliesEnrolled == 0 || weeks.Count() == 0) ? 0.00M : decimal.Round(((Decimal)totalFamiliesEnrolled / (Decimal)weeks.Count()), 0, MidpointRounding.AwayFromZero);
            report.FamiliesEnrolled = (int)Math.Round(familiesEnrolled);

            report.SpotsFilledPerFamily = (report.SpotsFilled == 0 || report.FamiliesEnrolled == 0) ? 0.00M : decimal.Round(((Decimal)report.SpotsFilled / (Decimal)report.FamiliesEnrolled), 2, MidpointRounding.AwayFromZero);
            //report.SpotsPerChild = (report.SpotsFilled == 0) ? 0.00M : decimal.Round(((Decimal)report.SpotsFilled / (Decimal)report.StudentsEnrolled), 2, MidpointRounding.AwayFromZero);
            report.SpotsPerChild = (report.SpotsFilled == 0 || report.StudentsEnrolled == 0) ? 0.00M : decimal.Round(((Decimal)report.SpotsFilled / (Decimal)report.StudentsEnrolled), 2, MidpointRounding.AwayFromZero);

            decimal totalCapacityPercentage = 0.00M;
            foreach (var weeklyDto in monthlyWeeks)
            {
                if (weeklyDto.Capacity > 0)
                {
                    decimal weekyCapacityPercentage = (decimal)weeklyDto.StudentsEnrolled / (decimal)weeklyDto.Capacity;
                    totalCapacityPercentage = Decimal.Add(totalCapacityPercentage, weekyCapacityPercentage);
                }
            }
            report.PercentageOfCapacity = totalCapacityPercentage > 0.00M ? decimal.Round(((totalCapacityPercentage / weeks.Count()) * 100), 2, MidpointRounding.AwayFromZero) : 0.00M;
            report.RegistrationsInProgress = monthlyWeeks.Max(r => r.RegistrationsInProgress);

            if (fromDate.Month == 6)
            {
                int i = 0;
            }
            // for the following fields, we need to get the number based on the exact begin and end dates for the month
            IMonthlyEnrollmentReportDataObject monthlyDto = da.GetMonthlyEnrollmentReport(fromDate, toDate, locationId);            
            report.NewRegistrations = monthlyDto.NewRegistrations;
            report.StudentsWithdrawn = monthlyDto.StudentsWithdrawn;
            report.PrivateSpotsWithdrawn = monthlyDto.PrivateSpotsWithdrawn;
            report.SemiPrivateSpotsWithdrawn = monthlyDto.SemiPrivateSpotsWithdrawn;
            report.SpotsWithdrawn = Enrollment.CalculateSpotsWithdrawn(report.StudentsWithdrawn, report.PrivateSpotsWithdrawn, report.SemiPrivateSpotsWithdrawn);
            if (report.SpotsWithdrawn == 0 || report.SpotsFilled == 0)
            {
                report.PercentageOfSpotsWithdrawn = 0.00M;
            }
            else
            {
                report.PercentageOfSpotsWithdrawn = decimal.Round((((decimal)report.SpotsWithdrawn / (decimal)report.SpotsFilled) * 100), 2, MidpointRounding.AwayFromZero);
            }

            // for NewSpotsFilled, we need to subtract current month - previous month totals
            DateTime priorMonthFromDate = fromDate.AddMonths(-1);
            DateTime priorMonthToDate = new DateTime(priorMonthFromDate.Year, priorMonthFromDate.Month, DateTime.DaysInMonth(priorMonthFromDate.Year, priorMonthFromDate.Month));
            string priorMonthKey = string.Format("{0}-{1}-{2}", priorMonthFromDate.ToShortDateString(), priorMonthToDate.ToShortDateString(), locationId.ToString());
            if (MonthlyReports.ContainsKey(priorMonthKey))
            {
                MonthlyEnrollmentReport priorMonthReport = MonthlyReports[priorMonthKey];
                //int priorMonthSpots = priorMonthReport.SpotsFilled - priorMonthReport.SpotsWithdrawn;
                //int thisMonthSpots = report.SpotsFilled;
                decimal priorMonthSpots = priorMonthReport.SpotsFilled - priorMonthReport.SpotsWithdrawn;
                decimal thisMonthSpots = report.SpotsFilled;
                //report.NewSpotsFilled = thisMonthSpots - priorMonthSpots;
                //report.NewSpotsFilled = Decimal.Subtract(thisMonthSpots, priorMonthSpots);
                 report.NewSpotsFilled = Convert.ToInt32(Decimal.Round(Decimal.Subtract(thisMonthSpots, priorMonthSpots), 2, MidpointRounding.AwayFromZero));
                //report.NewSpotsFilled = 
            }
            string key = string.Format("{0}-{1}-{2}", report.FromDate.ToShortDateString(), report.ToDate.ToShortDateString(), locationId.ToString());
            MonthlyReports[key] = report;
            return report;
        }

        private IEnumerable<ReportWeek> GenerateWeeks(DateTime fromDate, DateTime toDate, string dayOfWeek)
        {
            List<ReportWeek> weeks = new List<ReportWeek>();
            DateTime weekEndDate = fromDate;
            while (!weekEndDate.DayOfWeek.ToString().Equals(dayOfWeek))
            {
                weekEndDate = weekEndDate.AddDays(1);
            }
            while (weekEndDate.AddDays(-6).CompareTo(toDate) <= 0)
            {
                weeks.Add(new ReportWeek() { StartDate = new DateTime(weekEndDate.Year, weekEndDate.Month, weekEndDate.Day, 0, 0, 0).AddDays(-6), EndDate = new DateTime(weekEndDate.Year, weekEndDate.Month, weekEndDate.Day, 23, 59, 59) });
                weekEndDate = new DateTime(weekEndDate.Year, weekEndDate.Month, weekEndDate.Day, 23, 59, 59).AddDays(7);
            }
            return weeks;
        }
    }

}
