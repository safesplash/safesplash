﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SafeSplash.Service.Reports.MonthlyEnrollment
{
    public interface IMonthlyEnrollmentReport
    {
        DateTime FromDate { get; set; }
        DateTime ToDate { get; set; }
        //int StudentsEnrolled { get; set; }
        //int PrivateSpotsFilled { get; set; }
        //int SemiPrivateSpotsFilled { get; set; }
        //int SpotsFilled { get; set; }
        //int Capacity { get; set; }
        //int FamiliesEnrolled { get; set; }
        decimal StudentsEnrolled { get; set; }
        decimal PrivateSpotsFilled { get; set; }
        decimal SemiPrivateSpotsFilled { get; set; }
        decimal SpotsFilled { get; set; }
        decimal Capacity { get; set; }
        decimal FamiliesEnrolled { get; set; }
        decimal SpotsFilledPerFamily { get; set; }
        decimal SpotsPerChild { get; set; }
        decimal PercentageOfCapacity { get; set; }
        int FamiliesBilled { get; set; }
        int OutstandingAccounts { get; set; }
        int NewSpotsFilled { get; set; }
        //decimal NewSpotsFilled { get; set; }
        int NewRegistrations { get; set; }
        int RegistrationsInProgress { get; set; }
        int StudentsWithdrawn { get; set; }
        int PrivateSpotsWithdrawn { get; set; }
        int SemiPrivateSpotsWithdrawn { get; set; }
        int SpotsWithdrawn { get; set; }
        decimal PercentageOfSpotsWithdrawn { get; set; }
        string Month { get; }
        string MonthYear { get; }
    }

    [DataContract]
    public class MonthlyEnrollmentReport : IMonthlyEnrollmentReport
    {
        [DataMember]
        public DateTime FromDate { get; set; }
        [DataMember]
        public DateTime ToDate { get; set; }
        //[DataMember]
        //public int StudentsEnrolled { get; set; }
        [DataMember]
        public decimal StudentsEnrolled { get; set; }
        //[DataMember]
        //public int PrivateSpotsFilled { get; set; }
        [DataMember]
        public decimal PrivateSpotsFilled { get; set; }
        //[DataMember]
        //public int SemiPrivateSpotsFilled { get; set; }
        [DataMember]
        public decimal SemiPrivateSpotsFilled { get; set; }
        //[DataMember]
        //public int SpotsFilled { get; set; }
        [DataMember]
        public decimal SpotsFilled { get; set; }
        //[DataMember]
        //public int Capacity { get; set; }
        [DataMember]
        public decimal Capacity { get; set; }
        //[DataMember]
        //public int FamiliesEnrolled { get; set; }
        [DataMember]
        public decimal FamiliesEnrolled { get; set; }
        [DataMember]
        public decimal SpotsFilledPerFamily { get; set; }
        [DataMember]
        public decimal SpotsPerChild { get; set; }
        [DataMember]
        public decimal PercentageOfCapacity { get; set; }
        [DataMember]
        public int FamiliesBilled { get; set; }
        [DataMember]
        public int OutstandingAccounts { get; set; }
        [DataMember]
        public int NewSpotsFilled { get; set; }
        //[DataMember]
        //public decimal NewSpotsFilled { get; set; }
        [DataMember]
        public int NewRegistrations { get; set; }
        [DataMember]
        public int RegistrationsInProgress { get; set; }
        [DataMember]
        public int StudentsWithdrawn { get; set; }
        [DataMember]
        public int PrivateSpotsWithdrawn { get; set; }
        [DataMember]
        public int SemiPrivateSpotsWithdrawn { get; set; }
        [DataMember]
        public int SpotsWithdrawn { get; set; }
        [DataMember]
        public decimal PercentageOfSpotsWithdrawn { get; set; }
        [DataMember]
        public string Month
        {
            get
            {
                return ToDate.ToString("MMM");
            }
        }
        [DataMember]
        public string MonthYear
        {
            get
            {
                return string.Format("{0} {1}", ToDate.ToString("MMM"), ToDate.Year);
            }
        }
    }
}
