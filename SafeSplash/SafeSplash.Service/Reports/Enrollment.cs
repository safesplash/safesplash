﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.Reports
{
    public class Enrollment
    {
        public static int CalculateSpotsFilled(int studentsEnrolled, int privateSpots, int semiPrivateSpots)
        {
            int spots = (studentsEnrolled) + (privateSpots * 3) + (semiPrivateSpots);
            return spots;
        }

        public static int CalculateSpotsWithdrawn(int studentsWithdrawn, int privateSpotsWithdrawn, int semiPrivateSpotsWithdrawn)
        {
            int spots = (studentsWithdrawn) + (privateSpotsWithdrawn * 3) + (semiPrivateSpotsWithdrawn);
            return spots;
        }
    }
}
