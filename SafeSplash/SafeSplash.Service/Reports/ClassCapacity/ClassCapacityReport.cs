﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.Reports.ClassCapacity
{
    public class ClassCapacityReportRecord
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Guid LocationId { get; set; }
        public string ClassName { get; set; }
        public string LevelName { get; set; }
        public string StartTime { get; set; }
        public string Day { get; set; }
        public Guid? InstructorId { get; set; }
        public int Capacity { get; set; }
        public int StudentsEnrolled { get; set; }
    }
}
