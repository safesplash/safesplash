﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.NHibernate;

namespace SafeSplash.Service.Reports.ClassCapacity
{
    public interface IClassCapacityReportService
    {
        IEnumerable<ClassCapacityReportRecord> GetClassCapacityReportRecords(DateTime fromDate, DateTime toDate, Guid locationId, Guid? instructorId);
    }

    public class ClassCapacityReportService : IClassCapacityReportService
    {
        IClassCapacityReportDataAccess da = new ClassCapacityReportDataAccess();

        public IEnumerable<ClassCapacityReportRecord> GetClassCapacityReportRecords(DateTime fromDate, DateTime toDate, Guid locationId, Guid? instructorId)
        {
            List<ClassCapacityReportRecord> reportRecords = new List<ClassCapacityReportRecord>();
            IEnumerable<ClassCapacityReportDataObject> dataObjects = da.GetClassCapacityReports(fromDate, toDate, locationId, instructorId);
            foreach (var dataObject in dataObjects)
            {
                ClassCapacityReportRecord record = ClassCapacityReportConverter.Convert(dataObject);
                record.FromDate = fromDate;
                record.ToDate = toDate;
                record.LocationId = locationId;
                record.InstructorId = instructorId;
                reportRecords.Add(record);
            }
            return reportRecords;
        }
    }

    public class ClassCapacityReportConverter
    {
        public static ClassCapacityReportRecord Convert(ClassCapacityReportDataObject dataObject)
        {
            ClassCapacityReportRecord reportRecord = new ClassCapacityReportRecord();
            reportRecord.ClassName = dataObject.ClassName;
            reportRecord.LevelName = dataObject.LevelName;
            reportRecord.StartTime = dataObject.StartTime;
            reportRecord.Day = dataObject.Day;
            reportRecord.Capacity = dataObject.Capacity;
            reportRecord.StudentsEnrolled = dataObject.StudentsEnrolled;
            return reportRecord;
        }
    }
}
