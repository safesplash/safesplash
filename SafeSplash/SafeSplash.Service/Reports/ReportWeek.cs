﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.Reports
{
    public class ReportWeek
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
