﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using SafeSplash.DataAccess.NHibernate;
using AKMII.SafeSplash.BLL;
using AKMII.SafeSplash.Entity;

namespace SafeSplash.Service.Reports.WeeklyEnrollment
{
    [ServiceContract]
    public interface IWeeklyEnrollmentReportService
    {
        [OperationContract]
        IWeeklyEnrollmentReport GetEnrollmentReport(IEnumerable<ReportWeek> weeks, Guid locationId);
    }

    public class WeeklyEnrollmentReportService : IWeeklyEnrollmentReportService
    {
        public IWeeklyEnrollmentReport GetEnrollmentReport(IEnumerable<ReportWeek> weeks, Guid locationId)
        {
            IWeeklyEnrollmentReportDataAccess da = new WeeklyEnrollmentReportDataAccess();
            IWeeklyEnrollmentReport report = new WeeklyEnrollmentReport();
            List<IWeeklyEnrollmentReportRecord> records = new List<IWeeklyEnrollmentReportRecord>();
            LocationBll locationBll = new LocationBll();
            tb_Location location = locationBll.GetByID(locationId);
            report.LocationId = locationId;
            report.LocationName = location.loca_Name;
            foreach (var week in weeks)
            {
                IWeeklyEnrollmentReportDataObject dataObject = da.GetWeeklyEnrollmentReport(week.StartDate, week.EndDate, locationId);
                IWeeklyEnrollmentReportRecord reportRecord = WeeklyEnrollmentReportConverter.Convert(dataObject);
                reportRecord.FromDate = week.StartDate;
                reportRecord.ToDate = week.EndDate;
                reportRecord.SpotsFilled = Enrollment.CalculateSpotsFilled(reportRecord.StudentsEnrolled, reportRecord.PrivateSpotsFilled, reportRecord.SemiPrivateSpotsFilled);
                reportRecord.PercentageOfCapacity = (reportRecord.StudentsEnrolled == 0 || reportRecord.Capacity == 0) ? 0.00M : decimal.Round(((Decimal)reportRecord.StudentsEnrolled / (Decimal)reportRecord.Capacity) * 100, 2, MidpointRounding.AwayFromZero);
                reportRecord.SpotsWithdrawn = Enrollment.CalculateSpotsWithdrawn(reportRecord.StudentsWithdrawn, reportRecord.PrivateSpotsWithdrawn, reportRecord.SemiPrivateSpotsWithdrawn);
                reportRecord.PercentageOfSpotsWithdrawn = (reportRecord.SpotsWithdrawn == 0 || reportRecord.SpotsFilled == 0) ? 0.00M : decimal.Round(((Decimal)reportRecord.SpotsWithdrawn / (Decimal)reportRecord.SpotsFilled) * 100, 2, MidpointRounding.AwayFromZero); ;
                records.Add(reportRecord);
            }
            report.Records = records;

            return report;
        }
    }

    public class WeeklyEnrollmentReportConverter
    {
        public static IWeeklyEnrollmentReportRecord Convert(IWeeklyEnrollmentReportDataObject dataObject)
        {
            IWeeklyEnrollmentReportRecord reportRecord = new WeeklyEnrollmentReportRecord();
            reportRecord.StudentsEnrolled = dataObject.StudentsEnrolled;
            reportRecord.PrivateSpotsFilled = dataObject.PrivateSpotsFilled;
            reportRecord.SemiPrivateSpotsFilled = dataObject.SemiPrivateSpotsFilled;
            reportRecord.Capacity = dataObject.Capacity;
            reportRecord.RegistrationsInProgress = dataObject.RegistrationsInProgress;
            reportRecord.StudentsWithdrawn = dataObject.StudentsWithdrawn;
            reportRecord.PrivateSpotsWithdrawn = dataObject.PrivateSpotsWithdrawn;
            reportRecord.SemiPrivateSpotsWithdrawn = dataObject.SemiPrivateSpotsWithdrawn;
            return reportRecord;
        }
    }
}
