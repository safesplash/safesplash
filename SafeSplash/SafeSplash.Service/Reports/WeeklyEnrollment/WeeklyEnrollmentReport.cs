﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SafeSplash.Service.Reports.WeeklyEnrollment
{
    public interface IWeeklyEnrollmentReportRecord
    {
        Guid LocationId { get; set; }
        DateTime FromDate { get; set; }
        DateTime ToDate { get; set; }
        int StudentsEnrolled { get; set; }
        int PrivateSpotsFilled { get; set; }
        int SemiPrivateSpotsFilled { get; set; }
        int SpotsFilled { get; set; }
        int Capacity { get; set; }
        decimal PercentageOfCapacity { get; set; }
        int RegistrationsInProgress { get; set; }
        int StudentsWithdrawn { get; set; }
        int PrivateSpotsWithdrawn { get; set; }
        int SemiPrivateSpotsWithdrawn { get; set; }
        int SpotsWithdrawn { get; set; }
        decimal PercentageOfSpotsWithdrawn { get; set; }
    }

    public class WeeklyEnrollmentReportRecord : IWeeklyEnrollmentReportRecord
    {
        [DataMember]
        public Guid LocationId { get; set; }
        [DataMember]
        public DateTime FromDate { get; set; }
        [DataMember]
        public DateTime ToDate { get; set; }
        [DataMember]
        public int StudentsEnrolled { get; set; }
        [DataMember]
        public int PrivateSpotsFilled { get; set; }
        [DataMember]
        public int SemiPrivateSpotsFilled { get; set; }
        [DataMember]
        public int SpotsFilled { get; set; }
        [DataMember]
        public int Capacity { get; set; }
        [DataMember]
        public decimal PercentageOfCapacity { get; set; }
        [DataMember]
        public int RegistrationsInProgress { get; set; }
        [DataMember]
        public int StudentsWithdrawn { get; set; }
        [DataMember]
        public int PrivateSpotsWithdrawn { get; set; }
        [DataMember]
        public int SemiPrivateSpotsWithdrawn { get; set; }
        [DataMember]
        public int SpotsWithdrawn { get; set; }
        [DataMember]
        public decimal PercentageOfSpotsWithdrawn { get; set; }
    }

    public interface IWeeklyEnrollmentReport
    {
        Guid LocationId { get; set; }
        string LocationName { get; set; }
        DateTime FromDate { get; set; }
        DateTime ToDate { get; set; }
        IEnumerable<IWeeklyEnrollmentReportRecord> Records { get; set; }
    }
    
    public class WeeklyEnrollmentReport : IWeeklyEnrollmentReport
    {
        [DataMember]
        public Guid LocationId { get; set; }
        [DataMember]
        public string LocationName { get; set; }
        [DataMember]
        public DateTime FromDate { get; set; }
        [DataMember]
        public DateTime ToDate { get; set; }
        [DataMember]
        public IEnumerable<IWeeklyEnrollmentReportRecord> Records { get; set; }
    }
}
