﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.Reports
{
    public class Withdrawal
    {
        public static List<string> ReasonsToExclude()
        {
            List<string> reasons = new List<string>();
            reasons.Add("Page Layouts");
            reasons.Add("Completed");
            reasons.Add("Transfer");
            reasons.Add("Changed Class");
            reasons.Add("Change Classes");
            reasons.Add("Change Class");
            return reasons;
        }
    }
}
