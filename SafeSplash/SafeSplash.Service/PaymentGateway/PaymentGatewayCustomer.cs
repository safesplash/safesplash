
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PaymentGateway
{
    public class PaymentGatewayCustomer
    {
        public string clientID { get; set; }
        public string customerID { get; set; } 
        public string customerToken { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string locationID { get; set; }
        public string defaultPaymethodToken { get; set; }
        public string accountID { get; set; }
        public string status { get; set; }
        public string postalCode { get; set; }
        public string region { get; set; }
        public string street_line1 { get; set; }
        public string street_line2 { get; set; }
        public string locality { get; set; }
        public string email_address { get; set; }
        public string phone_number {get; set; }
        public string settlementPath { get; set; }
        public string transactionPath { get; set; }
        public string paymethodPath { get; set; }
        public string addressPath { get; set; }
        public string selfPath { get; set; }
        public List<PaymentGatewayCustomerPaymethod> paymethods { get; set; }
        public List<PaymentGatewayTransaction> transactions { get; set; } 
    }

    public class PaymentGatewayCustomerPaymethod
    {
        public bool procurement_card { get; set; }
        public string card_type { get; set; }
        public int expire_year { get; set; }
        public int expire_month { get; set; }
        public string masked_account_number { get; set; }
        public string name_on_card { get; set; }
        public string notes { get; set; }
        public string label { get; set; }
        public string customer_token { get; set; }
        public string location_id { get; set; }
        public string account_id { get; set; }
        public string paymethod_token { get; set; }
        public string routing_number { get; set; }
        public string account_type { get; set; }
        public string security_code { get; set; }
    }
}