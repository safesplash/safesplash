﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PaymentGateway
{
    public class JsonRestTransaction
    {
        public string action { get; set; }
        public string customer_token { get; set; }
        public string paymethod_token { get; set; }
        public string customer_id { get; set; }
        public string reference_id { get; set; }
        public double authorization_amount { get; set; }
        public string entered_by { get; set; }
        public double sales_tax_amount { get; set; }
        public string original_transaction_id { get; set; }
    }
}
