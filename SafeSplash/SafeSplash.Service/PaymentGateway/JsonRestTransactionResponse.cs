﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PaymentGateway
{
    public class JsonRestTransactionResponse
    {
        public string environment { get; set; }
        public string response_type { get; set; }
        public string response_code { get; set; }
        public string response_desc { get; set; }
        public string cvv_code { get; set; }
        public string avs_code { get; set; }
        public string authorization_code { get; set; }
        public string transaction_id { get; set; }
        public string account_id { get; set; }
        public string location_id { get; set; }
        public string customer_token { get; set; }
        public string paymethod_token { get; set; }
        public string customer_id { get; set; }
        public string reference_id { get; set; }
        public string action { get; set; }
        public string authorization_amount { get; set; }
        public string sales_tax_amount { get; set; }
        public string entered_by { get; set; }
    }

    public class JsonRestTransactionResponses
    {
        public JsonRestTransactionResponse response { get; set; }
    }
}
