﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PaymentGateway
{
    [Serializable]
    public class PaymentGatewayPayment
    {
        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string CustomerToken { get; set; }
        public string Type { get; set; }
        public DateTime Date { get; set; }
        public string transaction_id { get; set; }
        public string location_id { get; set; }
        public decimal Amount { get; set; }
        public string Status { get; set; }
        public string BillToFirstName { get; set; }
        public string BillToLastName { get; set; }
    }
}
