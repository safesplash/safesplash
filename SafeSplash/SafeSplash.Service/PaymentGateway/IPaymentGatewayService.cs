﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PaymentGateway
{
    public interface IPaymentGatewayService
    {
        IEnumerable<PaymentGatewayPayment> GetPayments(string location, DateTime fromDate, DateTime toDate);
        PaymentGatewayTransaction GetTransaction(string location, string transactionId);
        IEnumerable<PaymentGatewayCustomer> GetCustomerByLastName(string location, string lastName);
        PaymentGatewayCustomer GetCustomerByCustomerID(string location, int customerID); 
        IEnumerable<PaymentGatewayTransaction> GetTransactionsByPath(string path);
        IEnumerable<PaymentGatewayTransaction> GetTransactionsByCustomerID(string location, string customerID);
        IEnumerable<PaymentGatewayTransaction> GetTransactionsByMethodID(string location, string paymethod_token);
        IEnumerable<PaymentGatewayCustomerPaymethod> GetPaymethodsByCustomerID(string location, int customerID);
        String CreateCustomer(PaymentGatewayCustomer customer);
        List<PaymentGatewayCustomerPaymethod> GetPaymethodsByPath(string path);
        List<PaymentGatewaySettlement> GetSettlementsByPath(string path);
    }
}
