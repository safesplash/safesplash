﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PaymentGateway
{
    [Serializable]
    public class JsonSettlementsResult
    {
        public int number_results { get; set; }
        public IEnumerable<JsonSettlement> results { get; set; }
    }
}
