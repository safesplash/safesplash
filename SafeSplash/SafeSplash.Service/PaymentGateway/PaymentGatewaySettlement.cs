﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PaymentGateway
{
    [Serializable]
    public class PaymentGatewaySettlement
    {
        public string Id { get; set; }
        public string TransactionId { get; set; }
        public string customerToken { get; set; }
        public string transactionID { get; set; }
        public string locationID { get; set; }
        public string CustomerId { get; set; }
        public string BillToFirstName { get; set; }
        public string BillToLastName { get; set; }
        public string NameOnCard { get; set; }
        public DateTime Date { get; set; }
        public string Type { get; set; }
        public decimal Amount { get; set; }
        public string Method { get; set; }
        
    }
}
