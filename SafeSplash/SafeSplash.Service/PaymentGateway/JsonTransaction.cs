﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PaymentGateway
{
    [Serializable]
    public class JsonTransaction
    {
        public string transaction_id { get; set; }
        public string account_id { get; set; }
        public string location_id { get; set; }
        public string customer_token { get; set; }
        public string original_transaction_id { get; set; }
        public string customer_id { get; set; }
        public string order_number { get; set; }
        public string reference_id { get; set; }
        public string status { get; set; }
        public string action { get; set; }
        public decimal authorization_amount { get; set; }
        public string sales_tax_amount { get; set; }
        public string service_fee_amount { get; set; }
        public string authorization_code { get; set; }
        public string entered_by { get; set; }
        public string customer_accounting_code { get; set; }
        public DateTime received_date { get; set; }
        public billing_address billing_address { get; set; }
        public paymethod paymethod { get; set; }

        ////public string paymethod_token { get; set; }
        ////public string order_id { get; set; }
        //public string status { get; set; }
        //public string action { get; set; }
        //public decimal authorization_amount { get; set; }
        //public string authorization_code { get; set; }
        //public string entered_by { get; set; }
        //public DateTime received_date { get; set; }
        ////public DateTime origination_date { get; set; }
        ////public string bill_to_first_name { get; set; }
        ////public string bill_to_last_name { get; set; }

    }

    [Serializable]
    public class billing_address
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        //public string name_on_card { get; set; }
    }

    [Serializable]
    public class paymethod
    {
        public card card { get; set; }
    }

    [Serializable]
    public class card
    {
        public string name_on_card { get; set; }
        public string card_type { get; set; }
        public string masked_account_number { get; set; }
        public string expire_month { get; set; }
        public string expire_year { get; set; }
    }
}
