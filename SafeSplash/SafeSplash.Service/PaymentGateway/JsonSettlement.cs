﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PaymentGateway
{
    [Serializable]
    public class JsonSettlement
    {
        public string settle_id { get; set; }
        public string transaction_id { get; set; }
        public string customer_token { get; set; }
        public string location_id { get; set; }
        public string customer_id { get; set; }
        public DateTime settle_date { get; set; }
        public string settle_type { get; set; }
        public string settle_response_code { get; set; }
        public decimal settle_amount { get; set; }
        public string method { get; set; }
    }
}
