﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PaymentGateway
{
    [Serializable]
    public class PaymentGatewayTransaction
    {
        public string Id { get; set; }
        public string AccountId { get; set; }
        public string LocationId { get; set; }
        public string CustomerToken { get; set; }
        public string CustomerId { get; set; }
        public string OrderId { get; set; }
        public string Status { get; set; }
        public string Action { get; set; }
        public decimal Amount { get; set; }
        public string AuthorizationCode { get; set; }
        public string EnteredBy { get; set; }
        public DateTime ReceivedDate { get; set; }
        //public DateTime OriginationDate { get; set; }
        public string BillToFirstName { get; set; }
        public string BillToLastName { get; set; }
        //public string BillToCompanyName { get; set; }
        public string NameOnCard { get; set; }
        public string ResponseCode { get; set; }
        public string CardNumber { get; set; }
        public string CardType { get; set; }
        public string PaymethodToken { get; set; }
    }
}
