﻿
#region Commented Out

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Net;
//using System.IO;

//namespace SafeSplash.Service.PaymentGateway
//{
//    [Serializable]
//    public class PaymentGatewayService : IPaymentGatewayService
//    {
//        public IEnumerable<PaymentGatewayPayment> GetPayments(string apiLogonId, string secureTransactionKey, string account, string location, DateTime fromDate, DateTime toDate)
//        {
//            List<PaymentGatewayPayment> payments = new List<PaymentGatewayPayment>();

            

//            return payments.OrderBy(p => p.Date).ThenBy(p => p.Status).ThenBy(p => p.BillToLastName).ThenBy(p => p.BillToFirstName).ToList<PaymentGatewayPayment>();
//        }

       

//    }
//}

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Net.Security;
using SafeSplash.DataAccess.Client;
using SafeSplash.DataAccess.NHibernate;
using SafeSplash.DataAccess.PaymentGateway;

namespace SafeSplash.Service.PaymentGateway
{
    [Serializable]
    public class PaymentGatewayService : IPaymentGatewayService
    {
        private const string account = "act_300049";

        public IEnumerable<PaymentGatewayCustomerPaymethod> GetPaymethodsByCustomerID(string location, int customerID)
        {
            string url =
                string.Format(
                    "https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/customers/cst_{1}/paymethods",
                    location, customerID);

            string response = PaymentGatewayCall(url);
            return ConvertJsonToPaymethods(response);

        }

        
        public IEnumerable<PaymentGatewayCustomer> GetCustomerByLastName(string location, string lastName)
        {
            string jsonCustomers = GetCustomersJson(location, lastName);
            IEnumerable<PaymentGatewayCustomer> theseCustomers = ConvertJsonToCustomers(jsonCustomers);
            List<PaymentGatewayCustomer> thisReturn = new List<PaymentGatewayCustomer>();
            //lazy fix, fix this later -- JPH
            foreach (var cust in theseCustomers)
            {
                PaymentGatewayCustomer fullCust = GetCustomerByCustomerID(location, Convert.ToInt32(cust.customerID));
                thisReturn.Add(fullCust);
            }

            return thisReturn;
        }

        public string chargeTest(int customerID)
        {
            PaymentGatewayCustomer me = GetCustomerByCustomerID("151313", customerID);
            me.customerID = customerID.ToString();
            me.accountID = account;
            string thisReturn = chargeCCTest(me, 1);

            return thisReturn;
        }

        public string chargeCCTest(PaymentGatewayCustomer thisCust, double amount)
        {
            string url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/transactions",
    151313);

            ClientTransactionDataObject thisTrans = new ClientTransactionDataObject()
            {
                clientID = Guid.NewGuid(),
                accountID = account,
                transactionType = "sale",
                amount = Convert.ToDecimal(amount),
                customerToken = thisCust.customerToken,
                customerID = Convert.ToInt32(thisCust.customerID),
                locationID = thisCust.locationID,
                enteredBy = "Jhanna-test",
                dateCreated = DateTime.Now,
                paymethodToken = thisCust.defaultPaymethodToken
            };

            //IClientTransactionDataAccess repository = new ClientTransactionDataAccess();
            //repository.SaveClientTransaction(thisTrans);

            JsonRestTransaction thisJsonTrans = new JsonRestTransaction()
            {
                action = "sale",
                customer_token = thisCust.customerToken,
                paymethod_token = thisCust.defaultPaymethodToken,
                customer_id = thisCust.customerID,
                reference_id = thisTrans.clientTransactionID.ToString(),
                authorization_amount = amount,
                entered_by = "Jhanna-text",
                sales_tax_amount = 0
            };

            string json = JsonConvert.SerializeObject(thisJsonTrans, Formatting.None);
            string response = PaymentGatewayCall(url, json);
            return response;
        }

        public string voidTransaction(string locationID, string transactionID, string authCode)
        {
            string url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/transactions/{1}",
                locationID, transactionID);

            JsonVoidTransaction thisTrans = new JsonVoidTransaction();
            thisTrans.account_id = account;
            thisTrans.action = "void";
            thisTrans.authorization_code = authCode;
            thisTrans.entered_by = "Jhanna-test";
            
            string json = JsonConvert.SerializeObject(thisTrans, Formatting.None);
            string response = PaymentGatewayCall(url, json, "PUT");
            return response;
        }

        public PaymentGatewayCustomer GetCustomerByCustomerToken(string location, string customerToken)
        {
            string url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/customers/{1}", location, customerToken);
            string response = PaymentGatewayCall(url);

            PaymentGatewayCustomer cust = ConvertJsonToCustomer(response);
            url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/customers/{1}/paymethods", location, customerToken);
            string methods = PaymentGatewayCall(url);
            cust.paymethods = new List<PaymentGatewayCustomerPaymethod>(ConvertJsonToPaymethods(methods));
            url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/customers/{1}/transactions", location, customerToken);
            string transactions = PaymentGatewayCall(url);
            cust.transactions = new List<PaymentGatewayTransaction>(ConvertJsonToTransactions(transactions));
            return cust;
        }
        public PaymentGatewayCustomer GetCustomerByCustomerID(string location, int customerID)
        {
            string url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/customers/cst_{1}", location, customerID);
            string response = PaymentGatewayCall(url);

            PaymentGatewayCustomer cust = ConvertJsonToCustomer(response);
            url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/customers/cst_{1}/paymethods", location, customerID);
            string methods = PaymentGatewayCall(url);
            cust.paymethods = new List<PaymentGatewayCustomerPaymethod>(ConvertJsonToPaymethods(methods));
            cust.transactions = new List<PaymentGatewayTransaction>(GetTransactionsByCustomerID(location, customerID.ToString()));
            IClientTransactionDataAccess transRepository = new ClientTransactionDataAccess();
            IClientDataAccess clientRepository = new ClientDataAccess();
            foreach (var trans in cust.transactions)
            {
                if (!transRepository.CheckTransactionDupe(trans.Id))
                {
                    ClientMatch thisClient = clientRepository.getClientMatchByCustomerID(customerID);

                    ClientTransactionDataObject thisTransObj = new ClientTransactionDataObject()
                    {
                        clientID = thisClient.clientID,
                        customerID = customerID,
                        forteTransactionID = trans.Id,
                        transactionType = trans.Action,
                        amount = trans.Amount,
                        dateCreated = trans.ReceivedDate,
                        batched = true,
                        batchID = "0",
                        dateBatched = trans.ReceivedDate,
                        cardNumber = trans.CardNumber,
                        cardType = trans.CardType,
                        cardExp = "",
                        locationID = trans.LocationId,
                        merchantID = "0",
                        customerToken = trans.CustomerToken,
                        accountID = trans.AccountId,
                        firstName = trans.BillToFirstName,
                        lastName = trans.BillToLastName,
                        authCode = trans.AuthorizationCode,
                        enteredBy = trans.EnteredBy
                    };

                    if (trans.CustomerId.Length > 0)
                    {
                        thisTransObj.customerID = Convert.ToInt32(trans.CustomerId);
                    }
                    else
                    {
                        thisTransObj.customerID = 0;
                    }
                    transRepository.SaveClientTransaction(thisTransObj);
                }
            }
            return cust;
        }

        public string settleCC(string locationID, string transactionID, string authCode)
        {
            string url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/transactions/{1}/Capture",
    locationID, transactionID);

            JsonVoidTransaction thisTrans = new JsonVoidTransaction();
            thisTrans.action = "Capture";
            thisTrans.authorization_code = authCode;
            thisTrans.entered_by = "Jhanna-text";
            string json = JsonConvert.SerializeObject(thisTrans, Formatting.None);
            string response = PaymentGatewayCall(url, json, "PUT");
            return response;   
        }

        public string refundCC(Guid clientID, string customerID, string customerToken, string locationID, string paymethodToken, double amount, string enteredBy, string originalTransactionID)
        {
            string url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/transactions",
                locationID);
            ClientTransactionDataObject thisTrans = new ClientTransactionDataObject()
            {
                clientID = clientID,
                accountID = account,
                transactionType = "disburse",
                amount = Convert.ToDecimal(amount),
                customerToken = customerToken,
                customerID = Convert.ToInt32(customerID),
                locationID = locationID,
                enteredBy = enteredBy,
                dateCreated = DateTime.Now,
                dateBatched = DateTime.Now,
                batched = false,
                paymethodToken = paymethodToken
            };

            IClientTransactionDataAccess repository = new ClientTransactionDataAccess();
            repository.SaveClientTransaction(thisTrans);

            JsonRestTransaction thisJsonTrans = new JsonRestTransaction()
            {
                action = "disburse",
                customer_token = customerToken,
                paymethod_token = paymethodToken,
                customer_id = customerID,
                reference_id = thisTrans.clientTransactionID.ToString(),
                authorization_amount = amount,
                entered_by = enteredBy,
                sales_tax_amount = 0,
                original_transaction_id = originalTransactionID
            };

            string json = JsonConvert.SerializeObject(thisJsonTrans, Formatting.None);
            string response = PaymentGatewayCall(url, json);
            return response;
        }

        public string CreateSimpleCustomer(PaymentGatewayCustomer customer)
        {
            string url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/customers/",
                customer.locationID);

            JsonCreateSimpleCustomer thisCustomer = new JsonCreateSimpleCustomer()
            {
                first_name = customer.firstName,
                last_name = customer.lastName,
                company_name = String.Empty,
                customer_id = customer.clientID
            };

            string json = JsonConvert.SerializeObject(thisCustomer);
            string response = PaymentGatewayCall(url, json);
            JsonCreateSimpleCustomerResponse custResponse = JsonConvert.DeserializeObject<JsonCreateSimpleCustomerResponse>(response);
            return custResponse.customer_token;
        }

        public string CreateSimplePaymethod(string customerToken, PaymentGatewayCustomer customer)
        {
            string url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/customers/{1}/paymethods", customer.locationID, customerToken);

            JsonCreateCustomerCard thisCard = new JsonCreateCustomerCard()
            {
                account_number = customer.paymethods[0].masked_account_number,
                expire_month = customer.paymethods[0].expire_month,
                expire_year = customer.paymethods[0].expire_year,
                card_verification_value = customer.paymethods[0].security_code,
                name_on_card = customer.paymethods[0].name_on_card,
            };

            JsonCreateSimpleCardPaymethod thisMethod = new JsonCreateSimpleCardPaymethod()
            {
                notes = String.Empty,
                card = thisCard
            };
            string json = JsonConvert.SerializeObject(thisMethod);
            string response = PaymentGatewayCall(url, json);
            return response;
        }

        public string CreateSimplePaymethodBank(string customerToken, string accountName, string accountNumber, string routingNumber, string locationID)
        {
            string url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/customers/{1}/paymethods", locationID, customerToken);

            JsonBank thisBank = new JsonBank()
            {
                account_holder = accountName,
                account_number = accountNumber,
                routing_number = routingNumber,
                account_type = "checking"
            };

            JsonCreateCustomerBank thisCustomerBank = new JsonCreateCustomerBank()
            {
                notes = String.Empty,
                echeck = thisBank
            };
            string json = JsonConvert.SerializeObject(thisCustomerBank);
            string response = PaymentGatewayCall(url, json);
            return response;
        }

        public string CreateSimpleAddress(string customerToken, PaymentGatewayCustomer customer)
        {
            string url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/customers/{1}/addresses/", customer.locationID, customerToken);

            JsonCreateCustomerPhysicalAddress physAddress = new JsonCreateCustomerPhysicalAddress()
            {
                street_line1 = customer.street_line1,
                street_line2 = customer.street_line2,
                locality = customer.locality,
                region = customer.region,
                postal_code = customer.postalCode
            };

            JsonCreateSimpleAddress thisAddress = new JsonCreateSimpleAddress()
            {
                address_token = String.Empty,
                customer_token = customerToken,
                label = String.Empty,
                first_name = customer.firstName,
                last_name = customer.lastName,
                company_name = String.Empty,
                phone_number = customer.phone_number,
                fax_number = String.Empty,
                email_address = customer.email_address,
                physical_address = physAddress
            };

            string json = JsonConvert.SerializeObject(thisAddress);
            string response = PaymentGatewayCall(url, json);
            return response;

        }
        public string CreateCustomer(PaymentGatewayCustomer customer)
        {
            //translate to json customer

            string url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/customers/",
                customer.locationID);

            JsonCreateCustomerPhysicalAddress thisPhysAddy = new JsonCreateCustomerPhysicalAddress()
            {
                street_line1 = customer.street_line1,
                street_line2 = customer.street_line2,
                locality = customer.locality,
                region = customer.region,
                postal_code = customer.postalCode
            };

            JsonCreateCustomerAddress thisAddy = new JsonCreateCustomerAddress()
            {
                label = String.Empty,
                first_name = customer.firstName,
                last_name = customer.lastName,
                company_name = String.Empty,
                phone_number = customer.phone_number,
                fax_number = String.Empty,
                email_address = customer.email_address,
                shipping_address_type = "Residential",
                address_type = "default_billing",
                addresses = thisPhysAddy
            };

            List<JsonCreateCustomerAddress> theseAddresses = new List<JsonCreateCustomerAddress>();
            theseAddresses.Add(thisAddy);

            JsonCreateCustomerCard thisCard = new JsonCreateCustomerCard()
            {
                account_number = customer.paymethods[0].masked_account_number,
                expire_month = customer.paymethods[0].expire_month,
                expire_year = customer.paymethods[0].expire_year,
                card_verification_value = customer.paymethods[0].security_code,
                name_on_card = customer.paymethods[0].name_on_card,
            };

            JsonCreateCustomerPaymethod thisPaymethod = new JsonCreateCustomerPaymethod()
            {
                label = String.Empty,
                notes = String.Empty,
                card = thisCard
            };

            JsonCreateCustomer thisCustomer = new JsonCreateCustomer()
            {
                first_name = customer.firstName,
                last_name = customer.lastName,
                company_name = String.Empty,
                customer_id = customer.clientID,
                addresses = theseAddresses,
                paymethod = thisPaymethod
            };
            

            string json = JsonConvert.SerializeObject(thisCustomer);
            string response = PaymentGatewayCall(url, "POST", json);
            return response;
        }

        public string chargeCC(Guid clientID, string customerID, string customerToken, string locationID, string paymethodToken, double amount, string enteredBy, bool recurring, double recurringAmount)
        {
            string url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/transactions",
                locationID);
            ClientTransactionDataObject thisTrans = new ClientTransactionDataObject()
            {
                clientID = clientID,
                accountID = account,
                transactionType = "sale",
                amount = Convert.ToDecimal(amount),
                customerToken = customerToken,
                customerID = Convert.ToInt32(customerID),
                locationID = locationID,
                enteredBy = enteredBy,
                dateCreated = DateTime.Now,
                paymethodToken = paymethodToken,
                dateBatched = DateTime.Now
            };

            IClientTransactionDataAccess repository = new ClientTransactionDataAccess();
            repository.SaveClientTransaction(thisTrans);

            JsonRestTransaction thisJsonTrans = new JsonRestTransaction()
            {
                action = "sale",
                customer_token = customerToken,
                paymethod_token = paymethodToken,
                customer_id = customerID,
                reference_id = thisTrans.clientTransactionID.ToString(),
                authorization_amount = amount,
                entered_by = enteredBy,
                sales_tax_amount = 0
            };

            string json = JsonConvert.SerializeObject(thisJsonTrans, Formatting.None);
            string response = PaymentGatewayCall(url, json);
            JsonRestTransactionResponse transResponse = JsonConvert.DeserializeObject<JsonRestTransactionResponses>(response).response;
            thisTrans.forteTransactionID = transResponse.transaction_id;
            thisTrans.authCode = transResponse.authorization_code;
            repository.SaveClientTransaction(thisTrans);

            return transResponse.response_desc;
        }

        public string PaymentGatewayCall(string url, bool addPageSize = true)
        {
            if (addPageSize)
            {
                url += "?page_size=1000";
            }
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

            WebClient client = new WebClient();
            client = generateAuthHeader(client);
            client.Headers.Add("X-Forte-Auth-Account-Id", account);
            StreamReader reader;
            using (client)
            {
                Stream stream = client.OpenRead(url);
                reader = new StreamReader(stream);
            }

            return reader.ReadToEnd();
        }

        public string PaymentGatewayCall(string url, string payload)
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            WebClient client = new WebClient();
            client = generateAuthHeader(client);
            client.Headers.Add("X-Forte-Auth-Account-Id", account);
            string thisReturn = String.Empty;
            using (client)
            {
                thisReturn = client.UploadString(url, payload);
            }
            return thisReturn;
        }

        public string PaymentGatewayCall(string url, string payload, string method)
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            WebClient client = new WebClient();
            client = generateAuthHeader(client);
            client.Headers.Add("X-Forte-Auth-Account-Id", account);
            if (method == "POST")
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            }
            string thisReturn = String.Empty;
            using (client)
            {
                thisReturn = client.UploadString(url, payload);
            }
            return thisReturn;
        }


        public string PaymentGatewayCall(string url, int pageIndex)
        {
            url = url += string.Format("?page_size=1000&page_index={0}", pageIndex);
            
            WebClient client = new WebClient();
            client = generateAuthHeader(client);
            client.Headers.Add("X-Forte-Auth-Account-Id", account);
            Stream stream = client.OpenRead(url);
            StreamReader reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }

        public IEnumerable<PaymentGatewayTransaction> GetTransactionsByFirstAndLastNames(string location, string firstName,
            string lastName)
        {
            string url =
                string.Format(
                    "https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/trasnactions/?filter=bill_to_last_name+eq+{1}",
                    location, lastName);

            string response = PaymentGatewayCall(url);
            List<PaymentGatewayTransaction> trans = ConvertJsonToTransactions(response).ToList();
            List<PaymentGatewayTransaction> thisReturn = new List<PaymentGatewayTransaction>();
            foreach (var t in trans)
            {
                PaymentGatewayTransaction thisTrans = GetTransaction(location, t.Id);
                thisReturn.Add(thisTrans);
            }
            return thisReturn;
        }
        public IEnumerable<PaymentGatewayTransaction> GetTransactionsByCustomerID(string locationID, string customerID)
        {
            string url = string.Format("https://api.forte.net/v2/accounts/act_300049/locations/loc_{0}/customers/cst_{1}/transactions", locationID, customerID);
            string response = PaymentGatewayCall(url);

            List<PaymentGatewayTransaction> trans = ConvertJsonToTransactions(response).ToList();
            List<PaymentGatewayTransaction> thisReturn = new List<PaymentGatewayTransaction>();
            foreach (var t in trans)
            {
                PaymentGatewayTransaction thisTrans = GetTransaction(locationID, t.Id);
                thisReturn.Add(thisTrans);
            }
            return thisReturn;
        }

        public IEnumerable<PaymentGatewayTransaction> GetTransactionsByMethodID(string locationID,
            string paymethod_token)
        {
            string url =
                string.Format(
                    "https://api.forte.net/v2/accounts/{0}/locations/loc_{1}/transactions/?filter=paymethod_token eq {2}",
                    account, locationID, paymethod_token);
            string response = PaymentGatewayCall(url);
            List<PaymentGatewayTransaction> trans = ConvertJsonToTransactions(response).ToList();
            List<PaymentGatewayTransaction> thisReturn = new List<PaymentGatewayTransaction>();
            foreach (var t in trans)
            {
                PaymentGatewayTransaction thisTrans = GetTransaction(locationID, t.Id);
                thisReturn.Add(thisTrans);
            }
            return thisReturn;
        }

        public IEnumerable<PaymentGatewayTransaction> GetTransactionsByPath(string path)
        {
            string url = string.Format("https://api.forte.net{0}", path);
            String response = PaymentGatewayCall(url);

            return ConvertJsonToTransactions(response);
        }

        public List<PaymentGatewayCustomerPaymethod> GetPaymethodsByPath(string path)
        {
            string url = string.Format("https://api.forte.net{0}", path);
            String response = PaymentGatewayCall(url);

            return ConvertJsonToPaymethods(response).ToList();
        }

        public List<PaymentGatewaySettlement> GetSettlementsByPath(string path)
        {
            string url = string.Format("https://api.forte.net{0}", path);
            String response = PaymentGatewayCall(url);

            return ConvertJsonToSettlements(response).ToList();
        }
        public IEnumerable<PaymentGatewayPayment> GetPayments(string location, DateTime fromDate, DateTime toDate)
        {
            List<PaymentGatewayPayment> payments = new List<PaymentGatewayPayment>();

            // get transactions
            //IEnumerable<PaymentGatewayTransaction> transactions = GetTransactions(apiLogonId, secureTransactionKey, account, location, fromDate, toDate);
            //foreach (var t in transactions)
            //{
            //    payments.Add(new PaymentGatewayPayment() { Id = t.Id, Date = t.OriginationDate, Amount = t.Amount, Status = t.Status, BillToFirstName = t.BillToFirstName, BillToLastName = t.BillToLastName });
            //}

            // get settlements
            IEnumerable<PaymentGatewaySettlement> settlements = GetSettlements(location, fromDate, toDate);
            foreach (var s in settlements)
            {
                payments.Add(new PaymentGatewayPayment() { Id = s.TransactionId, CustomerId = s.CustomerId, CustomerToken = s.customerToken, Date = s.Date, Amount = s.Amount, Status = "settled", BillToFirstName = s.BillToFirstName, BillToLastName = s.BillToLastName });
                
            }


            return payments.OrderBy(p => p.Date).ThenBy(p => p.Status).ThenBy(p => p.BillToLastName).ThenBy(p => p.BillToFirstName).ToList<PaymentGatewayPayment>();
        }

        public PaymentGatewayTransaction GetTransaction(string location, string transactionId)
        {
            PaymentGatewayTransaction transaction = null;
            transaction = ConvertJsonToTransaction(GetTransactionJson(location, transactionId));
            return transaction;
        }

        public List<PaymentGatewayCustomer> GetCustomersByAccount()
        {
            List<PaymentGatewayCustomer> thisReturn = new List<PaymentGatewayCustomer>();
            string url = "https://api.forte.net/v2/accounts/act_300049/customers";
            int pageIndex = 0;
            int totalResultsCount = -1;
            int loops = 0;
            JsonCustomersResult result = new JsonCustomersResult();
            string json = PaymentGatewayCall(url, false);
            if (totalResultsCount < 0)
            {
                result = JsonConvert.DeserializeObject<JsonCustomersResult>(json);
                totalResultsCount = result.number_results;
                loops = Convert.ToInt32(totalResultsCount / 1000);
                foreach (var c in result.results)
                {
                    PaymentGatewayCustomer thisCust = new PaymentGatewayCustomer()
                    {
                        customerID = c.customer_id,
                        locationID = c.location_id,
                        firstName = c.first_name,
                        lastName = c.last_name,
                        status = c.status
                    };
                    thisReturn.Add(thisCust);
                    pageIndex++;
                }
            }

            while (loops >= pageIndex)
            {   
                json = PaymentGatewayCall(url, pageIndex);
                result = JsonConvert.DeserializeObject<JsonCustomersResult>(json);
                foreach (var c in result.results)
                {
                            PaymentGatewayCustomer thisCust = new PaymentGatewayCustomer()
                            {
                                customerID = c.customer_id,
                                customerToken = c.customer_token,
                                firstName = c.first_name,
                                lastName = c.last_name,
                                locationID = c.location_id,
                                status = c.status
                            };
                            thisReturn.Add(thisCust);
                }
                pageIndex++;
            }
            return thisReturn;

        }
        public List<PaymentGatewayCustomer> GetCustomersByLocation(string location)
        {
            List<PaymentGatewayCustomer> thisReturn = new List<PaymentGatewayCustomer>();            
            string url = string.Format("https://api.forte.net/v2/accounts/{0}/locations/loc_{1}/customers", account,
                location);

            int pageIndex = 0;
            int totalResultsCount = -1;
            int loops = 0;
            JsonCustomersResult result = new JsonCustomersResult();
            string json = PaymentGatewayCall(url);
            if (totalResultsCount < 0)
            {
                    result = JsonConvert.DeserializeObject<JsonCustomersResult>(json);
                    totalResultsCount = result.number_results;
                    loops = Convert.ToInt32(totalResultsCount/1000);
                    foreach (var c in result.results)
                        {
                            PaymentGatewayCustomer thisCust = new PaymentGatewayCustomer()
                            {
                                customerID = c.customer_id,
                                customerToken = c.customer_token,
                                firstName = c.first_name,
                                lastName = c.last_name,
                                locationID = c.location_id,
                                status = c.status
                            };
                            thisReturn.Add(thisCust);
                        }
                    pageIndex++;
            }

            while (loops >= pageIndex)
            {   
                json = PaymentGatewayCall(url, pageIndex);
                result = JsonConvert.DeserializeObject<JsonCustomersResult>(json);
                foreach (var c in result.results)
                {
                            PaymentGatewayCustomer thisCust = new PaymentGatewayCustomer()
                            {
                                customerID = c.customer_id,
                                customerToken = c.customer_token,
                                firstName = c.first_name,
                                lastName = c.last_name,
                                locationID = c.location_id,
                                status = c.status
                            };
                            thisReturn.Add(thisCust);
                }
                pageIndex++;
            }
            return thisReturn;
        }

        public string GetSettlementByTransactionID(string location, string transactionID)
        {
            string url =
                string.Format(
                    "https://api.forte.net/v2/accounts/{0}/locations/loc_{1}/settlements/?filter=start_settle_date+eq+'2002-01-08'+and+end_settle_date+eq+'2015-04-29'+and+transaction_id+eq+{2}",
                    account, location, transactionID);

            string response = PaymentGatewayCall(url);

            return response;
        }
        public List<PaymentGatewayTransaction> GetTransactionsByCustomerToken(string location, string customerToken)
        {
            List<PaymentGatewayTransaction> thisReturn = null;
            string url = string.Format("https://api.forte.net/v2/accounts/{0}/locations/loc_{1}/transactions/?filter=customer_token eq {2} and bill_to_first_name eq {3}", account, location, customerToken, "Luke");
            string response = PaymentGatewayCall(url);

            return thisReturn;
        }
        #region Transactions

        private IEnumerable<PaymentGatewayTransaction> GetTransactions(string apiLogonId, string secureTransactionKey, string location, DateTime fromDate, DateTime toDate)
        {
            List<PaymentGatewayTransaction> transactions = new List<PaymentGatewayTransaction>();

            int pageIndex = 0;
            int pageSize = 50;
            int recordIndex = 0;
            int totalResultsCount = -1;
            while (true)
            {
                string json = GetTransactionsJson(apiLogonId, secureTransactionKey, location, pageSize, pageIndex, fromDate, toDate);
                if (totalResultsCount < 0)
                {
                    //dynamic dyn = JsonConvert.DeserializeObject(json);
                    //totalResultsCount = (dyn.number_results != null) ? int.Parse(dyn.number_results.ToString()) : 0;
                    JsonTransactionsResult result = JsonConvert.DeserializeObject<JsonTransactionsResult>(json);
                    totalResultsCount = result.number_results;
                }
                IEnumerable<PaymentGatewayTransaction> trans = ConvertJsonToTransactions(json);
                if (trans.Count() > 0)
                {
                    transactions.AddRange(ConvertJsonToTransactions(json));
                }
                else
                {
                    break;
                }
                pageIndex++;
                recordIndex += pageSize;
            }

            return transactions.OrderBy(t => t.BillToLastName).ThenBy(t => t.BillToFirstName).ToList<PaymentGatewayTransaction>();
        }

        private WebClient generateAuthHeader(WebClient client)
        {
            string apiLogonId = "kX86f4e0d8576HjMP7iTAxhWYEy4Tk7F";
            string secureTransactionKey = "shYdz36sVJJMZbrCxmBatfDtLj9GyBcg";
            string authHeader = string.Format("Basic {0}",
                Convert.ToBase64String(
                    Encoding.Default.GetBytes(string.Format("{0}:{1}", apiLogonId, secureTransactionKey))).Trim());
            client.Headers.Add(HttpRequestHeader.Authorization, authHeader);
            return client;
        }

        private string GetCustomersJson(string location, string lastName)
        {
            string url = string.Format("https://api.forte.net/v2/accounts/{0}/locations/loc_{1}/customers/?filter=last_name eq {2}", account, location, lastName);
            return PaymentGatewayCall(url, false);
        }

        private string GetTransactionsJson(string apiLogonId, string secureTransactionKey, string location, int pageSize, int pageIndex, DateTime fromDate, DateTime toDate)
        {
            //ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

            string results = string.Empty;
            WebClient client = new WebClient();
            string authHeader = string.Format("Basic {0}", Convert.ToBase64String(Encoding.Default.GetBytes(string.Format("{0}:{1}", apiLogonId, secureTransactionKey))).Trim());
            client.Headers.Add(HttpRequestHeader.Authorization, authHeader);
            
            //// WORKING
            string url = string.Format("https://api.forte.net/v2/accounts/{0}/locations/{1}/transactions/?filter=start_origination_date+eq+'{2}'+and+end_origination_date+eq+'{3}'&page_size={4}&page_index={5}", account, location, fromDate.ToString("s"), toDate.ToString("s"), pageSize, pageIndex);

            // transactions
            //string url = "https://api.forte.net/v1/accounts/act_300049/locations/loc_114393/transactions/?filter=start_origination_date+eq+'2014-01-01T00:00:00'+and+end_origination_date+eq+'2014-01-31T00:00:00'";

            // settlements
            //string url = "https://api.forte.net/v1/accounts/act_300049/locations/loc_114393/settlements/?filter=start_settle_date+eq+'2014-01-01'+and+end_settle_date+eq+'2014-01-31'";

            client.Headers.Add("X-Forte-Auth-Account-Id", account);
            Stream stream = client.OpenRead(url);            
            StreamReader reader = new StreamReader(stream);
            String response = reader.ReadToEnd();
            results = response;

            return results;
        }

        private IEnumerable<PaymentGatewayCustomerPaymethod> ConvertJsonToPaymethods(string json)
        {
            JsonPaymethods result = JsonConvert.DeserializeObject<JsonPaymethods>(json);
            List<PaymentGatewayCustomerPaymethod> methods = new List<PaymentGatewayCustomerPaymethod>();
            foreach (var p in result.results)
            {
                methods.Add(ConvertJsonPaymethod(p));
            }
            return methods;
        }

        private PaymentGatewayCustomerPaymethod ConvertJsonPaymethod(JsonPaymethod json)
        {
            PaymentGatewayCustomerPaymethod thisReturn = new PaymentGatewayCustomerPaymethod()
            {
                account_id = json.account_id,
                customer_token = json.customer_token,
                label = json.label,
                location_id = json.location_id, 
                notes = json.notes,
                paymethod_token = json.paymethod_token
            };

            if (json.card != null)
            {
                thisReturn.card_type = json.card.card_type;
                thisReturn.expire_month = json.card.expire_month;
                thisReturn.expire_year = json.card.expire_year;
                thisReturn.masked_account_number = json.card.masked_account_number;
                thisReturn.name_on_card = json.card.name_on_card;
            }

            if (json.echeck != null)
            {
                thisReturn.card_type = "echeck";
                thisReturn.routing_number = json.echeck.routing_number;
                thisReturn.name_on_card = json.echeck.account_holder;
                thisReturn.masked_account_number = json.echeck.masked_account_number;
                thisReturn.account_type = json.echeck.account_type;
            }
            return thisReturn;
        }
        private IEnumerable<PaymentGatewayCustomer> ConvertJsonToCustomers(string json)
        {
            JsonCustomersResult result = JsonConvert.DeserializeObject<JsonCustomersResult>(json);
            List<PaymentGatewayCustomer> customers = new List<PaymentGatewayCustomer>();
            foreach (var c in result.results)
            {
                if (c.addresses == null && c.customer_id.Length > 0)
                {
                    customers.Add(GetCustomerByCustomerID(c.location_id.ToString().Split('_')[1], Convert.ToInt32(c.customer_id)));
                }
                else if (c.customer_id.Length > 0)
                {
                    customers.Add(ConvertJsonCustomer(c));
                }
            }
            return customers;
        }
 
        private IEnumerable<PaymentGatewayTransaction> ConvertJsonToTransactions(string json)
        {
            //dynamic dyn = JsonConvert.DeserializeObject(json);
            //var transactions = new List<PaymentGatewayTransaction>();
            //foreach (var obj in dyn.results)
            //{
            //    transactions.Add(new PaymentGatewayTransaction()
            //    {
            //        Id = (obj.transaction_id != null) ? obj.transaction_id.ToString() : string.Empty,
            //        AccountId = (obj.account_id != null) ? obj.account_id.ToString() : string.Empty,
            //        LocationId = (obj.location_id != null) ? obj.location_id.ToString() : string.Empty,
            //        CustomerToken = (obj.customer_token != null) ? obj.customer_token.ToString() : string.Empty,
            //        OrderId = (obj.order_id != null) ? obj.order_id.ToString() : string.Empty,
            //        Status = (obj.status != null) ? obj.status.ToString() : string.Empty,
            //        Action = (obj.action != null) ? obj.action.ToString() : string.Empty,
            //        Amount = (obj.authorization_amount != null) ? decimal.Parse(obj.authorization_amount.ToString()) : 0M,
            //        AuthorizationCode = (obj.authorization_code != null) ? obj.authorization_code.ToString() : string.Empty,
            //        EnteredBy = (obj.entered_by != null) ? obj.entered_by.ToString() : string.Empty,
            //        ReceivedDate = (obj.received_date != null) ? DateTime.Parse(obj.received_date.ToString()) : string.Empty,
            //        OriginationDate = (obj.origination_date != null) ? DateTime.Parse(obj.origination_date.ToString()) : string.Empty,
            //        BillToFirstName = (obj.bill_to_first_name != null) ? obj.bill_to_first_name.ToString() : string.Empty,
            //        BillToLastName = (obj.bill_to_last_name != null) ? obj.bill_to_last_name.ToString() : string.Empty,
            //        BillToCompanyName = (obj.bill_to_company_name != null) ? obj.bill_to_company_name.ToString() : string.Empty
            //    });
            //}
            //return transactions;

            JsonTransactionsResult result = JsonConvert.DeserializeObject<JsonTransactionsResult>(json);
            List<PaymentGatewayTransaction> transactions = new List<PaymentGatewayTransaction>();
            foreach (var t in result.results)
            {
                //transactions.Add(new PaymentGatewayTransaction()
                //{
                //    Id = t.transaction_id,
                //    AccountId = t.account_id,
                //    LocationId = t.location_id,
                //    CustomerToken = t.customer_token,
                //    OrderId = t.order_number,
                //    Status = t.status,
                //    Action = t.action,
                //    Amount = t.authorization_amount,
                //    AuthorizationCode = t.authorization_code,
                //    EnteredBy = t.entered_by,
                //    ReceivedDate = t.received_date,
                //    //OriginationDate = t.origination_date,
                //    //BillToFirstName = t.bill_to_first_name,
                //    //BillToLastName = t.bill_to_last_name
                //    BillToFirstName = t.billing_address.first_name,
                //    BillToLastName = t.billing_address.last_name,
                //    NameOnCard = t.billing_address.name_on_card
                //});
                transactions.Add(ConvertJsonTransaction(t));
            }

            return transactions;
        }

        private string GetTransactionJson(string location, string transactionId)
        {
            //// WORKING
            //string url = string.Format("https://api.forte.net/v1/accounts/{0}/locations/{1}/transactions/?filter=start_origination_date+eq+'{2}'+and+end_origination_date+eq+'{3}'&page_size={4}&page_index={5}", account, location, fromDate.ToString("s"), toDate.ToString("s"), pageSize, pageIndex);
            string url = string.Format("https://api.forte.net/v2/accounts/{0}/locations/loc_{1}/transactions/{2}", account, location, transactionId);
            //string url = string.Format("https://api.forte.net/v1/accounts/{0}/locations/{1}/transactions/{2}", account, location, transactionId);
            string response = PaymentGatewayCall(url);
            return response;
        }

        private PaymentGatewayCustomer ConvertJsonToCustomer(string json)
        {
            JsonCustomer customer = JsonConvert.DeserializeObject<JsonCustomer>(json);
            return ConvertJsonCustomer(customer);
        }

        private PaymentGatewayTransaction ConvertJsonToTransaction(string json)
        {
            JsonTransaction result = JsonConvert.DeserializeObject<JsonTransaction>(json);
            //PaymentGatewayTransaction transaction = new PaymentGatewayTransaction()
            //{
            //    //Id = result.transaction_id,
            //    //BillToFirstName = result.billing_address.first_name,
            //    //BillToLastName = result.billing_address.last_name
            //};
            //return transaction;
            return ConvertJsonTransaction(result);
        }

        private PaymentGatewayCustomer ConvertJsonCustomer(JsonCustomer jsonCust)
        {
            return new PaymentGatewayCustomer()
            {
                customerID = jsonCust.customer_id,
                customerToken = jsonCust.customer_token,
                locationID = jsonCust.location_id,
                status = jsonCust.status,
                firstName = jsonCust.first_name,
                lastName = jsonCust.last_name,
                email_address = jsonCust.addresses[0].email,
                phone_number = jsonCust.addresses[0].phone,
                postalCode = jsonCust.addresses[0].physical_address.postal_code,
                region = jsonCust.addresses[0].physical_address.region,
                locality = jsonCust.addresses[0].physical_address.locality,
                street_line1 = jsonCust.addresses[0].physical_address.street_line1,
                street_line2 = jsonCust.addresses[0].physical_address.street_line2,
                defaultPaymethodToken = jsonCust.default_paymethod_token,
                settlementPath = jsonCust.links.settlements,
                paymethodPath = jsonCust.links.paymethods,
                transactionPath = jsonCust.links.transactions,
                addressPath = jsonCust.links.addresses,
                selfPath = jsonCust.links.self
            };
        }

        private PaymentGatewayTransaction ConvertJsonTransaction(JsonTransaction jsonTran)
        {
            return new PaymentGatewayTransaction()
            {
                Id = jsonTran.transaction_id,
                AccountId = jsonTran.account_id,
                LocationId = jsonTran.location_id,
                CustomerToken = jsonTran.customer_token,
                CustomerId = jsonTran.customer_id,
                OrderId = jsonTran.order_number,
                Status = jsonTran.status,
                Action = jsonTran.action,
                Amount = jsonTran.authorization_amount,
                AuthorizationCode = jsonTran.authorization_code,
                EnteredBy = jsonTran.entered_by,
                ReceivedDate = jsonTran.received_date,
                BillToFirstName = (jsonTran.billing_address != null) ? jsonTran.billing_address.first_name : string.Empty,
                BillToLastName = (jsonTran.billing_address != null) ? jsonTran.billing_address.last_name : string.Empty,
                //NameOnCard = jsonTran.billing_address.name_on_card
                NameOnCard = (jsonTran.paymethod != null) ? ((jsonTran.paymethod.card != null) ? jsonTran.paymethod.card.name_on_card : string.Empty) : string.Empty,
                CardNumber = (jsonTran.paymethod != null) ? ((jsonTran.paymethod.card != null) ? jsonTran.paymethod.card.masked_account_number : string.Empty) : string.Empty,
                CardType = (jsonTran.paymethod != null) ? ((jsonTran.paymethod.card != null) ? jsonTran.paymethod.card.card_type : string.Empty) : string.Empty
            };
        }

        #endregion

        #region Settlements

        private IEnumerable<PaymentGatewaySettlement> GetSettlements(string location, DateTime fromDate, DateTime toDate)
        {
            List<PaymentGatewaySettlement> settlements = new List<PaymentGatewaySettlement>();

            int pageIndex = 0;
            int pageSize = 50;
            int recordIndex = 0;
            int totalResultsCount = -1;
            while (true)
            {
                string json = GetSettlementsJson(location, pageSize, pageIndex, fromDate, toDate);
                if (totalResultsCount < 0)
                {
                    //dynamic dyn = JsonConvert.DeserializeObject(json);
                    //totalResultsCount = (dyn.number_results != null) ? int.Parse(dyn.number_results.ToString()) : 0;
                    JsonSettlementsResult result = JsonConvert.DeserializeObject<JsonSettlementsResult>(json);
                    totalResultsCount = result.number_results;
                }
                IEnumerable<PaymentGatewaySettlement> setts = ConvertJsonToSettlements(json);
                if (setts.Count() > 0)
                {
                    settlements.AddRange(ConvertJsonToSettlements(json));
                }
                else
                {
                    break;
                }
                pageIndex++;
                recordIndex += pageSize;
            }

            return settlements;
        }

        private string GetSettlementsJson(string location, int pageSize, int pageIndex, DateTime fromDate, DateTime toDate)
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

            string results = string.Empty;
            WebClient client = new WebClient();
            client = generateAuthHeader(client);
            //string url = "https://api.forte.net/v2/accounts/act_300049/locations/loc_114393/settlements/?filter=start_settle_date+eq+'2014-01-01'+and+end_settle_date+eq+'2014-01-31'";
            string url = string.Format("https://api.forte.net/v2/accounts/{0}/locations/loc_{1}/settlements/?filter=start_settle_date+eq+'{2}'+and+end_settle_date+eq+'{3}'&page_size={4}&page_index={5}", account, location, fromDate.ToString("s"), toDate.ToString("s"), pageSize, pageIndex);

            client.Headers.Add("X-Forte-Auth-Account-Id", account);
            Stream stream = client.OpenRead(url);
            StreamReader reader = new StreamReader(stream);
            String response = reader.ReadToEnd();
            results = response;

            return results;
        }

        private IEnumerable<PaymentGatewaySettlement> ConvertJsonToSettlements(string json)
        {
            //dynamic dyn = JsonConvert.DeserializeObject(json);
            //var settlements = new List<PaymentGatewaySettlement>();
            //foreach (var obj in dyn.results)
            //{
            //    settlements.Add(new PaymentGatewaySettlement()
            //    {
            //        Id = (obj.settle_id != null) ? obj.settle_id.ToString() : string.Empty,
            //        Date = (obj.settle_date != null) ? DateTime.Parse(obj.settle_date.ToString()) : string.Empty,
            //        Amount = (obj.settle_amount != null) ? decimal.Parse(obj.settle_amount.ToString()) : 0M,
            //        Method = (obj.method != null) ? obj.method.ToString() : string.Empty
            //    });
            //}
            //return settlements;
            List<PaymentGatewaySettlement> settlements = new List<PaymentGatewaySettlement>();

            JsonSettlementsResult result = JsonConvert.DeserializeObject<JsonSettlementsResult>(json);
            foreach (var s in result.results)
            {
                settlements.Add(new PaymentGatewaySettlement()
                {
                    Id = s.settle_id,
                    TransactionId = s.transaction_id,
                    customerToken = s.customer_token,
                    CustomerId = s.customer_id,
                    locationID = s.location_id,
                    Date = s.settle_date,
                    Amount = s.settle_amount,
                    Method = s.method,
                    Type = s.settle_type
                });

            }

            return settlements;
        }

        #endregion

        private void Test()
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://www.mysite.com");

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Stream stream = response.GetResponseStream();

            StreamReader sr = new StreamReader(stream);

            string resp = sr.ReadToEnd();

        }
    }
}

