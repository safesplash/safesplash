﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PaymentGateway
{
    public class JsonCreateCustomer
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string company_name { get; set; }
        public string customer_id { get; set; }
        public List<JsonCreateCustomerAddress> addresses {get; set;}
        public JsonCreateCustomerPaymethod paymethod {get; set;}
    }

    public class JsonCreateSimpleCustomer
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string company_name {get; set;}
        public string customer_id { get; set; }
    }

    public class JsonCreateSimpleCustomerResponse
    {
        public string customer_token { get; set; }
        public string location_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string company_name { get; set; }
        public string customer_id { get; set; }
        public CreateResponse response { get; set; }
        public customerLinks links { get; set; }
    }

    public class JsonCreateSimpleCardPaymethod
    {
        public string notes { get; set; }
        public JsonCreateCustomerCard card { get; set; }
    }

    public class JsonCreateSimpleAddress
    {
        public string address_token { get; set; }
        public string customer_token { get; set; }
        public string label { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string company_name { get; set; }
        public string phone_number { get; set; }
        public string fax_number { get; set; }
        public string email_address { get; set; }
        public JsonCreateCustomerPhysicalAddress physical_address { get; set; }
    }
    public class CreateResponse
    {
        public string environment { get; set; }
        public string response_desc { get; set; }
    }

    public class JsonCreateCustomerBank
    {
        public string notes { get; set; }
        public JsonBank echeck { get; set; }
    }

    public class JsonBank
    {
        public string account_holder { get; set; }
        public string account_number { get; set; }
        public string routing_number { get; set; }
        public string account_type { get; set; }
    }
    public class JsonCreateCustomerAddress 
    {
        public string label {get; set;}
        public string first_name {get; set;}
        public string last_name {get; set;}
        public string company_name {get; set;}
        public string phone_number {get; set;}
        public string fax_number {get; set;}
        public string email_address {get; set;}
        public string shipping_address_type {get; set;}
        public string address_type {get; set;}
        public JsonCreateCustomerPhysicalAddress addresses {get; set;}
    }

    public class JsonCreateCustomerPhysicalAddress
    {
        public string street_line1 {get; set;}
        public string street_line2 {get; set;}
        public string locality {get; set;}
        public string region {get; set;}
        public string postal_code {get; set;}
    }

    public class JsonCreateCustomerPaymethod
    {
        public string label {get; set;}
        public string notes {get; set;}
        public JsonCreateCustomerCard card {get; set;}
    }

    public class JsonCreateCustomerCard
    {
        public string account_number {get; set;}
        public int expire_month {get; set;}
        public int expire_year {get; set;}
        public string card_verification_value {get; set;}
        public string name_on_card {get; set;}
    }


}
