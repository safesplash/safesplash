﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PaymentGateway
{
    public class JsonVoidTransaction
    {
        public string account_id { get; set; }
        public string action { get; set; }
        public string authorization_code { get; set; }
        public string entered_by { get; set; }
    }
}
