﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PaymentGateway
{
    [Serializable]
    public class JsonTransactionResult
    {
        public string TransactionId { get; set; }
    }
}
