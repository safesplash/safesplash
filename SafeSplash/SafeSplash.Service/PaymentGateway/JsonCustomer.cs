﻿
using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

namespace SafeSplash.Service.PaymentGateway
{
    [Serializable]
    public class JsonCustomer
    {
        public string account_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string customer_token { get; set; }
        public string default_paymethod_token { get; set; }
        public string status { get; set; }
        public string location_id { get; set; }
        public string customer_id { get; set; }
        public customerLinks links { get; set; }
        public List<physicalAddress> addresses { get; set; }
        public JsonPaymethod paymethod { get; set; }

    }

    [Serializable]
    public class physicalAddress
    {
        public customerAddress physical_address { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string label { get; set; }
    }

    [Serializable]
    public class customerAddress
    {
        public string postal_code { get; set; }
        public string region { get; set; }
        public string locality { get; set; }
        public string street_line1 { get; set; }
        public string street_line2 { get; set; }

    }

    [Serializable]
    public class customerLinks
    {
        public string settlements { get; set; }
        public string transactions { get; set; }
        public string paymethods { get; set; }
        public string addresses { get; set; }
        public string self { get; set; }
    }
}