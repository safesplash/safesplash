﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.Employee
{
    public interface IEmployeeAction
    {
        Guid Id { get; set; }
        Guid EmployeeId { get; set; }
        string Action { get; set; }
        DateTime Date { get; set; }
        string Description { get; set; }
        string Value { get; set; }
        string EmployeeFirstName { get; set; }
        string EmployeeLastName { get; set; }
        string EmployeeName { get; }
    }

    public class EmployeeAction : IEmployeeAction
    {
        public Guid Id { get; set; }
        public Guid EmployeeId { get; set; }
        public string Action { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeLastName { get; set; }
        public string EmployeeName
        {
            get { return string.Format("{0} {1}", EmployeeFirstName, EmployeeLastName); }
        }
    }

    public class EmployeeActionType
    {
        public int Id { get; private set; }
        public string Description { get; private set; }
        public int DisplaySortOrder { get; private set; }

        private static Dictionary<int, EmployeeActionType> allActionTypes = new Dictionary<int, EmployeeActionType>();

        public static readonly EmployeeActionType EmptyAction = new EmployeeActionType(0, string.Empty, 0);
        public static readonly EmployeeActionType BookedClass = new EmployeeActionType(1, "Booked Class", 1);
        public static readonly EmployeeActionType LoggedOn = new EmployeeActionType(2, "Logged On", 2);
        public static readonly EmployeeActionType RunReportActiveClasses = new EmployeeActionType(3, "Run Report - Active Classes", 3);
        public static readonly EmployeeActionType RunReportActiveClassesByType = new EmployeeActionType(4, "Run Report - Active Classes By Type", 4);
        public static readonly EmployeeActionType RunReportClientRegistration = new EmployeeActionType(5, "Run Report - Client Registration", 5);
        public static readonly EmployeeActionType RunReportCustomerBirthdays = new EmployeeActionType(6, "Run Report - Customer Birthdays", 6);
        public static readonly EmployeeActionType RunReportCustomerInformation = new EmployeeActionType(7, "Run Report - Customer Information", 7);
        public static readonly EmployeeActionType RunReportDailyClassesReport = new EmployeeActionType(8, "Run Report - Daily Classes Report", 8);
        public static readonly EmployeeActionType RunReportInstructorSubstitutions = new EmployeeActionType(9, "Run Report - Instructor Substitutions", 9);
        public static readonly EmployeeActionType RunReportLengthOfACustomer = new EmployeeActionType(10, "Run Report - Length of a Customer", 10);
        public static readonly EmployeeActionType Withdrawal = new EmployeeActionType(11, "Withdrawal", 11);
        public static readonly EmployeeActionType SearchClientRegistration = new EmployeeActionType(12, "Search - Client Registration", 12);
        public static readonly EmployeeActionType SearchClientInformation = new EmployeeActionType(13, "Search - Client Information", 13);
        public static readonly EmployeeActionType SearchClassSchedule = new EmployeeActionType(14, "Search - Class Schedule", 14);
        public static readonly EmployeeActionType SearchClassManagement = new EmployeeActionType(15, "Search - Class Management", 15);
        public static readonly EmployeeActionType EditBillingInformation = new EmployeeActionType(16, "Edit Billing Information", 16);
        public static readonly EmployeeActionType EditBillingPayment = new EmployeeActionType(17, "Billing Payment", 17);
        public static readonly EmployeeActionType EditClass = new EmployeeActionType(18, "Edit Class", 18);
        public static readonly EmployeeActionType PromoCodeRedemption = new EmployeeActionType(19, "Promo Code Redemption", 19);
        public static readonly EmployeeActionType RunReportMonthlyRevenue = new EmployeeActionType(20, "Run Report - Monthly Revenue", 20);
        public static readonly EmployeeActionType InactivateEmployee = new EmployeeActionType(21, "Inactivate Employee", 21);
        public static readonly EmployeeActionType DeleteEmployee = new EmployeeActionType(22, "Delete Employee", 22);
        
        
        private EmployeeActionType(int id, string description, int displaySortOrder)
        {
            Id = id;
            Description = description;
            DisplaySortOrder = displaySortOrder;
            allActionTypes.Add(id, this);
        }

        public static IEnumerable<EmployeeActionType> GetAllActionTypes()
        {
            return allActionTypes.Values;
        }
    }
}
