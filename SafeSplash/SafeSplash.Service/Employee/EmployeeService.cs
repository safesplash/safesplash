﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.NHibernate;
using AKMII.SafeSplash.BLL;
using AKMII.SafeSplash.Entity;

namespace SafeSplash.Service.Employee
{
    public interface IEmployeeService
    {
        IEmployeeAction Save(IEmployeeAction action);
        IEnumerable<IEmployeeAction> GetActions(Guid? employeeId, string action, DateTime startDate, DateTime endDate);
        Employee GetEmployeeById(Guid id);
    }

    public class EmployeeService : IEmployeeService
    {
        IEmployeeActionDataAccess dataAccess = new EmployeeActionDataAccess();
        Dictionary<Guid, tb_Employee> employeesById = new Dictionary<Guid, tb_Employee>();

        public IEmployeeAction Save(IEmployeeAction action)
        {
            IEmployeeActionDataObject dataObject = EmployeeActionHelper.Convert(action);
            dataObject = dataAccess.Save(dataObject);
            return EmployeeActionHelper.Convert(dataObject);
        }       

        public IEnumerable<IEmployeeAction> GetActions(Guid? employeeId, string action, DateTime startDate, DateTime endDate)
        {
            List<IEmployeeAction> actions = new List<IEmployeeAction>();
            DateTime startDateArg = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
            DateTime endDateArg = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
            //IEnumerable<IEmployeeActionDataObject> dataObjects = dataAccess.GetActions(employeeId, startDateArg, endDateArg);
            IEnumerable<IEmployeeActionDataObject> dataObjects = dataAccess.GetActions(employeeId, action, startDateArg, endDateArg);
            EmployeeBll employeeBll = new EmployeeBll();
            foreach (var dataObject in dataObjects)
            {
                IEmployeeAction employeeAction = EmployeeActionHelper.Convert(dataObject);
                employeeAction.EmployeeFirstName = "Unknown";
                if (!employeesById.ContainsKey(employeeAction.EmployeeId))
                {
                    tb_Employee employee = employeeBll.GetByID(employeeAction.EmployeeId);
                    if (employee != null)
                    {
                        employeesById.Add(employee.empl_ID, employee);
                    }
                }
                if (employeesById.ContainsKey(employeeAction.EmployeeId))
                {
                    employeeAction.EmployeeFirstName = employeesById[employeeAction.EmployeeId].empl_FirstName;
                    employeeAction.EmployeeLastName = employeesById[employeeAction.EmployeeId].empl_LastName;
                }
                actions.Add(employeeAction);
            }
            return actions.OrderBy(a => a.Date).ThenBy(a => a.EmployeeName);
        }


        public Employee GetEmployeeById(Guid id)
        {
            if (employeesById.ContainsKey(id))
            {
                tb_Employee emp = employeesById[id];
                return new Employee() { Id = emp.empl_ID, FirstName = emp.empl_FirstName, LastName = emp.empl_LastName };
            }
            Employee employee = new Employee() { Id = Guid.Empty, FirstName = "Employee", LastName = "Not Found." };
            EmployeeBll employeeBll = new EmployeeBll();
            tb_Employee tb_emp = employeeBll.GetByID(id);
            if (tb_emp != null)
            {
                employee = new Employee() { Id = tb_emp.empl_ID, FirstName = tb_emp.empl_FirstName, LastName = tb_emp.empl_LastName };
                employeesById[id] = tb_emp;
            }

            return employee;
        }
    }

    public class EmployeeActionHelper
    {
        public static IEmployeeAction Convert(IEmployeeActionDataObject dataObject)
        {
            IEmployeeAction action = new EmployeeAction()
            {
                Id = dataObject.Id,
                EmployeeId = dataObject.EmployeeId,
                Action = dataObject.Action,
                Date = dataObject.Date,
                Description = dataObject.Description,
                Value = dataObject.Value
            };
            return action;
        }

        public static IEmployeeActionDataObject Convert(IEmployeeAction action)
        {
            IEmployeeActionDataObject dataObject = new EmployeeActionDataObject()
            {
                Id = action.Id,
                EmployeeId = action.EmployeeId,
                Action = action.Action,
                Date = action.Date,
                Description = action.Description,
                Value = action.Value
            };
            return dataObject;
        }
    }
}
