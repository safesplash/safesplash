﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SafeSplash.Service
{
    public class Database
    {
        private static string connection = null;

        public static string ConnectionString
        {
            get
            {
                if (connection == null)
                {
                    connection = ConfigurationManager.ConnectionStrings["SafeSplashPortalConnectionString"].ConnectionString;
                }
                return connection;
            }
        }
    }
}
