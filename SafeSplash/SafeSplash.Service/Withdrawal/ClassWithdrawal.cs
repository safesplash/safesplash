﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.Withdrawal
{
    public interface IClassWithdrawal
    {
        Guid LocationId { get; set; }
        DateTime WithdrawalDate { get; set; }
        string Reason { get; set; }
        string ClassName { get; set; }
    }

    public class ClassWithdrawal : IClassWithdrawal
    {
        public Guid LocationId { get; set; }
        public DateTime WithdrawalDate { get; set; }
        public string Reason { get; set; }
        public string ClassName { get; set; }
    }
}
