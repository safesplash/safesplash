﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.NHibernate;

namespace SafeSplash.Service.Withdrawal
{
    public interface IWithdrawalService
    {
        IEnumerable<IWithdrawalReason> GetAllReasons();
        IEnumerable<IClassWithdrawal> GetWithdrawals(Guid locationId, DateTime fromDate, DateTime toDate);
    }

    public class WithdrawalService : IWithdrawalService
    {
        

        public IEnumerable<IWithdrawalReason> GetAllReasons()
        {
            IWithdrawalDataAccess da = new WithdrawalDataAccess();
            List<IWithdrawalReason> reasons = new List<IWithdrawalReason>();
            foreach (var reasonDataObject in da.GetAllWithdrawalReasons())
            {
                reasons.Add(new WithdrawalReason() { Id = reasonDataObject.Id, Name = reasonDataObject.Name, Description = reasonDataObject.Description, Active = reasonDataObject.Active });
            }
            return reasons;
        }

        public IEnumerable<IClassWithdrawal> GetWithdrawals(Guid locationId, DateTime fromDate, DateTime toDate)
        {
            SafeSplash.DataAccess.Sql.IWithdrawalDataAccess da = new SafeSplash.DataAccess.Sql.WithdrawalDataAccess();
            List<IClassWithdrawal> withdrawals = new List<IClassWithdrawal>();
            foreach (var withdrawalDataObject in da.GetWithdrawals(fromDate, toDate, locationId))
            {
                //withdrawals.Add(new ClassWithdrawal() { LocationId = withdrawalDataObject.LocationId, WithdrawalDate = withdrawalDataObject.WithdrawalDate.Value, Reason = withdrawalDataObject.Reason, ClassName = withdrawalDataObject.ClassName });
                withdrawals.Add(new ClassWithdrawal() { Reason = withdrawalDataObject.Reason, ClassName = withdrawalDataObject.ClassName });
            }
            return withdrawals;
        }
    }
}
