﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.Withdrawal
{
    public interface IWithdrawalReason
    {
        Guid Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        bool Active { get; set; }
    }

    public class WithdrawalReason : IWithdrawalReason
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
    }
}
