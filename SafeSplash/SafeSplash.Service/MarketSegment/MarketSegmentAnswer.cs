﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.MarketSegment
{
    [Serializable]
    public class MarketSegmentAnswer
    {
        public int Id { get; set; }
        public string Answer { get; set; }
        public MarketSegment Segment { get; set; }
    }
}
