﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.MarketSegment
{
    public interface IMarketSegmentService
    {
        MarketSegment DeterminePrimaryMarketSegment(IEnumerable<MarketSegmentAnswer> answers);
        MarketSegment DetermineSecondaryMarketSegment(IEnumerable<MarketSegmentAnswer> answers);
    }
}
