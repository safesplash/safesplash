﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.MarketSegment;

namespace SafeSplash.Service.MarketSegment
{
    [Serializable]
    public class MarketSegmentService : IMarketSegmentService
    {
        public MarketSegment DeterminePrimaryMarketSegment(IEnumerable<MarketSegmentAnswer> answers)
        {
            MarketSegment primarySegment = null;
            List<MarketSegment> allMarketSegments = answers.Select(a => a.Segment).Distinct<MarketSegment>().ToList<MarketSegment>();
            Dictionary<MarketSegment, int> points = new Dictionary<MarketSegment, int>();
            foreach (var s in allMarketSegments)
            {
                int segmentPoints = answers.Where(a => a.Segment.Id == s.Id).Count() * s.Weight;
                points.Add(s, segmentPoints);
            }

            IOrderedEnumerable<KeyValuePair<MarketSegment, int>> sortedPoints = points.OrderByDescending(i => i.Value).ThenByDescending(i => i.Key.Weight);
            if (sortedPoints.Count() > 0)
            {
                primarySegment = sortedPoints.First().Key;
            }

            return primarySegment;
        }

        public MarketSegment DetermineSecondaryMarketSegment(IEnumerable<MarketSegmentAnswer> answers)
        {
            MarketSegment secondarySegment = null;
            List<MarketSegment> allMarketSegments = answers.Select(a => a.Segment).Distinct<MarketSegment>().ToList<MarketSegment>();
            Dictionary<MarketSegment, int> points = new Dictionary<MarketSegment, int>();
            foreach (var s in allMarketSegments)
            {
                int segmentPoints = answers.Where(a => a.Segment.Id == s.Id).Count() * s.Weight;
                points.Add(s, segmentPoints);
            }

            IOrderedEnumerable<KeyValuePair<MarketSegment, int>> sortedPoints = points.OrderByDescending(i => i.Value).ThenByDescending(i => i.Key.Weight);
            if (sortedPoints.Count() > 1)
            {
                int counter = 1;
                foreach (var item in sortedPoints)
                {
                    if (counter == 2)
                    {
                        secondarySegment = item.Key;
                        break;
                    }
                    counter++;
                }
            }

            return secondarySegment;
        }
    }
}
