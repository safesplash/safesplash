﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.MarketSegment
{
    [Serializable]
    public class MarketSegment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Weight { get; set; }

        public override bool Equals(Object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            MarketSegment s = (MarketSegment)obj;
            return (Id == s.Id);
        }
        public override int GetHashCode()
        {
            return Id ^ Weight;
        }

    }
}
