﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.Content
{
    public interface IEditableContent
    {
        int Id { get; set; }
        string Description { get; set; }
        string Name { get; set; }
        string Value { get; set; }
    }

    public class EditableContent : IEditableContent
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public interface IEditableContentType
    {
        int Id { get; }
        string Name { get; }
    }

    public class EditableContentType : IEditableContentType
    {
        public int Id { get; private set; }
        public string Name { get; private set; }

        private static Dictionary<int, IEditableContentType> typesById = new Dictionary<int, IEditableContentType>();

        public static readonly IEditableContentType RegistrationFormLocations = new EditableContentType(1, "Registration Form Locations Information");

        private EditableContentType(int id, string name)
        {
            Id = id;
            Name = name;
            typesById.Add(id, this);
        }

        public static IEditableContentType GetById(int id)
        {
            if (!typesById.ContainsKey(id))
            {
                throw new KeyNotFoundException(string.Format("No EditableContentType found for Id: {0}", id));
            }
            return typesById[id];
        }
    }
}
