﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.NHibernate;

namespace SafeSplash.Service.Content
{
    public interface IEditableContentService
    {
        IEditableContent GetEditableContent(IEditableContentType type);
        IEditableContent SaveEditableContent(IEditableContent content);
    }

    public class EditableContentService : IEditableContentService
    {
        IEditableContentDataAccess dataAccess = new EditableContentDataAccess();

        public IEditableContent GetEditableContent(IEditableContentType type)
        {
            IEditableContentDataObject dataObject = dataAccess.Get(type.Id);
            if (dataObject == null)
                return null;
            return ContentConverter.Convert(dataObject);
        }

        public IEditableContent SaveEditableContent(IEditableContent content)
        {
            IEditableContentDataObject dataObject = ContentConverter.Convert(content);
            dataObject = dataAccess.Save(dataObject);
            return ContentConverter.Convert(dataObject);
        }
    }

    public class ContentConverter
    {
        public static IEditableContent Convert(IEditableContentDataObject dataObject)
        {
            IEditableContent content = new EditableContent()
            {
                Id = dataObject.Id,
                Description = dataObject.Description,
                Name = dataObject.Name,
                Value = dataObject.Value
            };
            return content;
        }

        public static IEditableContentDataObject Convert(IEditableContent content)
        {
            IEditableContentDataObject dataObject = new EditableContentDataObject()
            {
                Id = content.Id,
                Description = content.Description,
                Name = content.Name,
                Value = content.Value
            };
            return dataObject;
        }

    }
}
