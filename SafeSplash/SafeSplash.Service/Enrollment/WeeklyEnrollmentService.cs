﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.NHibernate;
using SafeSplash.Service.Reports;
using SafeSplash.Service.Reports.WeeklyEnrollment;

namespace SafeSplash.Service.Enrollment
{
    public interface IWeeklyEnrollmentService
    {
        WeeklyEnrollment SaveWeeklyEnrollment(WeeklyEnrollment weeklyEnrollment);
        IEnumerable<WeeklyEnrollment> GetWeeklyEnrollments(Guid locationId, DateTime fromDate, DateTime toDate);
        IEnumerable<WeeklyEnrollment> BuildWeeklyEnrollment(Guid locationId, DateTime fromDate, DateTime toDate, DayOfWeek dayOfWeek);
    }

    public class WeeklyEnrollmentService : IWeeklyEnrollmentService
    {
        IWeeklyEnrollmentDataAccess da = new WeeklyEnrollmentDataAccess();

        public WeeklyEnrollment SaveWeeklyEnrollment(WeeklyEnrollment weeklyEnrollment)
        {
            WeeklyEnrollmentDataObject dataObject = WeeklyEnrollmentConverter.Convert(weeklyEnrollment);
            dataObject = da.SaveWeeklyEnrollment(dataObject);
            WeeklyEnrollment enrollment = WeeklyEnrollmentConverter.Convert(dataObject);
            return enrollment;
        }

        public IEnumerable<WeeklyEnrollment> GetWeeklyEnrollments(Guid locationId, DateTime fromDate, DateTime toDate)
        {
            IEnumerable<WeeklyEnrollmentDataObject> enrollmentDataObjects = da.GetWeeklyEnrollments(locationId, fromDate, toDate);
            List<WeeklyEnrollment> enrollments = new List<WeeklyEnrollment>();
            foreach (var enrollmentDataObject in enrollmentDataObjects)
            {
                enrollments.Add(WeeklyEnrollmentConverter.Convert(enrollmentDataObject));
            }
            return enrollments;
        }


        public IEnumerable<WeeklyEnrollment> BuildWeeklyEnrollment(Guid locationId, DateTime fromDate, DateTime toDate, DayOfWeek dayOfWeek)
        {
            List<WeeklyEnrollment> enrollmentsAdded = new List<WeeklyEnrollment>();
            IEnumerable<WeeklyEnrollment> existingEnrollments = GetWeeklyEnrollments(locationId, fromDate, toDate);
            IEnumerable<ReportWeek> reportWeeks = WeeklyEnrollmentHelper.GenerateReportWeeks(fromDate, toDate, dayOfWeek);
            List<ReportWeek> weeksToBuildEnrollmentFor = new List<ReportWeek>();
            IWeeklyEnrollmentReportService reportService = new WeeklyEnrollmentReportService();
            foreach (var rw in reportWeeks)
            {
                if (existingEnrollments.Where(e => e.FromDate.CompareTo(rw.StartDate) == 0 && e.ToDate.CompareTo(rw.EndDate) == 0).Count() < 1)
                {
                    weeksToBuildEnrollmentFor.Add(rw);
                }
            }
            DateTime dateUpdated = DateTime.Now;
            if (weeksToBuildEnrollmentFor.Count() > 0)
            {
                IWeeklyEnrollmentReport report = reportService.GetEnrollmentReport(weeksToBuildEnrollmentFor, locationId);
                foreach (var r in report.Records)
                {
                    WeeklyEnrollment enrollment = new WeeklyEnrollment()
                    {
                        LocationId = report.LocationId,
                        FromDate = r.FromDate,
                        ToDate = r.ToDate,
                        StudentsEnrolled = r.StudentsEnrolled,
                        PrivateSpotsFilled = r.PrivateSpotsFilled,
                        SemiPrivateSpotsFilled = r.SemiPrivateSpotsFilled,
                        Capacity = r.Capacity,
                        StudentsWithdrawn = r.SpotsWithdrawn,
                        PrivateSpotsWithdrawn = r.PrivateSpotsWithdrawn,
                        SemiPrivateSpotsWithdrawn = r.SemiPrivateSpotsWithdrawn,
                        RIP = r.RegistrationsInProgress,
                        DateUpdated = dateUpdated
                    };
                    WeeklyEnrollment enrollmentAdded = SaveWeeklyEnrollment(enrollment);
                    enrollmentsAdded.Add(enrollmentAdded);
                }
            }
            

            return enrollmentsAdded;
        }
    }

    public class WeeklyEnrollmentHelper
    {
        public static DateTime GetBeginDate(DateTime fromDate, DayOfWeek dayOfWeek)
        {
            DateTime workingDate = fromDate;
            while (workingDate.DayOfWeek != dayOfWeek)
            {
                workingDate = workingDate.AddDays(1);
            }
            return workingDate;
        }

        public static IEnumerable<ReportWeek> GenerateReportWeeks(DateTime fromDate, DateTime toDate, DayOfWeek dayOfWeek)
        {
            List<ReportWeek> weeks = new List<ReportWeek>();
            DateTime weekStartDate = GetBeginDate(fromDate, dayOfWeek);
            DateTime weekEndDate = weekStartDate.AddDays(6);
            while (weekEndDate.CompareTo(toDate) <= 0)
            {
                weeks.Add(new ReportWeek() { StartDate = new DateTime(weekStartDate.Year, weekStartDate.Month, weekStartDate.Day, 0, 0, 0), EndDate = new DateTime(weekEndDate.Year, weekEndDate.Month, weekEndDate.Day, 23, 59, 59) });
                weekStartDate = weekEndDate.AddDays(1);
                weekEndDate = weekStartDate.AddDays(6);
            }

            return weeks;
        }
    }

    public class WeeklyEnrollmentConverter
    {
        public static WeeklyEnrollment Convert(WeeklyEnrollmentDataObject dataObject)
        {
            WeeklyEnrollment enrollment = new WeeklyEnrollment()
            {
                Id = dataObject.Id,
                LocationId = dataObject.LocationId,
                FromDate = dataObject.FromDate,
                ToDate = dataObject.ToDate,
                StudentsEnrolled = dataObject.StudentsEnrolled,
                PrivateSpotsFilled = dataObject.PrivateSpotsFilled,
                SemiPrivateSpotsFilled = dataObject.SemiPrivateSpotsFilled,
                Capacity = dataObject.Capacity,
                StudentsWithdrawn = dataObject.StudentsWithdrawn,
                PrivateSpotsWithdrawn = dataObject.PrivateSpotsWithdrawn,
                SemiPrivateSpotsWithdrawn = dataObject.SemiPrivateSpotsWithdrawn,
                RIP = dataObject.RIP,
                DateUpdated = dataObject.DateUpdated
            };
            return enrollment;
        }

        public static WeeklyEnrollmentDataObject Convert(WeeklyEnrollment enrollment)
        {
            WeeklyEnrollmentDataObject dataObject = new WeeklyEnrollmentDataObject()
            {
                Id = enrollment.Id,
                LocationId = enrollment.LocationId,
                FromDate = enrollment.FromDate,
                ToDate = enrollment.ToDate,
                StudentsEnrolled = enrollment.StudentsEnrolled,
                PrivateSpotsFilled = enrollment.PrivateSpotsFilled,
                SemiPrivateSpotsFilled = enrollment.SemiPrivateSpotsFilled,
                Capacity = enrollment.Capacity,
                StudentsWithdrawn = enrollment.StudentsWithdrawn,
                PrivateSpotsWithdrawn = enrollment.PrivateSpotsWithdrawn,
                SemiPrivateSpotsWithdrawn = enrollment.SemiPrivateSpotsWithdrawn,
                RIP = enrollment.RIP,
                DateUpdated = enrollment.DateUpdated
            };
            return dataObject;
        }
    }


}
