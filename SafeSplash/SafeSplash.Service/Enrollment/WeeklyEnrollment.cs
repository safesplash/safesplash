﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.Enrollment
{
    [Serializable]
    public class WeeklyEnrollment
    {
        public int Id { get; set; }
        public Guid LocationId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int StudentsEnrolled { get; set; }
        public int PrivateSpotsFilled { get; set; }
        public int SemiPrivateSpotsFilled { get; set; }
        public int Capacity { get; set; }
        public int StudentsWithdrawn { get; set; }
        public int PrivateSpotsWithdrawn { get; set; }
        public int SemiPrivateSpotsWithdrawn { get; set; }        
        public int RIP { get; set; }
        public decimal PercentageCapacity { get; set; }
        public decimal PercentageWithdrawn { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
