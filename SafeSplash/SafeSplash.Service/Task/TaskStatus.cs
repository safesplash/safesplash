﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SafeSplash.Service.Task
{
    [DataContract]
    public class TaskStatus
    {
        [DataMember]
        public string Value { get; private set; }
        [DataMember]
        public string Description { get; private set; }

        private static List<TaskStatus> allStatuses = new List<TaskStatus>();

        //public static readonly TaskStatus NotStarted = new TaskStatus("NotStarted", "Not Started");
        //public static readonly TaskStatus InProgress = new TaskStatus("InProgress", "In Progress");
        //public static readonly TaskStatus Completed = new TaskStatus("Completed", "Completed");
        public static readonly TaskStatus Open = new TaskStatus("Open", "Open");
        public static readonly TaskStatus Closed = new TaskStatus("Closed", "Closed");

        private TaskStatus(string value, string description)
        {
            Value = value;
            Description = description;
            allStatuses.Add(this);
        }

        public static IEnumerable<TaskStatus> AllTaskStatuses
        {
            get { return allStatuses; }
        }
    }
}
