﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SafeSplash.Service.Task
{
    [DataContract]
    public class TaskType
    {
        [DataMember]
        public string Value { get; private set; }
        [DataMember]
        public string Description { get; private set; }

        private static List<TaskType> allTasks = new List<TaskType>();

        public static readonly TaskType MyTasks = new TaskType("MyOpenTasks", "My Tasks");
        public static readonly TaskType Created = new TaskType("Created", "Tasks I Opened");
        //public static readonly TaskType Closed = new TaskType("Closed", "Closed Tasks");
        
        
        private TaskType(string value, string description)
        {
            Value = value;
            Description = description;
            allTasks.Add(this);
        }

        public override bool Equals(object obj)
        {
            if(obj == null) return false;
            TaskType t = obj as TaskType;
            if ((object)t == null) { return false; }
            return this.Value.Equals(t.Value);
        }

        public static IEnumerable<TaskType> AllTaskTypes
        {
            get { return allTasks; }
        }
    }
}
