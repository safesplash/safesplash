﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.Email
{
    public interface IAutomatedEmail
    {
        int Id { get; set; }
        string Name { get; set; }
        string Subject { get; set; }
        string Content { get; set; }
        string CcRecipients { get; set; }
        string BccRecipients { get; set; }
    }

    public class AutomatedEmail : IAutomatedEmail
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string CcRecipients { get; set; }
        public string BccRecipients { get; set; }
    }

    public interface IAutomatedEmailType
    {
        int Id { get; }
        string Name { get; }
    }

    public class AutomatedEmailType : IAutomatedEmailType
    {
        public int Id { get; private set; }
        public string Name { get; private set; }

        private static Dictionary<int, IAutomatedEmailType> typesById = new Dictionary<int, IAutomatedEmailType>();

        public static readonly IAutomatedEmailType ClassBookingConfirmation = new AutomatedEmailType(1, "Class Booking Confirmation Email");
        public static readonly IAutomatedEmailType ClassBookingCampConfirmation = new AutomatedEmailType(2, "Class Booking Camp Confirmation Email");
        public static readonly IAutomatedEmailType NewClientAccountNotification = new AutomatedEmailType(3, "New Client Account Notification Email");
        public static readonly IAutomatedEmailType NewEmployeeAccountNotification = new AutomatedEmailType(4, "New Employee Account Notification Email");
        public static readonly IAutomatedEmailType PasswordResetNotification = new AutomatedEmailType(5, "Password Reset Notification Email");
        public static readonly IAutomatedEmailType NewClientRegistrationNotification = new AutomatedEmailType(6, "New Client Registration Notification Email");
        public static readonly IAutomatedEmailType BillingConfirmation = new AutomatedEmailType(7, "Billing Confirmation Email");
        public static readonly IAutomatedEmailType BillingCancelation = new AutomatedEmailType(8, "Billing Cancelation Email");
        public static readonly IAutomatedEmailType WithdrawalConfirmation = new AutomatedEmailType(9, "Client Withdrawal Request Confirmation Email");

        private AutomatedEmailType(int id, string name)
        {
            Id = id;
            Name = name;
            typesById.Add(id, this);
        }

        public static IAutomatedEmailType GetById(int id)
        {
            if (!typesById.ContainsKey(id))
            {
                throw new KeyNotFoundException(string.Format("No AutomatedEmailType found for Id: {0}", id));
            }
            return typesById[id];
        }
    }


}
