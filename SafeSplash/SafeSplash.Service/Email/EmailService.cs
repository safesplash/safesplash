﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using SafeSplash.DataAccess.NHibernate;

namespace SafeSplash.Service.Email
{
    public interface IEmailService
    {
        IAutomatedEmail GetAutomatedEmail(IAutomatedEmailType emailType);
        IAutomatedEmail SaveAutomatedEmail(IAutomatedEmail email);
    }

    public class EmailService : IEmailService
    {
        IAutomatedEmailDataAccess emailDataAccess = new AutomatedEmailDataAccess();

        public IAutomatedEmail GetAutomatedEmail(IAutomatedEmailType emailType)
        {
            IAutomatedEmailDataObject dataObject = emailDataAccess.Get(emailType.Id);
            if (dataObject == null)
                return null;
            return EmailConverter.Convert(dataObject);
        }


        public IAutomatedEmail SaveAutomatedEmail(IAutomatedEmail email)
        {
            IAutomatedEmailDataObject dataObject = EmailConverter.Convert(email);
            dataObject = emailDataAccess.Save(dataObject);
            return EmailConverter.Convert(dataObject);
        }
    }

    public class EmailConverter
    {
        public static IAutomatedEmail Convert(IAutomatedEmailDataObject dataObject)
        {
            IAutomatedEmail email = new AutomatedEmail()
            {
                Id = dataObject.Id,
                Name = dataObject.Name,
                Subject = dataObject.Subject,
                Content = dataObject.Content,
                CcRecipients = dataObject.Cc,
                BccRecipients = dataObject.Bcc
            };
            return email;
        }

        public static IAutomatedEmailDataObject Convert(IAutomatedEmail email)
        {
            IAutomatedEmailDataObject dataObject = new AutomatedEmailDataObject()
            {
                Id = email.Id,
                Name = email.Name,
                Subject = email.Subject,
                Content = email.Content,
                Cc = email.CcRecipients,
                Bcc = email.BccRecipients
            };
            return dataObject;
        }

    }
}
