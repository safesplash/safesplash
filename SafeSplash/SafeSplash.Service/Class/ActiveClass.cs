﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SafeSplash.Service.Class
{
    [DataContract]
    public class ActiveClass
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid ClassId { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public Guid LevelId { get; set; }
        [DataMember]
        public string LevelName { get; set; }
        [DataMember]
        public string DayOfWeek { get; set; }
        [DataMember]
        public int Capacity { get; set; }
        [DataMember]
        public DateTime? StartDate { get; set; }
        [DataMember]
        public DateTime? EndDate { get; set; }
        [DataMember]
        public string StartTime { get; set; }
        [DataMember]
        public string EndTime { get; set; }
        [DataMember]
        public Guid ClassTypeId { get; set; }
        [DataMember]
        public string ClassTypeName { get; set; }
        [DataMember]
        public Guid LocationId { get; set; }
        [DataMember]
        public string LocationName { get; set; }
        [DataMember]
        public Guid InstructorId { get; set; }
        [DataMember]
        public string InstructorName { get; set; }
        [DataMember]
        public int StudentCount { get; set; }

        public bool IsActive { get { return (!EndDate.HasValue) || (EndDate.Value.CompareTo(DateTime.Today) >= 0); } }
    }
}
