﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SafeSplash.Service.Class
{
    public class ActiveClassHistory
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public Guid ActiveClassId { get; set; }
        [DataMember]
        public String Column { get; set; }
        [DataMember]
        public String OldValue { get; set; }
        [DataMember]
        public String NewValue { get; set; }
        [DataMember]
        public DateTime ChangeDate { get; set; }
        [DataMember]
        public bool IsInstructorChange
        {
            get { return Column.Trim().ToUpper().Equals("ACTIVECS_TEACHER"); }
        }
    }
}
