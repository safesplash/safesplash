﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.Class
{
    [Serializable]
    public class ActiveClassInstructor
    {
        public Guid Id { get; set; }
        public Guid ActiveClassId { get; set; }
        public Guid InstructorId { get; set; }
        public string InstructorName { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Comment { get; set; }
        public Guid LastUpdatedById { get; set; }
    }
}
