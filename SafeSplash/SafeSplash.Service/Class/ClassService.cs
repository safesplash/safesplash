﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AKMII.SafeSplash.Entity;
using AKMII.SafeSplash.BLL;
using System.Data.SqlTypes;
using SafeSplash.DataAccess.NHibernate;
using SafeSplash.DataAccess.ActiveClass;
//using SafeSplash.Service.Employee;

namespace SafeSplash.Service.Class
{
    public interface IClassService
    {
        IEnumerable<ActiveClass> GetActiveClasses(DateTime fromDate, DateTime toDate, List<string> locaIds, bool isCropAdmin);
        IEnumerable<ActiveClass> GetActiveClasses(List<string> daysOfWeek, string startTime, string endTime, List<string> classTypes, List<string> classLevels, List<Guid> locationIds, List<Guid> instructorIds);
        IEnumerable<ActiveClass> GetActiveClassesForInstructor(Guid locationId, DateTime asOfDate, Guid instructorId);
        bool IsClassPotentiallyOverCapacity(Guid activeClassId, DateTime startDate);
        bool IsClassEnding(Guid activeClassId, DateTime startDate);
        ActiveClassInstructor SaveActiveClassInstructorHistory(ActiveClassInstructor activeClassInstructor);
        ActiveClassInstructor GetActiveClassInstructorHistoryById(Guid id);
        void DeleteActiveClassInstructorHistory(ActiveClassInstructor activeClassInstructor);
        IEnumerable<ActiveClassInstructor> GetActiveClassInstructorHistoriesForClass(Guid activeClassId);
        IEnumerable<ActiveClassInstructor> GetActiveClassInstructorHistoriesForInstructorAsOfDate(Guid instructorId, DateTime asOfDate); 
        Guid GetActiveClassInstructorIdAsOfDate(Guid activeClassId, DateTime asOfDate);
        IEnumerable<ActiveClassInstructor> GetInstructorHistoriesForClassDateRange(Guid activeClassId, DateTime startDate, DateTime endDate);
    }

    public class ClassService : IClassService
    {
        protected SafeSplashDBDataContext ctx = null;
        private IActiveClassInstructorDataAccess repository = new ActiveClassInstructorDataAccess();
        private static Dictionary<Guid, tb_Location> LocationsByGuid = new Dictionary<Guid, tb_Location>();
        SafeSplash.Service.Employee.IEmployeeService employeeService = new SafeSplash.Service.Employee.EmployeeService();
        
        public ClassService()
        {
            ctx = new SafeSplashDBDataContext();
        }

        public IEnumerable<ActiveClass> GetActiveClasses(DateTime fromDate, DateTime toDate, List<string> locationIds, bool isCorpAdmin)
        {
            List<ActiveClass> classes = new List<ActiveClass>();
            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);
            LocationBll locationBll = new LocationBll();
            Dictionary<Guid, tb_Location> locationsById = new Dictionary<Guid, tb_Location>();
            foreach (var locationId in locationIds)
            {
                tb_Location location = locationBll.GetByID(new Guid(locationId));
                if (location != null) { locationsById[location.loca_ID] = location; }
            }

            if (fromDate.CompareTo(toDate) <= 0)
            {
                List<vw_ActiveClass> allActiveClasses = GetActiveClassesForRept(fromDate, toDate, isCorpAdmin, locationIds).OrderBy(c => c.activeDay_StartTime).ToList<vw_ActiveClass>();
                Dictionary<vw_ActiveClass, int> studentsInfo = ImportActiveClassStudentsInfo(allActiveClasses, DateTime.Today);
                for (int i = 0; fromDate.AddDays(i).CompareTo(toDate) <= 0; i++)
                {
                    DateTime tmp = fromDate.AddDays(i);
                    foreach (vw_ActiveClass var in allActiveClasses)
                    {
                        if (tmp.DayOfWeek.ToString().Equals(var.activeDay_Day))
                        {
                            if (var.activecs_StartDay.HasValue && var.activecs_StartDay.Value.CompareTo(tmp) <= 0)
                            {
                                if (!var.activecs_EndDay.HasValue || var.activecs_EndDay.Value.CompareTo(tmp) >= 0)
                                {
                                    ActiveClass activeClass = new ActiveClass();
                                    activeClass.Id = var.activecs_ID;
                                    activeClass.ClassId = var.class_ID;
                                    activeClass.Date = tmp;
                                    activeClass.Name = var.class_Name;
                                    activeClass.LevelName = var.level_Name;
                                    activeClass.DayOfWeek = var.activeDay_Day;
                                    activeClass.Capacity = var.class_StudentCapacity.Value;
                                    activeClass.StartTime = DateTime.MinValue.Add(var.activeDay_StartTime.Value).ToString("hh:mm tt");
                                    activeClass.StartDate = var.activecs_StartDay;
                                    activeClass.EndDate = var.activecs_EndDay;
                                    if (var.loca_ID.HasValue)
                                    {
                                        if (locationsById.ContainsKey(var.loca_ID.Value))
                                        {
                                            activeClass.LocationId = var.loca_ID.Value;
                                            activeClass.LocationName = locationsById[var.loca_ID.Value].loca_Name;
                                        }
                                    }
                                    // get instructor based on class instructor change history
                                    Guid instructorId = GetActiveClassInstructorIdAsOfDate(var.activecs_ID, tmp);
                                    SafeSplash.Service.Employee.Employee instructor = employeeService.GetEmployeeById(instructorId);
                                    activeClass.InstructorId = instructorId;
                                    activeClass.InstructorName = (instructor != null) ? string.Format("{0} {1}", instructor.FirstName, instructor.LastName) : "Instructor Not Found";
                                    //activeClass.InstructorId = (var.activecs_Teacher.HasValue) ? var.activecs_Teacher.Value : Guid.Empty;
                                    //SafeSplash.Service.Employee.Employee instructor = employeeService.GetEmployeeById(
                                    //activeClass.InstructorName = string.Format("{0} {1}", var.empl_FirstName, var.empl_LastName);
                                    activeClass.StudentCount = (studentsInfo.ContainsKey(var)) ? studentsInfo[var] : 0;
                                    classes.Add(activeClass);
                                }
                            }
                        }
                    }

                }
            }

            //IEnumerable<ActiveClass> classesWithChangesApplied = ApplyClassChanges(classes);
            //List<ActiveClass> classesWithInstructorUpdates = new List<ActiveClass>();
            //foreach (var classWithChangesApplied in classesWithChangesApplied)
            //{
            //    Guid instructorId = classWithChangesApplied.InstructorId;
            //    Guid historyInstructorId = GetActiveClassInstructorIdAsOfDate(classWithChangesApplied.ClassId, classWithChangesApplied.Date);
            //    if (!instructorId.Equals(historyInstructorId))
            //    {
            //        classWithChangesApplied.InstructorId = historyInstructorId;
            //        SafeSplash.Service.Employee.Employee instructor = employeeService.GetEmployeeById(historyInstructorId);
            //        classWithChangesApplied.InstructorName = (instructor != null) ? string.Format("{0} {1}", instructor.FirstName, instructor.LastName) : "Instructor Not Found";
            //    }
            //    classesWithInstructorUpdates.Add(classWithChangesApplied);
            //}
            //return classesWithInstructorUpdates;

            //return ApplyClassChanges(classes);

            return classes;
            
        }

        public IEnumerable<ActiveClass> GetActiveClasses(List<string> daysOfWeek, string startTime, string endTime, List<string> classTypes, List<string> classLevels, List<Guid> locationIds, List<Guid> instructorIds)
        {
            TimeSpan startTimeSpan = !string.IsNullOrEmpty(startTime) ? TimeSpan.Parse(startTime) : TimeSpan.Zero;
            TimeSpan endTimeSpan = !string.IsNullOrEmpty(endTime) ? TimeSpan.Parse(endTime) : TimeSpan.Zero;

            List<ActiveClass> classes = new List<ActiveClass>();
            classes = (from ac in ctx.vw_ActiveClasses
                       where (locationIds.Count > 0 ? locationIds.Contains(ac.loca_ID.Value) : true)
                          //&& (instructorIds.Count > 0 ? instructorIds.Contains(ac.activecs_Teacher.Value) : true)
                          && (daysOfWeek.Count > 0 ? daysOfWeek.Contains(ac.activeDay_Day) : true)
                          && (classTypes.Count > 0 ? classTypes.Contains(ac.class_Name) : true)
                          && (classLevels.Count > 0 ? classLevels.Contains(ac.level_Name) : true)
                          && (!TimeSpan.Zero.Equals(startTimeSpan) ? ac.activeDay_StartTime.Value.CompareTo(startTimeSpan) >= 0 : true)
                          && (!TimeSpan.Zero.Equals(endTimeSpan) ? ac.activeDay_EndTime.Value.CompareTo(endTimeSpan) <= 0 : true)
                       select new ActiveClass()
                       {
                           Id = ac.activecs_ID,
                           ClassId = ac.class_ID,
                           DayOfWeek = ac.activeDay_Day,
                           StartTime = new DateTime(ac.activeDay_StartTime.Value.Ticks).ToString("hh:mm tt"),
                           EndTime = new DateTime(ac.activeDay_EndTime.Value.Ticks).ToString("hh:mm tt"),
                           StartDate = ac.activecs_StartDay,
                           EndDate = ac.activecs_EndDay,
                           Name = ac.class_Name,
                           LevelName = ac.level_Name,
                           LocationId = ac.loca_ID.Value,
                           LocationName = GetLocationById(ac.loca_ID.Value).loca_Name,
                           InstructorName = string.Format("{0} {1}", ac.empl_FirstName, ac.empl_LastName)
                       }).ToList<ActiveClass>();

            //List<ActiveClass> classesWithInstructorHistoriesApplied = new List<ActiveClass>();
            //foreach (var cls in classes)
            //{
            //    Guid instructorId = GetActiveClassInstructorIdAsOfDate(cls.Id, DateTime.Now.Date);
            //    SafeSplash.Service.Employee.Employee emp = employeeService.GetEmployeeById(instructorId);
            //    ActiveClass classToAdd = cls;
            //    classToAdd.InstructorName = string.Format("{0} {1}", emp.FirstName, emp.LastName);
            //    classesWithInstructorHistoriesApplied.Add(classToAdd);
            //}            
            //return classesWithInstructorHistoriesApplied;

            List<ActiveClass> classesWithInstructorChanges = new List<ActiveClass>();
            foreach (var cls in classes)
            {
                Guid currentInstructorId = GetActiveClassInstructorIdAsOfDate(cls.Id, DateTime.Today);
                Employee.Employee instructor = employeeService.GetEmployeeById(currentInstructorId);
                string instructorName = (instructor != null) ? string.Format("{0} {1}", instructor.FirstName, instructor.LastName) : "Instructor Not Found";
                cls.InstructorId = currentInstructorId;
                cls.InstructorName = instructorName;
                if (instructorIds.Count() > 0)
                {
                    if (instructorIds.Contains(currentInstructorId))
                    {
                        classesWithInstructorChanges.Add(cls);
                    }
                }
                else
                {
                    classesWithInstructorChanges.Add(cls);
                }
            }


            return classesWithInstructorChanges;
            //return classes;
        }

        public IEnumerable<ActiveClass> GetActiveClassesForInstructor(Guid locationId, DateTime asOfDate, Guid instructorId)
        {
            List<ActiveClass> classes = new List<ActiveClass>();


            return classes;
        }


        public IEnumerable<ActiveClass> ApplyClassChanges(IEnumerable<ActiveClass> classes)
        {
            EmployeeBll employeeBll = new EmployeeBll();
            Dictionary<Guid, string> instructors = new Dictionary<Guid, string>();
            List<ActiveClass> classesWithChanges = new List<ActiveClass>();
            List<Guid> activeClassIds = classes.Select(cl => cl.Id).Distinct().ToList<Guid>();
            DateTime startTime = DateTime.Now;
            foreach (var classId in activeClassIds)
            {
                List<ActiveClass> classesById = classes.Where(cl => cl.Id == classId).OrderBy(cl => cl.Date).ToList<ActiveClass>();
                List<string> ids = new List<string>() { classId.ToString() };
                IEnumerable<ActiveClassHistory> classChanges = GetClassHistoryInstructorChanges(ids);
                DateTime firstClassDate = classesById.Min(cl => cl.Date);
                DateTime beginDateRange = classesById.Min(cl => cl.Date);
                DateTime endDateRange = classesById.Max(cl => cl.Date);
                if (classChanges.Count() > 0)
                {
                    foreach (var cls in classesById)
                    {
                        beginDateRange = firstClassDate;
                        foreach (var change in classChanges)
                        {
                            endDateRange = change.ChangeDate;
                            if (cls.Date >= beginDateRange && cls.Date < endDateRange)
                            {
                                if (change.IsInstructorChange)
                                {
                                    if (!string.IsNullOrEmpty(change.OldValue) && !string.IsNullOrEmpty(change.NewValue))
                                    {
                                        string instructorName = string.Empty;
                                        Guid employeeId = new Guid(change.OldValue);
                                        if (instructors.ContainsKey(employeeId))
                                        {
                                            instructorName = instructors[employeeId];
                                        }
                                        else
                                        {
                                            try
                                            {
                                                tb_Employee instructor = employeeBll.GetByID(employeeId);
                                                instructorName = (instructor != null) ? string.Format("{0} {1}", instructor.empl_FirstName, instructor.empl_LastName) : "Instructor Not Found";
                                            }
                                            catch (Exception e)
                                            {
                                                instructorName = "Error Finding Instructor";
                                            }
                                            instructors.Add(employeeId, instructorName);
                                        }
                                        cls.InstructorName = instructorName;
                                    }
                                }
                                break;
                            }
                            beginDateRange = change.ChangeDate;
                        }
                    }
                    classesWithChanges.AddRange(classesById);
                }
                else
                {
                    classesWithChanges.AddRange(classesById);
                }
            }
            string runTime = DateTime.Now.Subtract(startTime).ToString();

            return classesWithChanges.OrderBy(cl => cl.Date);
        }

        public IEnumerable<ActiveClassHistory> GetClassHistoryInstructorChanges(List<string> classIds)
        {
            return GetClassHistoryInstructorChanges(classIds, SqlDateTime.MinValue.Value, SqlDateTime.MaxValue.Value);
        }

        public IEnumerable<ActiveClassHistory> GetClassHistoryInstructorChanges(List<string> classIds, DateTime fromDate, DateTime toDate)
        {
            List<ActiveClassHistory> classHistories = new List<ActiveClassHistory>();

            List<tb_ActiveClassHistory> listActiveClassDays = new List<tb_ActiveClassHistory>();

            listActiveClassDays = (from c in ctx.tb_ActiveClassHistories
                                   where c.activecsHistory_Column.ToUpper().Equals("ACTIVECS_TEACHER")
                                      && (!c.activecsHistory_NewValue.Trim().ToUpper().Equals(c.activecsHistory_OldValue.Trim().ToUpper()))
                                      && (classIds.Contains(c.activecs_ID.ToString().ToUpper())) && (c.activecsHistory_Created >= fromDate && c.activecsHistory_Created <= toDate)
                                   orderby c.activecsHistory_Created descending
                                   select c).ToList<tb_ActiveClassHistory>();

            foreach (var hist in listActiveClassDays)
            {
                ActiveClassHistory history = new ActiveClassHistory();
                history.Id = hist.activecsHistory_ID;
                history.ActiveClassId = hist.activecs_ID.Value;
                history.Column = hist.activecsHistory_Column;
                history.OldValue = hist.activecsHistory_OldValue;
                history.NewValue = hist.activecsHistory_NewValue;
                history.ChangeDate = hist.activecsHistory_Created.Value;
                classHistories.Add(history);
            }

            return classHistories;
        }

        /// <summary>
        /// Checks to see if a class will be over capacity within the 28 days following the data specified.
        /// </summary>
        /// <param name="activeClassId"></param>
        /// <returns></returns>
        public bool IsClassPotentiallyOverCapacity(Guid activeClassId, DateTime startDate)
        {
            //Guid activeID = (Guid)ViewState["ID"];
            bool overCapacity = false;
            ActiveClassDaysBll acdb = new ActiveClassDaysBll();
            List<tb_ActiveClassDays> tbAcd = acdb.GetByActiveClassID(activeClassId);
            ClassBll acb = new ClassBll();
            tb_Class tc = acb.GetByActiveClassID(activeClassId);
            StudentBookBll studentBookBLL = new StudentBookBll();
            List<tb_StudentBooks> stuBooks = studentBookBLL.GetClassBooksForOverbook(activeClassId);
            if (stuBooks != null && stuBooks.Count >= tc.class_StudentCapacity)
            {
                List<DateTime> dt = new List<DateTime>();
                for (int i = 0; i < 7; i++)
                {
                    foreach (tb_ActiveClassDays val in tbAcd)
                    {
                        if (startDate.AddDays(i).DayOfWeek.ToString().Trim() == val.activeDay_Day.Trim())
                        {
                            dt.Add(startDate.AddDays(i));
                            dt.Add(startDate.AddDays(i + 7));
                            dt.Add(startDate.AddDays(i + 2 * 7));
                            dt.Add(startDate.AddDays(i + 3 * 7));
                        }
                    }
                }
                foreach (DateTime va in dt)
                {
                    int i = 0;
                    foreach (tb_StudentBooks tbsb in stuBooks)
                    {
                        if (tbsb.studch_JoinData.Value <= va)
                        {
                            if (!tbsb.studch_IsWithdrawal.HasValue || !tbsb.studch_IsWithdrawal.Value || tbsb.studch_WidthdrawalData.Value >= va)
                            {
                                i++;
                            }
                        }
                    }
                    if (i >= tc.class_StudentCapacity)
                    {
                        overCapacity = true;
                        break;
                    }
                }
            }

            return overCapacity;
        }

        /// <summary>
        /// Checks to see if a class will be ending within the 28 days following the data specified.
        /// </summary>
        /// <param name="activeClassId"></param>
        /// <returns></returns>
        public bool IsClassEnding(Guid activeClassId, DateTime startDate)
        {
            bool classEnding = false;
            ActiveClassBll activeClassBll = new ActiveClassBll();
            tb_ActiveClass activeClass = activeClassBll.GetByID(activeClassId);
            if (activeClass != null)
            {
                if (activeClass.activecs_EndDay.HasValue)
                {
                    classEnding = startDate.AddDays(28).CompareTo(activeClass.activecs_EndDay) >= 0;
                }
            }
            return classEnding;
        }

        private List<vw_ActiveClass> GetActiveClassesForRept(DateTime fromDate, DateTime endDate, bool isCropAdmin, List<string> locaIds)
        {
            List<vw_ActiveClass> activeClasses = null;
            if (isCropAdmin && locaIds != null && locaIds.Count == 1)
            {
                activeClasses = (from active in ctx.vw_ActiveClasses
                                 where active.activecs_StartDay.HasValue && active.activecs_StartDay.Value <= endDate
                                 && (!active.activecs_EndDay.HasValue || active.activecs_EndDay.Value >= fromDate)
                                 && (string.IsNullOrEmpty(locaIds[0]) || (active.loca_ID.HasValue && active.loca_ID.Value.ToString() == locaIds[0]))
                                 select active).Distinct().ToList<vw_ActiveClass>();
            }
            else
            {
                activeClasses = (from active in ctx.vw_ActiveClasses
                                 where active.activecs_StartDay.HasValue && active.activecs_StartDay.Value <= endDate
                                 && (!active.activecs_EndDay.HasValue || active.activecs_EndDay.Value >= fromDate)
                                 && locaIds.Contains(active.loca_ID.Value.ToString())
                                 select active).Distinct().ToList<vw_ActiveClass>();
            }
            return activeClasses;
        }

        private Dictionary<vw_ActiveClass, int> ImportActiveClassStudentsInfo(List<vw_ActiveClass> actives, DateTime day)
        {
            Dictionary<vw_ActiveClass, int> res = null;
            if (actives != null)
            {
                StudentBookBll studentBookBll = new StudentBookBll();
                res = new Dictionary<vw_ActiveClass, int>();
                List<tb_ClientStudent> studBooksList = null;
                foreach (vw_ActiveClass active in actives)
                {
                    studBooksList = studentBookBll.GetStudentsByBooks(day, active.activecs_ID);
                    if (studBooksList != null)
                    {
                        res.Add(active, studBooksList.Count);
                    }
                }
            }
            return res;
        }

        private tb_Location GetLocationById(Guid id)
        {
            tb_Location location = null;
            if (LocationsByGuid.ContainsKey(id))
            {
                location = LocationsByGuid[id];
            }
            else
            {
                LocationBll locationBll = new LocationBll();
                location = locationBll.GetByID(id);
                LocationsByGuid.Add(id, location);
            }
            return location;
        }


        public ActiveClassInstructor SaveActiveClassInstructorHistory(ActiveClassInstructor activeClassInstructor)
        {
            string instructorName = activeClassInstructor.InstructorName;
            ActiveClassInstructorDataObject dataObject = repository.Save(ActiveClassInstructorConverter.Convert(activeClassInstructor));
            ActiveClassInstructor instructor = ActiveClassInstructorConverter.Convert(dataObject);
            instructor.InstructorName = instructorName;
            return instructor;
        }

        public IEnumerable<ActiveClassInstructor> GetActiveClassInstructorHistoriesForClass(Guid activeClassId)
        {
            EmployeeBll employeeBll = new EmployeeBll();
            List<ActiveClassInstructor> instructors = new List<ActiveClassInstructor>();
            IEnumerable<ActiveClassInstructorDataObject> dataObjects = repository.GetInstructorHistoryForClass(activeClassId);
            foreach (var dataObject in dataObjects)
            {
                ActiveClassInstructor instructor = ActiveClassInstructorConverter.Convert(dataObject);
                tb_Employee employee = employeeBll.GetByID(instructor.InstructorId);
                string instructorName = employee != null ? string.Format("{0} {1}", employee.empl_FirstName, employee.empl_LastName) : "Not Found";
                instructor.InstructorName = instructorName;
                instructors.Add(instructor);
            }
            return instructors;
        }

        public IEnumerable<ActiveClassInstructor> GetActiveClassInstructorHistoriesForInstructorAsOfDate(Guid instructorId, DateTime asOfDate)
        {
            List<ActiveClassInstructor> instructorHistories = new List<ActiveClassInstructor>();
            IEnumerable<ActiveClassInstructorDataObject> dataObjects = repository.GetInstructorHistoryForInstructor(instructorId);
            foreach (var dataObject in dataObjects)
            {
                if (dataObject.EndDate.HasValue)
                {
                    if (asOfDate.CompareTo(dataObject.BeginDate) >= 0 && asOfDate.CompareTo(dataObject.EndDate) <= 0)
                    {
                        instructorHistories.Add(ActiveClassInstructorConverter.Convert(dataObject));
                    }
                }
                else
                {
                    if (asOfDate.CompareTo(dataObject.BeginDate) >= 0)
                    {
                        instructorHistories.Add(ActiveClassInstructorConverter.Convert(dataObject));
                    }
                }
            }
            return instructorHistories;
        }

        public void DeleteActiveClassInstructorHistory(ActiveClassInstructor activeClassInstructor)
        {
            repository.Delete(ActiveClassInstructorConverter.Convert(activeClassInstructor));
        }


        public ActiveClassInstructor GetActiveClassInstructorHistoryById(Guid id)
        {
            ActiveClassInstructorDataObject dataObject = repository.Get(id);
            ActiveClassInstructor instructor = ActiveClassInstructorConverter.Convert(dataObject);
            EmployeeBll employeeBll = new EmployeeBll();
            tb_Employee employee = employeeBll.GetByID(instructor.InstructorId);
            string instructorName = employee != null ? string.Format("{0} {1}", employee.empl_FirstName, employee.empl_LastName) : "Not Found";
            instructor.InstructorName = instructorName;
            return instructor;
        }


        public Guid GetActiveClassInstructorIdAsOfDate(Guid activeClassId, DateTime asOfDate)
        {
            Guid instructorId = Guid.Empty;
            bool instructorFound = false;
            IEnumerable<ActiveClassInstructor> histories = GetActiveClassInstructorHistoriesForClass(activeClassId);
            if (histories != null && histories.Count() > 0)
            {
                foreach (var hist in histories.OrderBy(h => h.BeginDate))
                {
                    if (hist.EndDate.HasValue)
                    {
                        if (asOfDate.CompareTo(hist.BeginDate.Date) >= 0 && asOfDate.CompareTo(hist.EndDate.Value.Date) <= 0)
                        {
                            instructorId = hist.InstructorId;
                            instructorFound = true;
                            break;
                        }                        
                    }
                    else
                    {
                        if (asOfDate.CompareTo(hist.BeginDate.Date) >= 0)
                        {
                            instructorId = hist.InstructorId;
                            instructorFound = true;
                            break;
                        }
                    }
                }
            }
            
            if (!instructorFound)
            {
                instructorId = GetInstructorForClassId(activeClassId);
            }

            return instructorId;
        }

        private Guid GetInstructorForClassId(Guid classId)
        {
            Guid instructorId = Guid.Empty;
            ActiveClassBll acBll = new ActiveClassBll();
            tb_ActiveClass ac = acBll.GetByID(classId);
            if(ac != null && ac.activecs_Teacher.HasValue) { instructorId = ac.activecs_Teacher.Value; }
            return instructorId;
        }

        public IEnumerable<ActiveClassInstructor> GetInstructorHistoriesForClassDateRange(Guid activeClassId, DateTime startDate, DateTime endDate)
        {
            List<ActiveClassInstructor> instructors = GetActiveClassInstructorHistoriesForClass(activeClassId).Where(ih => ih.BeginDate.CompareTo(endDate) <= 0 && (!ih.EndDate.HasValue || ih.EndDate.Value.CompareTo(startDate) >= 0)).OrderBy(ih => ih.BeginDate).ToList<ActiveClassInstructor>();
            DateTime? minInstructorBeginDate = null;
            if (instructors != null && instructors.Count() > 0)
            {
                minInstructorBeginDate = instructors.Min(ih => ih.BeginDate);
            }
            if(!minInstructorBeginDate.HasValue || minInstructorBeginDate.Value.CompareTo(startDate) >= 0)
            {
                // add the original instructor associated with the class
                ActiveClassBll acBll = new ActiveClassBll();
                tb_ActiveClass ac = acBll.GetByID(activeClassId);
                if (ac != null && ac.activecs_Teacher.HasValue && ac.activecs_StartDay.HasValue)
                {
                    instructors.Add(new ActiveClassInstructor() { Id = Guid.Empty, InstructorId = ac.activecs_Teacher.Value, BeginDate = ac.activecs_StartDay.Value, Comment = "Original Class Instructor" });
                }
            }

            return instructors;
        }
    }

    public class ActiveClassInstructorConverter
    {
        public static ActiveClassInstructor Convert(ActiveClassInstructorDataObject dataObject)
        {
            return new ActiveClassInstructor()
            {
                Id = dataObject.Id,
                ActiveClassId = dataObject.ActiveClassId,
                InstructorId = dataObject.InstructorId,
                BeginDate = dataObject.BeginDate,
                EndDate = dataObject.EndDate,
                Comment = dataObject.Comment,
                LastUpdatedById = dataObject.LastUpdatedById                
            };
        }

        public static ActiveClassInstructorDataObject Convert(ActiveClassInstructor activeClassInstructor)
        {
            return new ActiveClassInstructorDataObject()
            {
                Id = activeClassInstructor.Id,
                ActiveClassId = activeClassInstructor.ActiveClassId,
                InstructorId = activeClassInstructor.InstructorId,
                BeginDate = activeClassInstructor.BeginDate,
                EndDate = activeClassInstructor.EndDate,
                Comment = activeClassInstructor.Comment,
                LastUpdatedById = activeClassInstructor.LastUpdatedById
            };
        }
    }
}
