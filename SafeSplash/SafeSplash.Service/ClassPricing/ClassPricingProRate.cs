﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.ClassPricing
{
    public class ClassPricingProRate
    {
        public Guid Id { get; set; }
        public Guid? ActiveClassPriceId { get; set; }
        public Guid? LocationClassPriceId { get; set; }
        public Guid? LocationPriceId { get; set; }
        public int StudentNumber { get; set; }
        public int ClassesMissed { get; set; }
        public decimal Price { get; set; }
        public string Comment { get; set; }
    }
}
