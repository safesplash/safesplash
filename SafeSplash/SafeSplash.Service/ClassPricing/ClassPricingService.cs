﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.NHibernate;

namespace SafeSplash.Service.ClassPricing
{
    [Serializable]
    public class ClassPricingService : IClassPricingService
    {
        IClassPricingDataAccess repository = new ClassPricingDataAccess();
        Dictionary<string, IClassPricing> classPricingsByClass = new Dictionary<string, IClassPricing>();
        Dictionary<string, IClassPricing> classPricingsByLocationAndClass = new Dictionary<string, IClassPricing>();
        Dictionary<string, IClassPricing> classPricingsByLocation = new Dictionary<string, IClassPricing>();
        List<LocationFixedDiscountDataObject> fixedDiscounts = null;
        List<LocationPercentageDiscountDataObject> percentageDiscounts = null;

        public IClassPricing FindPricingByClassInstance(Guid activeClassId, DateTime asOfDate)
        {
            IClassPricing pricing = null;

            if (activeClassId != null && !Guid.Empty.Equals(activeClassId))
            {
                // see if there is a price for this specific class and date in the cache
                string cacheKey = string.Format("{0}, {1}", activeClassId.ToString(), asOfDate.ToShortDateString());
                if (classPricingsByClass.ContainsKey(cacheKey))
                {
                    return classPricingsByClass[cacheKey];
                }

                // see if there is pricing for this class and date in the database
                ActiveClassPriceDataObject dataObject = repository.GetActiveClassPricingForDate(activeClassId, asOfDate);
                if (dataObject != null)
                {
                    pricing = Convert(dataObject);
                    // add this to the cache so we don't query the db every time
                    classPricingsByClass.Add(cacheKey, pricing);
                    return pricing;
                }
                else
                {
                    // we add a null pricing to the cache so we don't continue to query the database
                    classPricingsByClass.Add(cacheKey, null);
                }
            }

            return pricing;
        }

        public IClassPricing FindPricingByLocationAndClassType(Guid locationId, Guid classId, DateTime asOfDate)
        {
            IClassPricing pricing = null;

            if ((locationId != null && !Guid.Empty.Equals(locationId)) && (classId != null && !Guid.Empty.Equals(classId)))
            {
                // see if there is a price for this specific location, class and date in the cache
                string cacheKey = string.Format("{0}, {1}, {2}", locationId.ToString(), classId.ToString(), asOfDate.ToShortDateString());
                if (classPricingsByLocationAndClass.ContainsKey(cacheKey))
                {
                    return classPricingsByLocationAndClass[cacheKey];
                }
                // see if there is a pricing for this location, class and date in the database
                LocationClassPriceDataObject locationClassPricingDataObject = repository.GetLocationClassPricingForDate(locationId, classId, asOfDate);
                if (locationClassPricingDataObject != null)
                {
                    pricing = Convert(locationClassPricingDataObject);
                    // add this to the cache so we don't query the db every time
                    classPricingsByLocationAndClass.Add(cacheKey, pricing);
                    return pricing;
                }
                else
                {
                    // we want to add a null to the cache so we don't continue to query the db every time
                    classPricingsByLocationAndClass.Add(cacheKey, null);
                }
            }

            return pricing;
        }

        public IClassPricing FindPricingByLocation(Guid locationId, DateTime asOfDate)
        {
            IClassPricing pricing = null;
            if (locationId != null && !Guid.Empty.Equals(locationId))
            {
                string cacheKey = string.Format("{0}, {1}", locationId.ToString(), asOfDate.ToShortDateString());
                if (classPricingsByLocation.ContainsKey(cacheKey))
                {
                    pricing = classPricingsByLocation[cacheKey];
                }
                else
                {
                    // see if there is a price for this location and date in the database
                    LocationPriceDataObject dataObject = repository.GetLocationPricingForDate(locationId, asOfDate);
                    if (dataObject != null)
                    {
                        pricing = Convert(dataObject);
                        //// if there are pro rates, populate them
                        //IEnumerable<ClassPriceProRateDataObject> pricings = repository.GetClassPriceProRatesByLocationPrice(pricing.Id);
                        //List<IClassPricingStudentProRate> proRates = new List<IClassPricingStudentProRate>();
                        //foreach (var pricingProRateDataObject in pricings)
                        //{
                        //    proRates.Add(Convert(pricingProRateDataObject));
                        //}
                        //pricing.StudentProRates = proRates;

                        // this pricing to the cache so we don't have to query the db each time
                        classPricingsByLocation.Add(cacheKey, pricing);
                    }
                    else
                    {
                        // add a null pricing to the cache so we don't have to continue to query the db each time
                        classPricingsByLocation.Add(cacheKey, null);
                    }
                }
            }

            return pricing;
        }

        public IEnumerable<IClassPricing> FindClassPricings(Guid locationId, Guid activeClassId, DateTime date)
        {
            throw new NotImplementedException();
        }

        public IClassPricing FindClassPricing(Guid activeClassId, Guid classId, Guid locationId, DateTime asOfDate)
        {
            IClassPricing pricing = null;

            // first we check to see if there is pricing for the specific class instance and date 
            pricing = FindPricingByClassInstance(activeClassId, asOfDate);
            if (pricing != null) { return pricing; }

            // next we check to see if there is pricing for the location and class type and date
            pricing = FindPricingByLocationAndClassType(locationId, classId, asOfDate);
            if (pricing != null) { return pricing; }

            // lastly we check to see if there is a pricing for the location and date
            pricing = FindPricingByLocation(locationId, asOfDate);

            return pricing;
        }

        private static IClassPricing Convert(ActiveClassPriceDataObject dataObject)
        {
            IClassPricing pricing = new LocationClassPricing()
            {
                Id = dataObject.Id,
                PricingType = PricingType.ACTIVECLASS,
                BeginDate = dataObject.BeginDate,
                EndDate = dataObject.EndDate,
                Price = dataObject.Price,
                Comment = dataObject.Comment
            };

            // if there are pro rates, populate them
            List<IClassPricingStudentProRate> proRates = new List<IClassPricingStudentProRate>();
            if (dataObject.ProRates != null)
            {
                foreach (var pricingProRateDataObject in dataObject.ProRates)
                {
                    proRates.Add(Convert(pricingProRateDataObject));
                }
            }
            pricing.StudentProRates = proRates;

            return pricing;
        }

        private static IClassPricing Convert(LocationClassPriceDataObject dataObject)
        {
            IClassPricing pricing = new LocationClassPricing()
            {
                Id = dataObject.Id,
                PricingType = PricingType.LOCATIONCLASS,
                BeginDate = dataObject.BeginDate,
                EndDate = dataObject.EndDate,
                Price = dataObject.Price,
                Comment = dataObject.Comment
            };

            // if there are pro rates, populate them
            List<IClassPricingStudentProRate> proRates = new List<IClassPricingStudentProRate>();
            if (dataObject.ProRates != null)
            {
                foreach (var pricingProRateDataObject in dataObject.ProRates)
                {
                    proRates.Add(Convert(pricingProRateDataObject));
                }
            }
            pricing.StudentProRates = proRates;

            return pricing;
        }

        private static IClassPricing Convert(LocationPriceDataObject dataObject)
        {
            IClassPricing pricing = new LocationClassPricing()
            {
                Id = dataObject.Id,
                PricingType = PricingType.LOCATION,
                BeginDate = dataObject.BeginDate,
                EndDate = dataObject.EndDate,
                Price = dataObject.Price,
                Comment = dataObject.Comment
            };

            // if there are pro rates, populate them
            List<IClassPricingStudentProRate> proRates = new List<IClassPricingStudentProRate>();
            if (dataObject.ProRates != null)
            {
                foreach (var pricingProRateDataObject in dataObject.ProRates)
                {
                    proRates.Add(Convert(pricingProRateDataObject));
                }
            }
            pricing.StudentProRates = proRates;

            return pricing;
        }

        //private static IClassPricingStudentProRate Convert(LocationPriceProRateDataObject dataObject)
        //{
        //    IClassPricingStudentProRate pricingProRate = new ClassPricingStudentProRate
        //    {
        //        Id = dataObject.Id,
        //        PricingType = PricingType.LOCATION,
        //        LocationPriceId = dataObject.LocationPricing.Id,
        //        StudentNumber = dataObject.StudentNumber,
        //        ClassesMissedInMonth = dataObject.ClassesMissed,
        //        Price = dataObject.Price,
        //        Comment = dataObject.Comment
        //    };                       

        //    return pricingProRate;
        //}

        private static IClassPricingStudentProRate Convert(ClassPriceProRateDataObject dataObject)
        {
            IClassPricingStudentProRate pricingProRate = new ClassPricingStudentProRate
            {
                StudentNumber = dataObject.StudentNumber,
                ClassesMissedInMonth = dataObject.ClassesMissed,
                Price = dataObject.Price,
                Comment = dataObject.Comment
            };
            if (dataObject.ActiveClassPriceId != null) { pricingProRate.PricingType = PricingType.ACTIVECLASS; }
            if (dataObject.LocationClassPriceId != null) { pricingProRate.PricingType = PricingType.LOCATIONCLASS; }
            if (dataObject.LocationPriceId != null) { pricingProRate.PricingType = PricingType.LOCATION; }

            return pricingProRate;
        }

        //public int DetermineClassesMissedInMonth(DateTime startDate, DateTime firstDayOfMonth, DateTime lastDayOfMonth, string dayOfWeek)
        //{
        //    int classesMissed = 0;
        //    if (startDate.CompareTo(firstDayOfMonth) >= 0 && startDate.CompareTo(lastDayOfMonth) <= 0)
        //    {
        //        DateTime workingDate = firstDayOfMonth;
        //        while (workingDate.CompareTo(lastDayOfMonth) <= 0 && workingDate.CompareTo(startDate) < 0)
        //        {
        //            if (workingDate.DayOfWeek.ToString().ToUpper() == dayOfWeek.ToUpper()) { classesMissed++; }
        //            workingDate = workingDate.AddDays(1);
        //        }
        //    }
        //    return classesMissed;
        //}

        public int DetermineClassesToBillForInMonth(DateTime startDate, DateTime firstDayOfMonth, DateTime lastDayOfMonth, string dayOfWeek)
        {
            int classes = 4;
            if (startDate.CompareTo(firstDayOfMonth) >= 0 && startDate.CompareTo(lastDayOfMonth) <= 0)
            {
                classes = 0;
                DateTime workingDate = startDate;
                while (workingDate.CompareTo(lastDayOfMonth) <= 0)
                {
                    if (workingDate.DayOfWeek.ToString().ToUpper() == dayOfWeek.ToUpper()) { classes++; }
                    workingDate = workingDate.AddDays(1);
                }
            }
            return classes;
        }

        public decimal CalculateExpectedTuitionAmount(Guid activeClassId, Guid classId, Guid locationId, int studentNumber, DateTime firstDayOfSelectedMonth, DateTime lastDayOfSelectedMonth, DateTime earliestClientEnrollmentDate, DateTime studentStartDate, string classDay)
        {
            decimal tuitionAmount = 0.00M;
            int maxPricingStudentNumber = 0;
            //decimal locationDiscountPercentage = 0.00M;
            //decimal locationFixedDiscount = 0.00M;
            if (fixedDiscounts == null) { fixedDiscounts = repository.GetAllLocationFixedDiscounts().ToList<LocationFixedDiscountDataObject>(); }
            if (percentageDiscounts == null) { percentageDiscounts = repository.GetAllLocationPercentageDiscounts().ToList<LocationPercentageDiscountDataObject>(); }
            int classesInMonth = DetermineClassesToBillForInMonth(studentStartDate, firstDayOfSelectedMonth, lastDayOfSelectedMonth, classDay);
            IClassPricing pricing = FindClassPricing(activeClassId, classId, locationId, earliestClientEnrollmentDate);
            if (pricing != null)
            {
                // set the default price to whatever the amount is on the pricing object - in case there are no pro rates
                tuitionAmount = pricing.Price;

                if (pricing.StudentProRates != null && pricing.StudentProRates.Count() > 0)
                {
                    maxPricingStudentNumber = pricing.StudentProRates.Max(pr => pr.StudentNumber);
                    if (studentNumber >= maxPricingStudentNumber) { studentNumber = maxPricingStudentNumber; }
                    IClassPricingStudentProRate proRate = pricing.StudentProRates.Where(spr => spr.StudentNumber == studentNumber).OrderBy(spr => spr.ClassesMissedInMonth).FirstOrDefault<IClassPricingStudentProRate>();
                    if (proRate != null) { tuitionAmount = proRate.Price; }
                }
            }

            if(tuitionAmount > 0)
            {
                decimal classPrice = tuitionAmount / 4;
                decimal calculatedTuitionAmount = classPrice * classesInMonth;
                tuitionAmount = (calculatedTuitionAmount > tuitionAmount) ? tuitionAmount : calculatedTuitionAmount;
                if (studentStartDate.CompareTo(firstDayOfSelectedMonth) >= 0 && studentStartDate.CompareTo(lastDayOfSelectedMonth) <= 0)
                {
                    //tuitionAmount = (tuitionAmount * ((locationDiscountPercentage > 0) ? locationDiscountPercentage : 1)) - locationFixedDiscount;
                    LocationFixedDiscountDataObject fixedDiscount = fixedDiscounts.Where(d => d.LocationId == locationId && studentStartDate.CompareTo(d.BeginDate) >= 0 && (!d.EndDate.HasValue || studentStartDate.CompareTo(d.EndDate) <= 0)).FirstOrDefault<LocationFixedDiscountDataObject>();
                    decimal fixedDiscountAmount = (fixedDiscount != null) ? fixedDiscount.Amount : 0.00M;
                    LocationPercentageDiscountDataObject percentageDiscount = percentageDiscounts.Where(d => d.LocationId == locationId && studentStartDate.CompareTo(d.BeginDate) >= 0 && (!d.EndDate.HasValue || studentStartDate.CompareTo(d.EndDate) <= 0)).FirstOrDefault<LocationPercentageDiscountDataObject>();
                    decimal percentageDiscountAmount = (percentageDiscount != null) ? (percentageDiscount.Percentage * .01M) : 1;
                    if (percentageDiscountAmount < 1) { percentageDiscountAmount = 1.00M - percentageDiscountAmount; }
                    tuitionAmount = (tuitionAmount * percentageDiscountAmount) - fixedDiscountAmount;
                }
            }

            return tuitionAmount;
        }


        //public decimal CalculateExpectedTuitionAmount(Guid activeClassId, Guid classId, Guid locationId, int studentNumber, DateTime firstDayOfSelectedMonth, DateTime lastDayOfSelectedMonth, DateTime earliestClientEnrollmentDate, DateTime studentStartDate, string classDay)        
        //{
        //    decimal tuitionAmount = 0.00M;
        //    int maxPricingStudentNumber = 0;
        //    int maxPricingClassesMissed = 0;
            
        //    //int classesMissedInMonth = DetermineClassesMissedInMonth(studentStartDate, firstDayOfSelectedMonth, lastDayOfSelectedMonth, classDay);
        //    int classesMissedInMonth = 0;
        //    IClassPricing pricing = FindClassPricing(activeClassId, classId, locationId, earliestClientEnrollmentDate);

        //    if (pricing != null)
        //    {
        //        // set the default price to whatever the amount is on the pricing object - in case there are no pro rates
        //        tuitionAmount = pricing.Price;

        //        if (pricing.StudentProRates != null && pricing.StudentProRates.Count() > 0)
        //        {
        //            maxPricingStudentNumber = pricing.StudentProRates.Max(pr => pr.StudentNumber);
        //            if (studentNumber >= maxPricingStudentNumber) { studentNumber = maxPricingStudentNumber; }

        //            maxPricingClassesMissed = pricing.StudentProRates.Max(pr => pr.ClassesMissedInMonth);
        //            if (classesMissedInMonth >= maxPricingClassesMissed) { classesMissedInMonth = maxPricingClassesMissed; }

        //            IClassPricingStudentProRate proRate = pricing.StudentProRates.Where(spr => spr.StudentNumber == studentNumber && spr.ClassesMissedInMonth == classesMissedInMonth).FirstOrDefault<IClassPricingStudentProRate>();
        //            if (proRate != null) { tuitionAmount = proRate.Price; }
        //        }
        //    }

        //    return tuitionAmount;
        //}
    }
}
