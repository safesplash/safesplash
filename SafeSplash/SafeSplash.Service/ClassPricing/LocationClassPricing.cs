﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.ClassPricing
{
    [Serializable]
    public class LocationClassPricing : IClassPricing
    {
        public Guid Id { get; set; }
        public string PricingType { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal Price { get; set; }
        public string Comment { get; set; }
        public IEnumerable<IClassPricingStudentProRate> StudentProRates { get; set; }
    }
}
