﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.ClassPricing
{
    [Serializable]
    public class PricingType
    {
        public static readonly string LOCATION = "Location";
        public static readonly string ACTIVECLASS = "ActiveClass";
        public static readonly string LOCATIONCLASS = "LocationClass";
    }
}
