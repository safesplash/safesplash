﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.ClassPricing
{
    public interface IClassPricingService
    {
        IClassPricing FindPricingByClassInstance(Guid activeClassId, DateTime asOfDate);
        IClassPricing FindPricingByLocationAndClassType(Guid locationId, Guid classId, DateTime asOfDate);
        IClassPricing FindPricingByLocation(Guid locationId, DateTime asOfDate);
        IEnumerable<IClassPricing> FindClassPricings(Guid locationId, Guid activeClassId, DateTime asOfDate);
        IClassPricing FindClassPricing(Guid activeClassId, Guid classId, Guid locationId, DateTime asOfDate);

        int DetermineClassesToBillForInMonth(DateTime startDate, DateTime firstDayOfMonth, DateTime lastDayOfMonth, string dayOfWeek);
        //decimal CalculateExpectedTuitionAmount(Guid activeClassId, Guid classId, Guid locationId, int studentNumber, DateTime firstDayOfSelectedMonth, DateTime lastDayOfSelectedMonth, DateTime studentStartDate, string classDay);        
        decimal CalculateExpectedTuitionAmount(Guid activeClassId, Guid classId, Guid locationId, int studentNumber, DateTime firstDayOfSelectedMonth, DateTime lastDayOfSelectedMonth, DateTime earliestClientEnrollmentDate, DateTime studentStartDate, string classDay);                
    }
}
