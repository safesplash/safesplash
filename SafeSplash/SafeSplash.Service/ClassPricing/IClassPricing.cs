﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.ClassPricing
{
    public interface IClassPricing
    {
        Guid Id { get; set; }
        string PricingType { get; set; }
        DateTime BeginDate { get; set; }
        DateTime? EndDate { get; set; }
        decimal Price { get; set; }
        string Comment { get; set; }
        IEnumerable<IClassPricingStudentProRate> StudentProRates { get; set; }
    }

    public interface IClassPricingStudentProRate
    {
        Guid Id { get; set; }
        string PricingType { get; set; }
        Guid ActiveClassPriceId { get; set; }
        Guid LocationClassPriceId { get; set; }
        Guid LocationPriceId { get; set; }
        int StudentNumber { get; set; }
        int ClassesMissedInMonth { get; set; }
        decimal Price { get; set; }
        string Comment { get; set; }
    }
}
