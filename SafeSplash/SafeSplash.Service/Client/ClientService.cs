﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.NHibernate;
//using SafeSplash.DataAccess.NHibernate;

namespace SafeSplash.Service.Client
{
    public interface IClientService
    {
        IClientCredit GetClientCredit(Guid id);
        IClientCredit SaveClientCredit(IClientCredit credit);
        void DeleteClientCredit(IClientCredit credit);
        IEnumerable<IClientCredit> GetClientCredits(Guid clientId);
        IEnumerable<IClientCredit> GetClientCredits(IEnumerable<Guid> clientIds, DateTime fromDate, DateTime toDate);
    }

    public class ClientService : IClientService
    {
        IClientCreditDataAccess clientCreditDataAccess = new ClientCreditDataAccess();

        public IClientCredit GetClientCredit(Guid id)
        {
            IClientCreditDataObject dataObject = clientCreditDataAccess.Get(id);
            if (dataObject == null)
                return null;
            return ClientCreditConverter.Convert(dataObject);
        }



        public IClientCredit SaveClientCredit(IClientCredit credit)
        {
            IClientCreditDataObject dataObject = ClientCreditConverter.Convert(credit);
            dataObject = clientCreditDataAccess.Save(dataObject);
            return ClientCreditConverter.Convert(dataObject);
        }

        public void DeleteClientCredit(IClientCredit credit)
        {
            IClientCreditDataObject dataObject = ClientCreditConverter.Convert(credit);
            clientCreditDataAccess.Delete(dataObject);
        }

        public IEnumerable<IClientCredit> GetClientCredits(Guid clientId)
        {
            List<IClientCredit> credits = new List<IClientCredit>();
            IEnumerable<IClientCreditDataObject> dataObjects = clientCreditDataAccess.GetClientCredits(clientId);
            foreach (var dataObject in dataObjects)
            {
                credits.Add(ClientCreditConverter.Convert(dataObject));
            }
            return credits;
        }


        public IEnumerable<IClientCredit> GetClientCredits(IEnumerable<Guid> clientIds, DateTime fromDate, DateTime toDate)
        {
            List<IClientCredit> credits = new List<IClientCredit>();
            IEnumerable<IClientCreditDataObject> dataObjects = clientCreditDataAccess.GetClientCredits(clientIds, fromDate, toDate);
            foreach (var dataObject in dataObjects)
            {
                credits.Add(ClientCreditConverter.Convert(dataObject));
            }
            return credits;
        }
    }

    public class ClientCreditConverter
    {
        public static IClientCredit Convert(IClientCreditDataObject dataObject)
        {
            IClientCredit credit = new ClientCredit()
            {
                Id = dataObject.Id,
                ClientId = dataObject.ClientId,
                Amount = dataObject.Amount,
                Comment = dataObject.Comment,
                IsUsed = dataObject.IsUsed,
                DateUsed = dataObject.DateUsed,
                EmployeeDescription = dataObject.EmployeeDescription,
                EmployeeId = dataObject.EmployeeId
            };
            return credit;
        }

        public static IClientCreditDataObject Convert(IClientCredit credit)
        {
            IClientCreditDataObject dataObject = new ClientCreditDataObject()
            {
                Id = credit.Id,
                ClientId = credit.ClientId,
                Amount = credit.Amount,
                Comment = credit.Comment,
                IsUsed = credit.IsUsed,
                DateUsed = credit.DateUsed,
                EmployeeDescription = credit.EmployeeDescription,
                EmployeeId = credit.EmployeeId
            };
            return dataObject;
        }

    }
}
