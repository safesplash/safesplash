﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.Client
{
    public interface IClientCredit
    {
        Guid Id { get; set; }
        Guid ClientId { get; set; }
        string Amount { get; set; }
        string Comment { get; set; }
        bool? IsUsed { get; set; }
        DateTime? DateUsed { get; set; }
        string EmployeeDescription { get; set; }
        Guid EmployeeId { get; set; }
    }

    public class ClientCredit : IClientCredit
    {
        public virtual Guid Id { get; set; }
        public virtual Guid ClientId { get; set; }
        public virtual string Amount { get; set; }
        public virtual string Comment { get; set; }
        public virtual bool? IsUsed { get; set; }
        public virtual DateTime? DateUsed { get; set; }
        public virtual string EmployeeDescription { get; set; }
        public virtual Guid EmployeeId { get; set; }
    }


}
