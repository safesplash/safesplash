﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PromoCodes
{
    public interface IPromoCodeRedemption
    {
        Guid Id { get; set; }
        Guid PromoCodeId { get; set; }
        Guid ClientId { get; set; }
        Guid LocationId { get; set; }
        Guid? EmployeeId { get; set; }
        //string Code { get; set; }
        decimal AmountBegin { get; set; }
        decimal AmountRedeemed { get; set; }
        decimal AmountRemaining { get; set; }
        DateTime DateEntered { get; set; }
        DateTime? DateRedeemed { get; set; }
        string Comment { get; set; }
    }

    public class PromoCodeRedemption : IPromoCodeRedemption
    {
        public Guid Id { get; set; }
        public Guid PromoCodeId { get; set; }
        public Guid ClientId { get; set; }
        public Guid LocationId { get; set; }
        public Guid? EmployeeId { get; set; }
        //public string Code { get; set; }
        public decimal AmountBegin { get; set; }
        public decimal AmountRedeemed { get; set; }
        public decimal AmountRemaining { get; set; }
        public DateTime DateEntered { get; set; }
        public DateTime? DateRedeemed { get; set; }
        public string Comment { get; set; }
    }

    //public interface IPromoCodeRedemptionDataAccess
    //{
    //    IPromoCodeRedemptionDataObject Get(Guid id);
    //    IPromoCodeRedemptionDataObject Save(IPromoCodeRedemptionDataObject redemption);
    //    IEnumerable<IEmployeeActionDataObject> Find(Guid id, Guid locationId, DateTime beginDateRange, DateTime endDateRange);
    //}

}
