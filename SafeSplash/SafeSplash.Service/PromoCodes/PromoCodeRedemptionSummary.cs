﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PromoCodes
{
    //[Serializable]
    //public class PromoCodeRedemptionSummary
    //{
    //        public Guid Id { get; set; }
    //        public Guid PromoCodeId { get; set; }
    //        public string PromoCode { get; set; }
    //        public decimal PromoCodeAmount { get; set; }
    //        public decimal BusinessValue { get; set; }
    //        public decimal AmountUsed { get; set; }
    //        public decimal PercentUsed { get; set; }
    //        public decimal? RetailValueUsed { get; set; }
    //        public decimal? RetailValueRemaining { get; set; }
    //        public decimal PercentRetailValueUsed { get; set; }
    //        public decimal? BusinessValueUsed { get; set; }
    //        public decimal? BusinessValueRemaining { get; set; }
    //        public decimal PercentBusinessValueUsed { get; set; }
    //        public DateTime AsOfDate { get; set; }
    //}

    [Serializable]
    public class PromoCodeRedemptionSummary
    {
        public Guid Id { get; set; }
        public Guid PromoCodeId { get; set; }
        public string PromoCode { get; set; }
        public Guid LocationId { get; set; }
        public string LocationName { get; set; }
        public decimal PromoCodeRetailValue { get; set; }
        public decimal PromoCodePaidBusinessValue { get; set; }
        public decimal PromoCodeMaxBusinessValue { get; set; }
        public decimal PromoCodeMaxBusinessRevenue { get; set; }
        public decimal PromoCodeRetailRedeemed { get; set; }
        public decimal PromoCodePercentRedeemed { get; set; }
        public decimal PromoCodeNetRevenueToCompany { get; set; }
        public decimal PromoCodeNetCostToCompany { get; set; }
    }
}
