﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SafeSplash.DataAccess.NHibernate;

namespace SafeSplash.Service.PromoCodes
{
    public interface IPromoCodeService
    {
        IPromoCode GetPromoCodeById(Guid id);
        IPromoCode GetPromoCodeByCode(string code);
        IEnumerable<IPromoCode> GetAllPromoCodes();
        IEnumerable<IPromoCode> GetPromoCodeByIds(IEnumerable<Guid> ids);
        IEnumerable<IPromoCode> FindPromoCodesByCode(string code);
        IPromoCodeRedemption GetPromoCodeRedemptionById(Guid id);
        IPromoCodeRedemption SavePromoCodeRedemption(IPromoCodeRedemption redemption);
        void DeletePromoCodeRedemption(IPromoCodeRedemption redemption);
        IEnumerable<IPromoCodeRedemption> FindPromoCodeRedemptions(Guid? clientId, Guid? locationId, IEnumerable<Guid> promoCodeIds, DateTime? beginDateRange, DateTime? endDateRange);
        IEnumerable<IPromoCodeRedemption> FindPromoCodeRedemptionsByPromoCodeId(Guid id);
        IPromoCodeRedemptionRequest SavePromoCodeRedemptionRequest(IPromoCodeRedemptionRequest request);
        IEnumerable<IPromoCodeRedemptionRequest> FindRedemptionRequestsByClientId(Guid clientId);
        //IEnumerable<PromoCodeRedemptionSummary> GetPromoCodeRedemptionSummaries(DateTime asOfDate);
        IEnumerable<PromoCodeRedemptionSummary> GetPromoCodeRedemptionSummaries(DateTime beginDateRange, DateTime endDateRange, decimal amount);
    }

    public class PromoCodeService : IPromoCodeService
    {
        private IPromoCodeDataAccess da = new PromoCodeDataAccess();
        private static List<IPromoCode> promoCodes = null;

        private List<IPromoCode> PromoCodes
        {
            get
            {
                if (promoCodes == null)
                {
                    IEnumerable<IPromoCodeDataObject> dataObjects = da.GetAllPromoCodes().OrderBy(pc => pc.Code);
                    promoCodes = new List<IPromoCode>();
                    foreach (var dataObject in dataObjects)
                    {
                        //promoCodes.Add(PromoCodeConverter.Convert(dataObject));
                        IPromoCode code = PromoCodeConverter.Convert(dataObject);
                        //code.InitialAmount = GetPromoCodeInitialAmount(code.Code);
                        promoCodes.Add(code);
                    }
                }
                return promoCodes;
            }

        }

        public IPromoCode GetPromoCodeById(Guid id)
        {
            return PromoCodes.Where(pc => pc.Id == id).FirstOrDefault();
        }

        public IPromoCode GetPromoCodeByCode(string code)
        {
            return promoCodes.Where(pc => pc.Code.Trim().ToUpper().Equals(code.Trim().ToUpper())).FirstOrDefault();
        }

        public IEnumerable<IPromoCode> GetAllPromoCodes()
        {
            return PromoCodes;
        }

        public IEnumerable<IPromoCode> GetPromoCodeByIds(IEnumerable<Guid> ids)
        {
            return PromoCodes.Where(pc => ids.Contains(pc.Id)).ToList<IPromoCode>().OrderBy(pc => pc.Code);
        }

        public IEnumerable<IPromoCode> FindPromoCodesByCode(string code)
        {
            return PromoCodes.Where(pc => pc.Code.Trim().ToUpper().Contains(code.Trim().ToUpper())).ToList<IPromoCode>().OrderBy(pc => pc.Code);
        }

        public IPromoCodeRedemption GetPromoCodeRedemptionById(Guid id)
        {
            IPromoCodeRedemptionDataObject dataObject = da.GetPromoCodeRedemptionById(id);
            if (dataObject == null) return null;
            IPromoCodeRedemption redemption = PromoCodeRedemptionConverter.Convert(dataObject);
            return redemption;
        }

        public IPromoCodeRedemption SavePromoCodeRedemption(IPromoCodeRedemption redemption)
        {
            IPromoCodeRedemptionDataObject dataObject = PromoCodeRedemptionConverter.Convert(redemption);
            dataObject = da.SavePromoCodeRedemption(dataObject);
            IPromoCodeRedemption savedRedemption = PromoCodeRedemptionConverter.Convert(dataObject);
            return savedRedemption;
        }

        public IEnumerable<IPromoCodeRedemption> FindPromoCodeRedemptions(Guid? clientId, Guid? locationId, IEnumerable<Guid> promoCodeIds, DateTime? beginDateRange, DateTime? endDateRange)
        {
            List<IPromoCodeRedemption> redemptions = new List<IPromoCodeRedemption>();
            IEnumerable<IPromoCodeRedemptionDataObject> dataObjects = da.FindPromoCodeRedemptions(clientId, locationId, promoCodeIds, beginDateRange, endDateRange);
            foreach (var dataObject in dataObjects)
            {
                IPromoCodeRedemption redemption = PromoCodeRedemptionConverter.Convert(dataObject);
                redemptions.Add(redemption);
            }
            return redemptions;
        }

        public IEnumerable<IPromoCodeRedemption> FindPromoCodeRedemptionsByPromoCodeId(Guid id)
        {
            List<IPromoCodeRedemption> redemptions = new List<IPromoCodeRedemption>();
            IEnumerable<IPromoCodeRedemptionDataObject> dataObjects = da.FindPromoCodeRedemptionsByPromoCodeId(id);
            foreach (var dataObject in dataObjects)
            {
                redemptions.Add(PromoCodeRedemptionConverter.Convert(dataObject));
            }
            return redemptions;
        }


        public IPromoCodeRedemptionRequest SavePromoCodeRedemptionRequest(IPromoCodeRedemptionRequest request)
        {
            IPromoCodeRedemptionRequestDataObject dataObject = PromoCodeRedemptionRequestConverter.Convert(request);
            dataObject = da.SavePromoCodeRedemptionClientRequest(dataObject);
            request = PromoCodeRedemptionRequestConverter.Convert(dataObject);
            return request;
        }

        public IEnumerable<IPromoCodeRedemptionRequest> FindRedemptionRequestsByClientId(Guid clientId)
        {
            List<IPromoCodeRedemptionRequest> requests = new List<IPromoCodeRedemptionRequest>();
            IEnumerable<IPromoCodeRedemptionRequestDataObject> dataObjects = da.FindPromoCodeRedemptionRequestsByClientId(clientId);
            foreach (var dataObject in dataObjects)
            {
                requests.Add(PromoCodeRedemptionRequestConverter.Convert(dataObject));
            }
            return requests;
        }


        public void DeletePromoCodeRedemption(IPromoCodeRedemption redemption)
        {
            da.DeletePromoCodeRedemption(PromoCodeRedemptionConverter.Convert(redemption));           
        }


        //private decimal GetPromoCodeInitialAmount(string code)
        //{
        //    decimal amount = 125.00M;
        //    if (FiveDollarPromoCodes.Contains(code)) { amount = 5.00M; }
        //    if (TenDollarPromoCodes.Contains(code)) { amount = 10.00M; }
        //    if (FifteenDollarPromoCodes.Contains(code)) { amount = 15.00M; }
        //    if (TwentyDollarPromoCodes.Contains(code)) { amount = 20.00M; }
        //    if (CancelledPromoCodes.Contains(code)) { amount = 0.00M; }
        //    return amount;
        //}

        //#region Promo Code Values

        //#region Cancelled Cards

        //// the following cards were reported missing from a TX location
        //// so we needed to ensure they cannot be used
        //private List<string> CancelledPromoCodes = new List<string>()
        //{
        //    // $15 Codes
        //    "TX B8LO-W882-84S7",
        //    "TX 55E1-8B7G-3861",
        //    "TX B6IX-O57U-582S",
        //    "TX 87E2-8P2M-NM28",
        //    "TX A366-4B2V-C6F4",
        //    "TX YK77-PUJL-65U2",
        //    "TX 27Y5-PL12-S788",
        //    "TX 88Q2-S1CG-U8SS",
        //    "TX MI37-H4D2-2R6T",
        //    "TX DMKY-G7Q3-1VN3",
        //    "TX O3E5-CTXM-FC5M",
        //    "TX 4I47-J733-5CK2",
        //    "TX QORH-W1D7-46AP",
        //    "TX 6B77-O382-67N3",
        //    "TX KG18-R8BM-63X4",

        //    // $5 Codes
        //    "TX UUH3-27G4-XQ8A",
        //    "TX 42Q4-6125-N28U",
        //    "TX E8QY-4E46-HRH1",
        //    "TX 8888-6B46-1D83",
        //    "TX X1W1-7VIG-1F68"
        //};

        //#endregion

        //#region $5 Codes

        //private List<string> FiveDollarPromoCodes = new List<string>()
        //{
        //    "CO Q8TY-56X6-2G8W",
        //    "CO 5BM8-6H4L-615D",
        //    "CO C77D-Y654-5YG6",
        //    "CO KW8T-V4DP-5A2K",
        //    "CO YRCO-JS1B-352R",
        //    "CO A47N-A31S-D446",
        //    "CO 8STE-HX1X-O4RL",
        //    "CO R13X-1354-5181",
        //    "CO 7D6C-U1P4-BLT4",
        //    "CO LMDX-2KAE-T434", 
        //    "CO P1H8-2E7W-D28F",
        //    "CO RVM3-O53N-4RS2",
        //    "CO 6UN7-S44M-56O6",
        //    "CO 81SJ-26Y1-EMAL",
        //    "CO H6LH-LN31-U83R",
        //    "CO D4LX-5O84-GNK4",
        //    "CO 423R-Y21A-6Y7R",
        //    "CO YB4X-5TPQ-8DIV",
        //    "CO 226S-P58K-LC7D",
        //    "CO 4TOP-4MY8-2854",
        //    "CO 5J25-5V68-W3X8",
        //    "CO EV3J-3H6B-5GVX",
        //    "CO 282U-8521-A38C",
        //    "CO 8A52-17UW-CP75",
        //    "CO H7BU-3A71-76TP",
        //    "CO 81U6-W3UR-X8P7",
        //    "CO PXS8-GFDF-AW78",
        //    "CO 7P7L-84M2-76P2",
        //    "CO 5R8S-B8KE-LU15",
        //    "CO V1VW-AB8R-H22U",
        //    "CO 24W1-I5KX-53UB",
        //    "CO L6UE-R2BD-M754",
        //    "CO LC81-PP78-332K",
        //    "CO 8136-1865-87F6",
        //    "CO 58Q5-4NJ8-6H17",
        //    "CO VJ7O-41Y7-XW21",
        //    "CO DSR8-3M4H-MX36",
        //    "CO E35L-HXGW-572F",
        //    "CO 66W8-S4HD-R165",
        //    "CO L4H1-3B6B-48AD",
        //    "CO M31N-3556-3Q2N",
        //    "CO 4BR7-A6LR-14E6",
        //    "CO H4I6-2W6N-13FB",
        //    "CO 11RE-2638-7FN1",
        //    "CO 6M28-1T4V-6L1O",
        //    "CO UIC5-TA2J-5VG3",
        //    "CO H8SE-2G83-738D",
        //    "CO 72PX-5SXN-FQGM",
        //    "CO 64DA-1652-8CRJ",
        //    "CO 3E2U-NM1S-457Q",
        //    "CO 141W-E34K-TRO3",
        //    "CO 7H1K-2KBV-7258",
        //    "CO 5MV4-2VH7-7IN8",
        //    "CO 4531-W6X7-5CB3",
        //    "CO OS35-2VW7-V5WJ",
        //    "CO F5HU-75T2-KG72",
        //    "CO 8YF5-14ON-513B",
        //    "CO FW47-GH18-BI4E",
        //    "CO 5W16-4261-261J",
        //    "CO NY2P-OBU2-D1B3",
        //    "CO G241-1845-JPAJ",
        //    "CO 86TY-NLR5-5UHK",
        //    "CO 4TQX-O558-TC87",
        //    "CO 8Y8J-873M-6774",
        //    "CO Y228-V763-PG7L",
        //    "CO 7O7G-2C54-2F1A",
        //    "CO 413R-VA77-Y255",
        //    "CO N87V-G387-882P",
        //    "CO 466P-3AU3-81EY",
        //    "CO 5AFP-R8P3-8C71",
        //    "CO 7B16-16BV-6JA8",
        //    "CO 17PK-16XE-Q6IY",
        //    "CO 3V2G-HBPR-PY75",
        //    "CO 3W6U-V1PS-6Q6L",
        //    "CO 5471-5MTJ-GW1O",
        //    "CO 7Y5I-23O3-T6L2",
        //    "CA 8U24-W2C8-VMY8",
        //    "CA T6I1-1123-76KN",
        //    "CA 57SJ-K7S4-PBYF",
        //    "CA 73IT-7IJ2-51P2",
        //    "CA 7A2W-1WH7-2JF1",
        //    "CA O42H-PC54-X34O",
        //    "CA U38D-4S12-VA46",
        //    "CA KWLD-27F8-WO67",
        //    "CA M2O2-86TW-434W",
        //    "CA H648-T678-1VI3",
        //    "CA 542Y-3O71-4XT4",
        //    "CA 555L-46NW-A521",
        //    "CA X636-361W-24E3",
        //    "CA 1477-5S54-5S8Q",
        //    "CA COE3-73R6-8535",
        //    "CA C23S-HWTD-BKCQ",
        //    "CA 6T1H-5F2H-5Y42",
        //    "CA A23V-T841-5PI5",
        //    "CA THX1-X3KU-RD17",
        //    "CA 3IJA-I815-OQD3",
        //    "CA C2IN-M4CK-5212",
        //    "CA 5K65-P17O-1T64",
        //    "CA L41B-5VE2-KR6Y",
        //    "CA T2OW-3MDA-44H1",
        //    "CA 4QB7-17A5-873I",
        //    "CA 824C-4GM6-U52R",
        //    "CA 4FU2-G162-742P",
        //    "CA 6YAA-633E-XSEN",
        //    "CA MF3Q-C755-EBEV",
        //    "CA JR1H-D27Y-SN83",
        //    "CO 3T75-VM51-11S7",
        //    "CO 1277-3B1Y-1P6K",
        //    "CO 6612-5W51-2H73",
        //    "CO 5OY5-2H14-F4T6",
        //    "CO SS52-VCVA-8B73",
        //    "CO R5EJ-MX2H-3374",
        //    "CO 7W2C-I2P4-K3R6",
        //    "CO 7YE2-8177-HLEL",
        //    "CO 2I3J-D6OF-76EC",
        //    "CO NMY7-412U-28A5",
        //    "CO 61LR-7B32-G23J",
        //    "CO 27KJ-5NAW-JH5R",
        //    "CO T616-KOP3-38W2",
        //    "CO 3438-YNY1-8U8O",
        //    "CO Q5X1-Q248-13XA",
        //    "TX NHWK-7L3P-75A8",
        //    "TX MXI2-QI5S-CW37",
        //    "TX B531-35LY-WT41",
        //    "TX 8XLD-57E5-53M3",
        //    "TX M4UO-42E4-38HG",
        //    "TX X3P8-TY2K-T3D5",
        //    "TX 6MF2-D8Q2-235J",
        //    "TX B861-EJ26-2B71",
        //    "TX VV12-5E7W-A3ON",
        //    "TX 18Q3-3J6G-V6KE",
        //    "TX 3RCO-YUNB-8X1Y",
        //    "TX 724P-IJM1-O251",
        //    "TX K7KQ-143C-VFD5",
        //    "TX 6EQ7-Y58W-RSY2",
        //    "TX NYI3-AP52-FARX",
        //    "TX 2OM7-B4I4-M58J",
        //    "TX KSQ3-5EX6-U74R",
        //    "TX E557-2BY7-WJR3",
        //    "TX 2O6L-848B-21K6",
        //    "TX IKAM-24SU-J3PQ",
        //    "TX 2N74-I8AT-1831",
        //    "TX JXFW-1K78-B3F4",
        //    "TX K723-TD3Q-ASEL",
        //    "TX 4843-536H-34EG",
        //    "TX 41H8-I7A5-1LUP",
        //    "TX 25XC-V888-4YW7",
        //    "TX T3RT-F134-K4M4",
        //    "TX Y31F-11Q3-1Q51",
        //    "TX 2Y54-PDKS-37V3",
        //    "TX 574Y-RRR3-QUW8",
        //    "TX PAB5-46B6-78VG",
        //    "TX E538-HK57-1126",
        //    "TX MLHB-UHWU-7832",
        //    "TX 184K-HAFK-CR87",
        //    "TX A17M-4ER4-RM3E",
        //    "TX KJSM-2876-762A",
        //    "TX SGU8-J5HO-76SS",
        //    "CO 8Y5A-1F2L-LJRD",
        //    "CO 1T41-77OO-6IR5",
        //    "CO 6LA4-E44B-X6PC",
        //    "CO L4MM-JQ3Q-A37Y",
        //    "CO 8PIC-28M4-R267",
        //    "CO D5XK-15AD-7N55", 
        //    "CO 4D6R-33N3-6546",
        //    "CO 8VMS-7848-17TP",
        //    "CO 6U7L-7XKC-3DF3",
        //    "CO K3CI-B124-1435",
        //    "CO 177C-8SN4-CB5M",
        //    "CO 33X8-4MG7-YEHB",
        //    "CO NT44-OVY2-NJ38",
        //    "CO KLKE-X8W5-MG8D",
        //    "CO 7L73-R2M3-3Q5N",
        //    "CO J8UE-LD65-E753",
        //    "CO Y5GS-J1QN-LT74",
        //    "CO 2I12-JD63-RP1G",
        //    "CO 4LF1-363K-J6EF",
        //    "CO I814-NMJQ-382N",
        //    // 2014 Codes
        //    "TX X3DQ-USWG-5EA1",
        //    "TX 4RD8-A316-5BY3",
        //    "TX 6X4A-3I23-LNF7",
        //    "TX KQ17-Q6E5-25N3",
        //    "TX 82FQ-AK2F-44QL",
        //    "TX 33YT-7U1C-2HTY",
        //    "TX 1J5O-8K8N-SC6E",
        //    "TX L3G6-54MW-17SU",
        //    "TX P27W-VW4H-R265",
        //    "TX 53JT-DI7O-A3P4",
        //    //"TX UUH3-27G4-XQ8A",
        //    //"TX 42Q4-6125-N28U",
        //    //"TX E8QY-4E46-HRH1",
        //    //"TX 8888-6B46-1D83",
        //    //"TX X1W1-7VIG-1F68",
        //    "TX 5J2N-TS26-N122",
        //    "TX 18LW-7734-63IK",
        //    "TX 1252-X85U-1B18",
        //    "TX OCN2-A4N6-AUYQ",
        //    "TX 8716-X8J2-65I1",
        //    "TX 57C5-J7O8-V3QY",
        //    "TX 57E2-P321-5JX6",
        //    "TX 75D5-5WTH-H112",
        //    "TX OHDJ-I6B6-HUOR",
        //    "TX GPU6-NWS5-4NOM",
        //    // Added 6/28/2014
        //    "TX SK72-6783-85X1",
        //    "TX CK81-63YI-5O4W",
        //    "TX 485V-56RX-T538",
        //    "TX QINV-3LJX-2GVG",
        //    "TX 2R4X-AB34-622E",
        //    "TX J72P-73RJ-577S",
        //    "TX 111R-2LE1-Q83G",
        //    "TX 82N5-I8G4-7OCK",
        //    "TX 87LA-43WN-4KHT",
        //    "TX FAW3-G3M8-5123",
        //    "TX 1L6B-T81D-6F68",
        //    "TX X24K-RNNW-C36H",
        //    "TX 7528-38LP-6R13",
        //    "TX 72AU-5583-518P",
        //    "TX AO1U-GX2F-V25N",
        //    "TX 82V6-744P-2614",
        //    "TX 8KTM-P1X8-3KS3",
        //    "TX X3E5-8ADY-P44G",
        //    "TX H4OY-3YC6-I48W",
        //    "TX 2K31-7XR7-XPP2"
        //};

        //#endregion

        //#region $10 Codes

        //private List<string> TenDollarPromoCodes = new List<string>()
        //{
        //    "CO B234-8T26-36YA",
        //    "CO 87L5-U1GQ-IPLN",
        //    "CO Q7M8-25AQ-QA56",
        //    "CO O2H8-J631-4676",
        //    "CO B63A-K3I4-7D42",
        //    "CO 6X48-2PQ4-3528",
        //    "CO 363E-58F1-67HS",
        //    "CO 7471-338H-DXE8",
        //    "CO 2A3U-27WM-456V",
        //    "CO VGS1-DL4R-OO2M",
        //    "CO 516G-4P5D-SG78",
        //    "CO W1KC-6WG8-G21V",
        //    "CO 5V57-B4Q8-MCF5",
        //    "CO 6L2A-51JA-Q66U",
        //    "CO 3PE2-252E-66V1",
        //    "CO HGQL-V44C-HQCH",
        //    "CO M5C8-33H6-S5WR",
        //    "CO 74OQ-UUW4-8D6O",
        //    "CO Y5RV-A86Y-P6J2",
        //    "CO C2F6-LXT5-V2U2",
        //    "CO SLW6-86FA-8382",
        //    "CO D6RN-E3NU-J55C",
        //    "CO XEW7-PS24-OVH7",
        //    "CO SR7O-3XP1-MSD6",
        //    "CO Y3N2-W84Q-86Y8",
        //    "CO PR85-221K-8HNC",
        //    "CO 6XD6-55V7-5A1E",
        //    "CO S3NI-1283-GPR1",
        //    "CO 22F3-4347-IQ2J",
        //    "CO 11UB-3CC6-VD37",
        //    "CO 2QL1-7AM1-3GPH",
        //    "CO I54Q-E2OB-E77J",
        //    "CO OEBG-OF37-81HR",
        //    "CO 3YV7-61PK-U126",
        //    "CO 8HUB-334D-5164",
        //    "CO 4C7V-1TMS-7115",
        //    "CO 2835-P3SB-62RV",
        //    "CO 47F5-C86T-XJ17",
        //    "CO 8B5M-GEN7-C746",
        //    "CO HR5D-872Y-T13A",
        //    "CO 1EJ6-DK51-887W",
        //    "CO PRUX-2833-U8SW",
        //    "CO B256-OEGY-8711",
        //    "CO LH1B-JT8S-1V15",
        //    "CO WS68-6852-267H",
        //    "TX EP7U-5V73-I7E3",
        //    "TX 4656-UX6S-E5DL",
        //    "TX 3UAB-U434-NT14",
        //    "TX 1LY6-2OK1-P3KK",
        //    "TX DE5N-PBB5-X3U6",
        //    "TX XB6J-B66M-31BS",
        //    "TX 86Q5-22T7-T6RP",
        //    "TX S5E3-RLRX-24DL",
        //    "TX O66T-6B71-SJK8",
        //    "TX 3ASF-E33V-5D2B",
        //    "TX 3BTD-WS21-5C54",
        //    "TX H61L-S2P3-32C6",
        //    "TX OY2I-U385-NF65",
        //    "TX 3S27-5I45-821X",
        //    "TX 1W1B-22Y3-NM2F",
        //    "TX 31W4-ACY7-VXTC",
        //    "TX 3KCV-UEA2-3667",
        //    "TX 75W5-TTF3-JWUE",
        //    "TX 17NS-4M67-863X",
        //    "TX 575W-DV6B-157E",
        //    "TX 22H5-4S5T-SI76",
        //    "TX 5TKV-U13O-85QF",
        //    "TX 7XXW-2LX8-6WD1",
        //    "TX LNP3-MFF3-3G3W",
        //    // 2014 Codes
        //    "TX 7651-3IK7-3787",
        //    "TX JVL4-4N67-V651",
        //    "TX X17J-W73G-186M",
        //    "TX 51XE-TAI1-TIGG",
        //    "TX BMA6-715P-X7IY",
        //    "TX N765-Q626-ETP5",
        //    "TX B21J-241A-4614",
        //    "TX P44L-U6B6-516E",
        //    "TX J3L8-U2TU-PLQ5",
        //    "TX 15A2-5A6O-28H1",
        //    "TX B8M6-4S73-8SX2",
        //    "TX X1FQ-3X37-8G3U",
        //    "TX 3563-YQ2D-GAL6",
        //    "TX 6718-RL5R-JM47",
        //    "TX P4Y5-6K86-O6L1",
        //    // Added 6/28/2014
        //    "TX WO78-11EY-36B7",
        //    "TX SW8R-GCPP-32OD",
        //    "TX 3W88-P1K2-5LY3",
        //    "TX QCKB-NDOK-WMEG",
        //    "TX 5D6C-T7N5-FHN8",
        //    "TX N3OF-T22D-BURT",
        //    "TX M834-22G8-2HI4",
        //    "TX 165K-6BO4-8S46",
        //    "TX 2M34-N3PR-8D1W",
        //    "TX Q5MT-NTJI-52PR",
        //    "TX VG6M-3V12-5137",
        //    "TX 5IRG-666S-U5SD",
        //    "TX 83ON-3L25-7T1E",
        //    "TX KCR1-HB2C-17KL",
        //    "TX 2SP5-456C-4C6N"
        //};

        //#endregion

        //#region $15 Codes

        //private List<string> FifteenDollarPromoCodes = new List<string>()
        //{
        //    "TX SM21-8416-53K5",
        //    "TX R4W5-7EDP-54U4",
        //    "TX 36W8-OQ6Q-RO63",
        //    "TX 7EY5-33Q8-U43G",
        //    "TX 21B2-4233-J6P8",
        //    "TX 7743-YQCE-4674",
        //    "TX 7S25-1N7S-7R71",
        //    "TX 62H4-4KI2-F87W",
        //    "TX W7C3-552R-1VNN",
        //    "TX 4215-BAX4-MY47",
        //    "TX QC7R-PW5D-W485",
        //    "TX 1V6I-C712-7765",
        //    "TX 1842-Q5OR-XU23",
        //    "TX E8CD-51CQ-1EA4",
        //    "TX U1WK-1Y2O-546X",
        //    "TX 42VF-57GJ-BD2N",
        //    "TX M76B-V72L-DIMP",
        //    "TX CYHN-UG52-N5PY",
        //    "TX C5US-R75F-W3R4",
        //    "TX 23TN-OV7C-M8RY",
        //    "TX 6M34-36X3-44HI",
        //    "TX 28V1-3QF6-O1M8",
        //    "TX 2678-7BSI-VC2U",
        //    "TX U436-1414-242G",
        //    "TX 3G12-1233-LO1K",
        //    "TX U36I-7A27-723E",
        //    "TX 8UEJ-G3X4-T7PE",
        //    "TX 7N6I-6IGN-CIMJ",
        //    "TX 45ML-N271-78I4",
        //    "TX KTN4-8A48-FI66",
        //    //"TX B8LO-W882-84S7",
        //    //"TX 55E1-8B7G-3861",
        //    //"TX B6IX-O57U-582S",
        //    //"TX 87E2-8P2M-NM28",
        //    //"TX A366-4B2V-C6F4",
        //    //"TX YK77-PUJL-65U2",
        //    //"TX 27Y5-PL12-S788",
        //    //"TX 88Q2-S1CG-U8SS",
        //    //"TX MI37-H4D2-2R6T",
        //    //"TX DMKY-G7Q3-1VN3",
        //    //"TX O3E5-CTXM-FC5M",
        //    //"TX 4I47-J733-5CK2",
        //    //"TX QORH-W1D7-46AP",
        //    //"TX 6B77-O382-67N3",
        //    //"TX KG18-R8BM-63X4",
        //    "TX 5C2Y-5OE2-2W2W",
        //    "TX RL32-F1FA-AB4F",
        //    "TX 5VY6-G35B-S2O8",
        //    "TX 61XJ-PWIR-5GDJ",
        //    "TX 44H6-34BY-W15Q",
        //    "TX T28S-CR43-17S7",
        //    "TX 57LI-43JD-XE86",
        //    "TX K4WS-GT74-5C3S",
        //    "TX B71B-WQ1C-OW1H",
        //    "TX JDM4-PL23-54CG",
        //    "TX AN54-2573-42OY",
        //    "TX I38K-1JSN-2D8D",
        //    "TX AV8S-T842-33HE",
        //    "TX M7W5-B8W1-8B5X",
        //    "TX YR1M-1W8H-IC36",
        //    // Added 6/28/2014
        //    "TX Q2L4-I215-2378",
        //    "TX OIC6-6W2G-GPGY",
        //    "TX 21VA-8NTS-EW5F",
        //    "TX 6438-Q24S-A533",
        //    "TX 26J7-6RW1-QAQ2",
        //    "TX 61VQ-VCR4-64J7",
        //    "TX 888B-5257-MPKN",
        //    "TX 43N2-B62E-SJUE",
        //    "TX 88CR-M5LD-6YR3",
        //    "TX 1J8A-Y381-S727",
        //    "TX UP8X-5A44-6V75",
        //    "TX TD46-ULKF-27T1",
        //    "TX G8QF-4J88-36V5",
        //    "TX 16QX-K154-154C",
        //    "TX 24OO-A8D7-8OAD"
        //};

        //#endregion

        //#region $20 Codes

        //private List<string> TwentyDollarPromoCodes = new List<string>()
        //{
        //    "CO 3U5N-2H8S-3Q6K",
        //    "CO 1MC7-6N88-RP74",
        //    "CO 63GG-T8V3-3WJ1",
        //    "CO 2QH6-7751-1F14",
        //    "CO 43A8-GFC8-WG4M",
        //    "CO 1R7V-43GJ-AQAB",
        //    "CO UYP6-H2IH-KP53",
        //    "CO 5TK1-2XN7-D473",
        //    "CO 8C1T-J55A-4U4K",
        //    "CO VA2T-V3XJ-13SJ",
        //    "CO WV4I-UBQ1-67C4",
        //    "CO I3P6-8RM2-VKHC",
        //    "CO 85I2-HD6N-73AX",
        //    "CO 148M-1KET-LKPL",
        //    "CO 83P7-R555-7713",
        //    "CO AK35-3828-4LUI",
        //    "CO X2F4-6C52-GO2C",
        //    "CO 8CS3-7F67-3J31",
        //    "CO OKO2-6BU2-IWBP",
        //    "CO 87K3-W841-136F",
        //    "CO 3F51-V5J2-5168",
        //    "CO RL3S-SR26-7C4O",
        //    "CO C354-364B-2A65",
        //    "CO 183O-167O-3M6J",
        //    "CO S8G3-R63Q-7RIY",
        //    "CO 3IYW-U2K7-48H1",
        //    "CO 1DUP-L3BR-5426",
        //    "CO 33I3-12M3-1R44",
        //    "CO 35I4-SSV3-5R2W",
        //    "CO 4745-1O3V-1324",
        //    "CO 86A3-C7H3-PL41",
        //    "CO 564T-P3L3-3Y2I",
        //    "CO 8I4Y-2I2O-S53L",
        //    "CO J85F-DO6P-653L",
        //    "CO LTL8-411J-25A4",
        //    "CO E61V-68H5-P3EF",
        //    "CO 4856-OY46-75SR",
        //    "CO SUE1-CD1J-KBSS",
        //    "CO J562-8381-611O",
        //    "CO 85G1-GP45-N7CG",
        //    "CO I667-6I3R-NQ34",
        //    "CO 3DQC-K2Q2-V165",
        //    "CO 744M-3645-JFQ3",
        //    "CO SYSW-8T35-HT8Y",
        //    "CO 1184-V66R-11JU",
        //    "CO 2OSP-7A6C-PD1F",
        //    "CO 283M-6CDW-L6D4",
        //    "CO 7Y21-V578-K52Y",
        //    "CO NNJY-OW45-1545",
        //    "CO U274-4WQ2-GSRG",
        //    "CO CW2V-71FU-G3FU",
        //    "CO 3H68-665T-Y73F",
        //    "CO 754W-KC24-78B7",
        //    "CO 484G-226O-FOJ7",
        //    "CO 285K-3PUQ-UYXH",
        //    "CO MHDX-SS5Q-33TF",
        //    "CO 3D4D-5622-3U1D",
        //    "CO 7FK8-C42A-25X4",
        //    "CO 1P14-1QCQ-6443",
        //    "CO 3K21-22B6-Q6SA",
        //    "CO H7EA-V4QM-8S5Y",
        //    "CO KJX5-GEIK-2H3P",
        //    "CO 1FRW-CQU7-6337",
        //    "CO 6D51-G5YI-YWUN",
        //    "CO 3C4R-OABM-U1AU",
        //    "CO 85C1-8DE2-73DX",
        //    "CO 3JEM-14X4-1QH3",
        //    "CO 6RI7-6DT8-JA2F",
        //    "CO 365E-7FJ6-SS1D",
        //    "CO 6A4Q-QUO4-3327",
        //    "CO 353M-547T-T261",
        //    "CO 84W7-W282-X3PI",
        //    "CO 185T-K1RA-1O22",
        //    "CO 533T-26V6-841R",
        //    "CO GKIX-W5VP-N4IF",
        //    "CO P327-CDP8-635Y",
        //    "CO JH15-3254-3ST7",
        //    "CO N67D-1266-LE23",
        //    "CO O1F7-ON5Q-2A2O",
        //    "CO PGQ8-PO78-QC4P",
        //    "CO 2A66-OCMI-1TSO",
        //    "CO ET4H-G116-E386",
        //    "CO I86J-7B1U-WR75",
        //    "CO X3C7-4WS8-PETY",
        //    "CO 28V6-2823-823J",
        //    "CO XEI3-I82N-KMMJ",
        //    "CO 315W-A332-JQ78",
        //    "CA 4565-6518-7E8A",
        //    "CA 36T3-H3B6-7574",
        //    "CA 4A12-T6R2-Y5ON",
        //    "CA 43O6-X8C8-AGXV",
        //    "CA G1H5-4CL5-LX15",
        //    "CA C5W2-1K81-4D53",
        //    "CA 35KK-M6VG-D7EA",
        //    "CA H2LL-48I3-1583",
        //    "CA 1YDW-6KMD-V5XC",
        //    "CA 438Q-WS3Y-831D",
        //    "CA BDNK-2OEM-4RTK",
        //    "CA 4B5P-FMRN-GK3A",
        //    "CA 55BI-5814-186C",
        //    "CA EMPE-5P55-YUB6",
        //    "CA WPUM-WAXN-J17Y",
        //    "CA XKTW-A53V-7P8A",
        //    "CA C6F5-LEGJ-732D",
        //    "CA 71O4-KY3W-K46O",
        //    "CA YP4W-LGXI-7DG3",
        //    "CA G24V-VJ73-QNVM",
        //    "CA Y412-K3SH-TDOU",
        //    "CA 1V35-BMGI-JU6I",
        //    "CA B176-744T-N3D5",
        //    "CA 3171-VYB1-26X7",
        //    "CA 521I-IO3P-78U7",
        //    "CA Y66G-Q7Q3-8W13",
        //    "CA 4S2V-VGVX-8S5G",
        //    "CA J5FC-5231-P2M5",
        //    "CA 8J47-U7OM-6H5T",
        //    "CA 2D33-GS6O-JCQ6",
        //    "CA D3PE-D246-4AUS",
        //    "CA 8PPV-623I-4X46",
        //    "CA 423F-6C4L-3B22",
        //    "CA 886P-2D33-F887",
        //    "CA TX6H-477C-1PS8",
        //    "CA 383R-MKR3-NRR5",
        //    "CA 77Y5-8X87-AITL",
        //    "CA 1H8B-2URA-FLO1"
        //};

        //#endregion

        //#endregion



        public IEnumerable<PromoCodeRedemptionSummary> GetPromoCodeRedemptionSummaries(DateTime beginDateRange, DateTime endDateRange, decimal amount)
        {
            IEnumerable<PromoCodeUsageSummaryReportDataObject> dataObjects = da.GetPromoCodeUsageReportSummary(beginDateRange, endDateRange, amount);
            List<PromoCodeRedemptionSummary> summaries = new List<PromoCodeRedemptionSummary>();
            foreach (var dataObject in dataObjects)
            {
                summaries.Add(PromoCodeRedemptionSummaryConverter.Convert(dataObject));
                //PromoCodeRedemptionSummary summary = PromoCodeRedemptionSummaryConverter.Convert(dataObject);
                //summary.AsOfDate = asOfDate;
                //summary.PercentUsed = (summary.AmountUsed > 0) ? decimal.Divide(summary.AmountUsed, summary.PromoCodeAmount) : 0.00M;
                //summary = calculator.CalculateValues(summary);
                //summaries.Add(summary);
            }
            return summaries;
        }
    }

    //public class CardValueCalculator
    //{
    //    decimal RetailValue = 125M;
    //    decimal BusinessValue = 92M;

    //    public PromoCodeRedemptionSummary CalculateValues(PromoCodeRedemptionSummary summary)
    //    {
    //        if (RetailValue == summary.PromoCodeAmount)
    //        {
    //            summary.BusinessValue = BusinessValue;
    //            summary.RetailValueUsed = summary.AmountUsed;
    //            summary.RetailValueRemaining = decimal.Subtract(RetailValue, summary.AmountUsed);
    //            summary.PercentRetailValueUsed = (summary.RetailValueUsed.HasValue && summary.RetailValueUsed.Value > 0) ? summary.RetailValueUsed.Value / RetailValue : 0.00M;
    //            summary.BusinessValueUsed = decimal.Subtract(summary.AmountUsed, decimal.Subtract(RetailValue, BusinessValue));
    //            summary.PercentBusinessValueUsed = (summary.BusinessValueUsed.HasValue && summary.BusinessValueUsed > 0) ? summary.BusinessValueUsed.Value / BusinessValue : 0.00M;
    //            summary.BusinessValueRemaining = decimal.Subtract(BusinessValue, summary.BusinessValueUsed.HasValue ? summary.BusinessValueUsed.Value : 0.00M);
    //        }
    //        return summary;
    //    }
    //}

    public class PromoCodeConverter
    {
        public static IPromoCode Convert(IPromoCodeDataObject dataObject)
        {
            IPromoCode promoCode = new PromoCode()
            {
                Id = dataObject.Id,
                Code = dataObject.Code,
                Description = dataObject.Description,
                InitialAmount = dataObject.Amount,
                IsCancelled = dataObject.IsCancelled
            };
            return promoCode;
        }

        public static IPromoCodeDataObject Convert(IPromoCode promoCode)
        {
            IPromoCodeDataObject dataObject = new PromoCodeDataObject()
            {
                Id = promoCode.Id,
                Code = promoCode.Code,
                Description = promoCode.Description,
                Amount = promoCode.InitialAmount,
                IsCancelled = promoCode.IsCancelled
            };
            return dataObject;
        }
    }

    public class PromoCodeRedemptionConverter
    {
        public static IPromoCodeRedemption Convert(IPromoCodeRedemptionDataObject dataObject)
        {
            IPromoCodeRedemption redemption = new PromoCodeRedemption()
            {
                Id = dataObject.Id,
                PromoCodeId = dataObject.PromoCodeId,
                ClientId = dataObject.ClientId,
                LocationId = dataObject.LocationId,
                EmployeeId = dataObject.EmployeeId,
                AmountBegin = dataObject.AmountBegin,
                AmountRedeemed = dataObject.AmountRedeemed,
                AmountRemaining = dataObject.AmountRemaining,
                DateEntered = dataObject.DateEntered,
                DateRedeemed = dataObject.DateRedeemed,
                Comment = dataObject.Comment
            };
            return redemption;
        }

        public static IPromoCodeRedemptionDataObject Convert(IPromoCodeRedemption redemption)
        {
            IPromoCodeRedemptionDataObject dataObject = new PromoCodeRedemptionDataObject()
            {
                Id = redemption.Id,
                PromoCodeId = redemption.PromoCodeId,
                ClientId = redemption.ClientId,
                LocationId = redemption.LocationId,
                EmployeeId = redemption.EmployeeId,
                AmountBegin = redemption.AmountBegin,
                AmountRedeemed = redemption.AmountRedeemed,
                AmountRemaining = redemption.AmountRemaining,
                DateEntered = redemption.DateEntered,
                DateRedeemed = redemption.DateRedeemed,
                Comment = redemption.Comment
            };
            return dataObject;
        }
    }

    public class PromoCodeRedemptionRequestConverter
    {
        public static IPromoCodeRedemptionRequest Convert(IPromoCodeRedemptionRequestDataObject dataObject)
        {
            IPromoCodeRedemptionRequest request = new PromoCodeRedemptionRequest()
            {
                Id = dataObject.Id,
                ClientId = dataObject.ClientId,
                LocationId = dataObject.LocationId,
                Code = dataObject.Code,
                DateRequested = dataObject.DateRequested
            };

            return request;
        }

        public static IPromoCodeRedemptionRequestDataObject Convert(IPromoCodeRedemptionRequest request)
        {
            IPromoCodeRedemptionRequestDataObject dataObject = new PromoCodeRedemptionRequestDataObject()
            {
                Id = request.Id,
                ClientId = request.ClientId,
                LocationId = request.LocationId,
                Code = request.Code,
                DateRequested = request.DateRequested
            };
            return dataObject;
        }
    }

    public class PromoCodeRedemptionSummaryConverter
    {
        public static PromoCodeRedemptionSummary Convert(PromoCodeUsageSummaryReportDataObject dataObject)
        {
            PromoCodeRedemptionSummary summary = new PromoCodeRedemptionSummary()
            {
                Id = dataObject.Id,
                PromoCodeId = dataObject.PromoCodeId,
                PromoCode = dataObject.PromoCode,
                LocationId = dataObject.LocationId,
                LocationName = dataObject.LocationName,
                PromoCodeRetailValue = dataObject.PromoCodeRetailValue,
                PromoCodePaidBusinessValue = dataObject.PromoCodePaidBusinessValue,
                PromoCodeMaxBusinessValue = dataObject.PromoCodeMaxBusinessValue,
                PromoCodeMaxBusinessRevenue = dataObject.PromoCodeMaxBusinessRevenue,
                PromoCodeRetailRedeemed = dataObject.PromoCodeRetailRedeemed,
                PromoCodePercentRedeemed = dataObject.PromoCodePercentRedeemed,
                PromoCodeNetRevenueToCompany = dataObject.PromoCodeNetRevenueToCompany,
                PromoCodeNetCostToCompany = dataObject.PromoCodeNetCostToCompany
            };
            return summary;
        }
    }
}
