﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PromoCodes
{
    public interface IPromoCodeRedemptionRequest
    {
        Guid Id { get; set; }
        Guid ClientId { get; set; }
        Guid LocationId { get; set; }
        string Code { get; set; }
        DateTime DateRequested { get; set; }
    }

    public class PromoCodeRedemptionRequest : IPromoCodeRedemptionRequest
    {
        public virtual Guid Id { get; set; }
        public virtual Guid ClientId { get; set; }
        public virtual Guid LocationId { get; set; }
        public virtual string Code { get; set; }
        public virtual DateTime DateRequested { get; set; }
    }
}
