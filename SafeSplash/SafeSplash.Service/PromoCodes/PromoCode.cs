﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SafeSplash.Service.PromoCodes
{
    public interface IPromoCode
    {
        Guid Id { get; set; }
        string Code { get; set; }
        string Description { get; set; }
        decimal? InitialAmount { get; set; }
        bool? IsCancelled { get; set; }
        IEnumerable<IPromoCodeRedemption> Redemptions { get; set; }
    }

    public class PromoCode : IPromoCode
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal? InitialAmount { get; set; }
        public bool? IsCancelled { get; set; }
       public IEnumerable<IPromoCodeRedemption> Redemptions { get; set; }
    }
}
